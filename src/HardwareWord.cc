//******************************************************************************
// file: HardwareWord.cc
// desc: library for L1CT HardwareCompiler
// auth: 19-MAR-2013 R. Spiwoks, re-design from RCDModuleDesign
//******************************************************************************

#include "RCDUtilities/RCDUtilities.h"
#include "L1CTHardwareCompiler/HardwareCommon.h"
#include "L1CTHardwareCompiler/HardwareWord.h"
#include "L1CTHardwareCompiler/HardwareElement.h"
#include "L1CTHardwareCompiler/HardwareBlock.h"
#include "L1CTHardwareCompiler/HardwareRegister.h"
#include "L1CTHardwareCompiler/HardwareCompiler.h"
#include "L1CTHardwareCompiler/CompilerOutputFile.h"
#include "L1CTHardwareCompiler/CompilerException.h"

using namespace L1CTHC;
using namespace XERCES_CPP_NAMESPACE;

//------------------------------------------------------------------------------

HardwareWord::HardwareWord(const std::string& name, const DECL_TYPE decl) : HardwareDataType(name,decl) {

    // nothing to be done
}

//------------------------------------------------------------------------------

HardwareWord::~HardwareWord() {

    // nothing to be done
}

//------------------------------------------------------------------------------

std::string HardwareWord::nameName() const {

    if(m_reference) return(m_reference->nameName());

    return(DATA_TYPE_TYPE[m_type]);
}

//------------------------------------------------------------------------------

void HardwareWord::dump(std::ostream& ostr, const unsigned int indt) const {

    char os[STRING_LENGTH];

    // name
    std::sprintf(os,"%sHardwareWord \"%s\", type = \"%s\"\n",HardwareCommon::spaceString(indt).c_str(),m_name.c_str(),DATA_TYPE_NAME[m_type].c_str()); ostr << os;
}

//------------------------------------------------------------------------------

void HardwareWord::read(const DOMNode* /* bit */) {

    // nothing to be done
}

//------------------------------------------------------------------------------

void HardwareWord::write(std::ostream& /* ostr */) const {

    // nothing to be done
}
