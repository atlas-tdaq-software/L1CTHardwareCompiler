//******************************************************************************
// file: MemoryEmitter.cc
// desc: library for L1CT HardwareCompiler
// auth: 21-JUN-2013 R. Spiwoks
//******************************************************************************

#include <vector>
#include <algorithm>

#include "RCDUtilities/RCDUtilities.h"
#include "L1CTHardwareCompiler/MemoryEmitter.h"
#include "L1CTHardwareCompiler/CompilerOutputFile.h"
#include "L1CTHardwareCompiler/CompilerException.h"
#include "L1CTHardwareCompiler/CompilerStringList.h"
#include "L1CTHardwareCompiler/HardwareCompiler.h"
#include "L1CTHardwareCompiler/HardwareBlock.h"
#include "L1CTHardwareCompiler/HardwareElement.h"
#include "L1CTHardwareCompiler/HardwareRegister.h"
#include "L1CTHardwareCompiler/HardwareMemory.h"
#include "L1CTHardwareCompiler/HardwareBitString.h"
#include "L1CTHardwareCompiler/HardwareWord.h"
#include "L1CTHardwareCompiler/HardwareField.h"
#include "L1CTHardwareCompiler/HardwareValue.h"
#include "L1CTHardwareCompiler/HardwareCommon.h"

using namespace L1CTHC;

//------------------------------------------------------------------------------

MemoryEmitter::MemoryEmitter() : CompilerCCEmitter() {

    // nothing to be done
}

//------------------------------------------------------------------------------

MemoryEmitter::~MemoryEmitter() {

    // nothing to be done
}

//-----------------------------------------------------------------------------

std::string MemoryEmitter::compileModuleHardwareIncludesDeclaration(const HardwareModule* /* mod */) {

    std::ostringstream buf;

    // nothing to be done

    return(buf.str());
}

//-----------------------------------------------------------------------------

std::string MemoryEmitter::compileModuleHardwareIncludesDefinition(const HardwareModule* /* mod */) {

    std::ostringstream buf;

    // nothing to be done

    return(buf.str());
}

//-----------------------------------------------------------------------------

std::string MemoryEmitter::compileModuleHardwareCtorDtorDeclaration(const HardwareModule* mod) {

    std::ostringstream buf;
    std::string modName = mod->name();

    buf << "    " << modName << "(bool = true);" << std::endl
        << "   ~" << modName << "();" << std::endl
        << std::endl
        << "    // public access method" << std::endl
        << "    bool bigEndian() const                                     { return(m_big_endian); }" << std::endl
        << std::endl
        << "    // helper data manipulator" << std::endl
        << "    class DATA {" << std::endl
        << "      public:" <<std::endl
        << "        // public constants" << std::endl
        << "        static const unsigned int                               SIZE_UINT = sizeof(unsigned int);"<< std::endl
        << "        static const unsigned int                               SIZE_USHORT = sizeof(unsigned short);" << std::endl
        << "        static const unsigned int                               SIZE_UCHAR = sizeof(unsigned char);" << std::endl
        << "        static const unsigned int                               NUMBER_UINT = SIZE_UINT/SIZE_UINT;"<< std::endl
        << "        static const unsigned int                               NUMBER_SHORT = SIZE_UINT/SIZE_USHORT;" << std::endl
        << "        static const unsigned int                               NUMBER_CHAR = SIZE_UINT/SIZE_UCHAR;" << std::endl
        << "        // public constructor and destructor" << std::endl
        << "        DATA(bool b, unsigned int d = 0)                        { m_data.uint = d; m_big_endian = b; }" << std::endl
        << "        DATA(unsigned int d = 0)                                { m_data.uint = d; m_big_endian = true; }" << std::endl
        << "        DATA(const DATA& d)                                     { m_data.uint = d.uint(); m_big_endian = d.bigEndian(); }" << std::endl
        << "       ~DATA()                                                  {}" << std::endl
        << "        // public assignment operator" << std::endl
        << "        DATA& operator=(const DATA& d)                          { if(this!=&d) m_data.uint = d.uint(); return(*this); }" << std::endl
        << "        // public access methods" << std::endl
        << "        bool bigEndian() const                                  { return(m_big_endian); }" << std::endl
        << "        unsigned int uint() const                               { return(m_data.uint); }" << std::endl
        << "        unsigned short ushort(const unsigned int i) const       { return(m_data.ushort[m_big_endian?NUMBER_SHORT-1-i:i]); }" << std::endl
        << "        unsigned char uchar(const unsigned int i) const         { return(m_data.uchar[m_big_endian?NUMBER_CHAR-1-i:i]); }" << std::endl
        << "        unsigned int& uint()                                    { return(m_data.uint); }" << std::endl
        << "        unsigned short& ushort(const unsigned int i)            { return(m_data.ushort[m_big_endian?NUMBER_SHORT-1-i:i]); }" << std::endl
        << "        unsigned char& uchar(const unsigned int i)              { return(m_data.uchar[m_big_endian?NUMBER_CHAR-1-i:i]); }" << std::endl
        << "      private:" << std::endl
        << "        // private members" << std::endl
        << "        union {" << std::endl
        << "            unsigned int        uint;" << std::endl
        << "            unsigned short      ushort[NUMBER_SHORT];" << std::endl
        << "            unsigned char       uchar[NUMBER_CHAR];" << std::endl
        << "        }               m_data;" << std::endl
        << "        bool            m_big_endian;" << std::endl
        << "    };" << std::endl
        << std::endl;

    return(buf.str());
}

//-----------------------------------------------------------------------------

std::string MemoryEmitter::compileModuleHardwareCtorDtorDefinition(const HardwareModule* mod) {

    std::ostringstream buf;
    std::string modName, sizName;

    modName = mod->name();
    sizName = mod->sizeName();

    buf << "//------------------------------------------------------------------------------" << std::endl
        << std::endl
        << modName << "::" << modName << "(bool big) : m_big_endian(big) {" << std::endl
        << std::endl
        << "    // create memory" << std::endl
        << "    m_data = new unsigned int[" << sizName << "];" << std::endl
        << "}" << std::endl
        << std::endl
        << "//------------------------------------------------------------------------------" << std::endl
        << std::endl
        << modName << "::~" << modName << "() {" << std::endl
        << std::endl
        << "    // delete memory" << std::endl
        << "    delete [] m_data;" << std::endl
        << "}" << std::endl
        << std::endl;

    return(buf.str());
}

//-----------------------------------------------------------------------------

std::string MemoryEmitter::compileModuleHardwareMembers(const HardwareModule* /* mod */) {

    std::ostringstream buf;

    buf << "    unsigned int*         m_data;" << std::endl
        << "    bool                  m_big_endian;" << std::endl
        << std::endl;

    return(buf.str());
}

// -----------------------------------------------------------------------------

std::string MemoryEmitter::compileModuleHardwareReadSingle(const HardwareElement* elm) {

    const CompilerConfiguration* cfg = getCompiler()->getConfig();
    const HardwareBlock* blk = elm->getBlock();
    const HardwareMemory* mem = dynamic_cast<const HardwareMemory*>(elm);

    std::ostringstream buf;
    CompilerStringList add; add.delimiter('+');

    // compose address
    if(blk->addr()) add.add(blk->addrName());
    add.add(elm->addrName());
    if(blk->multiple()>1) add.add("nblk*"+blk->offsetName());
    if(elm->multiple()>1) add.add("nelm*"+elm->offsetName());
    if(mem && (mem->access()==HardwareMemory::RAM)) add.add("indx*"+HardwareObject::DATA_TYPE_BYTE(mem->type()));

    // emit read single
    buf << "    unsigned int addr = " << add.str() << ";" << std::endl;
    if(cfg->debug_level>=1) buf << "    COUT(\"reading single addr = 0x%08x\",addr);" <<std::endl;
    if(elm->type() == HardwareObject::D32) {
        buf << "    rdat = m_data[addr/DATA::SIZE_UINT];" << std::endl;
    }
    else if(elm->type() == HardwareObject::D16) {
        buf << "    DATA xdat(m_big_endian); xdat.uint() = m_data[addr/DATA::SIZE_UINT];" << std::endl
            << "    rdat = xdat.ushort((addr%DATA::SIZE_UINT)/DATA::SIZE_USHORT);" << std::endl;
    }
    else if(elm->type() == HardwareObject::D08) {
        buf << "    DATA xdat(m_big_endian); xdat.uint() = m_data[addr/DATA::SIZE_UINT];" << std::endl
            << "    rdat = xdat.uchar((addr%DATA::SIZE_UINT)/DATA::SIZE_UCHAR);" << std::endl;
    }

    return(buf.str());
}

// -----------------------------------------------------------------------------

std::string MemoryEmitter::compileModuleHardwareWriteSingle(const HardwareElement* elm) {

    const CompilerConfiguration* cfg = getCompiler()->getConfig();
    const HardwareBlock* blk = elm->getBlock();
    const HardwareMemory* mem = dynamic_cast<const HardwareMemory*>(elm);

    std::ostringstream buf;
    CompilerStringList add; add.delimiter('+');

    // compose address
    if(blk->addr()) add.add(blk->addrName());
    add.add(elm->addrName());
    if(blk->multiple()>1) add.add("nblk*"+blk->offsetName());
    if(elm->multiple()>1) add.add("nelm*"+elm->offsetName());
    if(mem && (mem->access()==HardwareMemory::RAM)) add.add("indx*"+HardwareObject::DATA_TYPE_BYTE(mem->type()));

    // emit write single
    buf << "    unsigned int addr = " << add.str() << ";" << std::endl;
    if(cfg->debug_level>=1) buf << "    COUT(\"writing single addr = 0x%08x, data = 0x%08x\",addr,wdat);" <<std::endl;
    if(elm->type() == HardwareObject::D32) {
        buf << "    m_data[addr/DATA::SIZE_UINT] = wdat;" << std::endl;
    }
    if(elm->type() == HardwareObject::D16) {
        buf << "    DATA xdat(m_big_endian); xdat.uint() = m_data[addr/DATA::SIZE_UINT];" << std::endl
            << "    xdat.ushort((addr%DATA::SIZE_UINT)/DATA::SIZE_USHORT) = wdat;" << std::endl
            << "    m_data[addr/DATA::SIZE_UINT] = xdat.uint();" << std::endl;
    }
    if(elm->type() == HardwareObject::D08) {
        buf << "    DATA xdat(m_big_endian); xdat.uint() = m_data[addr/DATA::SIZE_UINT];" << std::endl
            << "    xdat.uchar((addr%DATA::SIZE_UINT)/DATA::SIZE_UCHAR) = wdat;" << std::endl
            << "    m_data[addr/DATA::SIZE_UINT] = xdat.uint();" << std::endl;
    }

    return(buf.str());
}

// -----------------------------------------------------------------------------

std::string MemoryEmitter::compileModuleHardwareReadVector(const HardwareElement* elm) {

    const CompilerConfiguration* cfg = getCompiler()->getConfig();
    const HardwareBlock* blk = elm->getBlock();
    const HardwareMemory* mem = dynamic_cast<const HardwareMemory*>(elm);

    std::string typName, elmMask;
    std::ostringstream buf;
    CompilerStringList add; add.delimiter('+');

    typName = HardwareObject::DATA_TYPE_TYPE[elm->type()];
    elmMask = (elm->mask() != HardwareObject::DATA_TYPE_MMAX[elm->type()]) ? ("&"+elm->maskName()) : "";
    if(mem->access()==HardwareMemory::RAM) add.add("offset*sizeof("+typName+")");

    // compose address
    if(blk->addr()) add.add(blk->addrName());
    add.add(elm->addrName());
    if(blk->multiple()>1) add.add("nblk*"+blk->offsetName());
    if(elm->multiple()>1) add.add("nelm*"+elm->offsetName());

    // emit read vector
    buf << "    unsigned int addr = " << add.str() << ";" << std::endl;
    if(cfg->debug_level>=1) buf << "    COUT(\"reading vector addr = 0x%08x, size = 0x%08x\",addr,data.size());" << std::endl;
    buf << "    " << typName << " rdat;" << std::endl
        << "    for(unsigned int i=0; i<data.size(); i++) {" << std::endl;
    if(elm->type() == HardwareObject::D32) {
        buf << "        rdat = m_data[addr/DATA::SIZE_UINT];" << std::endl;
    }
    else if(elm->type() == HardwareObject::D16) {
        buf << "        DATA xdat(m_big_endian); xdat.uint() = m_data[addr/DATA::SIZE_UINT];" << std::endl
            << "        rdat = xdat.ushort((addr%DATA::SIZE_UINT)/DATA::SIZE_USHORT);" << std::endl;
    }
    else if(elm->type() == HardwareObject::D08) {
        buf << "        DATA xdat(m_big_endian); xdat.uint() = m_data[addr/DATA::SIZE_UINT];" << std::endl
            << "        rdat = xdat.uchar((addr%DATA::SIZE_UINT)/DATA::SIZE_UCHAR);" << std::endl;
    }
    buf << "        data[i] = rdat" << elmMask << ";" << std::endl;
    if(mem->access() == HardwareMemory::RAM) buf << "        addr += sizeof(" << typName << ");" << std::endl;
    buf << "    }" << std::endl
        << std::endl;

    return(buf.str());
}

// -----------------------------------------------------------------------------

std::string MemoryEmitter::compileModuleHardwareWriteVector(const HardwareElement* elm) {

    const CompilerConfiguration* cfg = getCompiler()->getConfig();
    const HardwareBlock* blk = elm->getBlock();
    const HardwareMemory* mem = dynamic_cast<const HardwareMemory*>(elm);

    std::string typName, elmMask;
    std::ostringstream buf;
    CompilerStringList add; add.delimiter('+');

    typName = HardwareObject::DATA_TYPE_TYPE[elm->type()];
    elmMask = (elm->mask() != HardwareObject::DATA_TYPE_MMAX[elm->type()]) ? ("&"+elm->maskName()) : "";

    // compose address
    if(blk->addr()) add.add(blk->addrName());
    add.add(elm->addrName());
    if(blk->multiple()>1) add.add("nblk*"+blk->offsetName());
    if(elm->multiple()>1) add.add("nelm*"+elm->offsetName());
    if(mem->access()==HardwareMemory::RAM) add.add("offset*sizeof("+typName+")");

    // emit write vector
    buf << "    unsigned int addr = " << add.str() << ";" << std::endl;
    if(cfg->debug_level>=1) buf << "    COUT(\"writing vector addr = 0x%08x, size = 0x%08x\",addr,data.size());" << std::endl;
    buf << "    " << typName << " wdat;" << std::endl
        << "    for(unsigned int i=0; i<data.size(); i++) {" << std::endl
        << "        wdat = data[i]" << elmMask << ";" << std::endl;
    if(elm->type() == HardwareObject::D32) {
        buf << "        m_data[addr/DATA::SIZE_UINT] = wdat;" << std::endl;
    }
    if(elm->type() == HardwareObject::D16) {
        buf << "        DATA xdat(m_big_endian); xdat.uint() = m_data[addr/DATA::SIZE_UINT];" << std::endl
            << "        xdat.ushort((addr%DATA::SIZE_UINT)/DATA::SIZE_USHORT) = wdat;" << std::endl
            << "        m_data[addr/DATA::SIZE_UINT] = xdat.uint();" << std::endl;
    }
    if(elm->type() == HardwareObject::D08) {
        buf << "        DATA xdat(m_big_endian); xdat.uint() = m_data[addr/DATA::SIZE_UINT];" << std::endl
            << "        xdat.uchar((addr%DATA::SIZE_UINT)/DATA::SIZE_UCHAR) = wdat;" << std::endl
            << "        m_data[addr/DATA::SIZE_UINT] = xdat.uint();" << std::endl;
    }
    if(mem->access() == HardwareMemory::RAM) buf << "        addr += sizeof(" << typName << ");" << std::endl;
    buf << "    }" << std::endl
        << std::endl;

    return(buf.str());
}

// -----------------------------------------------------------------------------

std::string MemoryEmitter::compileModuleHardwareReadBlock(const HardwareElement* elm) {

    const CompilerConfiguration* cfg = getCompiler()->getConfig();
    const HardwareBlock* blk = elm->getBlock();
    const HardwareMemory* mem = dynamic_cast<const HardwareMemory*>(elm);

    std::string typName;
    std::ostringstream buf;
    CompilerStringList add; add.delimiter('+');

    typName = HardwareObject::DATA_TYPE_TYPE[elm->type()];

    // compose address
    if(blk->addr()) add.add(blk->addrName());
    add.add(elm->addrName());
    if(blk->multiple()>1) add.add("nblk*"+blk->offsetName());
    if(elm->multiple()>1) add.add("nelm*"+elm->offsetName());
    if(mem->access()==HardwareMemory::RAM) add.add("offset*sizeof("+typName+")");

    // emit read block
    if(cfg->debug_level>=1) buf << "    COUT(\"reading block addr = 0x%08x, size = 0x%08x\"," << add.str() << ",size);" << std::endl;
    buf << "    void* src = (void*)((unsigned int)m_data + " << add.str() << ");" << std::endl
        << "    void* dst = (void*)seg->VirtualAddress();" << std::endl
        << "    memcpy(dst,src,size);" << std::endl
        << std::endl;

    return(buf.str());
}

// -----------------------------------------------------------------------------

std::string MemoryEmitter::compileModuleHardwareWriteBlock(const HardwareElement* elm) {

    const CompilerConfiguration* cfg = getCompiler()->getConfig();
    const HardwareBlock* blk = elm->getBlock();
    const HardwareMemory* mem = dynamic_cast<const HardwareMemory*>(elm);

    std::string typName;
    std::ostringstream buf;
    CompilerStringList add; add.delimiter('+');

    typName = HardwareObject::DATA_TYPE_TYPE[elm->type()];

    // compose address
    if(blk->addr()) add.add(blk->addrName());
    add.add(elm->addrName());
    if(blk->multiple()>1) add.add("nblk*"+blk->offsetName());
    if(elm->multiple()>1) add.add("nelm*"+elm->offsetName());
    if(mem->access()==HardwareMemory::RAM) add.add("offset*sizeof("+typName+")");

    // emit write block
    if(cfg->debug_level>=1) buf << "    COUT(\"writing block addr = 0x%08x, size = 0x%08x\"," << add.str() << ",size);" << std::endl;
    buf << "    void* src = (void*)seg->VirtualAddress();" << std::endl
        << "    void* dst = (void*)((unsigned int)m_data + " << add.str() << ");" << std::endl
        << "    memcpy(dst,src,size);" << std::endl
        << std::endl;

    return(buf.str());
}

// -----------------------------------------------------------------------------

std::string MemoryEmitter::compileModuleHardwareReadFile(const HardwareElement* elm) {

    return(std::string());
}

// -----------------------------------------------------------------------------

std::string MemoryEmitter::compileModuleHardwareWriteFile(const HardwareElement* elm) {

    return(std::string());
}

// -----------------------------------------------------------------------------

std::string MemoryEmitter::compileMenuHardwareGlobals(const HardwareModule* /* mod */) {

    std::ostringstream buf;

    // nothing to be done

    return(buf.str());
}

// -----------------------------------------------------------------------------

std::string MemoryEmitter::compileMenuHardwareOpen(const HardwareModule* mod) {

    std::ostringstream buf;
    std::string modName, modDesc, addName, sizName;

    modName = mod->name();
    modDesc = mod->desc();
    addName = modName + "::addr()";
    sizName = modName + "::size()";

    buf << "        std::printf(\"Module \\\"" << modName << "\\\" (\\\"" << modDesc << "\\\"):\\n\");" <<std::endl
        << "        bool big = (enterInt(\"Module \\\"" << modName << "\\\" data endianess (0=BIG,1=LITTLE)\",0,1) == 0);" <<std::endl
        << std::endl
        << "        // create " << modName << " module" << std::endl
        << "        mod = new " << modName << "(big);" << std::endl
        << "        COUT(\"opened module \\\"" << modName << "\\\" with size = 0x%08x, data endianess = \\\"%s\\\"\"," << sizName << ",mod->bigEndian()?\"BIG\":\"LITTLE\");" << std::endl
        << std::endl;

    return(buf.str());
}

// -----------------------------------------------------------------------------

std::string MemoryEmitter::compileMenuHardwareClose(const HardwareModule* mod) {

    std::ostringstream buf;
    std::string modName;

    modName = mod->name();

    buf << "        // delete " << modName << " module" << std::endl
        << "        delete mod;" << std::endl
        << std::endl;

    return(buf.str());
}
