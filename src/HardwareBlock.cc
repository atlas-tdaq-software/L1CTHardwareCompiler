//******************************************************************************
// file: HardwareBlock.cc
// desc: library for L1CT HardwareCompiler
// auth: 01-MAR-2013 R. Spiwoks, re-design from RCDModuleDesign
//******************************************************************************

#include <algorithm>

#include "RCDUtilities/RCDUtilities.h"
#include "L1CTHardwareCompiler/HardwareCommon.h"
#include "L1CTHardwareCompiler/HardwareElement.h"
#include "L1CTHardwareCompiler/HardwareRegister.h"
#include "L1CTHardwareCompiler/HardwareMemory.h"
#include "L1CTHardwareCompiler/HardwareBlock.h"
#include "L1CTHardwareCompiler/HardwareCompiler.h"
#include "L1CTHardwareCompiler/CompilerOutputFile.h"
#include "L1CTHardwareCompiler/CompilerException.h"
#include "L1CTHardwareCompiler/CompilerStringList.h"

using namespace L1CTHC;
using namespace XERCES_CPP_NAMESPACE;

//------------------------------------------------------------------------------

const std::string HardwareBlock::XML_BLOCK              = "block";

const std::string HardwareBlock::VHDL_TYPE_NAME[VHDL_TYPE_NUMBER] = {
    "NONE", "MISO", "MOSI"
};

const std::string HardwareBlock::VHDL_TYPE_TAGS[VHDL_TYPE_NUMBER] = {
    "", "_miso", "_mosi"
};

//------------------------------------------------------------------------------

HardwareBlock::HardwareBlock(const std::string& name) : HardwareObject(name), m_upper_block(0) {

    // reset VHDL type
    for(unsigned int i=0; i < HardwareBlock::VHDL_TYPE_NUMBER; i++) m_vhdl_type[i] = false;
}

//------------------------------------------------------------------------------

HardwareBlock::~HardwareBlock() {

    // nothing to be done
}

//------------------------------------------------------------------------------

std::string HardwareBlock::nameName() const {

    std::string n(m_name);

    if(m_upper_block) n = m_upper_block->nameName() + "_" + n;
// 24-MAR-2014, R. Spiwoks, keep case of block from XML file
//    transform(n.begin(),n.end(),n.begin(),toupper);

    return(n);
}

//------------------------------------------------------------------------------

std::string HardwareBlock::menuName() const {

    std::string n(m_name);

    if(m_upper_block) n = m_upper_block->menuName() + "/" + n;
    transform(n.begin(),n.end(),n.begin(),toupper);

    return(n);
}

//------------------------------------------------------------------------------

void HardwareBlock::getBlockHierarchy(HardwareList<const HardwareBlock>& lst) const {

    const HardwareBlock* blk(this);

    lst.clear();
    while(blk) {
        lst.push_front(blk);
        blk = blk->getUpperBlock();
    }

    return;
}

//------------------------------------------------------------------------------

void HardwareBlock::dump(std::ostream& ostr, const unsigned int indt) const {

    char os[STRING_LENGTH];
    std::ostringstream buf;
    std::list<HardwareBlock*>::const_iterator iblk;
    std::list<HardwareElement*>::const_iterator ielm;

    // dump block
    if(m_multiple>1) buf << ", multiple = " << m_multiple << ", offset = " << m_offset;
    std::sprintf(os,"%sHardwareBlock \"%s\", addr = 0x%08x, type = \"%s\"%s:\n",HardwareCommon::spaceString(indt).c_str(),m_name.c_str(),m_addr,printTypeList().c_str(),buf.str().c_str()); ostr << os;

    // dump all elements
    for(ielm=m_elements.begin(); ielm!=m_elements.end(); ielm++) {
        (*ielm)->dump(ostr,indt+INDENT);
    }

    // dump all blocks
    for(iblk=m_blocks.begin(); iblk!=m_blocks.end(); iblk++) {
        (*iblk)->dump(ostr,indt+INDENT);
    }
}

//------------------------------------------------------------------------------

void HardwareBlock::read(const DOMNode* blk) {

    DOMNode* attr;
    std::string blkName, addName, typName, mdfName, mulName, offName;
    unsigned int iatt;
    bool flag;
    const HardwareModule* mod = getCompiler()->getModule();
    const CompilerConfiguration* cfg = getCompiler()->getConfig();

    // read and set block attributes
    attr = blk->getAttributes()->getNamedItem(TRX("name"));
    if(attr) blkName = TRX(attr->getNodeValue());
    attr = blk->getAttributes()->getNamedItem(TRX("addr"));
    if(attr) addName = TRX(attr->getNodeValue());
    attr = blk->getAttributes()->getNamedItem(TRX("type"));
    if(attr) typName = TRX(attr->getNodeValue());
    attr = blk->getAttributes()->getNamedItem(TRX("modf"));
    if(attr) mdfName = TRX(attr->getNodeValue());
    attr = blk->getAttributes()->getNamedItem(TRX("multiple"));
    if(attr) mulName = TRX(attr->getNodeValue());
    attr = blk->getAttributes()->getNamedItem(TRX("offset"));
    if(attr) offName = TRX(attr->getNodeValue());

    // print block attributes
    if(cfg->print_level>=4) {
        CompilerStringList att; att.delimiter(','); att.space(1);
        if(!mdfName.empty()) att.add("modf=\""+mdfName+"\"");
        if(!mulName.empty()||!offName.empty()) att.add("multiple=\""+mulName+"\", offset=\""+offName+"\"");
        COUT("found block \"%s\" with addr=\"%s\"%s%s",blkName.c_str(),addName.c_str(),!(att.str().empty())?", ":"",att.str().c_str());
    }

    // check block attribute NAME
    if(blkName == "") {
        CERR("block \"%s\" has no name",blkName.c_str());
        throw CompilerException("NO NAME");
    }

    // check block attribute ADDR
    if(!addName.empty()) {
        try {
            m_addr = HardwareCommon::readNumber(addName);
        }
        catch(const CompilerException& ce) {
            CERR("Exception \"%s\" while reading block \"%s\", address \"%s\"",ce.what(),blkName.c_str(),addName.c_str());
            throw;
        }
    }
    if(m_addr >= mod->size()) {
        CERR("block \"%s\" has address (0x%08x) outside of module \"%s\" size (0x%08x)",blkName.c_str(),m_addr,mod->name().c_str(),mod->size());
        throw CompilerException("INVALID ATTRIBUTE VALUE");
    }

    // check register attribute TYPE
    if(typName.empty()) {
        m_type = mod->type();
        // leave type list empty
    }
    else {
        try {
            createTypeList(typName);
        }
        catch(const CompilerException& ce) {
            CERR("Exception \"%s\" while reading block \"%s\", type \"%s\"",ce.what(),blkName.c_str(),typName.c_str());
            throw;
        }
        const HardwareObject* robj;
        if(!isTypeCompatible(mod,&robj)) {
            CERR("block \"%s\" type (\"%s\") incompatible with type of %s \"%s\" (\"%s\")",blkName.c_str(),printTypeList().c_str(),robj->hardwareType().c_str(),robj->name().c_str(),robj->printTypeList().c_str());
            throw CompilerException("INVALID ATTRIBUTE VALUE");
        }
    }

    // check register attribute MODF
    if(mdfName.empty()) {
        m_modf = RW;
    }
    else {
        for(flag=false, iatt=0; iatt<DATA_MODF_NUMBER; iatt++) {
            if(mdfName == DATA_MODF_NAME[iatt]) {
                m_modf = static_cast<DATA_MODF>(iatt);
                flag = true;
                break;
            }
        }
        if(!flag) {
            CERR("block \"%s\" has unknown modf \"%s\"",blkName.c_str(),mdfName.c_str());
            throw CompilerException("UNKNOWN MODIFIER");
        }
    }

    // check block attribute MULTIPLICITY and OFFSET
    if(!mulName.empty()) {
        try {
            m_multiple = HardwareCommon::readNumber(mulName);
        }
        catch(const CompilerException& ce) {
            CERR("Exception \"%s\" while reading block \"%s\", multiple \"%s\"",ce.what(),blkName.c_str(),mulName.c_str());
            throw;
        }
        if(m_multiple == 0) {
            CERR("block \"%s\" has empty multiple (%d)",blkName.c_str(),m_multiple);
            throw CompilerException("INVALID ATTRIBUTE VALUE");
        }
        if(m_multiple == 1) {
            if(!offName.empty()) {
                if(cfg->strict_check) {
                    CERR("block \"%s\" has offset \"%s\" but multiple 1",blkName.c_str(),offName.c_str());
                    throw CompilerException("INVALID ATTRIBUTE VALUE");
                }
                else {
                    if(cfg->print_level>=1) COUT("block \"%s\" has offset \"%s\" but multiple 1 => IGNORE OFFSET",blkName.c_str(),offName.c_str());
                }
            }
        }
        else {
            if(!offName.empty()) {
                try {
                   m_offset = HardwareCommon::readNumber(offName);
                }
                catch(const CompilerException& ce) {
                    CERR("Exception \"%s\" while reading block \"%s\", offset \"%s\"",ce.what(),blkName.c_str(),offName.c_str());
                    throw;
                }
                if(m_offset == 0) {
                    CERR("block \"%s\" has empty offset (%d)",blkName.c_str(),m_offset);
                    throw;
                }
            }
            else {
                if(cfg->strict_check) {
                    CERR("block \"%s\" has multiple \"%s\" but no offset",blkName.c_str(),mulName.c_str());
                    throw CompilerException("INVALID ATTRIBUTE VALUE");
                }
                else {
                    m_offset = 1;
                    if(cfg->print_level>=1) COUT("block \"%s\" has multiple \"%s\" but no offset => SET OFFSET TO %d",blkName.c_str(),mulName.c_str(),m_offset);
                }
            }
        }
    }
    else {
        if(!offName.empty()) {
            if(cfg->strict_check) {
                CERR("block \"%s\" has offset \"%s\" but no multiple",blkName.c_str(),offName.c_str());
                throw CompilerException("INVALID ATTRIBUTE VALUE");
            }
            else {
                if(cfg->print_level>=1) COUT("block \"%s\" has offset \"%s\" but no multiple => IGNORE OFFSET",blkName.c_str(),offName.c_str());
            }
        }
    }

    // set register attributes
    m_name = blkName;

    // read all elements
    readBlocksAndElements(blk);

    // promote implicit modf
    if(cfg->promote_modf) {
        if(m_modf == RW) {
            if(!hasReadableElement()) m_modf = W;
            if(!hasWritableElement()) m_modf = R;
        }
    }
}

//------------------------------------------------------------------------------

void HardwareBlock::readBlocksAndElements(const DOMNode* blk) {

    DOMNodeList* nodList;
    DOMNode* nod;
    unsigned int inod;
    std::string blkName, elmName;
    const CompilerConfiguration* cfg = getCompiler()->getConfig();

    // get all blocks and elements
    nodList = blk->getChildNodes();
    if(cfg->print_level>=4) {
        std::string blkName = (blk->getNodeType() == DOMNode::ELEMENT_NODE) ? m_name : TRX(blk->getNodeName());
        COUT("block \"%s\" has %d child%s",blkName.c_str(),nodList->getLength(),nodList->getLength()>1?"ren":"");
    }

    // traverse all blocks and elements
    for(inod=0; inod<nodList->getLength(); inod++) {
        nod = nodList->item(inod);

        // entity reference node type
        if(nod->getNodeType() == DOMNode::ENTITY_REFERENCE_NODE) {
            if(cfg->print_level>=4) COUT("found element entity reference \"%s\"",TRX(nod->getNodeName()));
            readBlocksAndElements(nod);
            continue;
        }
        // element node type
        else if(nod->getNodeType() != DOMNode::ELEMENT_NODE) continue;

        // node = block
        if(HardwareBlock::isBlock(nod)) {
            blkName = TRX(nod->getAttributes()->getNamedItem(TRX("name"))->getNodeValue());
            HardwareBlock* ablk;
            if(findBlock(blkName,&ablk)) {
                CERR("block name \"%s\" already exists",blkName.c_str());
                throw CompilerException("NAME EXISTS");
            }
            ablk = new HardwareBlock(blkName);
            ablk->setUpperBlock(this);
            if(cfg->print_level>=4) {
                COUT("found nested block \"%s\" in block \"%s\"",ablk->name().c_str(),name().c_str());
            }
            addBlock(ablk);
            ablk->read(nod);
        }

        // node = element/register
        else if(HardwareRegister::isRegister(nod)) {
             elmName = TRX(nod->getAttributes()->getNamedItem(TRX("name"))->getNodeValue());
             HardwareRegister* aelm;
             if(findElement(elmName,(HardwareElement**)&aelm)) {
                CERR("register name \"%s\" already exists for block \"%s\"",elmName.c_str(),m_name.c_str());
                throw CompilerException("NAME EXISTS");
             }
             aelm = new HardwareRegister(elmName);
             addElement(aelm);
             aelm->setBlock(this);
             aelm->read(nod);
        }

        // node = element/memory
        else if(HardwareMemory::isMemory(nod)) {
             elmName = TRX(nod->getAttributes()->getNamedItem(TRX("name"))->getNodeValue());
             HardwareMemory* aelm;
             if(findElement(elmName,(HardwareElement**)&aelm)) {
                CERR("memory/fifo name \"%s\" already exists for block \"%s\"",elmName.c_str(),m_name.c_str());
                throw CompilerException("NAME EXISTS");
             }
             aelm = new HardwareMemory(elmName);
             addElement(aelm);
             aelm->setBlock(this);
             aelm->read(nod);
        }

        // unknown type
        else {
            CERR("child of block \"%s\" has unknown type \"%s\"",m_name.c_str(),TRX(nod->getNodeName()));
            throw CompilerException("UNKNOWN TYPE");
        }
    }
}

//------------------------------------------------------------------------------

void HardwareBlock::addBlock(HardwareBlock* blk) {

    m_blocks.addItem(blk);
}

//------------------------------------------------------------------------------

bool HardwareBlock::findBlock(const std::string& name, HardwareBlock** rblk) const {

    return(m_blocks.findItem(name,rblk));
}

//------------------------------------------------------------------------------

void HardwareBlock::write(std::ostream& ostr) const {

    char os[STRING_LENGTH];
    std::ostringstream buf;
    std::list<HardwareElement*>::const_iterator ielm;

    // write block start tag
    if(!m_name.empty()) {
        if(m_addr) buf << " addr=\"" << hex(8) << m_addr << dec << "\"";
        if(!printTypeList().empty()) buf << " type=\"" << printTypeList() << "\"";
        if(m_modf != RW) buf << " modf=\"" << DATA_MODF_NAME[m_modf] << "\"";
        if(m_multiple > 1) buf << " multiple=\"" << m_multiple << "\" offset=\"" << hex(1) << m_offset << "\"";
        std::sprintf(os,"  <%s name=\"%s\"%s>\n",XML_BLOCK.c_str(),m_name.c_str(),buf.str().c_str()); ostr << os;
    }

    // write all elements
    for(ielm=m_elements.begin(); ielm!=m_elements.end(); ielm++) {
        (*ielm)->write(ostr);
    }

    // write block end tag
    if(!m_name.empty()) {
        std::sprintf(os,"  </%s>\n",XML_BLOCK.c_str()); ostr << os;
    }
}

//------------------------------------------------------------------------------

void HardwareBlock::addElement(HardwareElement* elm) {

    m_elements.push_back(elm);
}

//------------------------------------------------------------------------------

bool HardwareBlock::findElement(const std::string& name, HardwareElement** relm) const {

    return(m_elements.findItem(name,relm));
}
//------------------------------------------------------------------------------

bool HardwareBlock::isBlock(const XERCES_CPP_NAMESPACE::DOMNode* blk) {

    if(blk && (TRX(blk->getNodeName()) == XML_BLOCK)) return(true);

    return(false);
}

//------------------------------------------------------------------------------

bool HardwareBlock::hasReadableElement() const {

    HardwareList<HardwareElement>::const_iterator ielm;

    for(ielm=m_elements.begin(); ielm!=m_elements.end(); ielm++) {
        if(((*ielm)->modf() == R) || ((*ielm)->modf() == RW)) return(true);
    }

    return(false);
}

//------------------------------------------------------------------------------

bool HardwareBlock::hasWritableElement() const {

    HardwareList<HardwareElement>::const_iterator ielm;

    for(ielm=m_elements.begin(); ielm!=m_elements.end(); ielm++) {
        if(((*ielm)->modf() == W) || ((*ielm)->modf() == RW)) return(true);
    }

    return(false);
}
