//******************************************************************************
// file: runHardwareCompiler.cc
// desc: run L1CT HardwareCompiler
// auth: 27-FEB-2013 R. Spiwoks, re-design from RCDModuleDesign
//******************************************************************************

#include <cstdlib>

#include "L1CTHardwareCompiler/HardwareCompiler.h"

//------------------------------------------------------------------------------

int main(int argc, char* argv[]) {

    L1CTHC::CompilerConfiguration cc;
    L1CTHC::HardwareCompiler hc;

    // read command-line options
    cc.config(argc,argv);

    // run compiler
    try {
        hc.compile(cc);
    }
    catch (...) {
    }
}
