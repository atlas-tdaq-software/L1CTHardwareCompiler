//******************************************************************************
// file: CompilerErrorHandler.cc
// desc: DOM parse error handler: print-out of errors
// auth: 10-MAY-2013 R. Spiwoks
//******************************************************************************

#include "RCDUtilities/RCDUtilities.h"
#include "L1CTHardwareCompiler/CompilerErrorHandler.h"

#define TRX(string)     XMLString::transcode(string)

using namespace L1CTHC;
using namespace XERCES_CPP_NAMESPACE;

//------------------------------------------------------------------------------

void CompilerErrorHandler::warning(const SAXParseException& exc) {

    HandlerBase::warning(exc);
    COUT("DOM Parse warning = \"%s\" in file \"%s\", line %d, column %d",TRX(exc.getMessage()),TRX(exc.getSystemId()),exc.getLineNumber(),exc.getColumnNumber());
}

//------------------------------------------------------------------------------

void CompilerErrorHandler::error(const SAXParseException& exc) {

    HandlerBase::error(exc);
    CERR("DOM Parse error = \"%s\" in file \"%s\", line %d, column %d",TRX(exc.getMessage()),TRX(exc.getSystemId()),exc.getLineNumber(),exc.getColumnNumber());
}

//------------------------------------------------------------------------------

void CompilerErrorHandler::fatalError(const SAXParseException& exc) {

    HandlerBase::fatalError(exc);
    CERR("DOM Parse fatal error = \"%s\" in file \"%s\", line %d, column %d",TRX(exc.getMessage()),TRX(exc.getSystemId()),exc.getLineNumber(),exc.getColumnNumber());
}
