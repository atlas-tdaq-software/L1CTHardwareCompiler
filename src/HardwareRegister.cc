//******************************************************************************
// file: HardwareRegister.cc
// desc: library for L1CT HardwareCompiler
// auth: 01-MAR-2013 R. Spiwoks, re-design from RCDModuleDesign
//******************************************************************************

#include <sstream>
#include <algorithm>

#include "RCDUtilities/RCDUtilities.h"
#include "L1CTHardwareCompiler/HardwareWord.h"
#include "L1CTHardwareCompiler/HardwareRegister.h"
#include "L1CTHardwareCompiler/HardwareBitString.h"
#include "L1CTHardwareCompiler/HardwareCompiler.h"
#include "L1CTHardwareCompiler/CompilerException.h"
#include "L1CTHardwareCompiler/CompilerStringList.h"

using namespace L1CTHC;
using namespace XERCES_CPP_NAMESPACE;

//------------------------------------------------------------------------------

const std::string HardwareRegister::XML_REGISTER        = "register";

//------------------------------------------------------------------------------

HardwareRegister::HardwareRegister(const std::string& name) : HardwareElement(name) {

    // nothing to be done
}

//------------------------------------------------------------------------------

HardwareRegister::~HardwareRegister() {

    // nothing to be done
}

//------------------------------------------------------------------------------

void HardwareRegister::dump(std::ostream& ostr, const unsigned int indt) const {

    std::ostringstream buf;

    // dump register
    buf << HardwareCommon::spaceString(indt) << "HardwareRegister \"" << m_name << "\", addr = " << hex(8) << m_addr << dec << ", type = \"" << typeName() << "\", modf = \"" << DATA_MODF_NAME[m_modf] << "\"";
    if(m_multiple>1) buf << ", multiple = " << m_multiple << ", offset = " << m_offset;
    ostr << buf.str() << ":" << std::endl;

    // dump data type (reference or internal)
    if(m_data) {
        if(m_data->isReference()) {
            ostr << HardwareCommon::spaceString(indt+INDENT) << "HardwareBitString \"" << m_data->getReference()->nameName() << "\" (Reference)" << std::endl;
        }
        else {
            m_data->dump(ostr,indt+INDENT);
        }
    }
    if(m_datb) {
        if(m_datb->isReference()) {
            ostr << HardwareCommon::spaceString(indt+INDENT) << "HardwareBitString \"" << m_datb->getReference()->nameName() << "\" (Reference)" << std::endl;
        }
        else {
            m_datb->dump(ostr,indt+INDENT);
        }
    }
}

//------------------------------------------------------------------------------

void HardwareRegister::read(const DOMNode* reg) {

    DOMNode* attr;
    std::string regName, addName, typName, mskName, mdfName, mulName, offName, bitName;
    unsigned int addr, iatt;
    bool flag;
    const HardwareModule* mod = getCompiler()->getModule();
    const CompilerConfiguration* cfg = getCompiler()->getConfig();

    // read register attributes
    attr = reg->getAttributes()->getNamedItem(TRX("name"));
    if(attr) regName = TRX(attr->getNodeValue());
    attr = reg->getAttributes()->getNamedItem(TRX("addr"));
    if(attr) addName = TRX(attr->getNodeValue());
    attr = reg->getAttributes()->getNamedItem(TRX("type"));
    if(attr) typName = TRX(attr->getNodeValue());
    attr = reg->getAttributes()->getNamedItem(TRX("mask"));
    if(attr) mskName = TRX(attr->getNodeValue());
    attr = reg->getAttributes()->getNamedItem(TRX("modf"));
    if(attr) mdfName = TRX(attr->getNodeValue());
    attr = reg->getAttributes()->getNamedItem(TRX("multiple"));
    if(attr) mulName = TRX(attr->getNodeValue());
    attr = reg->getAttributes()->getNamedItem(TRX("offset"));
    if(attr) offName = TRX(attr->getNodeValue());
    attr = reg->getAttributes()->getNamedItem(TRX("bitstring"));
    if(attr) bitName = TRX(attr->getNodeValue());

    // print register attributes
    if(cfg->print_level>=4) {
        CompilerStringList att; att.delimiter(','); att.space(1);
        if(!typName.empty()) att.add("type=\""+typName+"\"");
        if(!mskName.empty()) att.add("mask=\""+mskName+"\"");
        if(!mdfName.empty()) att.add("modf=\""+mdfName+"\"");
        if(!mulName.empty()||!offName.empty()) att.add("multiple=\""+mulName+"\", offset=\""+offName+"\"");
        if(!bitName.empty()) att.add("bitstring=\""+bitName+"\"");
        COUT("found register \"%s\" with addr=\"%s\"%s%s",regName.c_str(),addName.c_str(),!(att.str().empty())?", ":"",att.str().c_str());
    }

    // check register attribute NAME
    if(regName == "") {
        CERR("register \"%s\" has no name",regName.c_str());
        throw CompilerException("NO NAME");
    }

    // check register attribute ADDR
    try {
        m_addr = HardwareCommon::readNumber(addName);
    }
    catch(const CompilerException& ce) {
        CERR("Exception \"%s\" while reading register \"%s\", address \"%s\"",ce.what(),regName.c_str(),addName.c_str());
        throw;
    }
    addr = m_block->addr() + m_addr;
    if(addr >= mod->size()) {
        CERR("register \"%s\" has address (0x%08x) outside of module \"%s\" size (0x%08x)",regName.c_str(),addr,mod->name().c_str(),mod->size());
        throw CompilerException("INVALID ATTRIBUTE VALUE");
    }

    // check register attrubute TYPE
    if(typName.empty()) {
        m_type = m_block->type();
    }
    else {
        for(flag=false, iatt=0; iatt<DATA_TYPE_NUMBER; iatt++) {
            if(typName == DATA_TYPE_NAME[iatt]) {
                m_type = static_cast<DATA_TYPE>(iatt);
                flag = true;
                break;
            }
        }
        if(!flag) {
            CERR("register \"%s\" has unknown type \"%s\"",regName.c_str(),typName.c_str());
            throw CompilerException("UNKNOWN TYPE");
        }
        else {
            const HardwareObject* robj;
            if(!isTypeCompatible(m_block,&robj)) {
                CERR("register \"%s\" type (\"%s\") incompatible with type of %s \"%s\" (\"%s\")",regName.c_str(),DATA_TYPE_NAME[m_type].c_str(),robj->hardwareType().c_str(),robj->name().c_str(),robj->printTypeList().c_str());
                throw CompilerException("INVALID ATTRIBUTE VALUE");
            }
        }
    }
    if(m_addr % (DATA_TYPE_SIZE[m_type]/HardwareCommon::BIT_SIZE) != 0) {
        CERR("register \"%s\" has address (0x%08x) incompatible with type \"%s\"",regName.c_str(),m_addr,DATA_TYPE_NAME[m_type].c_str());
        throw CompilerException("INVALID ATTRIBUTE VALUE");
    }

    // check register attribute MASK
    if(!mskName.empty()) {
        try {
            m_mask = HardwareCommon::readNumber(mskName);
        }
        catch(const CompilerException& ce) {
            CERR("Exception \"%s\" while reading register \"%s\", mask \"%s\"",ce.what(),regName.c_str(),mskName.c_str());
            throw;
        }
        if(m_mask & (~DATA_TYPE_MMAX[m_type])) {
            if(cfg->strict_check) {
                CERR("register \"%s\" has mask (0x%08x) incompatible with mask of type \"%s\" (0x%08x)",regName.c_str(),m_mask,typeName().c_str(),DATA_TYPE_MMAX[m_type]);
                throw CompilerException("INVALID ATTRIBUTE VALUE");
            }
            else {
                if(cfg->print_level>=1) COUT("register \"%s\" has mask (0x%08x) incompatible with mask of type \"%s\" (0x%08x) => SET MASK TO 0x%08x",regName.c_str(),m_mask,typeName().c_str(),DATA_TYPE_MMAX[m_type],m_mask&DATA_TYPE_MMAX[m_type]);
                m_mask &= DATA_TYPE_MMAX[m_type];
            }
        }
    }
    else {
        m_mask = DATA_TYPE_MMAX[m_type];
    }

    // check register attribute MODF
    if(mdfName.empty()) {
        m_modf = m_block->modf();
    }
    else {
        for(flag=false, iatt=0; iatt<DATA_MODF_NUMBER; iatt++) {
            if(mdfName == DATA_MODF_NAME[iatt]) {
                m_modf = static_cast<DATA_MODF>(iatt);
                flag = true;
                break;
            }
        }
        if(flag) {
            if(!modfIsCompatible(m_block->modf())) {
                CERR("register \"%s\" modf (\"%s\") incompatible with modf of block \"%s\" (\"%s\")",regName.c_str(),DATA_MODF_NAME[m_modf].c_str(),m_block->name().c_str(),DATA_MODF_NAME[m_block->modf()].c_str());
                throw CompilerException("INVALID ATTRIBUTE VALUE");
            }
        }
        else {
            CERR("register \"%s\" has unknown modf \"%s\"",regName.c_str(),mdfName.c_str());
            throw CompilerException("UNKNOWN MODIFIER");
        }
    }

    // check register attributes MULTIPLICITY and OFFSET
    if(!mulName.empty()) {
        try {
            m_multiple = HardwareCommon::readNumber(mulName);
        }
        catch(const CompilerException& ce) {
            CERR("Exception \"%s\" while reading register \"%s\", multiple \"%s\"",ce.what(),regName.c_str(),mulName.c_str());
            throw;
        }
        if(m_multiple == 0) {
            CERR("register \"%s\" has empty multiple (%d)",regName.c_str(),m_multiple);
            throw CompilerException("INVALID ATTRIBUTE VALUE");
        }
        if(m_multiple == 1) {
            if(!offName.empty()) {
                if(cfg->strict_check) {
                    CERR("register \"%s\" has offset \"%s\" but multiple 1",regName.c_str(),offName.c_str());
                    throw CompilerException("INVALID ATTRIBUTE VALUE");
                }
                else {
                    if(cfg->print_level>=1) COUT("register \"%s\" has offset \"%s\" but multiple 1 => IGNORE OFFSET",regName.c_str(),offName.c_str());
                }
            }
        }
        else {
            if(!offName.empty()) {
                try {
                   m_offset = HardwareCommon::readNumber(offName);
                }
                catch(const CompilerException& ce) {
                    CERR("Exception \"%s\" while reading register \"%s\", offset \"%s\"",ce.what(),regName.c_str(),offName.c_str());
                    throw;
                }
                if(m_offset == 0) {
                    if(cfg->strict_check) {
                        CERR("register \"%s\" has empty offset (%d)",regName.c_str(),m_offset);
                        throw CompilerException("INVALID ATTRIBUTE VALUE");
                    }
                    else {
                        if(cfg->print_level>=1) COUT("register \"%s\" has empty offset (%d) => CONTINUE USING ANYWAY",regName.c_str(),m_offset);
                    }
                }
                unsigned int offs = DATA_TYPE_SIZE[m_type]/HardwareCommon::BIT_SIZE;
                if(m_offset%offs != 0) {
                    CERR("register \"%s\" has offset (%d) not divisible by address offset (%d) for this type (%s)",regName.c_str(),m_offset,offs,typeName().c_str());
                    throw CompilerException("INVALID ATTRIBUTE VALUE");
                }
            }
            else {
                if(cfg->strict_check) {
                    CERR("register \"%s\" has multiple \"%s\" but no offset",regName.c_str(),mulName.c_str());
                    throw CompilerException("INVALID ATTRIBUTE VALUE");
                }
                else {
                    m_offset = DATA_TYPE_SIZE[m_type]/HardwareCommon::BIT_SIZE;
                    if(cfg->print_level>=1) COUT("register \"%s\" has multiple \"%s\" but no offset => SET OFFSET TO %d",regName.c_str(),mulName.c_str(),m_offset);
                }
            }
        }
    }
    else {
        if(!offName.empty()) {
            if(cfg->strict_check) {
                CERR("register \"%s\" has offset \"%s\" but no multiple",regName.c_str(),offName.c_str());
                throw CompilerException("INVALID ATTRIBUTE VALUE");
            }
            else {
                if(cfg->print_level>=1) COUT("register \"%s\" has offset \"%s\" but no multiple => IGNORE OFFSET",regName.c_str(),offName.c_str());
            }
        }
    }

    // check register attribute bitstring
    if(!bitName.empty()) {
        const HardwareDataType* dat;
        if(getCompiler()->getFromDataTypeMap(HardwareBitString::FILENAME(),bitName,&dat)) {
            if(dat->getFullMask() & (~m_mask)) {
                CERR("register \"%s\" mask (0x%08x) incompatible with mask of bitstring \"%s\" (0x%08x)",regName.c_str(),m_mask,dat->name().c_str(),dat->getFullMask());
                throw CompilerException("INVALID ATTRIBUTE VALUE");
            }
            if(!dat->modfIsCompatible(m_modf)) {
                CERR("register \"%s\" modf (\"%s\") incompatible with modf of bitstring \"%s\" (\"%s\")",regName.c_str(),DATA_MODF_NAME[m_modf].c_str(),dat->name().c_str(),DATA_MODF_NAME[dat->modf()].c_str());
                throw CompilerException("INVALID ATTRIBUTE VALUE");
            }
            HardwareBitString* bit = new HardwareBitString(dynamic_cast<const HardwareBitString*>(dat));
            bit->setElement(this);
            m_data = bit;
        }
        else {
            CERR("register \"%s\" references bitstring \"%s\" which cannot be found",regName.c_str(),bitName.c_str());
            throw CompilerException("INVALID ATTRIBUTE VALUE");
        }
    }

    // check and add to address map
    getCompiler()->addToAddressMap(this);

    // set register attributes
    m_name = regName;

    // read data type
    readDataType(reg);

    // promote implicit mask
    if(cfg->promote_mask) {
        const HardwareBitString* bit = dynamic_cast<const HardwareBitString*>(m_data);
        if(bit && bit->isReference()) bit = dynamic_cast<const HardwareBitString*>(bit->getReference());
        if(bit) {
            if(bit->isReference()) bit = dynamic_cast<const HardwareBitString*>(bit->getReference());
            if(m_mask & (~bit->getFullMask())) {
                m_mask = bit->getFullMask();
            }
        }
    }

    // promote implicit modf
    if(cfg->promote_modf) {
        if(m_modf == RW) {
            if(!m_data->isReadable()) m_modf = W;
            if(!m_data->isWritable()) m_modf = R;
        }
    }
}

//------------------------------------------------------------------------------

void HardwareRegister::readDataType(const DOMNode* reg) {

    const CompilerConfiguration* cfg = getCompiler()->getConfig();

    // attempt finding bitstring element
    readDataTypeLoop(reg);

    // attempt reading bitstring from level above (=register) if none found yet
    if(!m_data) {
        HardwareBitString* bit = new HardwareBitString();
        bit->setElement(this);
        bit->read(reg);
        if(!bit->isEmpty()) {
            m_data = bit;
            getCompiler()->addToDataTypeMap(bit);

            // set aternative data type
            if(cfg->alternative_methods) {
                HardwareWord* wrd = new HardwareWord();
                wrd->type(m_type);
                wrd->setElement(this);
                m_datb = wrd;
            }
        }
        // use HardwareWord if bitstring is empty
        else {
            HardwareWord* wrd = new HardwareWord();
            wrd->type(m_type);
            wrd->setElement(this);
            m_data = wrd;
            delete bit;
        }
    }
    else {

        // set aternative data type
        if(cfg->alternative_methods) {
            HardwareWord* wrd = new HardwareWord();
            wrd->type(m_type);
            wrd->setElement(this);
            m_datb = wrd;
        }
    }
}

//------------------------------------------------------------------------------

void HardwareRegister::readDataTypeLoop(const DOMNode* reg) {

    DOMNodeList* datList;
    DOMNode* dat;
    unsigned int idat, nfld(0);
    const CompilerConfiguration* cfg = getCompiler()->getConfig();

    // get all data
    datList = reg->getChildNodes();
    if(cfg->print_level>=4) {
        std::string regName = (reg->getNodeType() == DOMNode::ELEMENT_NODE) ? m_name : TRX(reg->getNodeName());
        COUT("register \"%s\" has %d child%s",regName.c_str(),datList->getLength(),datList->getLength()>1?"ren":"");
    }

    // traverse all data
    for(idat=0; idat<datList->getLength(); idat++) {
        dat = datList->item(idat);

        // entity reference node type
        if(dat->getNodeType() == DOMNode::ENTITY_REFERENCE_NODE) {
            if(cfg->print_level>=4) COUT("found data entity reference \"%s\"",TRX(dat->getNodeName()));
            readDataTypeLoop(dat);
            continue;
        }
        // element node type
        else if(dat->getNodeType() != DOMNode::ELEMENT_NODE) continue;

        // bitstring element
        if(HardwareBitString::isBitString(dat)) {
            if(nfld) {
                CERR("register \"%s\" cannot have bitstring and field elements at the same time",m_name.c_str());
                throw CompilerException("INVALID DATA TYPE");
            }
            if(m_data) {
                CERR("register \"%s\" already attached to bitstring \"%s\"",m_name.c_str(),m_data->name().c_str());
                throw CompilerException("BITSTRING ALREADY SET");
            }
            HardwareBitString* bit = new HardwareBitString();
            bit->setElement(this);
            bit->read(dat);
            if(bit->isEmpty()) {
                CERR("register \"%s\" cannot have empty bitstring \"%s\"",m_name.c_str(),bit->name().c_str());
                throw CompilerException("INVALID DATA TYPE");
            }
            m_data = bit;
            getCompiler()->addToDataTypeMap(bit);
        }

        // anonymous bitstring: field
        else if(HardwareField::isField(dat)) {
            if(m_data) {
                CERR("register \"%s\" cannot have bitstring and field elements at the same time",m_name.c_str());
                throw CompilerException("INVALID STRUCTURE");
            }
            nfld++;
        }

        // unknown type
        else {
            CERR("child of register \"%s\" has unknown type \"%s\"",m_name.c_str(),TRX(dat->getNodeName()));
            throw CompilerException("UNKNOWN TYPE");
        }
    }
}

//------------------------------------------------------------------------------

void HardwareRegister::write(std::ostream& ostr) const {

    char os[STRING_LENGTH];
    std::ostringstream buf;
    std::string indent;
    unsigned int rgt;

    // indentation
    indent = (m_block->name().empty()) ? "  " : "    ";
    rgt = XML_TAB_LENGTH - XML_REGISTER.size() - indent.size();

    // optional attributes
    if(m_type != D32) buf << " type=\"" << DATA_TYPE_NAME[m_type] << "\"";
    if(m_mask != DATA_TYPE_MMAX[m_type]) buf << " mask=\"" << hex(8) << m_mask << dec << "\"";
    if((m_modf != RW) && (m_modf != m_block->modf())) buf << " modf=\"" << DATA_MODF_NAME[m_modf] << "\"";
    if(m_multiple > 1) buf << " multiple=\"" << m_multiple << "\" offset=\"" << hex(1) << m_offset << "\"";

    // if data type is empty or reference
    if(m_data->isEmpty() || m_data->isReference()) {

        // if data type is reference
        if(m_data->isReference()) buf << " bitstring =\"" << m_data->getReference()->nameName() << "\"";

        // write register single tag
        std::sprintf(os,"%s<%s name=%s addr=\"0x%08x\"%s/>\n",indent.c_str(),XML_REGISTER.c_str(),HardwareCommon::rightFillString("\""+m_name+"\"",rgt).c_str(),m_addr,buf.str().c_str()); ostr << os;
    }

    // if data type is not empty and not reference
    else {

        // write register start tag
        std::sprintf(os,"%s<%s name=%s addr=\"0x%08x\"%s>\n",indent.c_str(),XML_REGISTER.c_str(),HardwareCommon::rightFillString("\""+m_name+"\"",rgt).c_str(),m_addr,buf.str().c_str()); ostr << os;

        // write data type
        if(m_data) m_data->write(ostr);

        // write register end tag
        std::sprintf(os,"%s</%s>\n",indent.c_str(),XML_REGISTER.c_str()); ostr << os;
    }
}

//------------------------------------------------------------------------------

bool HardwareRegister::isRegister(const XERCES_CPP_NAMESPACE::DOMNode* reg) {

    if(reg && (TRX(reg->getNodeName()) == XML_REGISTER)) return(true);

    return(false);
}
