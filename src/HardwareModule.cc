//******************************************************************************
// file: HardwareModule.cc
// desc: library for L1CT HardwareCompiler
// auth: 01-MAR-2013 R. Spiwoks, re-design from RCDModuleDesign
//******************************************************************************

#include <vector>

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/parsers/XercesDOMParser.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/sax/HandlerBase.hpp>

#include "RCDUtilities/RCDUtilities.h"
#include "L1CTHardwareCompiler/HardwareCommon.h"
#include "L1CTHardwareCompiler/HardwareBlock.h"
#include "L1CTHardwareCompiler/HardwareRegister.h"
#include "L1CTHardwareCompiler/HardwareMemory.h"
#include "L1CTHardwareCompiler/HardwareBitString.h"
#include "L1CTHardwareCompiler/HardwareModule.h"
#include "L1CTHardwareCompiler/HardwareCompiler.h"
#include "L1CTHardwareCompiler/CompilerOutputFile.h"
#include "L1CTHardwareCompiler/CompilerException.h"
#include "L1CTHardwareCompiler/CompilerErrorHandler.h"
#include "L1CTHardwareCompiler/CompilerStringList.h"

using namespace L1CTHC;
using namespace XERCES_CPP_NAMESPACE;

//------------------------------------------------------------------------------

const std::string HardwareModule::XML_MODULE    = "module";
const std::string HardwareModule::CPP_ADDR_NAME = "VMM_ADDR";
const std::string HardwareModule::CPP_SIZE_NAME = "VMM_SIZE";
const std::string HardwareModule::CPP_AMOD_NAME = "VMM_AMOD";
const std::string HardwareModule::CPP_CODE_NAME = "VMM_CODE";

//------------------------------------------------------------------------------

HardwareModule::HardwareModule(const std::string& name) : HardwareObject(name), m_size(0), m_form(0), m_amod(A32), m_desc("") {

    // nothing to be done
}

//------------------------------------------------------------------------------

HardwareModule::~HardwareModule() {

    // nothing to be done
}

//------------------------------------------------------------------------------

void HardwareModule::dump(std::ostream& ostr, const unsigned int indt) const {

    char os[STRING_LENGTH];
    HardwareList<HardwareBlock>::const_iterator iblk;

    // header
    std::printf("================================================================================\n");
    std::printf("HARDWARE MODULE:\n");
    std::printf("--------------------------------------------------------------------------------\n");

    // dump module
    std::sprintf(os,"%sHardwareModule \"%s\", addr = 0x%08x, size = 0x%08x, form = %1d, amod = \"%s\", type= \"%s\", desc = \"%s\":\n",HardwareCommon::spaceString(indt).c_str(),m_name.c_str(),m_addr,m_size,m_form,ADDR_TYPE_NAME[m_amod].c_str(),printTypeList().c_str(),m_desc.c_str()); ostr << os;

    // dump all external data types
    HardwareCompiler::HardwareDataTypeMapConstIterator idat;
    HardwareList<const HardwareDataType>* datList;
    HardwareList<const HardwareDataType>::const_iterator jdat;
    for(idat=getCompiler()->beginDataTypeMap(); idat!=getCompiler()->endDataTypeMap(); idat++) {
        datList = idat->second;
        for(jdat=datList->begin(); jdat!=datList->end(); jdat++) {
            if((*jdat)->decl() == HardwareDataType::EXTERNAL) (*jdat)->dump(ostr,indt+INDENT);
        }
    }

    // dump all blocks
    for(iblk=m_blocks.begin(); iblk!=m_blocks.end(); iblk++) {
        (*iblk)->dump(ostr,indt+INDENT);
    }
}

//------------------------------------------------------------------------------

void HardwareModule::read(const std::string& fn) {

    // perform per-process parser initialization
    try {
        XMLPlatformUtils::Initialize();
    }
    catch(const XMLException& xml_catch) {
        CERR("during Xerces initialisation, exception = \"%s\"",TRX(xml_catch.getMessage()));
        throw CompilerException("XERCES EXCEPTION");
    }

    // create a new parser with error handler
    XercesDOMParser* prs = new XercesDOMParser;
    prs->setValidationScheme(getCompiler()->getConfig()->parse_validate?XercesDOMParser::Val_Always:XercesDOMParser::Val_Never);
    ErrorHandler* err = (ErrorHandler*) new CompilerErrorHandler();
    prs->setErrorHandler(err);
    int errCount;
    bool errFlag = false;

    // parse xml file and catch errors
    try {
        CDBG("XML file = \"%s\"",fn.c_str());
        prs->parse(fn.c_str());
        errCount = prs->getErrorCount();
        if(errCount > 0) {
            errFlag = true;
            CERR("during parsing of XML read file \"%s\", errCount = %d",fn.c_str(),errCount);
        }
    }
    catch(const XMLException& toCatch) {
        errFlag = true;
        CERR("XML exception = \"%s\"",TRX(toCatch.getMessage()));
        throw CompilerException("XML EXCEPTION");
    }
    catch(const DOMException& toCatch) {
        errFlag = true;
        CERR("DOM exception = \"%s\"",TRX(toCatch.msg));
        throw CompilerException("DOM EXCEPTION");
    }
    catch(const SAXParseException& toCatch) {
        errFlag = true;
        CERR("SAX Parse exception = \"%s\" in line %d, column %d",TRX(toCatch.getMessage()),toCatch.getLineNumber(),toCatch.getColumnNumber());
        throw CompilerException("SAX EXCEPTION");
    }
    catch(...) {
        errFlag = true;
        CERR("other unspecified exception","");
        throw CompilerException("UNSPECIFIED EXCEPTION");
    }
    if(errFlag) {
        delete prs;
        delete err;
        throw CompilerException("PARSER ERROR");
    }

    DOMDocument* doc = prs->getDocument();
    DOMNode* mod;

    // find module
    try {
        mod = findModule(doc);
    }
    catch(const CompilerException& ce) {
        CERR("Exception \"%s\" while trying to find module",ce.what());
        throw;
    }

    // parse module
    try {
        read(mod);
    }
    catch(const CompilerException& ce) {
        CERR("Exception \"%s\" while trying to read module",ce.what());
        throw;
    }

    // clean up
    delete prs;
    delete err;

    // perform per-process parser termination
    XMLPlatformUtils::Terminate();
}

//------------------------------------------------------------------------------

DOMNode* HardwareModule::findModule(const DOMDocument* doc) const {

    DOMNodeList* modList;
    DOMNode* mod;
    int imod;

    // get "all" modules
    modList = doc->getChildNodes();
    if(getCompiler()->getConfig()->print_level>=4) COUT("document \"%s\" has %d child%s",TRX(doc->getNodeName()),modList->getLength(),modList->getLength()>1?"ren":"");

    // traverse "all" modules
    for(imod=0; imod<(int)modList->getLength(); imod++) {
        mod = modList->item(imod);
        // entity reference node type not possible here => only element node type
        if(mod->getNodeType() != DOMNode::ELEMENT_NODE) continue;
        if(isModule(mod)) return(mod);
    }

    CERR("Module not found","");
    throw CompilerException("MODULE NOT FOUND");
}

//------------------------------------------------------------------------------

void HardwareModule::read(const DOMNode* mod) {

    const CompilerConfiguration* cfg = getCompiler()->getConfig();
    DOMNode* attr;
    std::string modName, addName, sizName, amdName, typName, dscName;
    unsigned int iatt;
    bool flag;

    // read module attributes
    attr = mod->getAttributes()->getNamedItem(TRX("name"));
    if(attr) modName = TRX(attr->getNodeValue());
    attr = mod->getAttributes()->getNamedItem(TRX("addr"));
    if(attr) addName = TRX(attr->getNodeValue());
    attr = mod->getAttributes()->getNamedItem(TRX("size"));
    if(attr) sizName = TRX(attr->getNodeValue());
    attr = mod->getAttributes()->getNamedItem(TRX("amod"));
    if(attr) amdName = TRX(attr->getNodeValue());
    attr = mod->getAttributes()->getNamedItem(TRX("type"));
    if(attr) typName = TRX(attr->getNodeValue());
    attr = mod->getAttributes()->getNamedItem(TRX("desc"));
    if(attr) dscName = TRX(attr->getNodeValue());

    // print module attributes
    if(getCompiler()->getConfig()->print_level>=4) {
        CompilerStringList att; att.delimiter(','); att.space(1);
        if(!amdName.empty()) att.add("amod=\""+amdName+"\"");
        if(!typName.empty()) att.add("type=\""+typName+"\"");
        if(!dscName.empty()) att.add("desc=\""+dscName+"\"");
        COUT("found module \"%s\" with addr=\"%s\", size=\"%s\"%s%s",modName.c_str(),addName.c_str(),sizName.c_str(),!(att.str().empty())?", ":"",att.str().c_str());
    }

    // check module attribute NAME
    if(modName == "") {
        CERR("module \"%s\" has no name",modName.c_str());
        throw CompilerException("NO NAME");
    }

    // check module attribute ADDR
    try {
        m_addr = HardwareCommon::readNumber(addName);
    }
    catch(const CompilerException& ce) {
        CERR("Exception \"%s\" while reading module \"%s\", addr \"%s\"",ce.what(),modName.c_str(),addName.c_str());
        throw;
    }

    // check module attribute SIZE
    try {
        m_size = HardwareCommon::readNumber(sizName);
        m_form = HardwareCommon::getHexNumber(m_size);
    }
    catch(const CompilerException& ce) {
        CERR("Exception \"%s\" while reading module \"%s\", size \"%s\"",ce.what(),modName.c_str(),sizName.c_str());
        throw;
    }

    // check module attribute AMOD
    if(!amdName.empty()) {
        for(flag=false, iatt=0; iatt<ADDR_TYPE_NUMBER; iatt++) {
            if(amdName == ADDR_TYPE_NAME[iatt]) {
                if(amdName.find("ILLEGAL") != std::string::npos) {
                    CERR("module \"%s\" has illegal amod \"%s\"",modName.c_str(),amdName.c_str());
                    throw CompilerException("INVALID ATTRIBUTE VALUE");
                }
                m_amod = static_cast<ADDR_TYPE>(iatt);
                flag = true;
                break;
            }
        }
        if(!flag) {
            CERR("module \"%s\" has unknown amod \"%s\"",modName.c_str(),amdName.c_str());
            throw CompilerException("UNKNOWN AMOD");
        }
    }
    else {
        m_amod = A32;
    }
    if(m_addr & (~ADDR_TYPE_MMAX[m_amod])) {
        CERR("module \"%s\" has address (0x%08x) incompatible with amod \"%s\"",modName.c_str(),m_addr,ADDR_TYPE_NAME[m_amod].c_str());
        throw CompilerException("INVALID ATTRIBUTE VALUE");
    }
    if(m_size & (~ADDR_TYPE_MMAX[m_amod])) {
        CERR("module \"%s\" has size (0x%08x) incompatible with amod \"%s\"",modName.c_str(),m_size,ADDR_TYPE_NAME[m_amod].c_str());
        throw CompilerException("INVALID ATTRIBUTE VALUE");
    }

    // check module attribute TYPE
    if(typName.empty()) {
        m_type = D32;
        defaultTypeList();
    }
    else {
        try {
            createTypeList(typName);
        }
        catch(const CompilerException& ce) {
            CERR("Exception \"%s\" while reading module \"%s\", type \"%s\"",ce.what(),modName.c_str(),typName.c_str());
            throw;
        }
    }

    // check module attribute DESC
    if(dscName.empty()) {
        if(cfg->strict_check) {
            CERR("module \"%s\" has no description",modName.c_str());
            throw CompilerException("NO DESCRIPTION");
        }
        else {
            COUT("module \"%s\" has no description => IGNORE DESCRIPTION",modName.c_str());
        }
    }

    // set module attributes
    m_name = modName;
    m_desc = dscName;

    // read all data types
    readDataTypes(mod);

    // read all blocks
    readBlocks(mod);
}

//------------------------------------------------------------------------------

void HardwareModule::readBlocks(const DOMNode* mod) {

    DOMNodeList* blkList;
    DOMNode* blk;
    std::string blkName, elmName;
    unsigned int iblk;

    // get all blocks
    blkList = mod->getChildNodes();
    if(getCompiler()->getConfig()->print_level>=4) {
        std::string modName = (mod->getNodeType() == DOMNode::ELEMENT_NODE) ? m_name : TRX(mod->getNodeName());
        COUT("module \"%s\" has %d child%s",modName.c_str(),blkList->getLength(),blkList->getLength()>1?"ren":"");
    }

    // traverse all blocks
    for(iblk=0; iblk<blkList->getLength(); iblk++) {
        blk = blkList->item(iblk);

        // entity reference node type
        if(blk->getNodeType() == DOMNode::ENTITY_REFERENCE_NODE) {
            if(getCompiler()->getConfig()->print_level>=4) COUT("found block entity reference \"%s\"",TRX(blk->getNodeName()));
            readBlocks(blk);
            continue;
        }
        // element node type
        else if(blk->getNodeType() != DOMNode::ELEMENT_NODE) continue;

        // named block
        if(HardwareBlock::isBlock(blk)) {
            blkName = TRX(blk->getAttributes()->getNamedItem(TRX("name"))->getNodeValue());
            HardwareBlock* ablk;
            if(findBlock(blkName,&ablk)) {
                CERR("block name \"%s\" already exists",blkName.c_str());
                throw CompilerException("NAME EXISTS");
            }
            ablk = new HardwareBlock(blkName);
            addBlock(ablk);
            ablk->read(blk);
        }

        // anonymous block: register
        else if(HardwareRegister::isRegister(blk)) {
            HardwareBlock* ablk;
            if(!findBlock("",&ablk)) {
                ablk = new HardwareBlock();
                addBlock(ablk);
            }
            elmName = TRX(blk->getAttributes()->getNamedItem(TRX("name"))->getNodeValue());
            HardwareRegister* aelm;
            if(ablk->findElement(elmName,(HardwareElement**)(&aelm))) {
                CERR("register name \"%s\" already exists in anonymous block",elmName.c_str());
                throw CompilerException("NAME EXISTS");

            }
            aelm = new HardwareRegister(elmName);
            ablk->addElement(aelm);
            aelm->setBlock(ablk);
            aelm->read(blk);
        }

        // bitstring definition: skip
        else if(HardwareBitString::isBitString(blk)) {
            continue;
        }

        // unknown type
        else {
            CERR("child of module \"%s\" has unknown type \"%s\"",m_name.c_str(),TRX(blk->getNodeName()));
            throw CompilerException("UNKNOWN TYPE");
        }
    }
}

//------------------------------------------------------------------------------

void HardwareModule::readDataTypes(const DOMNode* mod) {

    DOMNodeList* datList;
    DOMNode* dat;
    unsigned int idat;

    // get all data types (if any)
    datList = mod->getChildNodes();
    if(getCompiler()->getConfig()->print_level>=4) {
        std::string modName = (mod->getNodeType() == DOMNode::ELEMENT_NODE) ? m_name : TRX(mod->getNodeName());
        COUT("module \"%s\" has %d child%s",modName.c_str(),datList->getLength(),datList->getLength()>1?"ren":"");
    }

    // traverse all data types (if any)
    for(idat=0; idat<datList->getLength(); idat++) {
        dat = datList->item(idat);

        // entity reference node type
        if(dat->getNodeType() == DOMNode::ENTITY_REFERENCE_NODE) {
            if(getCompiler()->getConfig()->print_level>=4) COUT("found entity reference \"%s\"",TRX(dat->getNodeName()));
            readDataTypes(dat);
            continue;
        }
        // element node type
        else if(dat->getNodeType() != DOMNode::ELEMENT_NODE) continue;

        if(HardwareBitString::isBitString(dat)) {
            HardwareBitString* bit = new HardwareBitString("",HardwareDataType::EXTERNAL);
            bit->read(dat);
            getCompiler()->addToDataTypeMap(bit);
        }
    }
}

//------------------------------------------------------------------------------

void HardwareModule::write(std::ostream& ostr) const {

    char os[STRING_LENGTH];
    std::ostringstream buf;
    HardwareList<HardwareBlock>::const_iterator iblk;

    // optional attributes
    if(m_amod != A32) buf << " amod=\"" << ADDR_TYPE_NAME[m_amod] << "\"";
    if(!isDefaultTypeList()) buf << " type=\"" << printTypeList() << "\"";
    if(!m_desc.empty()) buf << " desc=\"" << m_desc << "\"";

    // write module start tag
    std::sprintf(os,"<%s name=\"%s\" addr=\"0x%08x\" size=\"0x%08x\"%s>\n",XML_MODULE.c_str(),m_name.c_str(),m_addr,m_size,buf.str().c_str()); ostr << os;

    // write all external data types
    HardwareCompiler::HardwareDataTypeMapConstIterator idat;
    HardwareList<const HardwareDataType>* datList;
    HardwareList<const HardwareDataType>::const_iterator jdat;
    for(idat=getCompiler()->beginDataTypeMap(); idat!=getCompiler()->endDataTypeMap(); idat++) {
        datList = idat->second;
        for(jdat=datList->begin(); jdat!=datList->end(); jdat++) {
            if((*jdat)->decl() == HardwareDataType::EXTERNAL) (*jdat)->write(ostr);
        }
    }

    // write all blocks
    for(iblk=m_blocks.begin(); iblk!=m_blocks.end(); iblk++) {
        (*iblk)->write(ostr);
    }

    // write module end tag
    std::sprintf(os,"</%s>\n",XML_MODULE.c_str()); ostr << os;
}

//------------------------------------------------------------------------------

void HardwareModule::addBlock(HardwareBlock* blk) {

    m_blocks.addItem(blk);
}

//------------------------------------------------------------------------------

bool HardwareModule::findBlock(const std::string& name, HardwareBlock** rblk) const {

    return(m_blocks.findItem(name,rblk));
}

//------------------------------------------------------------------------------

bool HardwareModule::isModule(const XERCES_CPP_NAMESPACE::DOMNode* mod) {

    if(mod && (TRX(mod->getNodeName()) == XML_MODULE)) return(true);

    return(false);
}

//------------------------------------------------------------------------------

const HardwareMemory* HardwareModule::getMaximumSizeMemory() const {

    HardwareList<HardwareBlock>::const_iterator iblk;
    const HardwareBlock* blk;
    HardwareList<HardwareElement>::const_iterator ielm;
    const HardwareElement* elm;
    const HardwareMemory* mem;
    const HardwareMemory* max(0);
    unsigned int size(0);

    // loop over all blocks
    for(iblk=m_blocks.begin(); iblk!=m_blocks.end(); iblk++) {
        blk = *iblk;

        // loop over all elements
        for(ielm=blk->beginElement(); ielm!=blk->endElement(); ielm++) {
            elm = *ielm;
            if((mem = dynamic_cast<const HardwareMemory*>(elm)) && (mem->size() > size)) {
                size = mem->size();
                max = mem;
            }
        }
    }

    return(max);
}
