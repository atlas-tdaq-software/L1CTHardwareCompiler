//******************************************************************************
// file: HardwareCompiler.cc
// desc: library for L1CT HardwareCompiler
// auth: 27-FEB-2013 R. Spiwoks, re-design from RCDModuleDesign
//******************************************************************************

#include <ctime>
#include <iomanip>
#include <algorithm>
#include <unistd.h>
#include <pwd.h>

#include "RCDUtilities/RCDUtilities.h"
#include "L1CTHardwareCompiler/HardwareCommon.h"
#include "L1CTHardwareCompiler/HardwareCompiler.h"
#include "L1CTHardwareCompiler/HardwareRegister.h"
#include "L1CTHardwareCompiler/HardwareBlock.h"
#include "L1CTHardwareCompiler/VMEbusEmitter.h"
#include "L1CTHardwareCompiler/MemoryEmitter.h"
#include "L1CTHardwareCompiler/NullEmitter.h"
#include "L1CTHardwareCompiler/IPBusEmitter.h"
#include "L1CTHardwareCompiler/VHDLEmitter.h"
#include "L1CTHardwareCompiler/AXIInterfaceEmitter.h"
#include "L1CTHardwareCompiler/CompilerOutputFile.h"
#include "L1CTHardwareCompiler/CompilerException.h"

using namespace L1CTHC;

//------------------------------------------------------------------------------

const int               HardwareCompiler::MAJOR_REVISION_NUMBER = 0;
const int               HardwareCompiler::MINOR_REVISION_NUMBER = 68;
const std::string       HardwareCompiler::AUTHOR                = "R. Spiwoks";
const std::string       HardwareCompiler::DATE                  = "28-JUN-2019";
const std::string       HardwareCompiler::CPP_HEAD_EXTENSION    = ".h";
const std::string       HardwareCompiler::CPP_CODE_EXTENSION    = ".cc";
const std::string       HardwareCompiler::CPP_INCLUDE_POSTFIX   = "_H";
const std::string       HardwareCompiler::VHDL_PKG_EXTENSION    = "_pkg.vhd";
const std::string       HardwareCompiler::VHDL_XMP_EXTENSION    = "_example.vhd";

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

const std::string HardwareCompiler::getVersion() {

    // version number
    std::ostringstream buf;
    buf << MAJOR_REVISION_NUMBER << "." << std::setw(2) << std::setfill('0') << MINOR_REVISION_NUMBER << std::setfill(' ') << "(" << DATE << ")";

    return(buf.str());
}

//------------------------------------------------------------------------------

const std::string HardwareCompiler::getBuildTime() {

    // build time
    std::string date(__DATE__);
    std::ostringstream buf;

    transform(date.begin(),date.end(),date.begin(),toupper);
    buf << (date.substr(4,1)==" "?"0":date.substr(4,1)) << date.substr(5,1) << "-" << date.substr(0,3) << "-" << date.substr(7,4) << " " << __TIME__;

    return(buf.str());
}

//------------------------------------------------------------------------------

const std::string HardwareCompiler::banner() {

    std::ostringstream str;
    str << "L1CTHardwareCompiler " << getVersion() << ", " << getAuthor() << ", make=\"" << getBuildTime() << "\"";

    return(str.str());
}

//------------------------------------------------------------------------------

HardwareCompiler::HardwareCompiler() : m_map(0), m_emt(0), m_mod(0), m_modHH(0), m_modCC(0), m_menCC(0) {

    // name = version, author, and date
    std::ostringstream str;
    str << "L1CTHardwareCompiler version=\"" << getVersion() << "\" author=\"" << getAuthor() << "\" make=\"" << getBuildTime() << "\"";
    m_name = str.str();

    // name
    std::cout << "************************************************************************************************" << std::endl;
    std::cout << str.str() << std::endl;
    std::cout << "************************************************************************************************" << std::endl;
}

//------------------------------------------------------------------------------

HardwareCompiler::~HardwareCompiler() {

    std::string name;

    // delete module
    if(m_mod) delete m_mod;

    // delete emitter
    if(m_emt) delete m_emt;

    // delete address map
    if(m_map) delete m_map;

    // get stop time
    char os[STRING_LENGTH];
    double tnow;
    tnow = HardwareCommon::getTime();
    std::cout << "********************************************************************************" << std::endl;
    std::sprintf(os,"L1CTHardwareCompiler - %12.5f sec\n",tnow-m_time_start); std::cout << os;
    std::cout << "********************************************************************************" << std::endl;
}

//------------------------------------------------------------------------------

int HardwareCompiler::compile(const CompilerConfiguration& cfg) {

    // set configuration
    m_cfg = cfg;

    // time stamp
    std::time_t time_stamp;
    std::string date_stamp;
    time(&time_stamp);
    date_stamp = ctime(&time_stamp);
    date_stamp.erase(date_stamp.find('\n'));

    // print time stamp
    std::printf("Time = \"%s\"\n",date_stamp.c_str());

    // print user
    struct passwd* pwd = getpwuid(getuid());
    std::printf("User = \"%s\"\n",pwd->pw_name);

    // print program call
    std::printf("Call = \"%s\"\n",m_cfg.program_call.c_str());

    if(m_cfg.print_level>=1) m_cfg.print();
    if(m_cfg.check() != 0) {
        CERR("reading configuration","");
        return(FAILURE);
    }

    // get start time
    m_time_start = HardwareCommon::getTime();

    // create new address map
    m_map = new CompilerAddressMap();
    m_map->setCompiler(this);

    // create new emitter
    switch(m_cfg.compile_type) {
    case CompilerConfiguration::VME:
        m_emt = new VMEbusEmitter();
        break;

    case CompilerConfiguration::MEM:
        m_emt = new MemoryEmitter();
        break;

    case CompilerConfiguration::NUL:
        m_emt = new NullEmitter();
        break;

    case CompilerConfiguration::IPB:
        m_emt = new IPBusEmitter();
        break;

    case CompilerConfiguration::VHD:
        m_emt = new VHDLEmitter();
        break;

    case CompilerConfiguration::AXI:
        m_emt = new AXIInterfaceEmitter();
        break;
    }
    m_emt->setCompiler(this);

    // create new module
    m_mod = new HardwareModule();
    m_mod->setCompiler(this);

    // read module from input file (XML)
    try {
        m_mod->read(m_cfg.module_file);
    } catch (const CompilerException& ce) {
        CERR("Exception \"%s\" while trying to read module from file \"%s\"",ce.what(),m_cfg.module_file.c_str());
        return(FAILURE);
    }

    // dump module of necessary
    if(m_cfg.print_level>=3) m_mod->dump();

    // dump address map if necessary
    if(m_cfg.print_level>=2) m_map->dump();

    // dump data map if necessary
    if(m_cfg.print_level>=2) dumpDataTypeMap();
    if(m_cfg.print_level>=2) std::printf("================================================================================\n");

    // write module to output file (XML) if necessary
    if(!m_cfg.output_file.empty()) write();

    // open output files (C++ or VHDL)
    openFiles();

    // emit module to output files (C++ or VHDL)
    try {
        m_emt->compile();
    } catch (const CompilerException& ce) {
        CERR("Exception \"%s\" while emmitting module to output files ",ce.what());
        return(FAILURE);
    }

    // close output files (C++ or VHDL)
    closeFiles();

    return(SUCCESS);
}

//------------------------------------------------------------------------------

void HardwareCompiler::openFiles() {

    // VHDL mode ---------------------------------------------------------------
    if(m_cfg.compile_type == CompilerConfiguration::VHD) {
        try {
            m_pkgVHDL = new CompilerOutputFile(m_cfg.compile_file+VHDL_PKG_EXTENSION);
            if(m_cfg.print_level>=1) COUT("opened output file \"%s\"",m_pkgVHDL->name().c_str());
            m_xmpVHDL = new CompilerOutputFile(m_cfg.compile_file+VHDL_XMP_EXTENSION);
            if(m_cfg.print_level>=1) COUT("opened output file \"%s\"",m_xmpVHDL->name().c_str());
        }
        catch (const CompilerException& ce) {
            CERR("Exception \"%s\" while trying to create module output files",ce.what());
            throw;
        }
        return;
    }

    // define include name
    std::string nameHH, nameCC, type, incl;
    CompilerOutputFile* gHH;
    CompilerOutputFile* gCC;

    // create new module out files and write preamble
    try {
        m_modHH = new CompilerOutputFile(m_cfg.compile_file+CPP_HEAD_EXTENSION);
        if(m_cfg.print_level>=1) COUT("opened output file \"%s\"",m_modHH->name().c_str());
        m_modCC = new CompilerOutputFile(m_cfg.compile_file+CPP_CODE_EXTENSION);
        if(m_cfg.print_level>=1) COUT("opened output file \"%s\"",m_modCC->name().c_str());
    }
    catch (const CompilerException& ce) {
        CERR("Exception \"%s\" while trying to create module output files",ce.what());
        throw;
    }
    incl = m_cfg.namespace_name + "_" + m_cfg.compile_file + CPP_INCLUDE_POSTFIX; transform(incl.begin(),incl.end(),incl.begin(),toupper);
    m_modHH->writePreamble(this,m_mod->desc(),incl);
    m_modCC->writePreamble(this,m_mod->desc());

    // crate new data output files and write preamble
    HardwareDataTypeMapConstIterator idat;
    for(idat=m_dat.begin(); idat!=m_dat.end(); idat++) {
        type = idat->first;
        nameHH = m_cfg.compile_file + "_" + type + CPP_HEAD_EXTENSION;
        nameCC = m_cfg.compile_file + "_" + type + CPP_CODE_EXTENSION;
        try {
            gHH = new CompilerOutputFile(nameHH);
            m_datHH.push_back(gHH);
            if(m_cfg.print_level>=1) COUT("opened output file \"%s\"",gHH->name().c_str());
            gCC = new CompilerOutputFile(nameCC);
            m_datCC.push_back(gCC);
            if(m_cfg.print_level>=1) COUT("opened output file \"%s\"",gCC->name().c_str());
        }
        catch (const CompilerException& ce) {
            CERR("Exception \"%s\" while trying to create data output files",ce.what());
            throw;
        }
        incl= m_cfg.namespace_name + "_" + m_cfg.compile_file + "_" + type + CPP_INCLUDE_POSTFIX; transform(incl.begin(),incl.end(),incl.begin(),toupper);
        gHH->writePreamble(this,m_mod->desc()+" - bit string(s)",incl);
        gCC->writePreamble(this,m_mod->desc()+" - bit string(s)");
    }

    // create new menu output file and write preamble
    try {
        m_menCC = new CompilerOutputFile("menu"+m_cfg.compile_file+CPP_CODE_EXTENSION);
        if(m_cfg.print_level>=1) COUT("opened output file \"%s\"",m_menCC->name().c_str());
    }
    catch (const CompilerException& ce) {
        CERR("Exception \"%s\" while trying to create menu output files",ce.what());
        throw;
    }
    m_menCC->writePreamble(this,m_mod->desc()+" - menu program");
}

//------------------------------------------------------------------------------

void HardwareCompiler::closeFiles() {

    std::string name;

    // VHDL mode ---------------------------------------------------------------
    if(m_cfg.compile_type == CompilerConfiguration::VHD) {
        name = m_pkgVHDL->name();
        delete m_pkgVHDL; m_pkgVHDL = 0;
        if(m_cfg.print_level>=1) COUT("closed output file \"%s\"",name.c_str());
        name = m_xmpVHDL->name();
        delete m_xmpVHDL; m_xmpVHDL = 0;
        if(m_cfg.print_level>=1) COUT("closed output file \"%s\"",name.c_str());
        return;
    }

    // define include name
    HardwareList<CompilerOutputFile>::iterator datHH;
    HardwareList<CompilerOutputFile>::iterator datCC;

    // write postscript and delete module output files
    if(m_modHH) {
        m_modHH->writePostscript();
        name = m_modHH->name();
        delete m_modHH; m_modHH = 0;
        if(m_cfg.print_level>=1) COUT("closed output file \"%s\"",name.c_str());
    }
    if(m_modCC) {
        m_modCC->writePostscript();
        name = m_modCC->name();
        delete m_modCC; m_modCC = 0;
        if(m_cfg.print_level>=1) COUT("closed output file \"%s\"",name.c_str());
    }

    // write postscript and delete data output files
    for(datHH=m_datHH.begin(), datCC=m_datCC.begin(); datHH!=m_datHH.end(); ) {

        // write postscript
        (*datHH)->writePostscript();
        (*datCC)->writePostscript();

        // delete
        name = (*datHH)->name();
        delete (*datHH);
        m_datHH.erase(datHH++);
        if(m_cfg.print_level>=1) COUT("closed output file \"%s\"",name.c_str());
        name = (*datCC)->name();
        delete (*datCC);
        m_datCC.erase(datCC++);
        if(m_cfg.print_level>=1) COUT("closed output file \"%s\"",name.c_str());
    }

    // write postscript and delete menu output file
    if(m_menCC) {
        m_menCC->writePostscript();
        name = m_menCC->name();
        delete m_menCC; m_menCC = 0;
        if(m_cfg.print_level>=1) COUT("closed output file \"%s\"",name.c_str());
    }
}

//------------------------------------------------------------------------------

void HardwareCompiler::dumpDataTypeMap() {

    HardwareDataTypeMapConstIterator idat;
    HardwareList<const HardwareDataType>* datList;
    HardwareList<const HardwareDataType>::const_iterator dat;

    std::printf("================================================================================\n");
    std::printf("DATA TYPE MAP:\n");
    std::printf("--------------------------------------------------------------------------------\n");
    std::printf("DATA TYPE - NAME\n");
    std::printf("--------------------------------------------------------------------------------\n");
    for(idat=m_dat.begin(); idat!=m_dat.end(); idat++) {
        datList = idat->second;
        if(idat!=m_dat.begin()) {
            std::printf("--------------------------------------------------------------------------------\n");
        }
        for(dat=datList->begin(); dat!=datList->end(); dat++) {
            std::printf("%9s - %s\n",idat->first.c_str(),(*dat)->nameName().c_str());
        }
    }
}

//------------------------------------------------------------------------------

void HardwareCompiler::write() const {

    std::ofstream fout(m_cfg.output_file.c_str()); if(!fout) {
        CERR("opening XML write file \"%s\"",m_cfg.output_file.c_str());
        throw CompilerException("CANNOT OPEN FILE");
    }

    writeDocType(fout);

    try {
        m_mod->write(fout);
    }
    catch (const CompilerException& ce) {
        CERR("Exception \"%s\" while trying to write module to file \"%s\"",ce.what(),m_cfg.output_file.c_str());
        throw;
    }
}

//------------------------------------------------------------------------------

void HardwareCompiler::writeDocType(std::ostream& ostr) const {

    ostr << "<!DOCTYPE module SYSTEM \"L1CTHardwareModule.dtd\">" << std::endl;
    ostr << std::endl;
}

//------------------------------------------------------------------------------

void HardwareCompiler::getModuleFiles(CompilerOutputFile** fHH, CompilerOutputFile** fCC) {

    bool errFlag(false);

    if(m_modHH == 0) {
        CERR("HardwareModule not attached to any module header file","");
        errFlag = true;
    }
    if(m_modCC == 0) {
        CERR("HardwareModule not attached to any module code file","");
        errFlag = true;
    }
    if(errFlag) throw CompilerException("NO MODULE FILE");

    // return output files
    *fHH = m_modHH;
    *fCC = m_modCC;
}

//------------------------------------------------------------------------------

void HardwareCompiler::getModuleHeaderFileName(std::string& hdrName) const {

    hdrName = m_modHH->name();
}

//------------------------------------------------------------------------------

void HardwareCompiler::addToDataTypeMap(const HardwareDataType* dat) {

    // return if data emtpy
    if(!dat) return;

    std::string datType = dat->fileName();
    std::string datName = dat->nameName();

    // return if data type empty
    if(datType.empty()) return;

    HardwareDataTypeMapConstIterator idat;
    HardwareList<const HardwareDataType>* datList;
    HardwareList<const HardwareDataType>* typList(0);
    HardwareList<const HardwareDataType>::const_iterator jdat;
    const HardwareDataType* rdat;

    // check if data type already exists, even with different fileName; all data types are declared in the same namespace
    for(idat=m_dat.begin(); idat!=m_dat.end(); idat++) {
        if(idat->first == datType) typList = idat->second;

        // loop over data list
        datList = idat->second;
        for(jdat=datList->begin(); jdat!=datList->end(); jdat++) {
            if((*jdat)->nameName() == datName) {
                CERR("HardwareDataType \"%s\" of type \"%s\" already exists",dat->nameName().c_str(),datType.c_str());
                throw CompilerException("DATA TYPE ALREADY EXISTS");
            }
        }
    }

    // fileName already exists
    if(typList) {
        if(typList->findItem(datName,&rdat)) {
            CERR("HardwareDataType \"%s\" of type \"%s\" already exists",datName.c_str(),datType.c_str());
            throw CompilerException("DATA TYPE ALREADY EXISTS");
        }
        datList->push_back(dat);
    }

    // fileName does not exists; create a new list
    else {
        typList = new HardwareList<const HardwareDataType>;
        typList->push_back(dat);
        m_dat.insert(std::make_pair(datType,typList));
    }
}

//------------------------------------------------------------------------------

bool HardwareCompiler::getFromDataTypeMap(const std::string& type, const std::string& name, const HardwareDataType** rdat) {

    // return if name emtpy
    if(name.empty()) return(false);

    HardwareDataTypeMapConstIterator idat;
    HardwareList<const HardwareDataType>* datList;
    HardwareList<const HardwareDataType>::const_iterator jdat;

    // loop over map
    for(idat=m_dat.begin(); idat!=m_dat.end(); idat++) {
        if(idat->first == type) {

            // loop over data list
            datList = idat->second;
            for(jdat=datList->begin(); jdat!=datList->end(); jdat++) {
                if((*jdat)->nameName() == name) {
                    *rdat = (*jdat);
                    return(true);
                }
            }
        }
    }

    *rdat = 0;
    return(false);
}

//------------------------------------------------------------------------------

void HardwareCompiler::getDataTypeFiles(const HardwareDataType* dat, CompilerOutputFile** fHH, CompilerOutputFile** fCC) {

    CompilerOutputFile* gHH(0);
    CompilerOutputFile* gCC(0);
    std::string nameHH, nameCC;
    bool errFlag(false);

    nameHH = m_cfg.compile_file + "_" + dat->fileName() + CPP_HEAD_EXTENSION;
    nameCC = m_cfg.compile_file + "_" + dat->fileName() + CPP_CODE_EXTENSION;

    // look for files in list
    if(!m_datHH.findItem(nameHH,&gHH)) {
        CERR("HardwareDataType \"%s\" not attached to any data header file",dat->fileName().c_str());
        errFlag = true;
    }
    if(!m_datCC.findItem(nameCC,&gCC)) {
        CERR("HardwareDataType \"%s\" not attached to any data code file",dat->fileName().c_str());
        errFlag = true;
    }
    if(errFlag) throw CompilerException("NO DATA FILE");

    // return result
    *fHH = gHH;
    *fCC = gCC;
}

//------------------------------------------------------------------------------

void HardwareCompiler::getDataTypeHeaderFileNames(std::vector<std::string>& hdrName) const {

    HardwareList<CompilerOutputFile>::const_iterator datHH;

    for(datHH=m_datHH.begin(); datHH!=m_datHH.end(); datHH++) hdrName.push_back((*datHH)->name());
}

//------------------------------------------------------------------------------

void HardwareCompiler::getMenuFile(CompilerOutputFile** fCC) {

    if(m_menCC == 0) {
        CERR("HardwareModule not attached to any menu code file","");
        throw CompilerException("NO MENU FILE");
    }

    // return output file
    *fCC = m_menCC;
}

//------------------------------------------------------------------------------

void HardwareCompiler::getPackageVHDLFile(CompilerOutputFile** fVHD) {

    if(m_pkgVHDL == 0) {
        CERR("HardwareModule not attached to any package VHDL file","");
        throw CompilerException("NO MODULE FILE");
    }

    // return output files
    *fVHD = m_pkgVHDL;
}

//------------------------------------------------------------------------------

void HardwareCompiler::getExampleVHDLFile(CompilerOutputFile** fVHD) {

    if(m_xmpVHDL == 0) {
        CERR("HardwareModule not attached to any example VHDL file","");
        throw CompilerException("NO MODULE FILE");
    }

    // return output files
    *fVHD = m_xmpVHDL;
}

//------------------------------------------------------------------------------

void HardwareCompiler::addToAddressMap(const HardwareElement* elm) {

    if(m_map) {
        m_map->addElement(elm);
    }
    else {
        CERR("HardwareModule does not have address map","");
        throw CompilerException("NO ADDRESS MAP");
    }
}
