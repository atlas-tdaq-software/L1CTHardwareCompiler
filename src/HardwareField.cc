//******************************************************************************
// file: HardwareField.cc
// desc: library for L1CT HardwareCompiler
// auth: 19-MAR-2013 R. Spiwoks, re-design from RCDModuleDesign
//******************************************************************************

#include <sstream>
#include <algorithm>

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/parsers/XercesDOMParser.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/sax/HandlerBase.hpp>

#include "RCDUtilities/RCDUtilities.h"
#include "L1CTHardwareCompiler/HardwareCommon.h"
#include "L1CTHardwareCompiler/HardwareField.h"
#include "L1CTHardwareCompiler/HardwareValue.h"
#include "L1CTHardwareCompiler/HardwareBitString.h"
#include "L1CTHardwareCompiler/HardwareElement.h"
#include "L1CTHardwareCompiler/HardwareBlock.h"
#include "L1CTHardwareCompiler/HardwareCompiler.h"
#include "L1CTHardwareCompiler/CompilerException.h"
#include "L1CTHardwareCompiler/CompilerStringList.h"

using namespace L1CTHC;
using namespace XERCES_CPP_NAMESPACE;

//------------------------------------------------------------------------------

const std::string HardwareField::XML_FIELD              = "field";
const std::string HardwareField::XML_NO                 = "NO";
const std::string HardwareField::XML_YES                = "YES";
const std::string HardwareField::CPP_MASK_PREFIX        = "MASK_";

//------------------------------------------------------------------------------

HardwareField::HardwareField(const std::string& name) : HardwareObject(name), m_mask(DATA_TYPE_SIZE[D32]), m_form(STRING) {

    // set mask
    m_mask.set();
}

//------------------------------------------------------------------------------

HardwareField::~HardwareField() {

    // nothing to be done
}

//------------------------------------------------------------------------------

void HardwareField::dump(std::ostream& ostr, const unsigned int indt) const {

    char os[STRING_LENGTH];
    std::ostringstream buf;
    HardwareList<HardwareValue>::const_iterator ival;

    // dump field
    if(m_multiple>1) buf << ", multiple = " << m_multiple << ", offset = " << m_offset;
    std::sprintf(os,"%sHardwareField \"%s\", mask = %s, form = \"%s\", modf = \"%s\"%s:\n",HardwareCommon::spaceString(indt).c_str(),m_name.c_str(),m_mask.string().c_str(),DATA_FORM_NAME[m_form].c_str(),DATA_MODF_NAME[m_modf].c_str(),buf.str().c_str()); ostr << os;

    // dump all values
    for(ival=m_values.begin(); ival!=m_values.end(); ival++) {
        (*ival)->dump(ostr,indt+INDENT);
    }
}

//------------------------------------------------------------------------------

void HardwareField::read(const DOMNode* fld) {

    DOMNode* attr;
    HardwareField* rfld;
    std::string fldName, mskName, mdfName, frmName, mulName, offName;
    unsigned int iatt, smsk;
    bool flag;
    const CompilerConfiguration* cfg = getCompiler()->getConfig();

    // read field attributes
    attr = fld->getAttributes()->getNamedItem(TRX("name"));
    if(attr) fldName = TRX(attr->getNodeValue());
    attr = fld->getAttributes()->getNamedItem(TRX("mask"));
    if(attr) mskName = TRX(attr->getNodeValue());
    attr = fld->getAttributes()->getNamedItem(TRX("form"));
    if(attr) frmName = TRX(attr->getNodeValue());
    attr = fld->getAttributes()->getNamedItem(TRX("modf"));
    if(attr) mdfName = TRX(attr->getNodeValue());
    attr = fld->getAttributes()->getNamedItem(TRX("multiple"));
    if(attr) mulName = TRX(attr->getNodeValue());
    attr = fld->getAttributes()->getNamedItem(TRX("offset"));
    if(attr) offName = TRX(attr->getNodeValue());

    // print field attributes
    if(cfg->print_level>=4) {
        CompilerStringList att; att.delimiter(','); att.space(1);
        if(!frmName.empty()) att.add("form=\""+frmName+"\"");
        if(!mdfName.empty()) att.add("modf=\""+mdfName+"\"");
        if(!mulName.empty()||!offName.empty()) att.add("multiple=\""+mulName+"\", offset=\""+offName+"\"");
        COUT("found field \"%s\" with mask=\"%s\"%s%s",fldName.c_str(),mskName.c_str(),!(att.str().empty())?", ":"",att.str().c_str());
    }

    // check field attribute NAME
    if(fldName.empty()) {
        CERR("field \"%s\" has no name",fldName.c_str());
        throw CompilerException("NO NAME");
    }
    if(getBitString()->findField(fldName,&rfld)) {
        CERR("field name \"%s\" already exists for bitstring \"%s\"",fldName.c_str(),getBitString()->name().c_str());
        throw CompilerException("NAME EXISTS");
    }

    // check field attribute MASK
    if(mskName.empty()) {
        m_mask = getBitString()->mask();
    }
    else {
        if(m_mask.read(mskName)) {
            CERR("Error while reading field \"%s\", mask \"%s\"",fldName.c_str(),mskName.c_str());
            throw CompilerException("INVALID ATTRIBUTE VALUE");
        }
    }
    if(cfg->print_level>=10) COUT("field \"%s\" has mask \"%s\"",fldName.c_str(),m_mask.string().c_str());
    if(m_mask.null()) {
        CERR("field \"%s\" has empty mask \"%s\"",fldName.c_str(),m_mask.string().c_str());
        throw CompilerException("INVALID ATTRIBUTE VALUE");
    }
    if(cfg->print_level>=10) COUT("bitstring \"%s\", mask = \"%s\", field \"%s\", mask = \"%s\"",getBitString()->name().c_str(),getBitString()->mask().string().c_str(),fldName.c_str(),m_mask.string().c_str());
    if(!getBitString()->fitMaskMultiple(m_mask,1,0)) {
        CERR("mask %s of field \"%s\" is incompatible with\n>> mask %s of comprising bitstring \"%s\"",m_mask.string().c_str(),fldName.c_str(),getBitString()->mask().string().c_str(),getBitString()->name().c_str());
        throw CompilerException("INVALID ATTRIBUTE VALUE");
    }

    // check field attribute FORM
    if(frmName.empty()) {
        m_form = STRING;
    }
    else {
        for(flag=false, iatt=0; iatt<DATA_FORM_NUMBER; iatt++) {
            if((frmName == DATA_FORM_NAME[iatt]) || (frmName == DATA_FORM_NAME[iatt].substr(0,1))) {
                m_form = static_cast<DATA_FORM>(iatt);
                flag = true;
                break;
            }
        }
        if(!flag) {
            CERR("field \"%s\" has unknown form \"%s\"",fldName.c_str(),frmName.c_str());
            throw CompilerException("UNKNOWN FORM");
        }
    }

    // check field attribute MODF
    if(mdfName.empty()) {
        m_modf = getBitString()->modf();
    }
    else {
        for(flag=false, iatt=0; iatt<DATA_MODF_NUMBER; iatt++) {
            if(mdfName == DATA_MODF_NAME[iatt]) {
                m_modf = static_cast<DATA_MODF>(iatt);
                flag = true;
                break;
            }
        }
        if(flag) {
            if(!modfIsCompatible(getBitString()->modf())) {
                CERR("field \"%s\" modf (\"%s\") incompatible with modf of bitstring \"%s\" (\"%s\")",fldName.c_str(),DATA_MODF_NAME[m_modf].c_str(),getBitString()->name().c_str(),DATA_MODF_NAME[getBitString()->modf()].c_str());
                throw CompilerException("INVALID ATTRIBUTE VALUE");
            }
        }
        else {
            CERR("field \"%s\" has unknown modf \"%s\"",fldName.c_str(),mdfName.c_str());
            throw CompilerException("UNKNOWN MODIFIER");
        }
    }

    // check field attribute MULTIPLICITY and OFFSET
    if(!mulName.empty()) {
        try {
            m_multiple = HardwareCommon::readNumber(mulName);
        }
        catch(const CompilerException& ce) {
            CERR("Exception \"%s\" while reading field \"%s\", multiple \"%s\"",ce.what(),fldName.c_str(),mulName.c_str());
            throw;
        }
        if(m_multiple == 0) {
            CERR("field \"%s\" has empty multiple (%d)",fldName.c_str(),m_multiple);
            throw CompilerException("INVALID ATTRIBUTE VALUE");
        }
        if(m_multiple == 1) {
            if(!offName.empty()) {
                if(cfg->strict_check) {
                    CERR("field \"%s\" has offset \"%s\" but multiple 1",fldName.c_str(),offName.c_str());
                    throw CompilerException("INVALID ATTRIBUTE VALUE");
                }
                else {
                    if(cfg->print_level>=1) COUT("field \"%s\" has offset \"%s\" but multiple 1 => IGNORE OFFSET",fldName.c_str(),offName.c_str());
                }
            }
        }
        else {
            if(!offName.empty()) {
                try {
                   m_offset = HardwareCommon::readNumber(offName);
                }
                catch(const CompilerException& ce) {
                    CERR("Exception \"%s\" while reading field \"%s\", offset \"%s\"",ce.what(),fldName.c_str(),offName.c_str());
                    throw;
                }
                if(m_offset == 0) {
                    CERR("field \"%s\" has empty offset (%d)",fldName.c_str(),m_offset);
                    throw CompilerException("INVALID ATTRIBUTE VALUE");
                }
                else if(m_offset < HardwareCommon::getMaskWidth(m_mask)) {
                    CERR("field \"%s\" has offset (%d) incompatible with field mask %s",fldName.c_str(),m_offset,m_mask.string().c_str());
                    throw CompilerException("INVALID ATTRIBUTE VALUE");
                }
            }
            else {
                if(cfg->strict_check) {
                    CERR("field \"%s\" has multiple \"%s\" but no offset",fldName.c_str(),mulName.c_str());
                    throw CompilerException("INVALID ATTRIBUTE VALUE");
                }
                else {
                    m_offset = HardwareCommon::getMaskWidth(m_mask);
                    if(cfg->print_level>=1) COUT("field \"%s\" has multiple \"%s\" but no offset => SET OFFSET TO %d",fldName.c_str(),mulName.c_str(),m_offset);
                }
            }
            if((HardwareCommon::getMaskWidth(m_mask)+(m_multiple-1)*m_offset) > (HardwareCommon::getMaskWidth(getBitString()->mask()))) {
                CERR("mask %s of field \"%s\" with multiple %d and offset %d (total size = %d) is incompatible with maximum size (%d) of comprising bitstring \"%s\")",m_mask.string().c_str(),fldName.c_str(),m_multiple,m_offset,HardwareCommon::getMaskWidth(m_mask)+(m_multiple-1)*m_offset,HardwareCommon::getMaskWidth(getBitString()->mask()),getBitString()->name().c_str());
                throw CompilerException("INVALID ATTRIBUTE VALUE");
            }
            if(!getBitString()->fitMaskMultiple(m_mask,m_multiple,m_offset)) {
                CERR("mask %s of field \"%s\" with multiple %d and offset %d is incompatible with\n>> mask %s of comprising bitstring \"%s\"",m_mask.string().c_str(),fldName.c_str(),m_multiple,m_offset,getBitString()->mask().string().c_str(),getBitString()->name().c_str());
                throw CompilerException("INVALID ATTRIBUTE VALUE");
            }
        }
    }
    else {
        if(!offName.empty()) {
            if(cfg->strict_check) {
                CERR("field \"%s\" has offset \"%s\" but no multiple",fldName.c_str(),offName.c_str());
                throw CompilerException("INVALID ATTRIBUTE VALUE");
            }
            else {
                if(cfg->print_level>=1) COUT("field \"%s\" has offset \"%s\" but no multiple => IGNORE OFFSET",fldName.c_str(),offName.c_str());
            }
        }
    }
    if(getBitString()->findMaskOverlap(m_mask,m_multiple,m_offset,&rfld)) {
        if(cfg->strict_check) {
            CERR("mask %s of field \"%s\" with multiple %d and offset %d (total mask = %s) overlaps with\n>> mask %s of field \"%s\" with multiple %d and offset %d (total mask = %s)",m_mask.string().c_str(),fldName.c_str(),m_multiple,m_offset,HardwareCommon::getMaskMultiple(m_mask,m_multiple,m_offset).string().c_str(),rfld->mask().string().c_str(),rfld->name().c_str(),rfld->multiple(),rfld->offset(),HardwareCommon::getMaskMultiple(rfld->mask(),rfld->multiple(),rfld->offset()).string().c_str());
            throw CompilerException("MASK ALREADY USED");
        }
        else {
            if(cfg->print_level>=1) COUT("mask %s of field \"%s\" with multiple %d and offset %d (total mask = %s) overlaps with\n>> mask %s of field \"%s\" with multiple %d and offset %d (total mask = %s) => CONTINUE USING BOTH",m_mask.string().c_str(),fldName.c_str(),m_multiple,m_offset,HardwareCommon::getMaskMultiple(m_mask,m_multiple,m_offset).string().c_str(),rfld->mask().string().c_str(),rfld->name().c_str(),rfld->multiple(),rfld->offset(),HardwareCommon::getMaskMultiple(rfld->mask(),rfld->multiple(),rfld->offset()).string().c_str());
        }
    }

    // set field attributes
    m_name = fldName;

    // read values
    readValues(fld);

    // check consistency
    if((m_form == NUMBER) && (!m_values.empty())) {
        if(cfg->strict_check) {
            CERR("field \"%s\" has form \"%s\" but has %d value%s",m_name.c_str(),DATA_FORM_NAME[m_form].c_str(),m_values.size(),m_values.size()>1?"s":"");
            throw CompilerException("INCONSITENT FIELD");
        }
        else {
            if(cfg->print_level>=1) COUT("field \"%s\" has form \"%s\" but has %d value%s => IGNORE VALUES",m_name.c_str(),DATA_FORM_NAME[m_form].c_str(),m_values.size(),m_values.size()>1?"s":"");
        }
    }
    else if((m_form == STRING) && m_values.empty()) {
        if(cfg->strict_check) {
            CERR("field \"%s\" has form \"%s\" but has no values",m_name.c_str(),DATA_FORM_NAME[m_form].c_str());
            throw CompilerException("INCONSITENT FIELD");
        }
        else {
            if(cfg->print_level>=1) COUT("field \"%s\" has form \"%s\" but has no values => CHANGE FORM TO \"NUMBER\"",m_name.c_str(),DATA_FORM_NAME[m_form].c_str());
            m_form = NUMBER;
        }
    }
    else if((m_form == ASCII) && (!m_values.empty())) {
        if(cfg->strict_check) {
            CERR("field \"%s\" has form \"%s\" but has %d value%s",m_name.c_str(),DATA_FORM_NAME[m_form].c_str(),m_values.size(),m_values.size()>1?"s":"");
            throw CompilerException("INCONSITENT FIELD");
        }
        else {
            if(cfg->print_level>=1) COUT("field \"%s\" has form \"%s\" but has %d value%s => IGNORE VALUES",m_name.c_str(),DATA_FORM_NAME[m_form].c_str(),m_values.size(),m_values.size()>1?"s":"");
        }
    }
    else if((m_form == ASCII) && ((smsk = HardwareCommon::getMaskWidth(m_mask))%ASCII_WIDTH != 0)) {
        if(cfg->strict_check) {
            CERR("field \"%s\" has form \"%s\" but has size of %d bit%s",m_name.c_str(),DATA_FORM_NAME[m_form].c_str(),smsk,smsk>1?"s":"");
            throw CompilerException("INCONSITENT FIELD");
        }
        else {
            if(cfg->print_level>=1) COUT("field \"%s\" has form \"%s\" but has size of %d bit%s => TRUNCATE VALUE",m_name.c_str(),DATA_FORM_NAME[m_form].c_str(),smsk,smsk>1?"s":"");
        }
    }

    // sort values if necessary
    if(cfg->value_sort_type == CompilerConfiguration::BYNAME) {
        m_values.sort(compareHardwareValueByName);
    }
    else if(cfg->value_sort_type == CompilerConfiguration::BYDATA) {
        m_values.sort(compareHardwareValueByData);
    }
}

//------------------------------------------------------------------------------

void HardwareField::readValues(const DOMNode* fld) {

    DOMNodeList* valList;
    DOMNode* val;
    std::string valName;
    int ival;
    const CompilerConfiguration* cfg = getCompiler()->getConfig();

    // get all values
    valList = fld->getChildNodes();
    if(cfg->print_level>=4) {
        std::string fldName = (fld->getNodeType() == DOMNode::ELEMENT_NODE) ? m_name : TRX(fld->getNodeName());
        COUT("field \"%s\" has %d child%s",fldName.c_str(),valList->getLength(),valList->getLength()>1?"ren":"");
    }

    // traverse all values
    for(ival=0; ival<(int)valList->getLength(); ival++) {
        val = valList->item(ival);

        // entity reference node type
        if(val->getNodeType() == DOMNode::ENTITY_REFERENCE_NODE) {
            if(cfg->print_level>=4) COUT("found value entity reference \"%s\"",TRX(val->getNodeName()));
            readValues(val);
            continue;
        }
        // element node type
        else if(val->getNodeType() != DOMNode::ELEMENT_NODE) continue;

        // named value
        if(HardwareValue::isValue(val)) {
             valName = TRX(val->getAttributes()->getNamedItem(TRX("name"))->getNodeValue());
             HardwareValue* aval;
             if(findValue(valName,(HardwareValue**)&aval)) {
                CERR("value name \"%s\" already exists for field \"%s\"",valName.c_str(),m_name.c_str());
                throw CompilerException("NAME EXISTS");
             }
             aval = new HardwareValue(valName);
             addValue(aval);
             aval->setField(this);
             aval->read(val);
        }

        // unknown type
        else {
            CERR("child of field \"%s\" has unknown type \"%s\"",m_name.c_str(),TRX(val->getNodeName()));
            throw CompilerException("UNKNOWN TYPE");
        }
    }
}

//------------------------------------------------------------------------------

void HardwareField::write(std::ostream& ostr) const {

    char os[STRING_LENGTH];
    std::ostringstream buf;
    HardwareList<HardwareValue>::const_iterator ival;
    std::ostringstream indent;
    unsigned int rgt;

    // indentation
    if(m_bitstring->decl() == HardwareDataType::EXTERNAL) {
        indent << "    ";
    }
    else {
        indent << ((m_bitstring->getElement()->getBlock()->name().empty())? "" : "  ") << "      ";
    }
    rgt = XML_TAB_LENGTH - XML_FIELD.size() - indent.str().size();

    // optional attributes
    if((m_modf != RW) && (m_modf != m_bitstring->modf())) buf << " modf=\"" << DATA_MODF_NAME[m_modf] << "\"";
    if(m_multiple > 1) buf << " multiple=\"" << m_multiple << "\" offset=\"" << m_offset << "\"";

    // if field is empty
    if(m_values.empty()) {

        // write field single tag
        std::sprintf(os,"%s<%s name=%s mask=\"%s\" form=\"%s\"%s/>\n",indent.str().c_str(),XML_FIELD.c_str(),HardwareCommon::rightFillString("\""+m_name+"\"",rgt).c_str(),m_mask.string().c_str(),DATA_FORM_NAME[m_form].c_str(),buf.str().c_str()); ostr << os;
    }

    // if field is not empty
    else {

        // start field tag
        std::sprintf(os,"%s<%s name=%s mask=\"%s\" form=\"%s\"%s>\n",indent.str().c_str(),XML_FIELD.c_str(),HardwareCommon::rightFillString("\""+m_name+"\"",rgt).c_str(),m_mask.string().c_str(),DATA_FORM_NAME[m_form].c_str(),buf.str().c_str()); ostr << os;

        // write all values
        for(ival=m_values.begin(); ival!=m_values.end(); ival++) {
            (*ival)->write(ostr);
        }

        // write field end tag
        std::sprintf(os,"%s</%s>\n",indent.str().c_str(),XML_FIELD.c_str()); ostr << os;
    }
}

//------------------------------------------------------------------------------

std::string HardwareField::maskName() const {

    std::string n(CPP_MASK_PREFIX + nameName());
    transform(n.begin(),n.end(),n.begin(),toupper);
    return(n);
}

//------------------------------------------------------------------------------

void HardwareField::addValue(HardwareValue* val) {

    m_values.addItem(val);
}

//------------------------------------------------------------------------------

bool HardwareField::findValue(const std::string& name, HardwareValue** rval) const {

    return(m_values.findItem(name,rval));
}

//------------------------------------------------------------------------------

bool HardwareField::isEmpty() const {

    if(m_values.empty()) return(true);

    if(m_values.size() == 1) {
        if(m_values.front()->name() == "") return(true);
    }

    return(false);
}

//------------------------------------------------------------------------------

bool HardwareField::isField(const XERCES_CPP_NAMESPACE::DOMNode* fld) {

    if(fld && (TRX(fld->getNodeName()) == XML_FIELD)) return(true);

    return(false);
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

bool L1CTHC::compareHardwareFieldByName(HardwareField* flda, HardwareField* fldb) {

    return(flda->name().compare(fldb->name()) < 0);
}

//------------------------------------------------------------------------------

bool L1CTHC::compareHardwareFieldByAddr(HardwareField* flda, HardwareField* fldb) {

    return(flda->addr() < fldb->addr());
}
