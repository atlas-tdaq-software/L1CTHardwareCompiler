//******************************************************************************
// file: HardwareMemory.cc
// desc: library for L1CT HardwareCompiler
// auth: 01-MAR-2013 R. Spiwoks, re-design from RCDModuleDesign
//******************************************************************************

#include <sstream>
#include <algorithm>

#include "RCDUtilities/RCDUtilities.h"
#include "L1CTHardwareCompiler/HardwareWord.h"
#include "L1CTHardwareCompiler/HardwareMemory.h"
#include "L1CTHardwareCompiler/HardwareBitString.h"
#include "L1CTHardwareCompiler/HardwareCompiler.h"
#include "L1CTHardwareCompiler/CompilerException.h"
#include "L1CTHardwareCompiler/CompilerStringList.h"

using namespace L1CTHC;
using namespace XERCES_CPP_NAMESPACE;

//------------------------------------------------------------------------------

const std::string HardwareMemory::ACCESS_TYPE_NAME[ACCESS_TYPE_NUMBER] = {
    "RAM", "FIFO"
};

const std::string HardwareMemory::INTERFACE_TYPE_NAME[INTERFACE_TYPE_NUMBER] = {
    "VECTOR", "BLOCK", "BOTH"
};

const std::string HardwareMemory::XML_ACCESS_NAME[ACCESS_TYPE_NUMBER] = {
    "memory", "fifo"
};

const std::string HardwareMemory::CPP_SIZE_PREFIX     = "SIZE_";
const std::string HardwareMemory::CPP_INDX_POSTFIX    = "_INDEX_NUMBER";

//------------------------------------------------------------------------------

HardwareMemory::HardwareMemory(const std::string& name) : HardwareElement(name), m_size(0), m_access(RAM), m_interface(BOTH) {

    // nothing to be done
}

//------------------------------------------------------------------------------

HardwareMemory::~HardwareMemory() {

    // nothing to be done
}

//------------------------------------------------------------------------------

void HardwareMemory::dump(std::ostream& ostr, const unsigned int indt) const {

    std::ostringstream buf;

    // dump memory
    buf << HardwareCommon::spaceString(indt) << "HardwareMemory (" << accessType() << ") \"" << m_name << "\", addr = " << hex(8) << m_addr << ", size = " << m_size << dec << ", type = \"" << typeName() << "\", modf = \"" << DATA_MODF_NAME[m_modf] << "\", interface = \"" << INTERFACE_TYPE_NAME[m_interface] << "\"";
    if(m_multiple>1) buf << ", multiple = " << m_multiple << ", offset = " << m_offset;
    ostr << buf.str() << ":" << std::endl;

    // dump data type (reference or internal)
    if(m_data) {
        if(m_data->isReference()) {
            ostr << HardwareCommon::spaceString(indt+INDENT) << "HardwareBitString \"" << m_data->getReference()->nameName() << "\" (Reference)" << std::endl;
        }
        else {
            m_data->dump(ostr,indt+INDENT);
        }
    }
    if(m_datb) {
        if(m_datb->isReference()) {
            ostr << HardwareCommon::spaceString(indt+INDENT) << "HardwareBitString \"" << m_datb->getReference()->nameName() << "\" (Reference)" << std::endl;
        }
        else {
            m_datb->dump(ostr,indt+INDENT);
        }
    }
}

//------------------------------------------------------------------------------

void HardwareMemory::read(const DOMNode* mem) {

    DOMNode* attr;
    std::string accType, memName, addName, sizName, typName, mskName, mdfName, itfName, mulName, offName, bitName;
    unsigned int addr, iatt;
    bool flag;
    const HardwareModule* mod = getCompiler()->getModule();
    const CompilerConfiguration* cfg = getCompiler()->getConfig();

    // read memory attributes
    accType = TRX(mem->getNodeName());
    attr = mem->getAttributes()->getNamedItem(TRX("name"));
    if(attr) memName = TRX(attr->getNodeValue());
    attr = mem->getAttributes()->getNamedItem(TRX("addr"));
    if(attr) addName = TRX(attr->getNodeValue());
    attr = mem->getAttributes()->getNamedItem(TRX("size"));
    if(attr) sizName = TRX(attr->getNodeValue());
    attr = mem->getAttributes()->getNamedItem(TRX("type"));
    if(attr) typName = TRX(attr->getNodeValue());
    attr = mem->getAttributes()->getNamedItem(TRX("mask"));
    if(attr) mskName = TRX(attr->getNodeValue());
    attr = mem->getAttributes()->getNamedItem(TRX("modf"));
    if(attr) mdfName = TRX(attr->getNodeValue());
    attr = mem->getAttributes()->getNamedItem(TRX("interface"));
    if(attr) itfName = TRX(attr->getNodeValue());
    attr = mem->getAttributes()->getNamedItem(TRX("multiple"));
    if(attr) mulName = TRX(attr->getNodeValue());
    attr = mem->getAttributes()->getNamedItem(TRX("offset"));
    if(attr) offName = TRX(attr->getNodeValue());
    attr = mem->getAttributes()->getNamedItem(TRX("bitstring"));
    if(attr) bitName = TRX(attr->getNodeValue());

    // print memory attributes
    if(cfg->print_level>=4) {
        CompilerStringList att; att.delimiter(','); att.space(1);
        if(!typName.empty()) att.add("type=\""+typName+"\"");
        if(!mskName.empty()) att.add("mask=\""+mskName+"\"");
        if(!mdfName.empty()) att.add("modf=\""+mdfName+"\"");
        if(!itfName.empty()) att.add("interface=\""+itfName+"\"");
        if(!mulName.empty()||!offName.empty()) att.add("multiple=\""+mulName+"\", offset=\""+offName+"\"");
        if(!bitName.empty()) att.add("bitstring=\""+bitName+"\"");
        COUT("found %s \"%s\" with addr=\"%s\", size=\"%s\"%s%s",accType.c_str(),memName.c_str(),addName.c_str(),sizName.c_str(),!(att.str().empty())?", ":"",att.str().c_str());
    }

    // check memory attribute ACCESS
    for(flag=false, iatt=0; iatt<ACCESS_TYPE_NUMBER; iatt++) {
        if(accType == XML_ACCESS_NAME[iatt]) {
            m_access = static_cast<ACCESS_TYPE>(iatt);
            flag = true;
            break;
        }
    }
    if(!flag) {
        CERR("memory/fifo \"%s\" has unknown access \"%s\"",memName.c_str(),accType.c_str());
        throw CompilerException("UNKNOWN TYPE");
    }

    // check memory attribute NAME
    if(memName == "") {
        CERR("%s \"%s\" has no name",accessName().c_str(),memName.c_str());
        throw CompilerException("NO NAME");
    }

    // check memory attribute ADDR
    try {
        m_addr = HardwareCommon::readNumber(addName);
    }
    catch(const CompilerException& ce) {
        CERR("Exception \"%s\" while reading %s \"%s\", address \"%s\"",ce.what(),accessName().c_str(),memName.c_str(),addName.c_str());
        throw;
    }
    addr = m_block->addr() + m_addr;
    if(addr >= mod->size()) {
        CERR("%s \"%s\" has address (0x%08x) outside of module \"%s\" size (0x%08x)",accessName().c_str(),memName.c_str(),addr,mod->name().c_str(),mod->size());
        throw CompilerException("INVALID ATTRIBUTE VALUE");
    }

    // check memory attribute SIZE
    if(!sizName.empty()) {
        try {
            m_size = HardwareCommon::readNumber(sizName);
        }
        catch(const CompilerException& ce) {
            CERR("Exception \"%s\" while reading %s \"%s\", size \"%s\"",ce.what(),accessName().c_str(),memName.c_str(),sizName.c_str());
            throw;
        }
    }
    if((addr+m_size) > mod->size()) {
        CERR("%s \"%s\" has address (0x%08x) and size (0x%08x) greater than module \"%s\" size (0x%08x)",accessName().c_str(),memName.c_str(),addr,m_size,mod->name().c_str(),mod->size());
        throw CompilerException("INVALID ATTRIBUTE VALUE");
    }

    // check memory attribute TYPE
    if(typName.empty()) {
        m_type = m_block->type();
    }
    else {
        for(flag=false, iatt=0; iatt<DATA_TYPE_NUMBER; iatt++) {
            if(typName == DATA_TYPE_NAME[iatt]) {
                m_type = static_cast<DATA_TYPE>(iatt);
                flag = true;
                break;
            }
        }
        if(!flag) {
            CERR("%s \"%s\" has unknown type \"%s\"",accessName().c_str(),memName.c_str(),typName.c_str());
            throw CompilerException("UNKNOWN TYPE");
        }
        else {
            const HardwareObject* robj;
            if(!isTypeCompatible(m_block,&robj)) {
                CERR("%s \"%s\" type (\"%s\") incompatible with type of %s \"%s\" (\"%s\")",accessName().c_str(),memName.c_str(),DATA_TYPE_NAME[m_type].c_str(),robj->hardwareType().c_str(),robj->name().c_str(),robj->printTypeList().c_str());
                throw CompilerException("INVALID ATTRIBUTE VALUE");
            }
        }
    }

    // check memory attribute MASK
    if(!mskName.empty()) {
        try {
            m_mask = HardwareCommon::readNumber(mskName);
        }
        catch(const CompilerException& ce) {
            CERR("Exception \"%s\" while reading %s \"%s\", mask \"%s\"",ce.what(),accessName().c_str(),memName.c_str(),mskName.c_str());
            throw;
        }
        if(m_mask & (~DATA_TYPE_MMAX[m_type])) {
            if(cfg->strict_check) {
                CERR("%s \"%s\" has mask (0x%08x) incompatible with mask of type \"%s\" (0x%08x)",accessName().c_str(),memName.c_str(),m_mask,typeName().c_str(),DATA_TYPE_MMAX[m_type]);
                throw CompilerException("INVALID ATTRIBUTE VALUE");
            }
            else {
                if(cfg->print_level>=1) COUT("%s \"%s\" has mask (0x%08x) incompatible with mask of type \"%s\" (0x%08x) => SET MASK TO 0x%08x",accessName().c_str(),memName.c_str(),m_mask,typeName().c_str(),DATA_TYPE_MMAX[m_type],m_mask&DATA_TYPE_MMAX[m_type]);
                m_mask &= DATA_TYPE_MMAX[m_type];
            }
        }
    }
    else {
        m_mask = DATA_TYPE_MMAX[m_type];
    }

    // check memory attribute MODF
    if(mdfName.empty()) {
        m_modf = m_block->modf();
    }
    else {
        for(flag=false, iatt=0; iatt<DATA_MODF_NUMBER; iatt++) {
            if(mdfName == DATA_MODF_NAME[iatt]) {
                m_modf = static_cast<DATA_MODF>(iatt);
                flag = true;
                break;
            }
        }
        if(flag) {
            if(!modfIsCompatible(m_block->modf())) {
                CERR("%s \"%s\" modf (\"%s\") incompatible with modf of block \"%s\" (\"%s\")",accessName().c_str(),memName.c_str(),DATA_MODF_NAME[m_modf].c_str(),m_block->name().c_str(),DATA_MODF_NAME[m_block->modf()].c_str());
                throw CompilerException("INVALID ATTRIBUTE VALUE");
            }
        }
        else {
            CERR("%s \"%s\" has unknown modf \"%s\"",accessName().c_str(),memName.c_str(),mdfName.c_str());
            throw CompilerException("UNKNOWN MODIFIER");
        }
    }
    if(m_addr % (DATA_TYPE_SIZE[m_type]/HardwareCommon::BIT_SIZE) != 0) {
        CERR("%s \"%s\" has address (0x%08x) incompatible with type \"%s\"",accessName().c_str(),memName.c_str(),m_addr,DATA_TYPE_NAME[m_type].c_str());
        throw CompilerException("INVALID ATTRIBUTE VALUE");
    }

    // check memory attribute INTERFACE
    if(!itfName.empty()) {
        for(flag=false, iatt=0; iatt<INTERFACE_TYPE_NUMBER; iatt++) {
            if(itfName == INTERFACE_TYPE_NAME[iatt]) {
                m_interface = static_cast<INTERFACE_TYPE>(iatt);
                flag = true;
                break;
            }
        }
        if(!flag) {
            CERR("%s \"%s\" has unknown interface \"%s\"",accessName().c_str(),memName.c_str(),itfName.c_str());
            throw CompilerException("UNKNOWN INTERFACE");
        }
    }
    else {
        m_interface = BOTH;
    }
    if((m_interface == BLOCK) || (m_interface == BOTH)) {
        if(m_type != D32) {
            if(cfg->strict_check) {
                CERR("%s \"%s\" has interface \"%s\" and data type \"%s\" but DMA requires D32",accessName().c_str(),memName.c_str(),INTERFACE_TYPE_NAME[m_interface].c_str(),DATA_TYPE_NAME[m_type].c_str());
                throw CompilerException("INVALID ATTRIBUTE VALUE");
            }
            else {
                if(cfg->print_level>=1) COUT("%s \"%s\" has interface \"%s\" and data type \"%s\" but DMA requires D32 => SET INTERFACE TO \"%s\"",accessName().c_str(),memName.c_str(),DATA_TYPE_NAME[m_type].c_str(),INTERFACE_TYPE_NAME[m_interface].c_str(),DATA_TYPE_NAME[m_type].c_str(),INTERFACE_TYPE_NAME[VECTOR].c_str());
                m_interface = VECTOR;
            }
        }
        if(!((mod->amod() == A32) || (mod->amod() == A24))) {
            if(cfg->strict_check) {
                CERR("module \"%s\" has interface \"%s\" and addres type \"%s\" but DMA requires A32 or A24",mod->name().c_str(),INTERFACE_TYPE_NAME[m_interface].c_str(),ADDR_TYPE_NAME[mod->amod()].c_str());
                throw CompilerException("INVALID ATTRIBUTE VALUE");
            }
            else {
                if(cfg->print_level>=1) COUT("%s \"%s\" has interface \"%s\" and address type \"%s\" but DMA requires D32 => SET INTERFACE TO \"%s\"",accessName().c_str(),memName.c_str(),DATA_TYPE_NAME[m_type].c_str(),INTERFACE_TYPE_NAME[m_interface].c_str(),DATA_TYPE_NAME[m_type].c_str(),INTERFACE_TYPE_NAME[VECTOR].c_str());
                m_interface = VECTOR;
            }
        }
    }

    // check memory attribute MULTIPLICITY and OFFSET
    if(!mulName.empty()) {
        try {
            m_multiple = HardwareCommon::readNumber(mulName);
        }
        catch(const CompilerException& ce) {
            CERR("Exception \"%s\" while reading %s \"%s\", multiple \"%s\"",ce.what(),accessName().c_str(),memName.c_str(),mulName.c_str());
            throw;
        }
        if(m_multiple == 0) {
            CERR("%s \"%s\" has empty multiple (%d)",accessName().c_str(),memName.c_str(),m_multiple);
            throw CompilerException("INVALID ATTRIBUTE VALUE");
        }
        if(m_multiple == 1) {
            if(!offName.empty()) {
                if(cfg->strict_check) {
                    CERR("%s \"%s\" has offset \"%s\" but multiple 1",accessName().c_str(),memName.c_str(),offName.c_str());
                    throw CompilerException("INVALID ATTRIBUTE VALUE");
                }
                else {
                    if(cfg->print_level>=1) COUT("%s \"%s\" has offset \"%s\" but multiple 1 => IGNORE OFFSET",accessName().c_str(),memName.c_str(),offName.c_str());
                }
            }
        }
        else {
            if(!offName.empty()) {
                try {
                   m_offset = HardwareCommon::readNumber(offName);
                }
                catch(const CompilerException& ce) {
                    CERR("Exception \"%s\" while reading %s \"%s\", offset \"%s\"",ce.what(),accessName().c_str(),memName.c_str(),offName.c_str());
                    throw;
                }
                if(m_offset == 0) {
                    if(cfg->strict_check) {
                        CERR("%s \"%s\" has empty offset (%d)",accessName().c_str(),memName.c_str(),m_offset);
                        throw CompilerException("INVALID ATTRIBUTE VALUE");
                    }
                    else {
                        if(cfg->print_level>=1) COUT("%s \"%s\" has empty offset (%d) => CONTINUE USING ANYWAY",accessName().c_str(),memName.c_str(),m_offset);
                    }
                }
                unsigned int offs = DATA_TYPE_SIZE[m_type]/HardwareCommon::BIT_SIZE;
                if(m_offset%offs != 0) {
                    CERR("%s \"%s\" has offset (%d) not divisible by address offset (%d) for this type (%s)",accessName().c_str(),memName.c_str(),m_offset,offs,typeName().c_str());
                    throw CompilerException("INVALID ATTRIBUTE VALUE");
                }
            }
            else {
                if(cfg->strict_check) {
                    CERR("%s \"%s\" has multiple \"%s\" but no offset",accessName().c_str(),memName.c_str(),mulName.c_str());
                    throw CompilerException("INVALID ATTRIBUTE VALUE");
                }
                else {
                    m_offset = DATA_TYPE_SIZE[m_type]/HardwareCommon::BIT_SIZE;
                    if(cfg->print_level>=1) COUT("%s \"%s\" has multiple \"%s\" but no offset => SET OFFSET TO %d",accessName().c_str(),memName.c_str(),mulName.c_str(),m_offset);
                }
            }
        }
    }
    else {
        if(!offName.empty()) {
            if(cfg->strict_check) {
                CERR("%s \"%s\" has offset \"%s\" but no multiple",accessName().c_str(),memName.c_str(),offName.c_str());
                throw CompilerException("INVALID ATTRIBUTE VALUE");
            }
            else {
                if(cfg->print_level>=1) COUT("%s \"%s\" has offset \"%s\" but no multiple => IGNORE OFFSET",accessName().c_str(),memName.c_str(),offName.c_str());
            }
        }
    }

    // check memory attribute bitstring
    if(!bitName.empty()) {
        const HardwareDataType* dat;
        if(getCompiler()->getFromDataTypeMap(HardwareBitString::FILENAME(),bitName,&dat)) {
            if(dat->getFullMask() & (~m_mask)) {
                CERR("%s \"%s\" mask (0x%08x) incompatible with mask of bitstring \"%s\" (0x%08x)",accessName().c_str(),memName.c_str(),dat->getFullMask(),dat->name().c_str(),m_mask);
                throw CompilerException("INVALID ATTRIBUTE VALUE");
            }
            if(!dat->modfIsCompatible(m_modf)) {
                CERR("%s \"%s\" modf (\"%s\") incompatible with modf of bitstring \"%s\" (\"%s\")",accessName().c_str(),memName.c_str(),DATA_MODF_NAME[m_modf].c_str(),dat->name().c_str(),DATA_MODF_NAME[dat->modf()].c_str());
                throw CompilerException("INVALID ATTRIBUTE VALUE");
            }
            HardwareBitString* bit = new HardwareBitString(dynamic_cast<const HardwareBitString*>(dat));
            bit->setElement(this);
            m_data = bit;
        }
        else {
            CERR("%s \"%s\" references bitstring \"%s\" which cannot be found",accessName().c_str(),memName.c_str(),bitName.c_str());
            throw CompilerException("INVALID ATTRIBUTE VALUE");
        }
    }

    // check and add to address map
    getCompiler()->addToAddressMap(this);

    // set memory attributes
    m_name = memName;

    // read data type
    readDataType(mem);

    // promote implicit mask
    if(cfg->promote_mask) {
        const HardwareBitString* bit = dynamic_cast<const HardwareBitString*>(m_data);
        if(bit && bit->isReference()) bit = dynamic_cast<const HardwareBitString*>(bit->getReference());
       if(bit) {
           if(bit->isReference()) bit = dynamic_cast<const HardwareBitString*>(bit->getReference());
            if(m_mask & (~bit->getFullMask())) {
                m_mask = bit->getFullMask();
            }
        }
    }

    // promote implicit modf
    if(cfg->promote_modf) {
       if(m_modf == RW) {
            if(!m_data->isReadable()) m_modf = W;
            if(!m_data->isWritable()) m_modf = R;
        }
    }
}

//------------------------------------------------------------------------------

void HardwareMemory::readDataType(const DOMNode* mem) {

    // attempt finding bitstring element
    readDataTypeLoop(mem);

    // attempt reading bitstring from level above (=memory) if none found yet
    if(!m_data) {
        HardwareBitString* bit = new HardwareBitString();
        bit->setElement(this);
        bit->read(mem);
        if(!bit->isEmpty()) {
            m_data = bit;
            getCompiler()->addToDataTypeMap(bit);
        }
        // use HardwareWord if bitstring is empty
        else {
            HardwareWord* wrd = new HardwareWord();
            wrd->type(m_type);
            wrd->setElement(this);
            m_data = wrd;
            delete bit;
        }
    }
}

//------------------------------------------------------------------------------

void HardwareMemory::readDataTypeLoop(const DOMNode* mem) {

    DOMNodeList* datList;
    DOMNode* dat;
    unsigned int idat, nfld(0);
    const CompilerConfiguration* cfg = getCompiler()->getConfig();

    // get all data
    datList = mem->getChildNodes();
    if(cfg->print_level>=4) {
        std::string memName = (mem->getNodeType() == DOMNode::ELEMENT_NODE) ? m_name : TRX(mem->getNodeName());
        COUT("%s \"%s\" has %d child%s",accessName().c_str(),memName.c_str(),datList->getLength(),datList->getLength()>1?"ren":"");
    }

    // traverse all data
    for(idat=0; idat<datList->getLength(); idat++) {
        dat = datList->item(idat);

        // entity reference node type
        if(dat->getNodeType() == DOMNode::ENTITY_REFERENCE_NODE) {
            if(cfg->print_level>=4) COUT("found data entity reference \"%s\"",TRX(dat->getNodeName()));
            readDataTypeLoop(dat);
            continue;
        }
        // element node type
        else if(dat->getNodeType() != DOMNode::ELEMENT_NODE) continue;

        // bitstring element
        if(HardwareBitString::isBitString(dat)) {
            if(nfld) {
                CERR("%s \"%s\" cannot have bitstring and field elements at the same time",accessName().c_str(),m_name.c_str());
                throw CompilerException("INVALID DATA TYPE");
            }
            if(m_data) {
                CERR("%s \"%s\" already attached to bitstring \"%s\"",accessName().c_str(),m_name.c_str(),m_data->name().c_str());
                throw CompilerException("BITSTRING ALREADY SET");
            }
            HardwareBitString* bit = new HardwareBitString();
            bit->setElement(this);
            bit->read(dat);
            if(bit->isEmpty()) {
                CERR("%s \"%s\" cannot have empty bitstring \"%s\"",accessName().c_str(),m_name.c_str(),bit->name().c_str());
                throw CompilerException("INVALID DATA TYPE");
            }
            m_data = bit;
            getCompiler()->addToDataTypeMap(bit);
        }

        // anonymous bitstring: field
        else if(HardwareField::isField(dat)) {

            if(m_data) {
                CERR("%s \"%s\" cannot have bitstring and field elements at the same time",accessName().c_str(),m_name.c_str());
                throw CompilerException("INVALID STRUCTURE");
            }
            nfld++;
        }

        // unknown type
        else {
            CERR("child of %s \"%s\" has unknown type \"%s\"",accessName().c_str(),m_name.c_str(),TRX(dat->getNodeName()));
            throw CompilerException("UNKNOWN TYPE");
        }
    }
}

//------------------------------------------------------------------------------

void HardwareMemory::write(std::ostream& ostr) const {

    char os[STRING_LENGTH];
    std::ostringstream buf;
    std::string indent;
    unsigned int rgt;

    // indentation
    indent = (m_block->name().empty()) ? "  " : "    ";
    rgt = XML_TAB_LENGTH - accessName().size() - indent.size();

    // optional attributes
    if(m_type != D32) buf << " type=\"" << DATA_TYPE_NAME[m_type] << "\"";
    if(m_mask != DATA_TYPE_MMAX[m_type]) buf << " mask=\"" << hex(8) << m_mask << dec << "\"";
    if((m_modf != RW) && (m_modf != m_block->modf())) buf << " modf=\"" << DATA_MODF_NAME[m_modf] << "\"";
    if(m_interface != BOTH) buf << " interface=\"" << INTERFACE_TYPE_NAME[m_interface] << "\"";
    if(m_multiple > 1) buf << " multiple=\"" << m_multiple << "\" offset=\"" << hex(1) << m_offset << dec << "\"";

    // if data type is empty or reference
    if(m_data->isEmpty() || m_data->isReference()) {

        // if data type is reference
        if(m_data->isReference()) buf << " bitstring =\"" << m_data->getReference()->nameName() << "\"";

        // write memory single tag
        std::sprintf(os,"%s<%s name=%s addr=\"0x%08x\" size=\"0x%08x\"%s/>\n",indent.c_str(),accessName().c_str(),HardwareCommon::rightFillString("\""+m_name+"\"",rgt).c_str(),m_addr,m_size,buf.str().c_str()); ostr << os;
    }

    // if data type is not empty and not reference
    else {

        // write memory start tag
        std::sprintf(os,"%s<%s name=%s addr=\"0x%08x\" size=\"0x%08x\"%s>\n",indent.c_str(),accessName().c_str(),HardwareCommon::rightFillString("\""+m_name+"\"",rgt).c_str(),m_addr,m_size,buf.str().c_str()); ostr << os;

        // write data type
        if(m_data) m_data->write(ostr);

        // write memory end tag
        std::sprintf(os,"%s</%s>\n",indent.c_str(),accessName().c_str()); ostr << os;
    }
}

//------------------------------------------------------------------------------

std::string HardwareMemory::sizeName() const {

    std::string n = CPP_SIZE_PREFIX + nameName();
    transform(n.begin(),n.end(),n.begin(),toupper);

    return(n);
}

//------------------------------------------------------------------------------

std::string HardwareMemory::indxName() const {

    std::string n = nameName() + CPP_INDX_POSTFIX;
    transform(n.begin(),n.end(),n.begin(),toupper);

    return(n);
}

//------------------------------------------------------------------------------

bool HardwareMemory::isMemory(const XERCES_CPP_NAMESPACE::DOMNode* mem) {

    // check if empty node
    if(!(mem)) return(false);

    std::string name = TRX(mem->getNodeName());
    unsigned int ityp;

    // loop over all access types
    for(ityp=0; ityp<ACCESS_TYPE_NUMBER; ityp++) {
        if(name == XML_ACCESS_NAME[ityp]) return(true);
    }

    return(false);
}
