//******************************************************************************
// file: HardwareValue.cc
// desc: library for L1CT HardwareCompiler
// auth: 19-MAR-2013 R. Spiwoks, re-design from RCDModuleDesign
//******************************************************************************

#include <algorithm>

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/parsers/XercesDOMParser.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/sax/HandlerBase.hpp>

#include "RCDUtilities/RCDUtilities.h"
#include "L1CTHardwareCompiler/HardwareCommon.h"
#include "L1CTHardwareCompiler/HardwareValue.h"
#include "L1CTHardwareCompiler/HardwareField.h"
#include "L1CTHardwareCompiler/HardwareBitString.h"
#include "L1CTHardwareCompiler/HardwareElement.h"
#include "L1CTHardwareCompiler/HardwareBlock.h"
#include "L1CTHardwareCompiler/HardwareCompiler.h"
#include "L1CTHardwareCompiler/CompilerException.h"

using namespace L1CTHC;
using namespace XERCES_CPP_NAMESPACE;

//------------------------------------------------------------------------------

const std::string HardwareValue::XML_VALUE              = "value";
const std::string HardwareValue::CPP_DATA_PREFIX        = "VALUE_";

//------------------------------------------------------------------------------

HardwareValue::HardwareValue(const std::string& name) : HardwareObject(name), m_field(0), m_data(0) {

    // nothing to be done
}

//------------------------------------------------------------------------------

HardwareValue::~HardwareValue() {

    // nothing to be done
}

//------------------------------------------------------------------------------

void HardwareValue::dump(std::ostream& ostr, const unsigned int indt) const {

    char os[STRING_LENGTH];

    // dump value
    std::sprintf(os,"%sHardwareValue \"%s\", data = %s\n",HardwareCommon::spaceString(indt).c_str(),m_name.c_str(),m_data.string().c_str()); ostr << os;
}

//------------------------------------------------------------------------------

void HardwareValue::read(const DOMNode* fld) {

    DOMNode* attr;
    std::string valName, datName;
    const CompilerConfiguration* cfg = getCompiler()->getConfig();

    // read value attributes
    attr = fld->getAttributes()->getNamedItem(TRX("name"));
    if(attr) valName = TRX(attr->getNodeValue());
    attr = fld->getAttributes()->getNamedItem(TRX("data"));
    if(attr) datName = TRX(attr->getNodeValue());

    // print value attributes
    if(cfg->print_level>=4) COUT("found value \"%s\" with data=\"%s\"",valName.c_str(),datName.c_str());

    // check value attribute NAME
    if(valName.empty()) {
        CERR("value \"%s\" has no name",valName.c_str());
        throw CompilerException("NO NAME");
    }

    // check value attribute DATA
    if(m_data.read(datName)) {
        CERR("Error while reading value \"%s\", data \"%s\"",valName.c_str(),datName.c_str());
        throw CompilerException("INVALID ATTRIBUTE VALUE");
    }
    if(cfg->print_level>=10) COUT("value \"%s\" has data \"%s\"",valName.c_str(),m_data.string().c_str());
    if(!(m_data & (~(getField()->mask()))).null()) {
        CERR("data %s of value \"%s\" is incompatible with mask %s of comprising field \"%s\"",m_data.string().c_str(),valName.c_str(),getField()->mask().string().c_str(),getField()->name().c_str());
        throw CompilerException("INVALID ATTRIBUTE VALUE");
    }

    // set value attributes
    m_name = valName;
}

//------------------------------------------------------------------------------

void HardwareValue::write(std::ostream& ostr) const {

    char os[STRING_LENGTH];
    std::ostringstream indent;
    unsigned int rgt;

    // indentation
    if(m_field->getBitString()->decl() == HardwareDataType::EXTERNAL) {
        indent << "      ";
    }
    else {
        indent << ((m_field->getBitString()->getElement()->getBlock()->name().empty())? "" : "  ") << "        ";
    }
    rgt = XML_TAB_LENGTH - XML_VALUE.size() - indent.str().size();

    // write single value tag
    std::sprintf(os,"%s<%s name=%s data=\"%s\"/>\n",indent.str().c_str(),XML_VALUE.c_str(),HardwareCommon::rightFillString("\""+m_name+"\"",rgt).c_str(),m_data.string().c_str()); ostr << os;
}

//------------------------------------------------------------------------------

std::string HardwareValue::dataName() const {

    std::string n(CPP_DATA_PREFIX + getField()->nameName() + "_" + nameName());
    transform(n.begin(),n.end(),n.begin(),toupper);
    return(n);
}

//------------------------------------------------------------------------------

bool HardwareValue::isValue(const XERCES_CPP_NAMESPACE::DOMNode* val) {

    if(val && (TRX(val->getNodeName()) == XML_VALUE)) return(true);

    return(false);
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

bool L1CTHC::compareHardwareValueByName(HardwareValue* vala, HardwareValue* valb) {

    return(vala->name().compare(valb->name()) < 0);
}

//------------------------------------------------------------------------------

bool L1CTHC::compareHardwareValueByData(HardwareValue* vala, HardwareValue* valb) {

    return(vala->data() < valb->data());
}
