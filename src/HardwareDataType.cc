//******************************************************************************
// file: HardwareDataType.cc
// desc: library for L1CT HardwareCompiler
// auth: 01-MAR-2013 R. Spiwoks, re-design from RCDModuleDesign
//******************************************************************************

#include "RCDUtilities/RCDUtilities.h"
#include "L1CTHardwareCompiler/HardwareDataType.h"
#include "L1CTHardwareCompiler/CompilerException.h"

using namespace L1CTHC;

//------------------------------------------------------------------------------

const std::string HardwareDataType::DECL_TYPE_NAME[DECL_TYPE_NUMBER] = {
    "INTERNAL", "EXTERNAL"
};

//------------------------------------------------------------------------------

HardwareDataType::HardwareDataType(const std::string& name, const DECL_TYPE decl) : HardwareObject(name), m_decl(decl), m_reference(nullptr), m_element(nullptr) {

    // nothing to be done
}

//------------------------------------------------------------------------------

HardwareDataType::HardwareDataType(const HardwareDataType* d) : HardwareObject(""), m_decl(INTERNAL), m_reference(d), m_element(nullptr) {

    // check if referenced data type is empty
    if(!m_reference) {
        CERR("HardwareDataType of type cannot reference empty HardwareDataType","");
        throw CompilerException("REFERENCE TO EMPTY DATA TYPE");
    }

    // copy members
    m_name = "REFERENCE(" + m_reference->name() + ")";
    m_modf = m_reference->modf();
    m_multiple = m_reference->multiple();
    m_offset = m_reference->offset();
}

//------------------------------------------------------------------------------

HardwareDataType::~HardwareDataType() {

    // nothing to be done
}
