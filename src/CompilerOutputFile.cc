//******************************************************************************
// file: CompilerOutputFile.cc
// desc: output file for L1CT HardwareCompiler
// auth: 20-MAR-2013 R. Spiwoks
//******************************************************************************

#include <iomanip>
#include <unistd.h>
#include <time.h>
#include <pwd.h>

#include "RCDUtilities/RCDUtilities.h"
#include "L1CTHardwareCompiler/HardwareCommon.h"
#include "L1CTHardwareCompiler/CompilerException.h"
#include "L1CTHardwareCompiler/CompilerOutputFile.h"
#include "L1CTHardwareCompiler/HardwareCompiler.h"

using namespace L1CTHC;

//------------------------------------------------------------------------------

const char CompilerOutputFile::MON[12][4] = {
    "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"
};

//------------------------------------------------------------------------------

CompilerOutputFile::CompilerOutputFile(const std::string& name) : m_name(name) {

    // open file
    m_file.open(m_name.c_str());
    if(!m_file) {
        CERR("cannot open output file \"%s\"",m_name.c_str());
        throw CompilerException("CANNOT OPEN FILE");
    }
}

//------------------------------------------------------------------------------

CompilerOutputFile::~CompilerOutputFile() {

    // close file
    m_file.close();
}

//------------------------------------------------------------------------------

void CompilerOutputFile::writePreamble(const HardwareCompiler* comp, const std::string& desc, const std::string& incl) {

    // structure for time and user name
    time_t ntm;
    struct tm* now;
    struct passwd* pwd;

    // get date and user name
    ntm = time((time_t*)0);
    now = localtime(&ntm);
    pwd = getpwuid(getuid());

    m_incl = incl;
    if(!m_incl.empty()) {
        m_file << "#ifndef " << m_incl << std::endl
               << "#define " << m_incl << std::endl
               << std::endl;
    }

    m_file << "//******************************************************************************" << std::endl
           << "// file: " << m_name << std::endl
           << "// desc: " << desc << std::endl
           << "// auth: " << comp->name() << std::endl
           << "// time: " << std::setfill('0') << std::setw(2) << now->tm_mday
           << "-" << std::setw(2) << MON[now->tm_mon]
           << "-" << std::setw(4) << (now->tm_year+1900)
           << " " << std::setw(2) << (now->tm_hour)
           << ":" << std::setw(2) << (now->tm_min)
           << ":" << std::setw(2) << (now->tm_sec) << std::setfill(' ') << std::endl
           << "// user: " << pwd->pw_name << " (" << pwd->pw_gecos << ")" << std::endl
           << "// call: \"" << comp->getConfig()->program_call << "\"" << std::endl
           << "//******************************************************************************" << std::endl
           << std::endl;
}

//------------------------------------------------------------------------------

void CompilerOutputFile::writePostscript() {

    if(!m_incl.empty()) {
        m_file << "#endif  // " << m_incl << std::endl;
    }
}
