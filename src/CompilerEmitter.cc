//******************************************************************************
// file: CompilerEmitter.cc
// desc: library for L1CT HardwareCompiler
// auth: 25-APR-2013 R. Spiwoks, re-design from RCDModuleDesign
//******************************************************************************

#include <vector>
#include <algorithm>

#include "RCDUtilities/RCDUtilities.h"
#include "L1CTHardwareCompiler/CompilerEmitter.h"
#include "L1CTHardwareCompiler/CompilerOutputFile.h"
#include "L1CTHardwareCompiler/CompilerException.h"
#include "L1CTHardwareCompiler/CompilerStringList.h"
#include "L1CTHardwareCompiler/HardwareCompiler.h"
#include "L1CTHardwareCompiler/HardwareBlock.h"
#include "L1CTHardwareCompiler/HardwareElement.h"
#include "L1CTHardwareCompiler/HardwareRegister.h"
#include "L1CTHardwareCompiler/HardwareMemory.h"
#include "L1CTHardwareCompiler/HardwareBitString.h"
#include "L1CTHardwareCompiler/HardwareWord.h"
#include "L1CTHardwareCompiler/HardwareField.h"
#include "L1CTHardwareCompiler/HardwareValue.h"
#include "L1CTHardwareCompiler/HardwareCommon.h"

using namespace L1CTHC;

//------------------------------------------------------------------------------

CompilerEmitter::CompilerEmitter() : m_compiler(0) {

    // nothing to be done
}

//------------------------------------------------------------------------------

CompilerEmitter::~CompilerEmitter() {

    // nothing to be done
}

//------------------------------------------------------------------------------

void CompilerEmitter::setCompiler(HardwareCompiler* compiler) {

    if(m_compiler) {
        CERR("CompilerEmitter already attached to HardwareCompiler","");
        throw CompilerException("COMPILER ALREADY SET");
    }
    m_compiler = compiler;
}
