//******************************************************************************
// file: CompilerException.cc
// desc: exception of L1CT HardwareCompiler
// auth: 01-MAR-2013 R. Spiwoks, re-design from RCDModuleDesign
//******************************************************************************

#include <sstream>

#include "L1CTHardwareCompiler/HardwareCommon.h"
#include "L1CTHardwareCompiler/CompilerException.h"

using namespace L1CTHC;

//------------------------------------------------------------------------------

const std::string CompilerException::SEVERITY_NAME[SEVERITY_NUMBER] = {
    "DEBUG", "LOG", "INFO", "WARN", "ERROR", "FATAL"
};

//------------------------------------------------------------------------------

CompilerException::CompilerException(const std::string msg, const SEVERITY sev) : m_severity(sev) {

    char os[STRING_LENGTH];

    if(!msg.empty()) {
        std::sprintf(os,"CompilerException: \"%s\" - %s",SEVERITY_NAME[m_severity].c_str(),msg.c_str());
    }
    else {
        std::sprintf(os,"CompilerException: \"%s\" - L1CT Hardware Compiler Exception",SEVERITY_NAME[m_severity].c_str());
    }
    m_message = os;
}

//------------------------------------------------------------------------------

CompilerException::~CompilerException() throw() {

    // nothing to be done
}
