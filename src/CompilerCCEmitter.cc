//******************************************************************************
// file: CompilerCCEmitter.cc
// desc: library for L1CT HardwareCompiler
// auth: 25-APR-2013 R. Spiwoks, re-design from RCDModuleDesign
//******************************************************************************

#include <vector>
#include <algorithm>

#include "RCDUtilities/RCDUtilities.h"
#include "L1CTHardwareCompiler/CompilerCCEmitter.h"
#include "L1CTHardwareCompiler/CompilerOutputFile.h"
#include "L1CTHardwareCompiler/CompilerException.h"
#include "L1CTHardwareCompiler/CompilerStringList.h"
#include "L1CTHardwareCompiler/HardwareCompiler.h"
#include "L1CTHardwareCompiler/HardwareBlock.h"
#include "L1CTHardwareCompiler/HardwareElement.h"
#include "L1CTHardwareCompiler/HardwareRegister.h"
#include "L1CTHardwareCompiler/HardwareMemory.h"
#include "L1CTHardwareCompiler/HardwareBitString.h"
#include "L1CTHardwareCompiler/HardwareWord.h"
#include "L1CTHardwareCompiler/HardwareField.h"
#include "L1CTHardwareCompiler/HardwareValue.h"
#include "L1CTHardwareCompiler/HardwareCommon.h"

using namespace L1CTHC;

//------------------------------------------------------------------------------

CompilerCCEmitter::CompilerCCEmitter() : CompilerEmitter() {

    // nothing to be done
}

//------------------------------------------------------------------------------

CompilerCCEmitter::~CompilerCCEmitter() {

    // nothing to be done
}

//-----------------------------------------------------------------------------
// COMPILE
//-----------------------------------------------------------------------------

void CompilerCCEmitter::compile() {

    const HardwareModule* mod = getCompiler()->getModule();

    // compile module
    compileModule(mod);

    // compile data
    compileDataType();

    // compile menu
    compileMenu(mod);
}

//=====================================================================================================================
//=====================================================================================================================

//-----------------------------------------------------------------------------
// COMPILE MODULE
//-----------------------------------------------------------------------------

void CompilerCCEmitter::compileModule(const HardwareModule* mod) {

    // get module output files
    CompilerOutputFile* modHH;
    CompilerOutputFile* modCC;
    try {
        m_compiler->getModuleFiles(&modHH,&modCC);
    }
    catch (const CompilerException& ce) {
        CERR("Exception \"%s\" while trying to get module files",ce.what());
        throw;
    }

    // get module output streams
    std::ofstream& fmodHH(modHH->file());
    std::ofstream& fmodCC(modCC->file());

    const CompilerConfiguration* cfg = getCompiler()->getConfig();
    std::string modName, maxName, elmType, datType, refType, typType;
    std::vector<std::string> hdrName;
    unsigned int i;
    std::ostringstream smodHH, smodCC;
    CompilerStringList par; par.space(1);

    HardwareList<HardwareBlock>::const_iterator iblk;

    modName = mod->name();
    maxName = (mod->getMaximumSizeMemory()) ? mod->getMaximumSizeMemory()->sizeName() : "0";
    getCompiler()->getDataTypeHeaderFileNames(hdrName);

    // module dedclaration; includes
    if(cfg->compile_type != CompilerConfiguration::AXI) {
        for(i=0; i<hdrName.size(); i++) {
            fmodHH << "#include <" << cfg->package_name << "/" << hdrName[i] << ">" << std::endl;
        }
        fmodHH << "#include \"RCDVme/RCDVme.h\"" <<std::endl;
        fmodHH << std::endl;
    }
    fmodHH << compileModuleHardwareIncludesDeclaration(mod);    // --- HARDWARE INCLUDES !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    // module declaration: contructors and destructors
    fmodHH << "namespace " << cfg->namespace_name << " {" << std::endl;
    fmodHH << std::endl;
    fmodHH << "class " << modName << " {" << std::endl;
    fmodHH << std::endl;
    fmodHH << "  public:" << std::endl;
    fmodHH << std::endl;
    fmodHH << "    // constructor and desctructor" << std::endl;
    fmodHH << compileModuleHardwareCtorDtorDeclaration(mod);    // --- HARDWARE CONSTRUCTOR/DESTRUCTOR DEFINITION !!!!!!
    fmodHH << "    static unsigned int addr()                  { return(" << mod->addrName() << "); }" << std::endl;
    fmodHH << "    static unsigned int size()                  { return(" << mod->sizeName() << "); }" << std::endl;
    if(cfg->compile_type != CompilerConfiguration::AXI) {
        fmodHH << "    static unsigned int amod()                  { return(" << mod->amodName() << "); }" << std::endl;
        fmodHH << "    static std::string code()                   { return(" << mod->codeName() << "); }" << std::endl;
        fmodHH << "    static unsigned int mmax()                  { return(" << maxName << "); }" << std::endl;
    }
    fmodHH << std::endl;
    fmodHH << "    // read/write functions" << std::endl;

    // module definition: includes
    if(cfg->compile_type != CompilerConfiguration::AXI) {
        fmodCC << "#include <" << cfg->package_name << "/" << modHH->name() << ">" << std::endl;
        fmodCC << "#include \"RCDUtilities/RCDUtilities.h\"" << std::endl;
        fmodCC << std::endl;
    }
    fmodCC << compileModuleHardwareIncludesDefinition(mod);     // --- HARDWARE INCLUDES !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    fmodCC << "#include <iostream>" << std::endl;
    fmodCC << "#include <iomanip>" << std::endl;
    fmodCC << std::endl;
    fmodCC << "using namespace " << cfg->namespace_name << ";" << std::endl;
    fmodCC << std::endl;

    // define module constructor and destructor
    fmodCC << compileModuleHardwareCtorDtorDefinition(mod);     // --- HARDWARE CONSTRUCTOR/DESTRUCTOR DECLARATION !!!!!

    // ADDR, SIZE, TYPE, and AMOD constant declaration
    smodHH << "    static const unsigned int " << mod->addrName() << ";" << std::endl;
    smodHH << "    static const unsigned int " << mod->sizeName() << ";" << std::endl;
    if(cfg->compile_type != CompilerConfiguration::AXI) {
        smodHH << "    static const unsigned int " << mod->amodName() << ";" << std::endl;
        smodHH << "    static const std::string " << mod->codeName() << ";" << std::endl;
    }
    smodHH << std::endl;

    // ADDR, SIZE, TYPE, and AMOD constant definition
    smodCC << "const unsigned int " << modName << "::" << mod->addrName() << " = " << hex(8) << mod->addr() << dec << ";" << std::endl;
    smodCC << "const unsigned int " << modName << "::" << mod->sizeName() << " = " << hex(8) << mod->size() << dec << ";" << std::endl;
    if(cfg->compile_type != CompilerConfiguration::AXI) {
        smodCC << "const unsigned int " << modName << "::" << mod->amodName() << " = " << mod->amod() << ";" << std::endl;
        smodCC << "const std::string " << modName << "::" << mod->codeName() << " = \"" << HardwareObject::ADDR_TYPE_NAME[mod->amod()] << "\";" << std::endl;
    }
    smodCC << std::endl;

    // loop over all blocks
    for(iblk=mod->beginBlock(); iblk!=mod->endBlock(); iblk++) {
        compileModuleBlock(*iblk,smodHH,smodCC);
    }

    // finish declaration of module
    fmodHH << "  private:" << std::endl;
    fmodHH << std::endl;
    fmodHH << compileModuleHardwareMembers(mod);                // --- HARDWARE MEMBERS !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    if(cfg->address_public) {
        fmodHH << "  public:" << std::endl;
        fmodHH << std::endl;
    }
    fmodHH << smodHH.str();
    fmodHH << std::endl;
    fmodHH << "};      // class " << modName << std::endl;
    fmodHH << std::endl;
    fmodHH << "}       // namespace " << cfg->namespace_name << std::endl;
    fmodHH << std::endl;

    // finish definition of module
    fmodCC << "//------------------------------------------------------------------------------" << std::endl;
    fmodCC << std::endl;
    fmodCC << smodCC.str();
}

//-----------------------------------------------------------------------------

void CompilerCCEmitter::compileModuleBlock(const HardwareBlock* blk, std::ostringstream& smodHH, std::ostringstream& smodCC) {

    // get module output files
    CompilerOutputFile* modHH;
    CompilerOutputFile* modCC;
    try {
        m_compiler->getModuleFiles(&modHH,&modCC);
    }
    catch (const CompilerException& ce) {
        CERR("Exception \"%s\" while trying to get module files",ce.what());
        throw;
    }

    // get module output streams
    std::ofstream& fmodHH(modHH->file());
    std::ofstream& fmodCC(modCC->file());

    const CompilerConfiguration* cfg = getCompiler()->getConfig();
    const HardwareModule* mod = getCompiler()->getModule();
    std::string modName, maxName, elmType, datType, refType, typType;
    unsigned int typSize;
    CompilerStringList par; par.space(1);

    HardwareList<HardwareBlock>::const_iterator iblk;
    HardwareList<HardwareElement>::const_iterator ielm;
    const HardwareElement* elm;
    const HardwareRegister* reg;
    const HardwareMemory* mem;
    const HardwareDataType* dat;
    const HardwareWord* wrd;
    const HardwareBitString* bit;

    modName = mod->name();

    if(blk->addr()) {

        // -- ADDR(block) constant declaration
        smodHH << "    static const unsigned int " << blk->addrName() << ";" << std::endl;

        // --- ADDR(block) constant definition
        unsigned int num = (cfg->compile_type == CompilerConfiguration::AXI) ? blk->addr()/sizeof(uint32_t) : blk->addr();
        smodCC << "const unsigned int " << modName << "::" << blk->addrName() << " = " << hex(8) << num << dec << ";" << std::endl;
    }
    if(blk->multiple() > 1) {

        // --- MULT(block) declaration/definition
        fmodHH << "    static const unsigned int " << blk->multipleName() << " = " << blk->multiple() << ";" << std::endl
               << std::endl;

        // -- OFFS(block) constant declaration
        smodHH << "    static const unsigned int " << blk->offsetName() << ";" << std::endl;

        // --- OFFS(block) constant definition
        unsigned int num = (cfg->compile_type == CompilerConfiguration::AXI) ? blk->offset()/sizeof(uint32_t) : blk->offset();
        smodCC << "const unsigned int " << modName << "::" << blk->offsetName() << " = " << hex(8) << num << dec << ";" << std::endl;
    }

    // loop over all elements
    for(ielm=blk->beginElement(); ielm!=blk->endElement(); ielm++) {
        elm = *ielm;
        reg = dynamic_cast<const HardwareRegister*>(elm);
        mem = dynamic_cast<const HardwareMemory*>(elm);
        elmType = elm->nameName();
        typType = HardwareObject::DATA_TYPE_TYPE[elm->type()];
        typSize = HardwareObject::DATA_TYPE_SIZE[elm->type()]/HardwareCommon::BIT_SIZE;

        // -- MULT declaration/definition
        if(elm->multiple() > 1) {
            fmodHH << "    static const unsigned int " << elm->multipleName() << " = " << elm->multiple() << ";" << std::endl;
        }

        // -- MEMORY INDEX NUMBER declaration/definition
        if(mem) {
            fmodHH << "    static const unsigned int " << mem->indxName() << " = " << mem->indx() << ";" << std::endl;
        }

        // -- MASK declaration/definition
        fmodHH << "    static const " << typType << " " << elm->maskName() << " = " << hex(typSize) << elm->mask() << dec << ";" << std::endl;

        for(unsigned int idt=0; idt<HardwareElement::NUM_DATA_TYPES; idt++) {
            dat = (idt == 0) ? elm->getDataType() : elm->getAlternativeDataType();
            if(!dat) continue;

            wrd = dynamic_cast<const HardwareWord*>(dat);
            bit = dynamic_cast<const HardwareBitString*>(dat);
            refType = (wrd) ? "" : ((bit) ? "&" : "");
            datType = dat->nameName();

            // parameters
            par.clear();
            compileModuleBlockList(blk,par,"const unsigned int");
            if(elm->multiple()>1) par.add("const unsigned int");

            // --- HardwareRegister ----------------------------------------
            if(reg) {

                // SINGLE access functions' declaration
                if(elm->isReadable()) {
                    fmodHH << "    int " << elmType << "_Read(" << par.rstr() << datType << "&) const;" << std::endl;
                }
                if(elm->isWritable()) {
                    fmodHH << "    int " << elmType << "_Write(" << par.rstr() << "const " << datType << refType << ");" << std::endl;
                }
            }

            // --- HardwareMemory ------------------------------------------
            else if(mem) {

                // SINGLE access functions' declaration
                if(mem->access() == HardwareMemory::RAM) par.add("const unsigned int");
                if(elm->isReadable()) {
                    fmodHH << "    int " << elmType << "_Read(" << par.rstr() << datType << "&) const;" << std::endl;
                }
                if(elm->isWritable()) {
                    fmodHH << "    int " << elmType << "_Write(" << par.rstr() << "const " << datType << refType << ");" << std::endl;
                }

                // VECTOR access functions' declaration
                if((mem->interface() == HardwareMemory::VECTOR) || (mem->interface()) == HardwareMemory::BOTH) {
                    if(elm->isReadable()) {
                        par.clear();
                        compileModuleBlockList(blk,par,"const unsigned int");
                        if(elm->multiple()>1) par.add("const unsigned int");
                        par.add("std::vector<"+datType+">&");
                        if(mem->access() == HardwareMemory::RAM) par.add("const unsigned int off = 0");
                        fmodHH << "    int " << elmType << "_Read(" << par.str() << ");" << std::endl;
                    }
                    if(elm->isWritable()) {
                        par.clear();
                        compileModuleBlockList(blk,par,"const unsigned int");
                        if(elm->multiple()>1) par.add("const unsigned int");
                        par.add("const std::vector<"+datType+">&");
                        if(mem->access() == HardwareMemory::RAM) par.add("const unsigned int off = 0");
                        fmodHH << "    int " << elmType << "_Write(" << par.str() << ");" << std::endl;
                    }
                }

                // BLOCK access functions' declaration (not for IPbus !!!!)
                if(cfg->compile_type != CompilerConfiguration::IPB) {
                    if((mem->interface() == HardwareMemory::BLOCK) || (mem->interface()) == HardwareMemory::BOTH) {
                        par.clear();
                        compileModuleBlockList(blk,par,"const unsigned int");
                        if(elm->multiple()>1) par.add("const unsigned int");
                        if(cfg->compile_type != CompilerConfiguration::AXI) {
                            par.add("RCD::CMEMSegment*");
                        }
                        else {
                            par.add("uint32_t*");
                        }
                        par.add("const unsigned int size = "+mem->indxName());
                        if(cfg->compile_type != CompilerConfiguration::AXI) {
                            if(mem->access() == HardwareMemory::RAM) par.add("const unsigned int vme_off = 0");
                            par.add("const unsigned int seg_off = 0");
                        }
                        else {
                            if(mem->access() == HardwareMemory::RAM) par.add("const unsigned int axi_off = 0");
                            par.add("const unsigned int dat_off = 0");
                        }

                        if(elm->isReadable()) {
                            fmodHH << "    int " << elmType << "_Read(" << par.str() << ");" << std::endl;
                        }
                        if(elm->isWritable()) {
                            fmodHH << "    int " << elmType << "_Write(" << par.str() << ");" << std::endl;
                        }
                    }
                }

                // FILE access functions' declaration (only for AXI !!!!)
                if(cfg->compile_type == CompilerConfiguration::AXI) {
                    if((mem->interface() == HardwareMemory::BLOCK) || (mem->interface()) == HardwareMemory::BOTH) {
                        par.clear();
                        compileModuleBlockList(blk,par,"const unsigned int");
                        if(elm->multiple()>1) par.add("const unsigned int");
                        par.add("const std::string&");
                        par.add("const unsigned int size = "+mem->indxName());
                        if(mem->access() == HardwareMemory::RAM) par.add("const unsigned int axi_off = 0");

                        if(elm->isReadable()) {
                            fmodHH << "    int " << elmType << "_Read(" << par.str() << ");" << std::endl;
                        }
                        if(elm->isWritable()) {
                            fmodHH << "    int " << elmType << "_Write(" << par.str() << ");" << std::endl;
                        }
                    }
                }
            }
        }
        fmodHH << std::endl;

        // -- ADDR, OFFSET, and SIZE constants' declaration
        smodHH << "    static const unsigned int " << elm->addrName() << ";" << std::endl;
        if(elm->multiple() > 1) {
            smodHH << "    static const unsigned int " << elm->offsetName() << ";" << std::endl;
        }
        if(mem) {
            smodHH << "    static const unsigned int " << mem->sizeName() << ";" << std::endl;
        }

        // --- SINGLE access functions' definitions ----------------------------
        fmodCC << compileModuleElementSingle(elm);

        // --- VECTOR access functions' definitions ----------------------------
        if(mem && ((mem->interface() == HardwareMemory::VECTOR) || (mem->interface() == HardwareMemory::BOTH))) {
            fmodCC << compileModuleElementVector(elm);
        }
        // --- BLOCK access functions' definitions (not for IPbus !!!!) --------
        if(cfg->compile_type != CompilerConfiguration::IPB) {
            if(mem && ((mem->interface() == HardwareMemory::BLOCK) || (mem->interface() == HardwareMemory::BOTH))) {
                fmodCC << compileModuleElementBlock(elm);
            }
        }

        // --- FILE access functions' definitions (only for AXI !!!!) ----------
        if(cfg->compile_type == CompilerConfiguration::AXI) {
            if(mem && ((mem->interface() == HardwareMemory::BLOCK) || (mem->interface() == HardwareMemory::BOTH))) {
                fmodCC << compileModuleElementFile(elm);
            }
        }

        // --- ADDR, OFFFSET, and SIZE constants' definition
        unsigned int num;
        num = (cfg->compile_type == CompilerConfiguration::AXI) ? elm->addr()/sizeof(uint32_t) : elm->addr();
        smodCC << "const unsigned int " << modName << "::" << elm->addrName() << " = " << hex(8) << num << dec << ";" << std::endl;
        if(elm->multiple()>1) {
            num = (cfg->compile_type == CompilerConfiguration::AXI) ? elm->offset()/sizeof(uint32_t) : elm->offset();
            smodCC << "const unsigned int " << modName << "::" << elm->offsetName() << " = " << hex(8) << num << dec << ";" << std::endl;
        }
        if(mem) {
            num = (cfg->compile_type == CompilerConfiguration::AXI) ? mem->size()/sizeof(uint32_t) : mem->size();
            smodCC << "const unsigned int " << modName << "::" << mem->sizeName() << " = " << hex(8) << num << dec << ";" << std::endl;
        }
    }

    // loop over all blocks
    for(iblk=blk->beginBlock(); iblk!=blk->endBlock(); iblk++) {
        compileModuleBlock(*iblk,smodHH,smodCC);
    }
}

//-----------------------------------------------------------------------------

void CompilerCCEmitter::compileModuleBlockList(const HardwareBlock* blk, CompilerStringList& par, const std::string& str, const bool nfl) {

    HardwareList<const HardwareBlock> blst;
    HardwareList<const HardwareBlock>::iterator ilst;
    const HardwareBlock* ablk;
    unsigned int iblk;

    blk->getBlockHierarchy(blst);

    if(nfl) {
        std::ostringstream buf;
        for(iblk=0, ilst=blst.begin(); ilst!=blst.end(); ilst++, iblk++) {
            ablk = *ilst;
            if(ablk->multiple() <= 1) continue;
            buf.str(""); buf << str << iblk;
            par.add(buf.str());
        }
    }
    else {
        for(iblk=0, ilst=blst.begin(); ilst!=blst.end(); ilst++, iblk++) {
            ablk = *ilst;
            if(ablk->multiple() <= 1) continue;
            par.add(str);
        }
    }
}

//-----------------------------------------------------------------------------

std::string CompilerCCEmitter::compileModuleBlockMultipleRange(const HardwareBlock* blk) {

    HardwareList<const HardwareBlock> blst;
    HardwareList<const HardwareBlock>::iterator ilst;
    const HardwareBlock* ablk;
    unsigned int iblk;
    std::ostringstream buf;

    blk->getBlockHierarchy(blst);

    for(iblk=0, ilst=blst.begin(); ilst!=blst.end(); ilst++, iblk++) {
        ablk = *ilst;
        if(ablk->multiple() <= 1) continue;

        // code for range checking
        buf << "    if(nbk" << iblk << " >= " << ablk->multipleName() << ") {" << std::endl
            << "        CERR(\"block \\\"" << ablk->nameName() << "\\\" number %d not in allowed range ([0..%d])\",nbk" << iblk << "," << ablk->multipleName() << "-1);" << std::endl
            << "        return(-1);" << std::endl
            << "    }" <<std::endl
            << std::endl;

    }

    return(buf.str());
}

//-----------------------------------------------------------------------------

std::string CompilerCCEmitter::compileModuleElementMultipleRange(const HardwareElement* elm) {

    // return if no multiple
    if(elm->multiple() <= 1) return("");

    std::ostringstream buf;
    const HardwareRegister* reg = dynamic_cast<const HardwareRegister*>(elm);
    const HardwareMemory* mem = dynamic_cast<const HardwareMemory*>(elm);
    std::string elmName;

    elmName = (reg) ? "register" : ((mem) ? mem->accessName() : "");

    // code for range checking
    buf << "    if(nelm >= " << elm->multipleName() << ") {" << std::endl
        << "        CERR(\"" << elmName << " number %d not in allowed range ([0..%d])\",nelm," << elm->multipleName() << "-1);" << std::endl
        << "        return(-1);" << std::endl
        << "    }" << std::endl
        << std::endl;

    return(buf.str());
}

//-----------------------------------------------------------------------------

std::string CompilerCCEmitter::compileModuleMemoryIndexRange(const HardwareMemory* mem) {

    // return if no memory
    if(!mem) return("");

    // return if not memory(RAM)
    if(mem->access() != HardwareMemory::RAM) return("");

    std::ostringstream buf;

    // code for index range checking
    buf << "    if(indx >= " << mem->indxName() << ") {" << std::endl
        << "        CERR(\"index %d not in allowed range ([0..%d])\",indx," << mem->indxName() << "-1);" << std::endl
        << "        return(-1);" << std::endl
        << "    }" <<std::endl
        << std::endl;

    return(buf.str());
}

//-----------------------------------------------------------------------------

std::string CompilerCCEmitter::compileModuleMemorySizeRange(const HardwareMemory* mem, const bool vect) {

    // return if no memory
    if(!mem) return("");

    const CompilerConfiguration* cfg = getCompiler()->getConfig();
    std::string memName, varName, sizName, errName;
    std::ostringstream buf;

    memName = (cfg->compile_type != CompilerConfiguration::AXI) ? "VME" : "AXI";
    varName = (cfg->compile_type != CompilerConfiguration::AXI) ? "vme_off" : "axi_off";
    sizName = vect ? "data.size()" : "size";
    errName = vect ? "vector size" : "requested size";

    // code for size range checking
    if(mem->access() == HardwareMemory::RAM) {
        buf << "    if((" << varName << " + " << sizName << ") > " << mem->indxName() << ") {" << std::endl;
        buf << "        CERR(\"" << memName << " address offset %d %d plus " << errName << " not in allowed range ([0..%d])\"," << varName << "," << sizName << "," << mem->indxName() << ");" << std::endl;
        buf << "        return(-1);" << std::endl;
        buf << "    }" <<std::endl;
        buf << std::endl;
    }
    if(mem->access() == HardwareMemory::FIFO) {
        buf << "    if(" << sizName << " > " << mem->indxName() << ") {" << std::endl;
        buf << "        CERR(\"" << errName << " %d not in allowed range ([0..%d])\"," << sizName << "," << mem->indxName() << ");" << std::endl;
        buf << "        return(-1);" << std::endl;
        buf << "    }" <<std::endl;
        buf << std::endl;
    }

    return(buf.str());
}

//-----------------------------------------------------------------------------
// COMPILE MODULE: ELEMENT READ/WRITE SINGLE FUNCTIONS
//-----------------------------------------------------------------------------

std::string CompilerCCEmitter::compileModuleElementSingle(const HardwareElement* elm) {

    const HardwareModule* mod = getCompiler()->getModule();
    const HardwareBlock* blk = elm->getBlock();
    const HardwareMemory* mem = dynamic_cast<const HardwareMemory*>(elm);
    const HardwareDataType* dat;
    const HardwareWord* wrd;
    const HardwareBitString* bit;

    std::string modName, elmType, elmMask, typName, datType, refType;
    std::ostringstream buf;
    CompilerStringList var; var.space(1);

    modName = mod->name();
    elmType = elm->nameName();
    elmMask = (elm->mask() != HardwareObject::DATA_TYPE_MMAX[elm->type()]) ? ("&"+elm->maskName()) : "";
    typName = HardwareObject::DATA_TYPE_TYPE[elm->type()];

    // variables
    compileModuleBlockList(blk,var,"const unsigned int nbk",true);
    if(elm->multiple()>1) var.add("const unsigned int nelm");
    if(mem && (mem->access()==HardwareMemory::RAM)) var.add("const unsigned int indx");

    for(unsigned int idt=0; idt<HardwareElement::NUM_DATA_TYPES; idt++) {
        dat = (idt == 0) ? elm->getDataType() : elm->getAlternativeDataType();
        if(!dat) continue;

        wrd = dynamic_cast<const HardwareWord*>(dat);
        bit = dynamic_cast<const HardwareBitString*>(dat);
        datType = dat->nameName();
        refType = (wrd) ? "" : ((bit) ? "&" : "");

        // --- READ SINGLE function definition
        if(elm->isReadable()) {
            buf << "//------------------------------------------------------------------------------" << std::endl
                << std::endl
                << "int " << modName << "::" << elmType << "_Read(" << var.rstr() << datType << "& data) const {" << std::endl
                << std::endl
                << compileModuleBlockMultipleRange(blk)
                << compileModuleElementMultipleRange(elm)
                << compileModuleMemoryIndexRange(mem)
                << "    " << typName << " rdat(0);" << std::endl
                << "    int rtnv(0);" << std::endl
                << std::endl
                << compileModuleHardwareReadSingle(elm); // --- HARDWARE READ SINGLE !!!!!!!!!!!!!!!!!!!!!!!!!!!!
            if(wrd) {
                buf << "    data = rdat" << elmMask << ";" << std::endl;
            }
            else if(bit) {
                buf << "    std::vector<unsigned int> r; r.push_back(static_cast<unsigned int>(rdat)" << elmMask << ");" << std::endl
                    << "    data = (RCD::BitSet)(r);" << std::endl;
            }
            buf << std::endl
                << "    return(rtnv);" << std::endl
                << "}" <<std::endl
                << std::endl;
        }

        // -- WRITE SINGLE function definition
       if(elm->isWritable()) {
            buf << "//------------------------------------------------------------------------------" << std::endl
                << std::endl
                << "int " << modName << "::" << elmType << "_Write(" << var.rstr() << "const " << datType << refType << " data) {" << std::endl
                << std::endl
                << compileModuleBlockMultipleRange(blk)
                << compileModuleElementMultipleRange(elm)
                << compileModuleMemoryIndexRange(mem);
            if(wrd) {
                buf << "    " << typName << " wdat;" << std::endl
                    << "    int rtnv(0);" << std::endl
                    << std::endl
                    << "    wdat = data" << elmMask << ";" << std::endl;
            }
            else if(bit) {
                buf << "    if(data.number().empty()) {" << std::endl
                    << "        CERR(\"bit string is empty\",\"\");" << std::endl
                    << "        return(-1);" << std::endl
                    << "    }" << std::endl
                    << std::endl
                    << "    std::vector<unsigned int> w = data.number();" << std::endl
                    << "    " << typName << " wdat;" << std::endl
                    << "    int rtnv(0);" << std::endl
                    << std::endl
                    << "    wdat = w[0]" << elmMask << ";" << std::endl;
            }
            buf << compileModuleHardwareWriteSingle(elm) // --- HARDWARE WRITE SINGLE !!!!!!!!!!!!!!!!!!!!!!!!!!!
                << std::endl
                << "    return(rtnv);" << std::endl
                << "}" << std::endl
                << std::endl;
        }
    }

    return(buf.str());
}

//-----------------------------------------------------------------------------
// COMPILE MODULE: ELEMENT READ/WRITE VECTOR FUNCTIONS
//-----------------------------------------------------------------------------

std::string CompilerCCEmitter::compileModuleElementVector(const HardwareElement* elm) {

    const HardwareMemory* mem = dynamic_cast<const HardwareMemory*>(elm);

    // return if not memeory
    if(!mem) return("");

    const CompilerConfiguration* cfg = getCompiler()->getConfig();
    const HardwareModule* mod = getCompiler()->getModule();
    const HardwareBlock* blk = elm->getBlock();
    const HardwareDataType* dat = elm->getDataType();

    std::string modName, elmType, sizName, datType;
    std::ostringstream buf;
    CompilerStringList var; var.space(1);

    modName = mod->name();
    elmType = elm->nameName();
    sizName = (mem->access()==HardwareMemory::RAM) ? mem->sizeName() : "size";
    datType = dat->nameName();

    // variables

    // --- READ VECTOR function definition
    if(elm->isReadable()) {
        compileModuleBlockList(blk,var,"const unsigned int nbk",true);
        if(elm->multiple()>1) var.add("const unsigned int nelm");
        var.add("std::vector<"+datType+">& data");
        if(mem->access() == HardwareMemory::RAM) var.add((cfg->compile_type != CompilerConfiguration::AXI)?"const unsigned int vme_off":"const unsigned int axi_off");
        buf << "//------------------------------------------------------------------------------" << std::endl
            << std::endl
            << "int " << modName << "::" << elmType << "_Read(" << var.str() << ") {" << std::endl
            << std::endl
            << compileModuleBlockMultipleRange(blk)
            << compileModuleElementMultipleRange(elm)
            << compileModuleMemorySizeRange(mem,true)
            << "    int rtnv(0);" << std::endl
            << std::endl
            << compileModuleHardwareReadVector(elm)// --- HARDWARE READ VECTOR !!!!!!!!!!!!!!!!!!!!!!!!!!
            << "    return(rtnv);" << std::endl
            << "}" <<std::endl
            << std::endl;
    }

    // --- WRITE VECTOR function definition
    if(elm->isWritable()) {
        var.clear();
        compileModuleBlockList(blk,var,"const unsigned int nbk",true);
        if(elm->multiple()>1) var.add("const unsigned int nelm");
        var.add("const std::vector<"+datType+">& data");
        if(mem->access() == HardwareMemory::RAM) var.add((cfg->compile_type != CompilerConfiguration::AXI)?"const unsigned int vme_off":"const unsigned int axi_off");
        buf << "//------------------------------------------------------------------------------" << std::endl
            << std::endl
            << "int " << modName << "::" << elmType << "_Write(" << var.str() << ") {" << std::endl
            << std::endl
            << compileModuleBlockMultipleRange(blk)
            << compileModuleElementMultipleRange(elm)
            << compileModuleMemorySizeRange(mem,true)
            << "    int rtnv(0);" << std::endl
            << std::endl
            << compileModuleHardwareWriteVector(elm)// --- HARDWARE WRITE VECTOR !!!!!!!!!!!!!!!!!!!!!!!!
            << "    return(rtnv);" << std::endl
            << "}" <<std::endl
            << std::endl;
    }

    return(buf.str());
}

//-----------------------------------------------------------------------------
// COMPILE MODULE: ELEMENT READ/WRITE BLOCK FUNCTIONS
//-----------------------------------------------------------------------------

std::string CompilerCCEmitter::compileModuleElementBlock(const HardwareElement* elm) {

    const HardwareMemory* mem = dynamic_cast<const HardwareMemory*>(elm);

    // return if not memory
    if(!mem) return("");

    const CompilerConfiguration* cfg = getCompiler()->getConfig();
    const HardwareModule* mod = getCompiler()->getModule();
    const HardwareBlock* blk = elm->getBlock();

    std::string modName, elmType;
    std::ostringstream buf;
    CompilerStringList var; var.space(1);

    modName = mod->name();
    elmType = elm->nameName();

    // variables
    compileModuleBlockList(blk,var,"const unsigned int nbk",true);
    if(elm->multiple()>1) var.add("const unsigned int nelm");
    if(cfg->compile_type != CompilerConfiguration::AXI) {
        var.add("RCD::CMEMSegment* seg");
    }
    else {
        var.add("uint32_t* data");
    }
    var.add("const unsigned int size");
    if(cfg->compile_type != CompilerConfiguration::AXI) {
        if(mem->access() == HardwareMemory::RAM) var.add("const unsigned int vme_off");
        var.add("const unsigned int seg_off");
    }
    else {
        if(mem->access() == HardwareMemory::RAM) var.add("const unsigned int axi_off");
        var.add("const unsigned int dat_off");
    }

    // --- READ BLOCK function definition
    if(elm->isReadable()) {
        buf << "//------------------------------------------------------------------------------" << std::endl
            << std::endl
            << "int " << modName << "::" << elmType << "_Read(" << var.str() << ") {" << std::endl
            << std::endl
            << compileModuleBlockMultipleRange(blk)
            << compileModuleElementMultipleRange(elm)
            << compileModuleMemorySizeRange(mem,false)
            << "    int rtnv(0);" << std::endl
            << std::endl
            << compileModuleHardwareReadBlock(elm)// --- HARDWARE READ BLOCK !!!!!!!!!!!!!!!!!!!!!!!!!!!!
            << "    return(rtnv);" << std::endl
            << "}" <<std::endl
            << std::endl;
    }

    // --- WRITE BLOCK function definition
    if(elm->isWritable()) {
        buf << "//------------------------------------------------------------------------------" << std::endl
            << std::endl
            << "int " << modName << "::" << elmType << "_Write(" << var.str() << ") {" << std::endl
            << std::endl
            << compileModuleBlockMultipleRange(blk)
            << compileModuleElementMultipleRange(elm)
            << compileModuleMemorySizeRange(mem,false)
            << "    int rtnv(0);" << std::endl
            << std::endl
            << compileModuleHardwareWriteBlock(elm)// --- HARDWARE WRITE BLOCK !!!!!!!!!!!!!!!!!!!!!!!!!!
            << "    return(rtnv);" << std::endl
            << "}" <<std::endl
            << std::endl;
    }

    return(buf.str());
}

//-----------------------------------------------------------------------------
// COMPILE MODULE: ELEMENT READ/WRITE FILE FUNCTIONS
//-----------------------------------------------------------------------------

std::string CompilerCCEmitter::compileModuleElementFile(const HardwareElement* elm) {

    const HardwareMemory* mem = dynamic_cast<const HardwareMemory*>(elm);

    // return if not memory
    if(!mem) return("");

    const CompilerConfiguration* cfg = getCompiler()->getConfig();
    const HardwareModule* mod = getCompiler()->getModule();
    const HardwareBlock* blk = elm->getBlock();

    std::string modName, elmType;
    std::ostringstream buf;
    CompilerStringList var; var.space(1);

    modName = mod->name();
    elmType = elm->nameName();

    // variables
    compileModuleBlockList(blk,var,"const unsigned int nbk",true);
    if(elm->multiple()>1) var.add("const unsigned int nelm");
    var.add("const std::string& fn");
    var.add("const unsigned int size");
    if(cfg->compile_type != CompilerConfiguration::AXI) {
        if(mem->access() == HardwareMemory::RAM) var.add("const unsigned int vme_off");
        var.add("const unsigned int seg_off");
    }
    else {
        if(mem->access() == HardwareMemory::RAM) var.add("const unsigned int axi_off");
    }

    // --- READ FILE function definition
    if(elm->isReadable()) {
        buf << "//------------------------------------------------------------------------------" << std::endl
            << std::endl
            << "int " << modName << "::" << elmType << "_Read(" << var.str() << ") {" << std::endl
            << std::endl
            << compileModuleBlockMultipleRange(blk)
            << compileModuleElementMultipleRange(elm)
            << compileModuleMemorySizeRange(mem,false)
            << "    int rtnv(0);" << std::endl
            << std::endl
            << compileModuleHardwareReadFile(elm)// --- HARDWARE READ BLOCK !!!!!!!!!!!!!!!!!!!!!!!!!!!!
            << "    return(rtnv);" << std::endl
            << "}" <<std::endl
            << std::endl;
    }

    // --- WRITE FILE function definition
    if(elm->isWritable()) {
        buf << "//------------------------------------------------------------------------------" << std::endl
            << std::endl
            << "int " << modName << "::" << elmType << "_Write(" << var.str() << ") {" << std::endl
            << std::endl
            << compileModuleBlockMultipleRange(blk)
            << compileModuleElementMultipleRange(elm)
            << compileModuleMemorySizeRange(mem,false)
            << "    int rtnv(0);" << std::endl
            << std::endl
            << compileModuleHardwareWriteFile(elm)// --- HARDWARE WRITE BLOCK !!!!!!!!!!!!!!!!!!!!!!!!!!
            << "    return(rtnv);" << std::endl
            << "}" <<std::endl
            << std::endl;
    }

    return(buf.str());
}

//=====================================================================================================================
//=====================================================================================================================

//-----------------------------------------------------------------------------
// COMPILE DATA TYPE
//-----------------------------------------------------------------------------

void CompilerCCEmitter::compileDataType() {

    HardwareCompiler::HardwareDataTypeMapConstIterator idat;
    HardwareList<const HardwareDataType>* datList;
    HardwareList<const HardwareDataType>::const_iterator jdat;
    const HardwareDataType* dat;

    // get all data types
    for(idat=getCompiler()->beginDataTypeMap(); idat!=getCompiler()->endDataTypeMap(); idat++) {
        datList = idat->second;
        dat = *(datList->begin());

        // get data type output files
        CompilerOutputFile* datHH;
        CompilerOutputFile* datCC;
        try {
            m_compiler->getDataTypeFiles(dat,&datHH,&datCC);
        }
        catch (const CompilerException& ce) {
            CERR("Exception \"%s\" while trying to get data files",ce.what());
            throw;
        }

        // write head
        compileDataTypeWriteHead(dat,datHH,datCC);

        // loop over all data of same type
        for(jdat=datList->begin(); jdat!=datList->end(); jdat++) {
            compileDataType(*jdat,datHH,datCC);
        }

        // write tail
        compileDataTypeWriteTail(dat,datHH,datCC);
    }
}

//-----------------------------------------------------------------------------
// COMPILE DATA TYPE - EACH DATA TYPE
//-----------------------------------------------------------------------------

void CompilerCCEmitter::compileDataType(const HardwareDataType* dat, CompilerOutputFile* datHH, CompilerOutputFile* datCC) {

    const HardwareBitString* bit;

    // attempt converting to BitString
    if((bit = dynamic_cast<const HardwareBitString*>(dat))) {
        compileBitString(bit,datHH,datCC);
    }
    else {
        CERR("Unknown HardwareDataType \"%s\"",dat->fileName().c_str());
        throw CompilerException("UNKNOWN DATA TYPE");
    }
}

//-----------------------------------------------------------------------------
// COMPILE DATA TYPE - BITSTRING
//-----------------------------------------------------------------------------

void CompilerCCEmitter::compileBitString(const HardwareBitString* bit, CompilerOutputFile* bitHH, CompilerOutputFile* bitCC) {

    const CompilerConfiguration* cfg = getCompiler()->getConfig();

    std::string bitName, fldName, fldForm, fldMask, fldMnam, mulName, valName;
    unsigned int typSize, fldMult;
    std::ostringstream sbitHH, sbitCC, vbitHH;
    CompilerStringList arg;
    arg.space(1);

    HardwareList<HardwareField>::const_iterator ifld;
    HardwareField* fld;
    HardwareList<HardwareValue>::const_iterator ival;
    HardwareValue* val;

    bitName = bit->nameName();
    typSize = (bit->getElement()) ? HardwareObject::DATA_TYPE_SIZE[bit->getElement()->type()] : sizeof(unsigned int)*HardwareCommon::BIT_SIZE;

    // get data type output streams
    std::ofstream& fbitHH(bitHH->file());
    std::ofstream& fbitCC(bitCC->file());

    // bit string class declaration
    fbitHH << "//------------------------------------------------------------------------------" << std::endl
           << std::endl
           << "class " << bitName << " : public RCD::BitSet {" << std::endl
           << std::endl
           << "  public:" << std::endl
           << std::endl;
    if(cfg->compile_enum) {
        fbitHH << compileBitStringEnumHead(bit);
    }
    fbitHH << "    // constructor and destructor" << std::endl
           << "    " << bitName << "();" << std::endl
           << "   ~" << bitName << "();" << std::endl
           << std::endl
           << "    // assignment operator" << std::endl
           << "    " << bitName << "& operator=(const RCD::BitSet&);" << std::endl
           << std::endl
           << "    // field functions" << std::endl;

    // bit string class definition
    fbitCC << std::endl
           << "//------------------------------------------------------------------------------" << std::endl
           << "//------------------------------------------------------------------------------" << std::endl
           << std::endl;
    if(cfg->compile_enum) {
        fbitCC << compileBitStringEnumBody(bit);
    }
    fbitCC << bitName << "::" << bitName << "() : RCD::BitSet(" << typSize << ") {" << std::endl
           << std::endl
           << "    // nothing to be done" << std::endl
           << "}" << std::endl
           << std::endl
           << "//------------------------------------------------------------------------------" << std::endl
           << std::endl
           << bitName << "::~" << bitName << "() {" << std::endl
           << std::endl
           << "    // nothing to be done" << std::endl
           << "}" << std::endl
           << std::endl
           << "//------------------------------------------------------------------------------" << std::endl
           << std::endl
           << bitName << "& " << bitName << "::operator=(const RCD::BitSet& bs) {" << std::endl
           << std::endl
           << "    if(this != &bs) {" << std::endl
           << "        m_number = bs.number();" << std::endl
           << "    }" << std::endl
           << "    return(*this);" << std::endl
           << "}" << std::endl;

    // for all FIELDS
    for(ifld=bit->beginField(); ifld!= bit->endField(); ifld++) {
        fld = *ifld;
        fldName = fld->name();
        fldMult = fld->multiple();
        fldMnam = fld->multipleName();
        fldForm = HardwareObject::DATA_FORM_TYPE[fld->form()];
        fldMask = fld->maskName();
        mulName = (fld->multiple()>1) ? ("["+fld->multipleName()+"]") : "";

        // --- FIELD MULT declaration/definition
        if(fld->multiple()>1) {
            fbitHH << "    static const unsigned int " << fldMnam << " = " << fldMult << ";" << std::endl;
        }

        // --- FIELD READ function declarations
        if(fld->isReadable()) {
            arg.clear();
            if(fld->multiple()>1) arg.add("const unsigned int");
            fbitHH << "    " << left(CPP_TAB_LENGTH) << fldForm << " " << fldName << "(" << arg.str() << ") const;" << std::endl;
        }

        // --- FIELD WRITE function declarations
        if(fld->isWritable()) {
            arg.clear();
            if(fld->multiple()>1) arg.add("const unsigned int");
            arg.add("const " + fldForm + ((fld->form()==HardwareObject::STRING)?"&":""));
            fbitHH << "    void         " << fldName << "(" << arg.str() << ");" << std::endl;
        }
        fbitHH << std::endl;

        // --- FIELD MASK constant declaration
        sbitHH << "    static const RCD::BitSet " << fldMask << mulName << ";" << std::endl;

        // --- FIELD MASK constant definition
        if(fld->multiple()>1) {
            sbitCC << "const RCD::BitSet " << bitName << "::" << fldMask << "[" << fldMnam << "] = {" << std::endl;
            RCD::BitSet msk;
            for(unsigned int i=0; i<fld->multiple(); i++) {
                msk = fld->mask() << (fld->offset()*i);
                msk.crop(bit->mask().size());
                sbitCC << "    RCD::BitSet(\"" << msk.string() << "\")" << (i==fldMult-1?"":",") << std::endl;
            }
            sbitCC << "};" << std::endl;
        }
        else {
            sbitCC << "const RCD::BitSet " << bitName << "::" << fldMask << " = RCD::BitSet(\"" << fld->mask().string() << "\");" << std::endl;
        }

        // --- FIELD MASK constant statics
        if(cfg->bitmask_statics) {
            if(fld->multiple()>1) {
                vbitHH << "    static constexpr unsigned int STAT_" << fldMask << "[" << fldMnam << "] = {" << std::endl;
                for(unsigned int i=0; i<fld->multiple(); i++) {
                    vbitHH << "        " << (fld->mask()<<(fld->offset()*i)).string() << (i==fldMult-1?"":",") << std::endl;
                }
                vbitHH << "    };" << std::endl;
            }
            else {
                vbitHH << "    static const unsigned int STAT_" << fldMask << " = " << fld->mask().string() << ";" << std::endl;
            }
        }

        // for all VALUES
        for(ival=fld->beginValue(); ival!= fld->endValue(); ival++) {
            val = *ival;
            valName = val->dataName();

            // --- VALUE constant declaration
            sbitHH << "    static const RCD::BitSet " << valName << mulName << ";" << std::endl;

            // --- VALUE constant definition
            sbitCC << "const RCD::BitSet " << bitName << "::" << valName << mulName << " = ";
            if(fld->multiple() > 1) {
                sbitCC << "{" << std::endl;
                for(unsigned int i=0; i<fld->multiple(); i++) {
                    sbitCC << "    RCD::BitSet(\"" << (val->data()<<fld->offset()*i).string() << "\")" << (i==fldMult-1?"":",") << std::endl;
                }
                sbitCC << "};" << std::endl;
            }
            else {
                sbitCC << "RCD::BitSet(\"" << val->data().string() << "\");" << std::endl;
            }
        }
        sbitHH << std::endl;

        // --- FIELD READ function definition
        if(fld->isReadable()) fbitCC << compileFieldRead(fld);

        // --- FIELD WRITE function definition
        if(fld->isWritable()) fbitCC << compileFieldWrite(fld);
    }

    // finish bitstring declaration
    fbitHH << "    // printing function" << std::endl;
    fbitHH << "    void print() const;" << std::endl;
    fbitHH << std::endl;
    if(cfg->bitmask_public) {
        fbitHH << "  public:" << std::endl;
    }
    else {
        fbitHH << "  private:" << std::endl;
    }
    fbitHH << std::endl;
    fbitHH << sbitHH.str();
    if(cfg->bitmask_statics) {
        fbitHH << vbitHH.str();
        fbitHH << std::endl;
    }
    fbitHH << "};     // class " << bitName << std::endl;
    fbitHH << std::endl;

    // --- bitstring PRINT function definition
    fbitCC << compileBitStringPrint(bit);

    // --- bitstring ADDR constants' definition
    fbitCC << "//------------------------------------------------------------------------------" << std::endl
           << std::endl
           << sbitCC.str();
}

//------------------------------------------------------------------------------

std::string CompilerCCEmitter::compileBitStringEnumHead(const HardwareBitString* bit) {

    HardwareList<HardwareField>::const_iterator ifld;
    HardwareField* fld;
    HardwareList<HardwareValue>::const_iterator ival;
    HardwareValue* val;

    std::ostringstream buf;
    unsigned int vnum;
    bool flag;

    // for all FIELDS
    for(ifld=bit->beginField(); ifld!= bit->endField(); ifld++) {
        fld = *ifld;
        vnum = 0;
        flag = true;

        // for all VALUES
        for(ival=fld->beginValue(); ival!= fld->endValue(); ival++) {
            val = *ival;
            if(flag) {
                buf << "    // type definition" << std::endl
                    << "    typedef enum { " << val->getField()->nameName() << "_" << val->nameName();
                flag = false;
            }
            else {
                buf << ",";
                if(vnum%8==7) buf << std::endl << "                   "; else buf << " ";
                buf << val->getField()->nameName() << "_" << val->nameName();
            }
            vnum++;
        }
        if(!flag) {
            if(vnum>7) buf << std::endl << "    "; else buf << " ";
            buf << "} " << fld->nameName() << "_TYPE;" << std::endl
                << "    static const unsigned int " << fld->nameName() << "_TYPE_SIZE = " << val->getField()->nameName() << "_" << val->nameName() << " + 1;" << std::endl
                << "    static const std::string  " << fld->nameName() << "_TYPE_NAME[" << fld->nameName() << "_TYPE_SIZE];" << std::endl
                << std::endl;
        }
    }

    return(buf.str());
}

//------------------------------------------------------------------------------

std::string CompilerCCEmitter::compileBitStringEnumBody(const HardwareBitString* bit) {

    HardwareList<HardwareField>::const_iterator ifld;
    HardwareField* fld;
    HardwareList<HardwareValue>::const_iterator ival;
    HardwareValue* val;

    std::ostringstream buf;
    unsigned int vnum;
    bool flag;

    // for all FIELDS
    for(ifld=bit->beginField(); ifld!= bit->endField(); ifld++) {
        fld = *ifld;
        vnum = 0;
        flag = true;

        // for all VALUES
        for(ival=fld->beginValue(); ival!= fld->endValue(); ival++) {
            val = *ival;
            if(flag) {
                buf << "const std::string " << bit->nameName() << "::" << val->getField()->nameName() << "_TYPE_NAME[" << val->getField()->nameName() << "_TYPE_SIZE] = {" << std::endl
                    << "    \"" << val->nameName() << "\"";
                flag = false;
            }
            else {
                buf << ",";
                if(vnum%8==7) buf << std::endl << "    "; else buf << " ";
                buf << "\"" << val->nameName() << "\"";
            }
            vnum++;
        }
        if(!flag) {
            buf << std::endl
                << "};" << std::endl
                << std::endl
                << "//------------------------------------------------------------------------------" << std::endl
                << std::endl;
        }
    }

    return(buf.str());
}

//------------------------------------------------------------------------------

std::string CompilerCCEmitter::compileFieldRead(const HardwareField* fld) {

    // return if fld empty
    if(!fld) return("");

    std::string bitName, fldName, fldMult, fldMask, fldForm, valName, datName, idxName;
    std::ostringstream buf;

    HardwareList<HardwareValue>::const_iterator ival;
    HardwareValue* val;

    bitName = fld->getBitString()->nameName();
    fldName = fld->name();
    fldMask = fld->maskName();
    fldMult = (fld->multiple()>1) ? "const unsigned int nfld" : "";
    fldForm = HardwareObject::DATA_FORM_TYPE[fld->form()];
    idxName = (fld->multiple()>1) ? "[nfld]" : "";

    // --- FIELD READ function definition
    buf << std::endl
        << "//------------------------------------------------------------------------------" << std::endl
        << std::endl
        << fldForm << " " << bitName << "::" << fldName << "(" << fldMult << ") const {" << std::endl
        << std::endl;
    if(fld->multiple() > 1) {
        buf << "    if(nfld > " << fld->multipleName() << ") {" << std::endl
            << "        CERR(\"number %d not in allowed range ([0..%d])\",nfld," << fld->multipleName() << "-1);" << std::endl
            << "        return(" << HardwareObject::DATA_FORM_RTNV[fld->form()] << ");" << std::endl
            << "    }" <<std::endl
            << std::endl;
    }

    // --- FIELD FORM = STRING
    if(fld->form() == HardwareObject::STRING) {
        bool notFoundFlag(false);
        unsigned int valCount(0);
        buf << "    std::string str;" << std::endl
            << std::endl;

        // for all VALUES
        for(ival=fld->beginValue(); ival!= fld->endValue(); ival++) {
            val = *ival;
            valName = val->name();
            datName = val->dataName();

            if(valName == "____NOT_FOUND____") {
                notFoundFlag = true;
                continue;
            }

            if(valCount == 0) {
                buf << "    if(((*this) & " << fldMask << idxName << ") == " << datName << idxName << ") {" << std::endl;
            }
            else {
                buf << "    else if(((*this) & " << fldMask << idxName << ") == " << datName << idxName << ") {" << std::endl;
            }
            valCount++;
            buf << "        str = \"" << valName << "\";" << std::endl;
            buf << "    }" << std::endl;
        }

        if(valCount) {
            buf << "    else {" << std::endl;
            if(notFoundFlag) {
                buf << "        str = \"____NOT_FOUND____\";" << std::endl;
            }
            else {
                buf << "        CERR(\"finding value for data \\\"%s\\\"\",((*this) & " << fldMask << idxName << ").string().c_str());" << std::endl;
            }
            buf << "    }" << std::endl;
        }
        else {
            if(notFoundFlag) {
                buf << "        str = \"____NOT_FOUND____\";" << std::endl;
            }
            else {
                buf << "    CERR(\"no value defined\",\"\");" << std::endl;
            }
        }
        buf << std::endl;
        buf << "    return(str);" << std::endl;

    }

    // --- FIELD FORM = NUMBER
    else if(fld->form() == HardwareObject::NUMBER) {
        buf << "    BitSet obs(m_size);" << std::endl
            << "    int idx = 0;" << std::endl
            << std::endl
            << "    for(size_t i = 0; i < m_size; i++) {" << std::endl
            << "        if(" << fldMask << idxName << "[i] == 1) {" << std::endl
            << "            obs[idx] = (*this)[i];" << std::endl
            << "            idx++;" << std::endl
            << "         }" << std::endl
            << "    }" <<std::endl
            << std::endl
            << "    return(obs.number()[0]);" << std::endl;
    }

    // --- FIELD FORM = BOOLEAN
    else if(fld->form() == HardwareObject::BOOLEAN) {
        buf << "    return(((*this) & " << fldMask << idxName << ") == " << fldMask << idxName << ");" << std::endl;
    }

    // --- FIELD FORM = ASCII
    else if(fld->form() == HardwareObject::ASCII) {
        buf << "    BitSet obs(m_size);" << std::endl;
        buf << "    int idx(0);" << std::endl;
        buf << "    std::string str;" << std::endl;
        buf << std::endl;
        buf << "    for(size_t i=0; i<m_size; i++) {" << std::endl;
        buf << "        if(" << fldMask << idxName << "[i] == 1) {" << std::endl;
        buf << "            obs[idx] = (*this)[i];" << std::endl;
        buf << "            if(idx == 7) {" << std::endl;
        buf << "                unsigned int dat(obs.number()[0]);" << std::endl;
        buf << "                str.insert(0,1,(dat<32) ? \'?\' : ((dat>=127) ? \'.\' : (char)dat));" << std::endl;
        buf << "                obs.reset();" << std::endl;
        buf << "                idx = 0;" << std::endl;
        buf << "            }" << std::endl;
        buf << "            else {" << std::endl;
        buf << "                idx++;" << std::endl;
        buf << "            }" << std::endl;
        buf << "        }" << std::endl;
        buf << "    }" <<std::endl;
        buf << std::endl;
        buf << "    return(str);" << std::endl;
    }

    buf << "}" << std::endl;

    return(buf.str());
}

//------------------------------------------------------------------------------

std::string CompilerCCEmitter::compileFieldWrite(const HardwareField* fld) {

    // return if field empty
    if(!fld) return("");

    std::string bitName, fldName, fldMask, fldMult, fldForm, valName, datName, idxName;
    std::ostringstream buf;

    HardwareList<HardwareValue>::const_iterator ival;
    HardwareValue* val;

    bitName = fld->getBitString()->nameName();
    fldName = fld->name();
    fldMask = fld->maskName();
    fldMult = (fld->multiple()>1) ? "const unsigned int nfld, " : "";
    fldForm = HardwareObject::DATA_FORM_TYPE[fld->form()];
    if(fld->form() == HardwareObject::STRING) fldForm += "&";
    idxName = (fld->multiple()>1) ? "[nfld]" : "";

    // --- FIELD WRITE function definition
    buf << std::endl
        << "//------------------------------------------------------------------------------" << std::endl
        << std::endl
        << "void " << bitName << "::" << fldName << "(" << fldMult << "const " << fldForm << " value) {" << std::endl
        << std::endl;
    if(fld->multiple() > 1) {
        buf << "    if(nfld > " << fld->multipleName() << ") {" << std::endl
            << "        CERR(\"number %d not in allowed range ([0..%d])\",nfld," << fld->multipleName() << "-1);" << std::endl
            << "        return;" << std::endl
            << "    }" <<std::endl
            << std::endl;
    }

    // --- FIELD FORM = STRING
    if(fld->form() == HardwareObject::STRING) {

         // --- for all VALUES
        for(ival=fld->beginValue(); ival!= fld->endValue(); ival++) {
            val = *ival;
            valName = val->name();
            datName = val->dataName();

            if(fld->firstValue(val)) {
                buf << "    if(value == \"" << valName << "\") {" << std::endl;
            }
            else {
                buf << "    else if(value == \"" << valName << "\") {" << std::endl;
            }
            buf << "        *this = (*this & (~" << fldMask << idxName << ")) | " << datName << idxName << ";" << std::endl
                << "    }" << std::endl;
        }

        if(!buf.str().empty()) {
            buf << "    else {" << std::endl
                << "        CERR(\"finding value \\\"%s\\\"\",value.c_str());" << std::endl
                << "    }" << std::endl;
        }
        else {
            buf << "    CERR(\"no value defined\",\"\");" << std::endl;
        }
    }

    // --- FIELD FORM = NUMBER
    else if(fld->form() == HardwareObject::NUMBER) {
        buf << "    std::vector<unsigned int> v; v.push_back(value);" << std::endl
            << "    BitSet ibs(v);" << std::endl
            << "    int idx = 0;" << std::endl
            << std::endl
            << "    for(size_t i = 0; i < m_size; i++) {" << std::endl
            << "        if(" << fldMask << idxName << "[i] == 1) {" << std::endl
            << "            (*this)[i] = ibs[idx];" << std::endl
            << "            idx++;" << std::endl
            << "        }" << std::endl
            << "    }" << std::endl;
    }

    // --- FIELD FORM = BOOLEAN
    else if(fld->form() == HardwareObject::BOOLEAN) {
        buf << "    (*this) = value ? ((*this) | " << fldMask << idxName << ") : ((*this) & (~" << fldMask << idxName << "));" << std::endl;
    }

    // --- FIELD FORM = ASCII
    else if(fld->form() == HardwareObject::ASCII) {
        buf << "    std::string str(value);" << std::endl;
        buf << "    int idx(0);" << std::endl;
        buf << "    std::vector<unsigned int> vct;" << std::endl;
        buf << "    BitSet ibs(vct);" << std::endl;
        buf << std::endl;
        buf << "    for(size_t i=0; i<m_size; i++) {" << std::endl;
        buf << "        if(" << fldMask << idxName << "[i] == 1) {" << std::endl;
        buf << "            if(idx == 0) {" << std::endl;
        buf << "                ibs.reset(); vct.clear();" << std::endl;
        buf << "                unsigned int dat(str[str.size()-1]); str.pop_back();" << std::endl;
        buf << "                vct.push_back(dat); ibs = vct;" << std::endl;
        buf << "            }" << std::endl;
        buf << "            (*this)[i] = ibs[idx];" << std::endl;
        buf << "            idx++;" << std::endl;
        buf << "            if(idx == 8) idx = 0;" << std::endl;
        buf << "        }" << std::endl;
        buf << "    }" << std::endl;
    }

    buf << "}" << std::endl;

    return(buf.str());
}

//------------------------------------------------------------------------------

std::string CompilerCCEmitter::compileBitStringPrint(const HardwareBitString* bit) {

    // return if bit string empty
    if(!bit) return("");

    std::string bitName;
    std::ostringstream buf;
    unsigned int fldSize;

    HardwareList<HardwareField>::const_iterator ifld;
    HardwareField* fld;

    bitName = bit->nameName();

    // --- BITSTRING PRINT function definition
    buf << std::endl
        << "//------------------------------------------------------------------------------" << std::endl
        << std::endl
        << "void " << bitName << "::print() const {" << std::endl
        << std::endl
        << "    std::ostringstream fld, val, num;" << std::endl
        << "    val << std::hex << std::setfill('0');" << std::endl
        << std::endl;
// 15-MAR-2018, R. Spiwoks
//    buf << "    std::cout << std::endl;" << std::endl;
    buf << "    std::cout << \"  \\\"" << bitName << "\\\": \" << string() << std::endl;" << std::endl
        << std::endl;

    // for all FIELDS
    for(ifld=bit->beginField(); ifld!= bit->endField(); ifld++) {
        fld = *ifld;
        if(!fld->isReadable()) continue;

        // --- FIELD PRINT function definition
        fldSize = HardwareCommon::getMaskValue(fld->mask());
        if(fld->multiple() > 1) {
            buf << "    for(unsigned int i=0; i<" << fld->multipleName() << "; i++) {" << std::endl
                << "        fld.str(\"\"); fld << \"\\\"" << fld->name() << "[\" << i << \"]\\\"\";" << std::endl;
            if(fld->form() == HardwareObject::NUMBER) {
                buf << "        val.str(\"\"); val << \"\\\"0x\" << std::setw(" << HardwareCommon::getHexNumber(fldSize) << ") << "
                    << fld->name() << "(i) << \"\\\"\";" << std::endl
                    << "        num.str(\"\"); num << \" (= \" << std::setw(" << HardwareCommon::getDecNumber(fldSize) << ") << "
                    << fld->name() << "(i) << \")\";" << std::endl;
            }
            else {
                buf << "        val.str(\"\"); val << std::boolalpha << \"\\\"\" << " << fld->name() << "(i) << \"\\\"\"; num.str(\"\");" << std::endl;
            }
            buf << "        std::cout << \"    \" << std::setw(28) << std::left << fld.str() << \": \""
                << " << std::setw(20) << std::right << val.str() << num.str() << std::endl;" << std::endl
                << "    }" << std::endl;
        }
        else {
            buf << "    fld.str(\"\"); fld << \"\\\"" << fld->name() << "\\\"\";" << std::endl;
            if(fld->form() == HardwareObject::NUMBER) {
                buf << "    val.str(\"\"); val << \"\\\"0x\" << std::setw(" << HardwareCommon::getHexNumber(fldSize) << ") << "
                    << fld->name() << "() << \"\\\"\";" << std::endl
                    << "    num.str(\"\"); num << \" (= \" << std::setw(" << HardwareCommon::getDecNumber(fldSize) << ") << "
                    << fld->name() << "() << \")\";" << std::endl;
            }
            else {
                buf << "    val.str(\"\"); val << std::boolalpha << \"\\\"\" << " << fld->name() << "() << \"\\\"\"; num.str(\"\");" << std::endl;
            }
            buf << "    std::cout << \"    \" << std::setw(28) << std::left << fld.str() << \": \""
                << " << std::setw(20) << std::right << val.str() << num.str() << std::endl;" << std::endl;
        }
    }

    buf << "}" << std::endl
        << std::endl;

    return(buf.str());
}

//------------------------------------------------------------------------------
// COMPILE DATA TYPE - OPEN FILE
//------------------------------------------------------------------------------

void CompilerCCEmitter::compileDataTypeWriteHead(const HardwareDataType* dat, CompilerOutputFile* datHH, CompilerOutputFile* datCC) {

    // get data output streams
    std::ofstream& fdatHH(datHH->file());
    std::ofstream& fdatCC(datCC->file());
    const CompilerConfiguration* cfg = getCompiler()->getConfig();

    const HardwareBitString* bit = dynamic_cast<const HardwareBitString*>(dat);

    if(bit) {
        if(cfg->compile_type != CompilerConfiguration::AXI) {
            fdatHH << "#include \"RCDBitString/BitSet.h\"" << std::endl;
            fdatHH << std::endl;
        }
        else {
            fdatHH << "#include \"RCDBitString/BitSet.h\"" << std::endl;
            fdatHH << std::endl;
        }
    }
    fdatHH << "namespace " << cfg->namespace_name << " {" << std::endl;
    fdatHH << std::endl;

    if(cfg->compile_type != CompilerConfiguration::AXI) {
        fdatCC << "#include <" << cfg->package_name << "/" << datHH->name() << ">" << std::endl;
        fdatCC << "#include \"RCDUtilities/RCDUtilities.h\"" << std::endl;
    }
    else {
        fdatCC << "#include \"" << cfg->package_name << "/" << datHH->name() << "\"" << std::endl;
        fdatCC << std::endl;
        fdatCC << "#ifdef __ZYNQ__" << std::endl;
        fdatCC << "#include \"zynq_log/Logging.h\"" << std::endl;
        fdatCC << "#else" << std::endl;
        fdatCC << "#include \"RCDUtilities/RCDUtilities.h\"" << std::endl;
        fdatCC << "#endif" << std::endl;
    }
    fdatCC << std::endl;
    fdatCC << "#include <iostream>" << std::endl;
    fdatCC << "#include <iomanip>" << std::endl;
    fdatCC << "#include <sstream>" << std::endl;
    fdatCC << std::endl;
    fdatCC << "using namespace " << cfg->namespace_name << ";" << std::endl;
}

//------------------------------------------------------------------------------
// COMPILE DATA TYPE - CLOSE FILE
//------------------------------------------------------------------------------

void CompilerCCEmitter::compileDataTypeWriteTail(const HardwareDataType* /* dat */, CompilerOutputFile* datHH, CompilerOutputFile* /* datCC */) {

    // get data output stream
    std::ofstream& fdatHH(datHH->file());
    const CompilerConfiguration* cfg = getCompiler()->getConfig();

    fdatHH << "}       // namespace " << cfg->namespace_name << std::endl
           << std::endl;
}

//=====================================================================================================================
//=====================================================================================================================

//-----------------------------------------------------------------------------
// COMPILE MENU
//-----------------------------------------------------------------------------

void CompilerCCEmitter::compileMenu(const HardwareModule* mod) {

    // get menu output files
    CompilerOutputFile* menCC;
    try {
        m_compiler->getMenuFile(&menCC);
    }
    catch (const CompilerException& ce) {
        CERR("Exception \"%s\" while trying to get module and menu files",ce.what());
        throw;
    }

    // get menu output streams
    std::ofstream& fmenCC(menCC->file());

    const CompilerConfiguration* cfg = getCompiler()->getConfig();
    std::string modName, sizName, hdrName, blkName, itmName, ddtName, typName, menName;
    std::ostringstream smenCC;

    HardwareList<HardwareBlock>::const_iterator iblk;

    modName = mod->name();
    m_compiler->getModuleHeaderFileName(hdrName);

    fmenCC << "#include <" << cfg->package_name << "/" << hdrName << ">" << std::endl
           << "#include \"RCDUtilities/RCDUtilities.h\"" << std::endl
           << "#include \"RCDMenu/RCDMenu.h\"" << std::endl
           << std::endl
           << "#include <cstdlib>" << std::endl
           << "#include <sstream>" << std::endl
           << "#include <fstream>" << std::endl
           << "#include <iomanip>" << std::endl
           << std::endl
           << "using namespace " << cfg->namespace_name << ";" << std::endl
           << std::endl
           << compileMenuHardwareGlobals(mod);                  // --- MENU: HARDWARE GLOBALS !!!!!!!!!!!!!!!!!!!!!!!!!!

    if(cfg->compile_type != CompilerConfiguration::IPB) {
        fmenCC << "// CMEM segment" << std::endl
               << "std::string nam;" << std::endl
               << "RCD::CMEMSegment* seg = 0;" << std::endl
               << std::endl;
    }

    fmenCC << "// " << modName << " module" << std::endl
           << modName << "* mod;" << std::endl
           << std::endl
           << "//------------------------------------------------------------------------------" << std::endl
           << std::endl
           << "class " << modName << "Open : public RCD::MenuItem {" << std::endl
           << "  public:" << std::endl
           << "    " << modName << "Open() { setName(\"open " << modName << "\"); }" << std::endl
           << "    int action() {" << std::endl
           << std::endl
           << compileMenuHardwareOpen(mod);                     // --- MENU: HARDWARE OPEN !!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    if(cfg->compile_type != CompilerConfiguration::IPB) {
        fmenCC << "        // create CMEM segment" << std::endl;
        fmenCC << "        if(mod->mmax()) {" << std::endl;
        fmenCC << "            nam = \"menu" << modName << "\";" << std::endl;
        if(cfg->menu_cmem_bpa == CompilerConfiguration::MBPA_NO) {
            fmenCC << "            seg =  new RCD::CMEMSegment(nam,sizeof(unsigned int)*mod->mmax());" << std::endl;
        }
        else if(cfg->menu_cmem_bpa == CompilerConfiguration::MBPA_YES) {
            fmenCC << "            seg =  new RCD::CMEMSegment(nam,sizeof(unsigned int)*mod->mmax(),true);" << std::endl;
        }
        else {
            fmenCC << "            bool bpa = (bool)enterInt(\"CMEM/BPA flag [0=NO,1=YES]\",0,1);" << std::endl;
            fmenCC << "            seg =  new RCD::CMEMSegment(nam,sizeof(unsigned int)*mod->mmax(),bpa);" << std::endl;
        }
        fmenCC << "            COUT(\"opened CMEM segment \\\"%s\\\" at pci = 0x%08x, virt = 0x%08x, size = 0x%08x - %s\",nam.c_str(),seg->PhysicalAddress(),seg->VirtualAddress(),seg->Size(),seg->BPA()?\"BPA\":\"NO_BPA\");" << std::endl;
        fmenCC << "        }" <<std::endl;
        fmenCC << std::endl;
    }

    fmenCC << "        return(0);" << std::endl
           << "    }" << std::endl
           << "};" << std::endl
           << std::endl
           << "//------------------------------------------------------------------------------" << std::endl
           << std::endl
           << "class " << modName << "Close : public RCD::MenuItem {" << std::endl
           << "  public:" << std::endl
           << "    " << modName << "Close() { setName(\"close " << modName << "\"); }" << std::endl
           << "    int action() {" << std::endl
           << std::endl;

    if(cfg->compile_type != CompilerConfiguration::IPB) {
        fmenCC << "        // delete CMEM segment" << std::endl
               << "        if(seg) {" << std::endl
               << "            delete seg;" << std::endl
               << "            COUT(\"closed CMEM segment \\\"%s\\\"\",nam.c_str());" << std::endl
               << "        }" << std::endl
               << std::endl;
    }

    fmenCC << compileMenuHardwareClose(mod)                     // --- MENU: HARDWARE CLOSE !!!!!!!!!!!!!!!!!!!!!!!!!!!!
           << "        return(0);" << std::endl
           << "    }" << std::endl
           << "};" << std::endl
           << std::endl;

    // loop over all blocks
    for(iblk=mod->beginBlock(); iblk!=mod->endBlock(); iblk++) {
        compileMenuBlock(*iblk,smenCC);
    }

    // add items to menu
    fmenCC << "//------------------------------------------------------------------------------" << std::endl
           << std::endl
           << "int main() {" <<std::endl
           << std::endl
           << "    RCD::Menu menu(\"" << mod->name() << " main menu\");" << std::endl
           << std::endl
           << "    // menu for " << mod->name() << std::endl
           << "    menu.init(new " << mod->name() << "Open);" << std::endl
           << smenCC.str() << std::endl
           << "    menu.exit(new " << mod->name() << "Close);" << std::endl
           << std::endl
           << "    // execute menu" << std::endl
           << "    menu.execute();" << std::endl
           << std::endl
           << "    exit(0);" << std::endl
           << "}" <<std::endl;
}

//-----------------------------------------------------------------------------

void CompilerCCEmitter::compileMenuBlock(const HardwareBlock* blk, std::ostringstream& smenCC) {

    // get menu output files
    CompilerOutputFile* menCC;
    try {
        m_compiler->getMenuFile(&menCC);
    }
    catch (const CompilerException& ce) {
        CERR("Exception \"%s\" while trying to get module and menu files",ce.what());
        throw;
    }

    // get menu output streams
    std::ofstream& fmenCC(menCC->file());

    const CompilerConfiguration* cfg = getCompiler()->getConfig();
    const HardwareModule* mod = getCompiler()->getModule();
    std::string sizName, hdrName, blkName, itmName, ddtName, typName, menName, uppMenu;

    HardwareList<HardwareBlock>::const_iterator iblk;
    const HardwareBlock* upp;
    HardwareList<HardwareElement>::const_iterator ielm;
    const HardwareElement* elm;
    const HardwareMemory* mem;
    const HardwareDataType* dat;
    const HardwareWord* wrd;
    const HardwareBitString* bit;

    upp = blk->getUpperBlock();
    blkName = blk->nameName();
    menName = "submenu" + blk->nameName();

    // MENU definition list
    if(blk->name() == "") {
        smenCC << std::endl
               << "    // menu for <anonymous> block" << std::endl
               << "    RCD::Menu " << menName << "(\"" << " <anonymous> menu\");" << std::endl
               << "    menu.add(&" << menName <<");" << std::endl;
    }
    else {
        if(upp == 0) {
            smenCC << std::endl
                   << "    // menu for \"" << blk->name() << "\" block" << std::endl
                   << "    RCD::Menu " << menName << "(\"" << blk->name() << " menu\");" << std::endl
                   << "    menu.add(&" << menName <<");" << std::endl;
        }
        else {
            uppMenu = "submenu" + upp->nameName();
            smenCC << std::endl
                   << "    // menu for \"" << blk->menuName() << "\" block" << std::endl
                   << "    RCD::Menu " << menName << "(\"" << blk->name() << " menu\");" << std::endl
                   << "    " << uppMenu << ".add(&" << menName <<");" << std::endl;
        }
    }

    // loop over all elements
    for(ielm=blk->beginElement(); ielm!=blk->endElement(); ielm++) {
        elm = *ielm;
        itmName = mod->name() + elm->nameName();
        mem = dynamic_cast<const HardwareMemory*>(elm);

        // --- menu items for SINGLE access functions
        fmenCC << compileMenuElementSingle(elm);
        for(unsigned int idt=0; idt<HardwareElement::NUM_DATA_TYPES; idt++) {
            dat = (idt == 0) ? elm->getDataType() : elm->getAlternativeDataType();
            if(!dat) continue;

            wrd = dynamic_cast<const HardwareWord*>(dat);
            bit = dynamic_cast<const HardwareBitString*>(dat);
            ddtName = itmName + ((wrd) ? "Number" : ((bit) ? "Bitstring" : ""));

            if(elm->isReadable()) {
                smenCC << "    " << menName << ".add(new " << ddtName << "Read);" << std::endl;
            }
            if(elm->isWritable()) {
                smenCC << "    " << menName << ".add(new " << ddtName << "Write);" << std::endl;
            }
        }

        // --- menu items for VECTOR access functions
        if(mem && ((mem->interface() == HardwareMemory::VECTOR) || (mem->interface() == HardwareMemory::BOTH))) {
            fmenCC << compileMenuElementVector(elm);
            if(elm->isReadable()) {
                smenCC << "    " << menName << ".add(new " << itmName << "ReadVector);" << std::endl;
            }
            if(elm->isWritable()) {
                smenCC << "    " << menName << ".add(new " << itmName << "WriteVector);" << std::endl;
            }
        }

        // --- menu items for BLOCK access functions
        if(mem && ((mem->interface() == HardwareMemory::BLOCK) || (mem->interface() == HardwareMemory::BOTH))) {
            if(cfg->compile_type != CompilerConfiguration::IPB) {
                fmenCC << compileMenuElementBlock(elm);
                if(elm->isReadable()) {
                    smenCC << "    " << menName << ".add(new " << itmName << "ReadBlock);" << std::endl;
                }
                if(elm->isWritable()) {
                    smenCC << "    " << menName << ".add(new " << itmName << "WriteBlock);" << std::endl;
                }
            }
            fmenCC << compileMenuElementFile(elm);
            if(elm->isReadable()) {
                smenCC << "    " << menName << ".add(new " << itmName << "ReadFile);" << std::endl;
            }
            if(elm->isWritable()) {
                smenCC << "    " << menName << ".add(new " << itmName << "WriteFile);" << std::endl;
            }
        }
    }

    // loop over all blocks
    for(iblk=blk->beginBlock(); iblk!=blk->endBlock(); iblk++) {
        compileMenuBlock(*iblk,smenCC);
    }
}

//-----------------------------------------------------------------------------

std::string CompilerCCEmitter::compileMenuBlockListForm(const HardwareBlock* blk) {

    HardwareList<const HardwareBlock> blst;
    HardwareList<const HardwareBlock>::iterator ilst;
    const HardwareBlock* ablk;
    unsigned int iblk;
    std::ostringstream buf;

    blk->getBlockHierarchy(blst);

    for(iblk=0, ilst=blst.begin(); ilst!=blst.end(); ilst++, iblk++) {
        ablk = *ilst;
        if(ablk->multiple() <= 1) continue;
        buf << "\\\"" << ablk->nameName() << "[%d]\\\"/";
    }

    return(buf.str());
}

//-----------------------------------------------------------------------------

std::string CompilerCCEmitter::compileMenuBlockMultipleEnter(const HardwareBlock* blk) {

    HardwareList<const HardwareBlock> blst;
    HardwareList<const HardwareBlock>::iterator ilst;
    const HardwareModule* mod = getCompiler()->getModule();
    const HardwareBlock* ablk;
    unsigned int iblk;
    std::ostringstream buf;

    blk->getBlockHierarchy(blst);

    for(iblk=0, ilst=blst.begin(); ilst!=blst.end(); ilst++, iblk++) {
        ablk = *ilst;
        if(ablk->multiple() <= 1) continue;

        // code for entering block index
        buf << "        unsigned int nbk" << iblk << " = enterInt(\"\\\"" << ablk->nameName() << "\\\" index\",0," << (mod->name()+"::"+ablk->multipleName()) << "-1);" << std::endl;
    }

    return(buf.str());
}

//-----------------------------------------------------------------------------

std::string CompilerCCEmitter::compileMenuElementMultipleEnter(const HardwareElement* elm) {

    // return if no multiple
    if(elm->multiple() <= 1) return("");

    const HardwareModule* mod = getCompiler()->getModule();

    std::ostringstream buf;

    // code for range checking
    buf << "        unsigned int nelm = enterInt(\"\\\"" << elm->nameName() << "\\\" index\",0," << (mod->name()+"::"+elm->multipleName()) << "-1);" << std::endl;

    return(buf.str());
}

//-----------------------------------------------------------------------------

std::string CompilerCCEmitter::compileMenuMemoryIndexEnter(const HardwareMemory* mem) {

    // return if no memory
    if(!mem) return("");

    // return if not memory(RAM)
    if(mem->access() != HardwareMemory::RAM) return("");

    const HardwareModule* mod = getCompiler()->getModule();

    std::ostringstream buf;

    // code for index entering
    buf << "        unsigned int indx = enterInt(\"\\\"" << mem->nameName() << "\\\" address index \",0," << (mod->name()+"::"+mem->indxName()) << "-1);" << std::endl;

    return(buf.str());
}

//-----------------------------------------------------------------------------

std::string CompilerCCEmitter::compileMenuElementWriteForm(const HardwareElement* elm) {

    const HardwareBlock* blk = elm->getBlock();
    bool bflg, eflg(elm->multiple()>1);
    HardwareList<const HardwareBlock> blst;
    HardwareList<const HardwareBlock>::iterator ilst;
    const HardwareBlock* ablk;
    unsigned int iblk;

    // check if at least one multiple in block hierarchy
    blk->getBlockHierarchy(blst);
    for(bflg=false, ilst=blst.begin(); ilst!=blst.end(); ilst++) {
        ablk = *ilst;
        if(ablk->multiple() > 1) {
            bflg = true;
            break;
        }
    }

    // return if no multiple
    if(!bflg && !eflg) return("");

    const HardwareMemory* mem = dynamic_cast<const HardwareMemory*>(elm);
    const HardwareWord* wrd = dynamic_cast<const HardwareWord*>(elm->getDataType());
    const HardwareBitString* bit = dynamic_cast<const HardwareBitString*>(elm->getDataType());
    std::ostringstream buf;

    buf << "        std::ostringstream buf; buf << \"";
    bflg = false;
    for(iblk=0, ilst=blst.begin(); ilst!=blst.end(); ilst++, iblk++) {
        ablk = *ilst;
        if(ablk->multiple() <= 1) continue;

        // add block name
        if(bflg) buf << "/";
        buf << "\\\"" << ablk->nameName() << "[\" << nbk" << iblk << " << \"]\\\"";
        bflg = true;
    }

    // add element name
    if(bflg && eflg) buf << "/";
    if(eflg) buf << "\\\"" << elm->nameName() << "[\" << nelm << \"]\\\"";
    buf << " " << ((wrd)?"data":((bit)?"bit string":"")) << ((mem&&(mem->access()==HardwareMemory::RAM))?"[\" << indx << \"]":"") << "\";\n";

    return(buf.str());
}

//------------------------------------------------------------------------------

std::string CompilerCCEmitter::compileMenuDataTypeWriteItem(const HardwareDataType* dat) {

    // convert to bit string
    const HardwareBitString* bit = dynamic_cast<const HardwareBitString*>(dat);

    // return if no bit string
    if(!bit) return("");

    const CompilerConfiguration* cfg = getCompiler()->getConfig();

    std::ostringstream buf;
    std::string fldName;
    unsigned int fldMult, fldMask, i;
    CompilerStringList lst;

    HardwareList<HardwareField>::const_iterator ifld;
    HardwareField* fld;
    HardwareList<HardwareValue>::const_iterator ival;
    HardwareValue* val;

    // loop over all fields
    for(ifld=bit->beginField(); ifld!= bit->endField(); ifld++) {
        fld = *ifld;
        if(!fld->isWritable()) continue;

        fldName = fld->name();
        fldMult = fld->multiple();

        // field has form STRING
        if(fld->form() == HardwareObject::STRING) {

            // loop over all values
            lst.clear();
            for(ival=fld->beginValue(); ival!= fld->endValue(); ival++) {
                val = *ival;
                lst.add("\\\""+val->name()+"\\\"");
            }
            if(fldMult>1) {
                for(i=0; i<fldMult; i++) {
                   buf << "        data." << fldName << "(" << i << ",enterString(\"\\\"" << fldName << "[" << i << "]\\\" value (" << lst.str() << ")\"));" << std::endl;
                }
            }
            else {
                buf << "        data." << fldName << "(enterString(\"\\\"" << fldName << "\\\" value (" << lst.str() << ")\"));" << std::endl;
            }
        }

        // field has form NUMBER
        else if(fld->form() == HardwareObject::NUMBER) {
            fldMask = HardwareCommon::getMaskValue(fld->mask());
            if(fldMult>1) {
                for(i=0; i<fldMult; i++) {
                   buf << "        data." << fldName << "(" << i << ",enterInt(\"\\\"" << fldName << "[" << i << "]\\\" value\",0," << fldMask << "));" << std::endl;
                }
            }
            else {
                buf << "        data." << fldName << "(enterInt(\"\\\"" << fldName << "\\\" value\",0," << fldMask << "));" << std::endl;
            }
        }

        // field has form BOOLEAN
        else if(fld->form() == HardwareObject::BOOLEAN) {
            if(fldMult>1) {
                for(i=0; i<fldMult; i++) {
                   buf << "        data." << fldName << "(" << i << ",static_cast<bool>(enterInt(\"\\\"" << fldName << "[" << i << "]\\\" value (0=no,1=yes)\",0,1)));" << std::endl;
                }
            }
            else {
                buf << "        data." << fldName << "(static_cast<bool>(enterInt(\"\\\"" << fldName << "\\\" value (0=no,1=yes)\",0,1)));" << std::endl;
            }
        }
    }

    // add debug source code if necessary
    if(cfg->debug_level>=1) buf << "        data.print();" << std::endl;

    return(buf.str());
}

//-----------------------------------------------------------------------------
// COMPILE MENU: ELEMENT READ/WRITE SINGLE FUNCTIONS
//-----------------------------------------------------------------------------

std::string CompilerCCEmitter::compileMenuElementSingle(const HardwareElement* elm) {

    const HardwareModule* mod = getCompiler()->getModule();
    const HardwareBlock* blk = elm->getBlock();
    const HardwareMemory* mem = dynamic_cast<const HardwareMemory*>(elm);
    const HardwareDataType* dat;
    const HardwareWord* wrd;
    const HardwareBitString* bit;

    std::string itmName, blkName, elmName, typName, menName, datName, rdtForm, datForm, wdtForm;
    std::ostringstream buf;
    CompilerStringList arg, prt;

    blkName = blk->nameName();
    elmName = elm->nameName();
    typName = HardwareObject::DATA_TYPE_TYPE[elm->type()];
    rdtForm = compileMenuBlockListForm(blk);
    rdtForm += ((elm->multiple()>1) ? ("\\\"" + elmName + "[%d]\\\"") : ("\\\"" + elmName + "\\\""));

    compileModuleBlockList(blk,arg,"nbk",true);
    if(elm->multiple()>1) arg.add("nelm");
    if(mem && (mem->access()==HardwareMemory::RAM)) arg.add("indx");
    arg.add("data");

    for(unsigned int idt=0; idt<HardwareElement::NUM_DATA_TYPES; idt++) {
        dat = (idt == 0) ? elm->getDataType() : elm->getAlternativeDataType();
        if(!dat) continue;

        wrd = dynamic_cast<const HardwareWord*>(dat);
        bit = dynamic_cast<const HardwareBitString*>(dat);
        menName = (wrd) ? "[number]" : ((bit) ? "[bit string]" : "");
        datName = (wrd) ? "data" : ((bit) ? "bit string" : "");
        datForm = " " + datName + ((mem&&(mem->access()==HardwareMemory::RAM))?("["+HardwareCommon::getNumberFormat(mem->indx())+"]"):"") + " = ";
        wdtForm = ((blk->multiple()>1) || (elm->multiple()>1)) ? ("buf.str().c_str()") : ("\"\\\"" + elmName + "\\\" " + datName + "\"");
        itmName = mod->name() + elm->nameName() + ((wrd) ? "Number" : ((bit) ? "Bitstring" : ""));

        // --- menu item for READ SINGLE data
        if(elm->isReadable()) {
            buf << "//------------------------------------------------------------------------------" << std::endl
                << std::endl
                << "class " << itmName << "Read : public RCD::MenuItem {" << std::endl
                << "  public:" << std::endl
                << "    " << itmName << "Read() { setName(\"read  \\\"" << elmName << "\\\" " << menName << "\"); }" << std::endl
                << "    int action() {" << std::endl
                << std::endl;
            if(wrd) {
                buf << "        " << typName << " data;" << std::endl
                    << "        int rtnv;" << std::endl
                    << std::endl
                    << compileMenuBlockMultipleEnter(blk)
                    << compileMenuElementMultipleEnter(elm)
                    << compileMenuMemoryIndexEnter(mem)
                    << "        if((rtnv = mod->" << elmName << "_Read(" << arg.str() << ")) != 0) {" << std::endl
                    << "            CERR(\"reading \\\"" << elmName << "\\\"\",\"\");" << std::endl
                    << "            return(rtnv);" << std::endl
                    << "        }" << std::endl
                    << "        std::printf(\"" << rdtForm << datForm << "0x%0" << HardwareCommon::getHexNumber(elm->mask()) << "x"
                    << " (= %" << HardwareCommon::getDecNumber(elm->mask()) << "d)\\n\"," << arg.str() << ",data);" << std::endl;
            }
            else if(bit) {
                compileModuleBlockList(blk,prt,"nbk",true);
                if(elm->multiple()>1) prt.add("nelm");
                if(mem && (mem->access()==HardwareMemory::RAM)) prt.add("indx");
                buf << "        " << bit->nameName() << " data;" << std::endl
                    << "        int rtnv;" << std::endl
                    << std::endl
                    << compileMenuBlockMultipleEnter(blk)
                    << compileMenuElementMultipleEnter(elm)
                    << compileMenuMemoryIndexEnter(mem)
                    << "        if((rtnv = mod->" << elmName << "_Read(" << arg.str() << ")) != 0) {" << std::endl
                    << "            CERR(\"reading \\\"" << elmName << "\\\"\",\"\");" << std::endl
                    << "            return(rtnv);" << std::endl
                    << "        }" << std::endl
                    << "        std::printf(\"" << rdtForm << datForm << "\"" << prt.lstr() << ");" << std::endl
                    << "        data.print();" << std::endl;
            }
            buf << std::endl
                << "        return(rtnv);" << std::endl
                << "    }" << std::endl
                << "};" << std::endl
                << std::endl;
        }

        // --- menu item for WRITE SINGLE data
        if(elm->isWritable()) {
            buf << "//------------------------------------------------------------------------------" << std::endl
                << std::endl
                << "class " << itmName << "Write : public RCD::MenuItem {" << std::endl
                << "  public:" << std::endl
                << "    " << itmName << "Write() { setName(\"write \\\"" << elmName << "\\\" " << menName << "\"); }" << std::endl
                << "    int action() {" << std::endl
                << std::endl;
            if(wrd) {
                buf << compileMenuBlockMultipleEnter(blk)
                    << compileMenuElementMultipleEnter(elm)
                    << compileMenuMemoryIndexEnter(mem)
                    << compileMenuElementWriteForm(elm)
                    << "        " << typName << " data = enterHex(" << wdtForm << ",0," << mod->name() << "::" << elm->maskName() << ");" << std::endl
                    << std::endl
                    << "        return(mod->" << elmName << "_Write(" << arg.str() << "));" << std::endl;
            }
            else if(bit) {
                buf << "        " << bit->nameName() << " data;" << std::endl
                    << std::endl
                    << compileMenuBlockMultipleEnter(blk)
                    << compileMenuElementMultipleEnter(elm)
                    << compileMenuMemoryIndexEnter(mem)
                    << compileMenuDataTypeWriteItem(bit)
                    << std::endl
                    << "        return(mod->" << elmName << "_Write(" << arg.str() << "));" << std::endl;
            }
            buf << "    }" << std::endl
                << "};" << std::endl
                << std::endl;
        }
    }

    return(buf.str());
}

//-----------------------------------------------------------------------------
// COMPILE MENU: ELEMENT READ/WRITE VECTOR FUNCTIONS
//-----------------------------------------------------------------------------

std::string CompilerCCEmitter::compileMenuElementVector(const HardwareElement* elm) {

    const HardwareMemory* mem = dynamic_cast<const HardwareMemory*>(elm);

    if(!mem) return("");

    const HardwareModule* mod = getCompiler()->getModule();
    const HardwareBlock* blk = elm->getBlock();
    const HardwareDataType* dat = elm->getDataType();
    const HardwareWord* wrd = dynamic_cast<const HardwareWord*>(dat);
    const HardwareBitString* bit = dynamic_cast<const HardwareBitString*>(dat);

    std::string itmName, blkName, elmName, sizName, typName, datName, datType, rdtForm, datForm, offInput;

    std::ostringstream buf;
    CompilerStringList arg, prt, err;

    itmName = mod->name() + elm->nameName();
    blkName = blk->nameName();
    elmName = elm->nameName();
    sizName = mod->name() + "::" + mem->indxName();
    typName = HardwareObject::DATA_TYPE_TYPE[elm->type()];
    datName = (wrd) ? "data" : ((bit) ? "bit string" : "");
    datType = dat->nameName();
    rdtForm = compileMenuBlockListForm(blk);
    rdtForm += ((elm->multiple()>1) ? ("\\\"" + elmName + "[%d]\\\"") : ("\\\"" + elmName + "\\\""));
    datForm = " " + datName + "[" + HardwareCommon::getNumberFormat(mem->indx()) + "]" + " = ";

    compileModuleBlockList(blk,arg,"nbk",true);
    if(elm->multiple()>1) arg.add("nelm");
    arg.add("data");

    if(mem->access()==HardwareMemory::RAM) {
        offInput = "        unsigned int vme_off = enterHex(\"offset\",0," + mod->name() + "::" + elm->maskName() + "-1);\n";
        arg.add("vme_off");
    }

    compileModuleBlockList(blk,err,"nbk",true);
    if(elm->multiple()>1) err.add("nelm");

    // --- menu item for READ VECTOR data
    if(elm->isReadable()) {
        compileModuleBlockList(blk,prt,"nbk",true);
        if(elm->multiple()>1) prt.add("nelm");
        prt.add(std::string("indx") + (offInput.empty()?"":"+vme_off"));
        prt.add("data");
        buf << "//------------------------------------------------------------------------------" << std::endl
            << std::endl
            << "class " << itmName << "ReadVector : public RCD::MenuItem {" << std::endl
            << "  public:" << std::endl
            << "    " << itmName << "ReadVector() { setName(\"read  \\\"" << elmName << "\\\" [vector]\"); }" << std::endl
            << "    int action() {" << std::endl
            << std::endl
            << "        std::vector<" << datType << "> data;" << std::endl
            << "        int rtnv;" << std::endl
            << std::endl
            << compileMenuBlockMultipleEnter(blk)
            << compileMenuElementMultipleEnter(elm)
            << offInput
            << "        unsigned int size = enterHex(\"size\",0," + mod->name() + "::" + elm->maskName() + (offInput.empty()?"":"-vme_off") + ");\n"
            << "        data.resize(size);" << std::endl
            << "        if((rtnv = mod->" << elmName << "_Read(" << arg.str() << ")) != 0) {" << std::endl
            << "            CERR(\"reading " << rdtForm << "\"," << (err.str().empty()?"\"\"":err.str()) << ");" << std::endl
            << "            return(rtnv);" << std::endl
            << "        }" << std::endl
            << std::endl
            << "        unsigned int indx;" << std::endl
            << "        for(indx=0; indx<size; indx++) {" << std::endl
            << "            std::printf(\"" << rdtForm << datForm << "0x%0" << HardwareCommon::getHexNumber(elm->mask()) << "x"
            << " (= %" << HardwareCommon::getDecNumber(elm->mask()) << "d)\\n\"," << prt.str() << "[indx],data[indx]" <<");" << std::endl
            << "        }" << std::endl
            << std::endl
            << "        return(rtnv);" << std::endl
            << "    }" << std::endl
            << "};" << std::endl
            << std::endl;
    }

    // --- menu item for WRITE VECTOR data
    if(elm->isWritable()) {
        buf << "//------------------------------------------------------------------------------" << std::endl
            << std::endl
            << "class " << itmName << "WriteVector : public RCD::MenuItem {" << std::endl
            << "  public:" << std::endl
            << "    " << itmName << "WriteVector() { setName(\"write \\\"" << elmName << "\\\" [vector]\"); }" << std::endl
            << "    int action() {" << std::endl
            << std::endl
            << "        std::vector<" << datType << "> data;" << std::endl
            << "        int rtnv;" << std::endl
            << std::endl
            << compileMenuBlockMultipleEnter(blk)
            << compileMenuElementMultipleEnter(elm)
            << offInput
            << "        unsigned int size = enterHex(\"size\",0," + mod->name() + "::" + elm->maskName() + (offInput.empty()?"":"-vme_off") + ");\n"
            << "        " << typName << " sdat = enterHex(\"start value\",0," << mod->name() << "::" << elm->maskName() << ");" << std::endl
            << "        " << typName << " idat = enterHex(\"increment value\",0," << mod->name() << "::" << elm->maskName() << ");" << std::endl
            << "        unsigned int xdat(sdat), indx;" << std::endl
            << std::endl
            << "        for(indx=0; indx<size; indx++) {" << std::endl
            << "            data.push_back(xdat&" << mod->name() << "::" << elm->maskName() << ");" << std::endl
            << "            xdat += idat;" << std::endl
            << "        }" << std::endl
            << std::endl
            << "        if((rtnv = mod->" << elmName << "_Write(" << arg.str() << ")) != 0) {" << std::endl
            << "            CERR(\"writing " << rdtForm << "\"," << (err.str().empty()?"\"\"":err.str()) << ");" << std::endl
            << "            return(rtnv);" << std::endl
            << "        }" << std::endl
            << std::endl
            << "        return(rtnv);" << std::endl
            << "    }" << std::endl
            << "};" << std::endl
            << std::endl;
    }

    return(buf.str());
}

//-----------------------------------------------------------------------------
// COMPILE MENU: ELEMENT READ/WRITE BLOCK FUNCTIONS
//-----------------------------------------------------------------------------

std::string CompilerCCEmitter::compileMenuElementBlock(const HardwareElement* elm) {

    const HardwareMemory* mem = dynamic_cast<const HardwareMemory*>(elm);

    if(!mem) return("");

    const HardwareModule* mod = getCompiler()->getModule();
    const HardwareBlock* blk = elm->getBlock();
    const HardwareDataType* dat = elm->getDataType();
    const HardwareWord* wrd = dynamic_cast<const HardwareWord*>(dat);
    const HardwareBitString* bit = dynamic_cast<const HardwareBitString*>(dat);

    std::string itmName, blkName, elmName, sizName, typName, datName, rdtForm, datForm, offInput;
    std::ostringstream buf;
    CompilerStringList arg, prt, err;

    itmName = mod->name() + elm->nameName();
    blkName = blk->nameName();
    elmName = elm->nameName();
    sizName = mod->name() + "::" + mem->indxName();
    typName = HardwareObject::DATA_TYPE_TYPE[elm->type()];
    datName = (wrd) ? "data" : ((bit) ? "bit string" : "");
    rdtForm = compileMenuBlockListForm(blk);
    rdtForm += ((elm->multiple()>1) ? ("\\\"" + elmName + "[%d]\\\"") : ("\\\"" + elmName + "\\\""));
    datForm = " " + datName + "[" + HardwareCommon::getNumberFormat(mem->indx()) + "]" + " = ";

    compileModuleBlockList(blk,arg,"nbk",true);
    if(elm->multiple()>1) arg.add("nelm");
    arg.add("seg");

    compileModuleBlockList(blk,err,"nbk",true);
    if(elm->multiple()>1) err.add("nelm");

    arg.add("size");
    if(mem->access()==HardwareMemory::RAM) {
        offInput = "        unsigned int vme_off = enterHex(\"offset\",0," + mod->name() + "::" + elm->maskName() + "-1);\n";
        arg.add("vme_off");
    }

    // --- menu item for READ BLOCK data
    if(elm->isReadable()) {
        compileModuleBlockList(blk,prt,"nbk",true);
        if(elm->multiple()>1) prt.add("nelm");
        prt.add(std::string("indx") + (offInput.empty()?"":"+vme_off"));
        prt.add("data");
        buf << "//------------------------------------------------------------------------------" << std::endl
            << std::endl
            << "class " << itmName << "ReadBlock : public RCD::MenuItem {" << std::endl
            << "  public:" << std::endl
            << "    " << itmName << "ReadBlock() { setName(\"read  \\\"" << elmName << "\\\" [block]\"); }" << std::endl
            << "    int action() {" << std::endl
            << std::endl
            << "        int rtnv;" << std::endl
            << std::endl
            << compileMenuBlockMultipleEnter(blk)
            << compileMenuElementMultipleEnter(elm)
            << offInput
            << "        unsigned int size = enterHex(\"size\",0," + mod->name() + "::" + elm->maskName() + (offInput.empty()?"":"-vme_off") + ");\n"
            << "        if((rtnv = mod->" << elmName << "_Read(" << arg.str() << ")) != 0) {" << std::endl
            << "            CERR(\"reading " << rdtForm << " using block transfer\"," << (err.str().empty()?"\"\"":err.str()) << ");" << std::endl
            << "            return(rtnv);" << std::endl
            << "        }" << std::endl
            << std::endl
            << "        unsigned int indx;" << std::endl
            << "        " << typName << "* data = (" << typName << "*)seg->VirtualAddress();" << std::endl
            << "        for(indx=0; indx<" << sizName << "; indx++) {" << std::endl
            << "            std::printf(\"" << rdtForm << datForm << "0x%0" << HardwareCommon::getHexNumber(elm->mask()) << "x"
            << " (= %" << HardwareCommon::getDecNumber(elm->mask()) << "d)\\n\"," << prt.str() << "[indx],data[indx]);" << std::endl
            << "        }" << std::endl
            << std::endl
            << "        return(rtnv);" << std::endl
            << "    }" << std::endl
            << "};" << std::endl
            << std::endl;
    }

    // --- menu item for WRITE BLOCK data
    if(elm->isWritable()) {
        buf << "//------------------------------------------------------------------------------" << std::endl
            << std::endl
            << "class " << itmName << "WriteBlock : public RCD::MenuItem {" << std::endl
            << "  public:" << std::endl
            << "    " << itmName << "WriteBlock() { setName(\"write \\\"" << elmName << "\\\" [block]\"); }" << std::endl
            << "    int action() {" << std::endl
            << std::endl
            << "        int rtnv;" << std::endl
            << std::endl
            << compileMenuBlockMultipleEnter(blk)
            << compileMenuElementMultipleEnter(elm)
            << offInput
            << "        unsigned int size = enterHex(\"size\",0," + mod->name() + "::" + elm->maskName() + (offInput.empty()?"":"-vme_off") + ");\n"
            << "        " << typName << " sdat = enterHex(\"start value\",0," << mod->name() << "::" << elm->maskName() << ");" << std::endl
            << "        " << typName << " idat = enterHex(\"increment value\",0," << mod->name() << "::" << elm->maskName() << ");" << std::endl
            << "        unsigned int xdat(sdat), indx;" << std::endl
            << "        " << typName << "* data = (" << typName << "*)seg->VirtualAddress();" << std::endl
            << std::endl
            << "        for(indx=0; indx<" << sizName << "; indx++) {" << std::endl
            << "            data[indx] = xdat & " << mod->name() << "::" << elm->maskName() << ";" << std::endl
            << "            xdat += idat;" << std::endl
            << "        }" << std::endl
            << std::endl
            << "        if((rtnv = mod->" << elmName << "_Write(" << arg.str() << ")) != 0) {" << std::endl
            << "            CERR(\"writing " << rdtForm << " using block transfer\"," << (err.str().empty()?"\"\"":err.str()) << ");" << std::endl
            << "            return(rtnv);" << std::endl
            << "        }" << std::endl
            << std::endl
            << "        return(rtnv);" << std::endl
            << "    }" << std::endl
            << "};" << std::endl
            << std::endl;
    }

    return(buf.str());
}

//-----------------------------------------------------------------------------
// COMPILE MENU: ELEMENT READ/WRITE FILE FUNCTIONS
//-----------------------------------------------------------------------------

std::string CompilerCCEmitter::compileMenuElementFile(const HardwareElement* elm) {

    const HardwareMemory* mem = dynamic_cast<const HardwareMemory*>(elm);

    if(!mem) return("");

    const CompilerConfiguration* cfg = getCompiler()->getConfig();
    const HardwareModule* mod = getCompiler()->getModule();
    const HardwareBlock* blk = elm->getBlock();
    const HardwareDataType* dat = elm->getDataType();
    const HardwareWord* wrd = dynamic_cast<const HardwareWord*>(dat);
    const HardwareBitString* bit = dynamic_cast<const HardwareBitString*>(dat);

    std::string itmName, blkName, elmName, sizName, typName, datName, rdtForm, datForm, errName;
    std::ostringstream buf;
    CompilerStringList arg, prt, err;

    itmName = mod->name() + elm->nameName();
    blkName = blk->nameName();
    elmName = elm->nameName();
    sizName = mod->name() + "::" + mem->indxName();
    typName = HardwareObject::DATA_TYPE_TYPE[elm->type()];
    datName = (wrd) ? "data" : ((bit) ? "bit string" : "");
    rdtForm = compileMenuBlockListForm(blk);
    rdtForm += ((elm->multiple()>1) ? ("\\\"" + elmName + "[%d]\\\"") : ("\\\"" + elmName + "\\\""));
    datForm = " " + datName + "[" + HardwareCommon::getNumberFormat(mem->indx()) + "]" + " = ";
    errName = (cfg->compile_type != CompilerConfiguration::IPB) ? "block transfer" : "vector";

    compileModuleBlockList(blk,arg,"nbk",true);
    if(elm->multiple()>1) arg.add("nelm");
    (cfg->compile_type != CompilerConfiguration::IPB) ? arg.add("seg") : arg.add("data");

    compileModuleBlockList(blk,err,"nbk",true);
    if(elm->multiple()>1) err.add("nelm");

    // --- menu item for READ BLOCK data (write to file)
    if(elm->isReadable()) {
        compileModuleBlockList(blk,prt,"nbk",true);
        if(elm->multiple()>1) prt.add("nelm");
        prt.add("indx");
        prt.add("data");
        buf << "//------------------------------------------------------------------------------" << std::endl
            << std::endl
            << "class " << itmName << "ReadFile : public RCD::MenuItem {" << std::endl
            << "  public:" << std::endl
            << "    " << itmName << "ReadFile() { setName(\"read  \\\"" << elmName << "\\\" [file]\"); }" << std::endl
            << "    int action() {" << std::endl
            << std::endl
            << "        int rtnv;" << std::endl
            << std::endl
            << compileMenuBlockMultipleEnter(blk)
            << compileMenuElementMultipleEnter(elm)
            << "        std::string fn = enterString(\"File name\");" << std::endl
            << std::endl
            << "        std::ofstream outf(fn.c_str()); if(!outf) {" << std::endl
            << "            CERR(\"cannot open output file \\\"%s\\\"\",fn.c_str());" << std::endl
            << "            return(-1);" << std::endl
            << "        }" << std::endl
            << std::endl;

        if(cfg->compile_type == CompilerConfiguration::IPB) {
            buf << "        std::vector<unsigned int> data(" << sizName << ");" << std::endl;
        }

        buf << "        if((rtnv = mod->" << elmName << "_Read(" << arg.str() << ")) != 0) {" << std::endl
            << "            CERR(\"reading " << rdtForm << " using " << errName << "\"," << (err.str().empty()?"\"\"":err.str()) << ");" << std::endl
            << "            return(rtnv);" << std::endl
            << "        }" << std::endl
            << std::endl;

        buf << "        unsigned int indx;" << std::endl;

        if(cfg->compile_type != CompilerConfiguration::IPB) {
            buf << "        unsigned int* data = (unsigned int*)(seg->VirtualAddress());" << std::endl;
        }

        buf << "        outf << std::hex << std::setfill('0');" << std::endl
            << "        for(indx=0; indx<" << sizName << "; indx++) {" << std::endl
            << "            if(! (outf << \"0x\" << std::setw(8) << (data[indx]&" << mod->name() << "::" << elm->maskName() << ") << std::endl)) {" << std::endl
            << "                CERR(\"writing word %d to output file \\\"%s\\\"\",indx,fn.c_str());" << std::endl
            << "                return(-1);" << std::endl
            << "            }" << std::endl
            << "        }" << std::endl
            << std::endl
            << "        return(rtnv);" << std::endl
            << "    }" << std::endl
            << "};" << std::endl
            << std::endl;
    }

    // --- menu item for WRITE BLOCK data (read from file)
    if(elm->isWritable()) {
        buf << "//------------------------------------------------------------------------------" << std::endl
            << std::endl
            << "class " << itmName << "WriteFile : public RCD::MenuItem {" << std::endl
            << "  public:" << std::endl
            << "    " << itmName << "WriteFile() { setName(\"write \\\"" << elmName << "\\\" [file]\"); }" << std::endl
            << "    int action() {" << std::endl
            << std::endl
            << "        int rtnv;" << std::endl
            << std::endl
            << compileMenuBlockMultipleEnter(blk)
            << compileMenuElementMultipleEnter(elm)
            << "        std::string fn = enterString(\"File name\");" << std::endl
            << std::endl
            << "        std::ifstream inf(fn.c_str()); if(!inf) {" << std::endl
            << "            CERR(\"cannot open input file \\\"%s\\\"\",fn.c_str());" << std::endl
            << "            return(-1);" << std::endl
            << "        }" << std::endl
            << std::endl
            << "        unsigned int indx;" << std::endl;

        if(cfg->compile_type != CompilerConfiguration::IPB) {
            buf << "        unsigned int* data = (unsigned int*)(seg->VirtualAddress());" << std::endl;
        }
        else {
            buf << "        std::vector<unsigned int> data(" << sizName << ");" << std::endl;
        }

        buf << "        std::string buf;" << std::endl
            << "        for(indx=0; indx<" << sizName << "; indx++) {" << std::endl
            << "            if(! (inf >> buf)) {" << std::endl
            << "                CERR(\"reading word %d from input file \\\"%s\\\"\",indx,fn.c_str());" << std::endl
            << "                return(-1);" << std::endl
            << "            }" << std::endl
            << "            if(std::sscanf(buf.c_str(),\"%x\",&data[indx]) != 1) {;" << std::endl
            << "                CERR(\"scanning word %d from input file \\\"%s\\\"\",indx,fn.c_str());" << std::endl
            << "                return(-1);" << std::endl
            << "            }" << std::endl
            << "        }" << std::endl
            << std::endl
            << "        if((rtnv = mod->" << elmName << "_Write(" << arg.str() << ")) != 0) {" << std::endl
            << "            CERR(\"writing " << rdtForm << " using " << errName << "\"," << (err.str().empty()?"\"\"":err.str()) << ");" << std::endl
            << "            return(rtnv);" << std::endl
            << "        }" << std::endl
            << std::endl
            << "        return(rtnv);" << std::endl
            << "    }" << std::endl
            << "};" << std::endl
            << std::endl;
    }

    return(buf.str());
}
