//******************************************************************************
// file: CompilerConfiguration.cc
// desc: configuration for L1CT HardwareCompiler
// auth: 27-FEB-2013 R. Spiwoks, re-design from RCDModuleDesign
//******************************************************************************

#include <cstdlib>
#include <sstream>
#include <getopt.h>

#include "RCDUtilities/RCDUtilities.h"
#include "L1CTHardwareCompiler/HardwareCommon.h"
#include "L1CTHardwareCompiler/CompilerConfiguration.h"

using namespace L1CTHC;

//------------------------------------------------------------------------------

const std::string CompilerConfiguration::TYPE_NAME[TYPE_NUMBER] = {
    "NULL", "MEM", "VME", "IPBus", "VHDL", "AXIInterface"
};

const std::string CompilerConfiguration::SORT_NAME[SORT_NUMBER] = {
    "NONE", "BYNAME", "BYDATA", "BYADDR"
};

const std::string CompilerConfiguration::MBPA_NAME[MBPA_NUMBER] = {
    "NOBPA", "BPA", "BPAOPT"
};

//------------------------------------------------------------------------------

CompilerConfiguration::CompilerConfiguration() {

    // default values
    module_file         = "../data/ctpd.xml";
    output_file         = "";
    compile_file        = "CTPD";
    namespace_name      = "LVL1";
    package_name        = "CTPD";
    compile_type        = VME;
    parse_validate      = true;
    strict_check        = false;
    address_public      = true;
    bitmask_public      = false;
    bitmask_statics     = false;
    alternative_methods = false;
    value_sort_type     = NONE;
    field_sort_type     = NONE;
    promote_mask        = true;
    promote_modf        = true;
    compile_enum        = false;
    menu_cmem_bpa       = MBPA_NO;
    debug_level         = 0;
    print_level         = 1;
    program_call        = "";
}

//------------------------------------------------------------------------------

CompilerConfiguration::~CompilerConfiguration() {

    // nothing to be done
}

//------------------------------------------------------------------------------

void CompilerConfiguration::config(int argc, char* argv[]) {

    int c, sort=0, type;

    // read command-line options
    while((c = getopt(argc,argv,":a:B:c:d:E:f:F:N:o:p:P:s:S:T:u:U:v:V:X:Y:h")) != -1) {
        switch(c) {
        case 'a':
            alternative_methods = static_cast<bool>(atoi(optarg));
            break;

        case 'B':
            if(((menu_cmem_bpa = static_cast<CompilerConfiguration::MBPA>(atoi(optarg))) < 0 ) or (menu_cmem_bpa > MBPA_NUMBER)) {
                CERR("menu CMEM BPA type %d not in allowed range [0..%d]",menu_cmem_bpa,MBPA_NUMBER);
                usage();
                std::exit(0);
            }
            break;

        case 'c':
            strict_check = static_cast<bool>(atoi(optarg));
            break;

        case 'd':
            debug_level = atoi(optarg);
            break;

        case 'E':
            compile_enum = static_cast<bool>(atoi(optarg));
            break;

        case 'f':
            module_file = optarg;
            break;

        case 'F':
            compile_file = optarg;
            break;

        case 'N':
            namespace_name = optarg;
            break;

        case 'o':
            output_file = optarg;
            break;

        case 'p':
            print_level = atoi(optarg);
            break;

        case 'P':
            package_name = optarg;
            break;

        case 's':
            if(((sort = atoi(optarg)) < 0) || (sort > (int)(SORT_NUMBER-1))) {
                CERR("value sorting type %d not in allowed range [0..%d]",sort,SORT_NUMBER-1);
                usage();
                std::exit(0);
            }
            value_sort_type = static_cast<SORT>(sort);
            break;

        case 'S':
            if(((sort = atoi(optarg)) < 0) || (sort > (int)(SORT_NUMBER-1))) {
                CERR("field sorting type %d not in allowed range [0..%d]",sort,SORT_NUMBER-1);
                usage();
                std::exit(0);
            }
            field_sort_type = static_cast<SORT>(sort);
            break;

        case 'T':
            if(((type = atoi(optarg)) < 0) || (type > (int)(TYPE_NUMBER-1))) {
                CERR("compile type %d not in allowed range [0..%d]",sort,TYPE_NUMBER-1);
                usage();
                std::exit(0);
            }
            compile_type = static_cast<TYPE>(type);
            break;

        case 'u':
            address_public = static_cast<bool>(atoi(optarg));
            break;

        case 'U':
            bitmask_public = static_cast<bool>(atoi(optarg));
            break;

        case 'v':
            parse_validate = static_cast<bool>(atoi(optarg));
            break;

        case 'V':
            bitmask_statics = static_cast<bool>(atoi(optarg));
            break;

        case 'X':
            promote_mask = static_cast<bool>(atoi(optarg));
            break;

        case 'Y':
            promote_modf = static_cast<bool>(atoi(optarg));
            break;

        case 'h':
            usage();
            std::exit(0);
            break;

        case '?':
            CERR("invalid option \"%c\"",optopt);
            usage();
            std::exit(-1);
            break;

        case ':':
            CERR("missing argument for option \"%c\"",optopt);
            usage();
            std::exit(-1);
            break;
        }
    }

    program_call = programCall(argc,argv);
}

//------------------------------------------------------------------------------

void CompilerConfiguration::usage(std::ostream& ostr) const {

    char os[STRING_LENGTH];

    std::sprintf(os,"--------------------------------------------------------------------------------\n"); ostr << os;
    std::sprintf(os,"runL1CTHardwareCompiler <OPTIONS>:\n"); ostr << os;
    std::sprintf(os,"--------------------------------------------------------------------------------\n"); ostr << os;
    std::sprintf(os," -f <file> => hardware module file (XML)                           (def = \"%s\")\n",module_file.c_str()); ostr << os;
    std::sprintf(os," -F <file> => compile file name                                    (def = \"%s\")\n",compile_file.c_str()); ostr << os;
    std::sprintf(os," -N <file> => compile namespace name                               (def = \"%s\")\n",namespace_name.c_str()); ostr << os;
    std::sprintf(os," -P <file> => compile package name                                 (def = \"%s\")\n",package_name.c_str()); ostr << os;
    std::sprintf(os," -T <int>  => compile type [0=NULL,1=MEM,2=VME,3=IPB,4=VHDL,5=AXI] (def = \"%s\")\n",TYPE_NAME[compile_type].c_str()); ostr << os;
    std::sprintf(os,"--------------------------------------------------------------------------------\n"); ostr << os;
    std::sprintf(os," -o <file> => output file name (XML)                               (def = \"%s\")\n",output_file.c_str()); ostr << os;
    std::sprintf(os,"--------------------------------------------------------------------------------\n"); ostr << os;
    std::sprintf(os," -v <int>  => parser validation                       [0=NO,1=YES] (def = \"%s\")\n",parse_validate?"YES":"NO"); ostr << os;
    std::sprintf(os," -c <int>  => strict input check                      [0=NO,1=YES] (def = \"%s\")\n",strict_check?"YES":"NO"); ostr << os;
    std::sprintf(os," -u <int>  => module constants public                 [0=NO,1=YES] (def = \"%s\")\n",address_public?"YES":"NO"); ostr << os;
    std::sprintf(os," -U <int>  => bitstring constants public              [0=NO,1=YES] (def = \"%s\")\n",bitmask_public?"YES":"NO"); ostr << os;
    std::sprintf(os," -V <int>  => bitstring constants statics             [0=NO,1=YES] (def = \"%s\")\n",bitmask_statics?"YES":"NO"); ostr << os;
    std::sprintf(os," -a <int>  => alternative methods (string&number)     [0=NO,1=YES] (def = \"%s\")\n",alternative_methods?"YES":"NO"); ostr << os;
    std::sprintf(os," -s <int>  => value sort type               [0=NONE,1=NAME,2=DATA] (def = \"%s\")\n",SORT_NAME[value_sort_type].c_str()); ostr << os;
    std::sprintf(os," -S <int>  => field sort type               [0=NONE,1=NAME,3=ADDR] (def = \"%s\")\n",SORT_NAME[field_sort_type].c_str()); ostr << os;
    std::sprintf(os," -X <int>  => promote mask (DAT->ELM)                 [0=NO,1=YES] (def = \"%s\")\n",promote_mask?"YES":"NO"); ostr << os;
    std::sprintf(os," -Y <int>  => promote modf (DAT->ELM->BLK)            [0=NO,1=YES] (def = \"%s\")\n",promote_modf?"YES":"NO"); ostr << os;
    std::sprintf(os," -E <int>  => compile values as enums                 [0=NO,1=YES] (def = \"%s\")\n",compile_enum?"YES":"NO"); ostr << os;
    std::sprintf(os," -B <int>  => menu program CMEM using BPA       [0=NO,1=YES,2=OPT] (def = \"%s\")\n",MBPA_NAME[menu_cmem_bpa].c_str()); ostr << os;
    std::sprintf(os," -d <int>  => debug level for generated code                       (def = %d)\n",debug_level); ostr << os;
    std::sprintf(os," -p <int>  => print level for L1CT hardware compiler               (def = %d)\n",print_level); ostr << os;
}

//------------------------------------------------------------------------------

void CompilerConfiguration::print(std::ostream& ostr) const {

    char os[STRING_LENGTH];

    std::sprintf(os,"--------------------------------------------------------------------------------\n"); ostr << os;
    std::sprintf(os,"runL1CTHardwareCompiler\n"); ostr << os;
    std::sprintf(os,"--------------------------------------------------------------------------------\n"); ostr << os;
    std::sprintf(os,"Hardware module file                = \"%s\"\n",module_file.c_str()); ostr << os;
    std::sprintf(os,"Compile file name                   = \"%s\"\n",compile_file.c_str()); ostr << os;
    std::sprintf(os,"Compile namespace name              = \"%s\"\n",namespace_name.c_str()); ostr << os;
    std::sprintf(os,"Compile package name                = \"%s\"\n",package_name.c_str()); ostr << os;
    std::sprintf(os,"Compile type                        = \"%s\"\n",TYPE_NAME[compile_type].c_str()); ostr << os;
    std::sprintf(os,"--------------------------------------------------------------------------------\n"); ostr << os;
    std::sprintf(os,"Output file name                    = \"%s\"\n",output_file.c_str()); ostr << os;
    std::sprintf(os,"--------------------------------------------------------------------------------\n"); ostr << os;
    std::sprintf(os,"Parser validation                   = \"%s\"\n",parse_validate?"YES":"NO"); ostr << os;
    std::sprintf(os,"Strict input check                  = \"%s\"\n",strict_check?"YES":"NO"); ostr << os;
    std::sprintf(os,"HardwareModule constants public     = \"%s\"\n",address_public?"YES":"NO"); ostr << os;
    std::sprintf(os,"HardwareBitString constants public  = \"%s\"\n",bitmask_public?"YES":"NO"); ostr << os;
    std::sprintf(os,"HardwareBitString constants statics = \"%s\"\n",bitmask_statics?"YES":"NO"); ostr << os;
    std::sprintf(os,"Alternative methods (string&number) = \"%s\"\n",alternative_methods?"YES":"NO"); ostr << os;
    std::sprintf(os,"HardwareValue sort type             = \"%s\"\n",SORT_NAME[value_sort_type].c_str()); ostr << os;
    std::sprintf(os,"HardwareField sort type             = \"%s\"\n",SORT_NAME[field_sort_type].c_str()); ostr << os;
    std::sprintf(os,"Promote mask (DAT->ELM)             = \"%s\"\n",promote_mask?"YES":"NO"); ostr << os;
    std::sprintf(os,"Promote modf (DAT->ELM->BLK)        = \"%s\"\n",promote_modf?"YES":"NO"); ostr << os;
    std::sprintf(os,"Compile HardwareValues as enums     = \"%s\"\n",compile_enum?"YES":"NO"); ostr << os;
    std::sprintf(os,"Menu program CMEM using BPA         = \"%s\"\n",MBPA_NAME[menu_cmem_bpa].c_str()); ostr << os;
    std::sprintf(os,"Debug level for generated code      = %d\n",debug_level); ostr << os;
    std::sprintf(os,"Print level for L1CT compiler       = %d\n",print_level); ostr << os;
    std::sprintf(os,"--------------------------------------------------------------------------------\n"); ostr << os;
}

//------------------------------------------------------------------------------

int CompilerConfiguration::check() const {

    int fail(0);

    return(fail);
}

//------------------------------------------------------------------------------

const std::string CompilerConfiguration::programCall(int argc, char* argv[]) {

    int iarg;
    std::ostringstream buf;
    bool first = true;

    // store full program call
    for(iarg=0; iarg<argc; iarg++) {
        if(!first) buf << " ";
        buf << ((std::strcmp(argv[iarg],"") == 0) ? "\"\"" : argv[iarg]);
        if(first) first = false;
    }
    return(buf.str());
}
