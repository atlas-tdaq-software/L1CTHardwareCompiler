//******************************************************************************
// file: HardwareObject.cc
// desc: library for L1CT HardwareCompiler
// auth: 01-MAR-2013 R. Spiwoks, re-design from RCDModuleDesign
//******************************************************************************

#include <sstream>
#include <algorithm>

#include "RCDUtilities/RCDUtilities.h"
#include "L1CTHardwareCompiler/HardwareCommon.h"
#include "L1CTHardwareCompiler/HardwareObject.h"
#include "L1CTHardwareCompiler/HardwareCompiler.h"
#include "L1CTHardwareCompiler/HardwareModule.h"
#include "L1CTHardwareCompiler/HardwareBlock.h"
#include "L1CTHardwareCompiler/HardwareElement.h"
#include "L1CTHardwareCompiler/CompilerException.h"
#include "L1CTHardwareCompiler/CompilerStringList.h"

using namespace L1CTHC;

//------------------------------------------------------------------------------

// cf. vme_rrc/vme_rcc/vme_rcc.h
const std::string HardwareObject::ADDR_TYPE_NAME[ADDR_TYPE_NUMBER] = {
    "A16", "A24", "A32", "ILLEGAL3", "ILLEGAL4", "CRCSR", "USER1", "USER2"
};

const unsigned int HardwareObject::ADDR_TYPE_MMAX[ADDR_TYPE_NUMBER] = {
    0x0000ffff, 0x00ffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff
};

const std::string HardwareObject::DATA_TYPE_NAME[DATA_TYPE_NUMBER] = {
    "D08", "D16", "D32"
};

const std::string HardwareObject::DATA_TYPE_TYPE[DATA_TYPE_NUMBER] = {
    "unsigned char", "unsigned short", "unsigned int"
};

const std::string HardwareObject::DATA_TYPE_MASK[DATA_TYPE_NUMBER] = {
    "0x000000ff", "0x0000ffff", "0xffffffff"
};

const unsigned int HardwareObject::DATA_TYPE_MMAX[DATA_TYPE_NUMBER] = {
    0x000000ff, 0x0000ffff, 0xffffffff
};

const unsigned int HardwareObject::DATA_TYPE_SIZE[DATA_TYPE_NUMBER] = {
    8, 16, 32
};

const std::string HardwareObject::DATA_MODF_NAME[DATA_MODF_NUMBER] = {
    "R", "W", "RW"
};

const std::string HardwareObject::DATA_FORM_NAME[DATA_FORM_NUMBER] = {
    "STRING", "NUMBER", "BOOLEAN", "ASCII"
};

const std::string HardwareObject::DATA_FORM_TYPE[DATA_FORM_NUMBER] = {
    "std::string", "unsigned int", "bool", "std::string"
};

const std::string HardwareObject::DATA_FORM_RTNV[DATA_FORM_NUMBER] = {
    "\"<INVALID>\"", "-1", "false", "\"<INVALID>\""
};

const std::string HardwareObject::CPP_ADDR_PREFIX       = "ADDR_";
const std::string HardwareObject::CPP_NUMBER_POSTFIX    = "_NUMBER";
const std::string HardwareObject::CPP_OFFSET_PREFIX     = "OFFS_";

//------------------------------------------------------------------------------

HardwareCompiler* HardwareObject::HardwareObject::m_compiler = 0;

//------------------------------------------------------------------------------

HardwareObject::HardwareObject(const std::string& name) : m_name(name), m_addr(0), m_type(D32), m_modf(RW), m_multiple(1), m_offset(0) {

    // nothing to be done
}

//------------------------------------------------------------------------------

HardwareObject::~HardwareObject() {

    // nothing to be done
}

//------------------------------------------------------------------------------

const std::string HardwareObject::DATA_TYPE_BYTE(const DATA_TYPE typ) {

    std::ostringstream buf;
    buf << (HardwareObject::DATA_TYPE_SIZE[typ]/HardwareCommon::BIT_SIZE);
    return(buf.str());
}

//------------------------------------------------------------------------------

bool HardwareObject::isTypeCompatible(const HardwareObject* obj, const HardwareObject** robj) const {

    const HardwareModule* mod = dynamic_cast<const HardwareModule*>(obj);
    const HardwareBlock* blk = dynamic_cast<const HardwareBlock*>(obj);

    // reset return object
    *robj = 0;

    if(mod) {

        // return if calling object is not HardwareBlock
        if(!dynamic_cast<const HardwareBlock*>(this)) return(true);

        // return if type list of HardwareBlock empty
        if(m_type_list.empty()) return(true);

        std::list<DATA_TYPE>::const_iterator ityp;

        // all types of HardwareBlock must be in HardwareModule's type list
        *robj = mod;
        for(ityp=m_type_list.begin(); ityp!=m_type_list.end(); ityp++) {
            if(!mod->findTypeList(*ityp)) return(false);
        }
        return(true);
    }
    else if(blk) {

        // return if calling object is not HardwareElement
        if(!dynamic_cast<const HardwareElement*>(this)) return(true);

        // if HardwareBlock's list is empty or HadrwareBlock is anonymous then HardwareElement's type must be in HardwareModule's list
        if(blk->hasEmptyTypeList() || blk->name().empty()) {
            *robj = blk->getCompiler()->getModule();
            return(blk->getCompiler()->getModule()->findTypeList(m_type));
        }

        // HardwareElement's type must be in HardwareBlock's type list
        else {
            *robj = blk;
            return(blk->findTypeList(m_type));
        }
    }

    return(true);
}

//------------------------------------------------------------------------------

bool HardwareObject::findTypeList(const DATA_TYPE typ) const {

    return(find(m_type_list.begin(),m_type_list.end(),typ) != m_type_list.end());
}

//------------------------------------------------------------------------------

void HardwareObject::createTypeList(const std::string& s) {

    const HardwareModule* mod = dynamic_cast<const HardwareModule*>(this);
    const HardwareBlock* blk = dynamic_cast<const HardwareBlock*>(this);

    if(!mod && !blk) return;

    std::vector<std::string> tlst;

    // get type list from input string or use default list
    if(HardwareCommon::splitString(s,",",tlst) == 0) {
        defaultTypeList();
        return;
    }

    std::string objName;
    unsigned int i, j;
    bool flag;

    objName = (mod) ? "module" : "block";

    for(i=0; i<tlst.size(); i++) {
        for(flag=false, j=0; j<DATA_TYPE_NUMBER; j++) {
            if(tlst[i] == DATA_TYPE_NAME[j]) {
                if(!findTypeList(static_cast<DATA_TYPE>(j))) m_type_list.push_back(static_cast<DATA_TYPE>(j));
                flag = true;
                break;
            }
        }
        if(!flag) {
            CERR("%s \"%s\" has unknown type \"%s\"",objName.c_str(),m_name.c_str(),tlst[i].c_str());
            throw CompilerException("UNKNOWN TYPE");
        }
    }

    m_type_list.sort(compareDataType);
}

//------------------------------------------------------------------------------

void HardwareObject::copyTypeList(const HardwareObject* obj) {

    std::list<DATA_TYPE>::const_iterator ityp;

    m_type_list.clear();
    for(ityp=obj->m_type_list.begin(); ityp!=obj->m_type_list.end(); ityp++) {
        m_type_list.push_back(*ityp);
    }
}

//------------------------------------------------------------------------------

void HardwareObject::defaultTypeList() {

    m_type_list.clear();
    m_type_list.push_back(D32);
}

//------------------------------------------------------------------------------

bool HardwareObject::isDefaultTypeList() const {

    if(m_type_list.empty() || (m_type_list.size() > 1)) return(false);

    if(m_type_list.front() != D32) return(false);

    return(true);
}

//------------------------------------------------------------------------------

std::string HardwareObject::printTypeList() const {

    std::list<DATA_TYPE>::const_iterator ityp;
    CompilerStringList lst;

    for(ityp=m_type_list.begin(); ityp!=m_type_list.end(); ityp++) {
        lst.add(DATA_TYPE_NAME[*ityp]);
    }

    return(lst.str());
}

//------------------------------------------------------------------------------

std::string HardwareObject::addrName() const {

     std::string n(CPP_ADDR_PREFIX + nameName());
     transform(n.begin(),n.end(),n.begin(),toupper);
     return(n);
}

//------------------------------------------------------------------------------

std::string HardwareObject::multipleName() const {

    std::string mult = nameName() + CPP_NUMBER_POSTFIX;
    transform(mult.begin(),mult.end(),mult.begin(),toupper);
    return(mult);
}

//------------------------------------------------------------------------------

std::string HardwareObject::offsetName() const {

    std::string offs = CPP_OFFSET_PREFIX + nameName();
    transform(offs.begin(),offs.end(),offs.begin(),toupper);
    return(offs);
}

//------------------------------------------------------------------------------

void HardwareObject::setCompiler(HardwareCompiler* compiler) {

    if(m_compiler) {
        CERR("HardwareObject already attached to HardwareCompiler","");
        throw CompilerException("COMPILER ALREADY SET");
    }
    m_compiler = compiler;
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

bool L1CTHC::compareDataType(HardwareObject::DATA_TYPE typa, HardwareObject::DATA_TYPE typb) {

    return(typa > typb);
}
