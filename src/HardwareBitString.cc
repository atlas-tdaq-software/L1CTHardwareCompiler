//******************************************************************************
// file: HardwareBitString.cc
// desc: library for L1CT HardwareCompiler
// auth: 19-MAR-2013 R. Spiwoks, re-design from RCDModuleDesign
//******************************************************************************

#include <algorithm>

#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/parsers/XercesDOMParser.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/sax/HandlerBase.hpp>

#include "RCDUtilities/RCDUtilities.h"
#include "L1CTHardwareCompiler/HardwareCommon.h"
#include "L1CTHardwareCompiler/HardwareBitString.h"
#include "L1CTHardwareCompiler/HardwareField.h"
#include "L1CTHardwareCompiler/HardwareRegister.h"
#include "L1CTHardwareCompiler/HardwareMemory.h"
#include "L1CTHardwareCompiler/HardwareElement.h"
#include "L1CTHardwareCompiler/HardwareBlock.h"
#include "L1CTHardwareCompiler/HardwareCompiler.h"
#include "L1CTHardwareCompiler/CompilerOutputFile.h"
#include "L1CTHardwareCompiler/CompilerException.h"
#include "L1CTHardwareCompiler/CompilerStringList.h"

using namespace L1CTHC;
using namespace XERCES_CPP_NAMESPACE;

//------------------------------------------------------------------------------

const std::string HardwareBitString::XML_BITSTRING    = "bitstring";
const std::string HardwareBitString::CPP_FILE_NAME    = "BITSTRING";
const std::string HardwareBitString::CPP_TYPE_POSTFIX = "_BITSTRING";

//------------------------------------------------------------------------------

HardwareBitString::HardwareBitString(const std::string& name, const DECL_TYPE decl) : HardwareDataType(name,decl) {

    // nothing to be done
}

//------------------------------------------------------------------------------

HardwareBitString::~HardwareBitString() {

    // nothing to be done
}

//------------------------------------------------------------------------------

std::string HardwareBitString::nameName() const {

    if(m_reference) return(m_reference->nameName());

    std::string n(m_name);
    transform(n.begin(),n.end(),n.begin(),toupper);
    return(n);
}

//------------------------------------------------------------------------------

void HardwareBitString::dump(std::ostream& ostr, const unsigned int indt) const {

    char os[L1CTHC::STRING_LENGTH];
    HardwareList<HardwareField>::const_iterator ifld;

    // dump bitstring
    std::sprintf(os,"%sHardwareBitString \"%s\":\n",HardwareCommon::spaceString(indt).c_str(),m_name.c_str()); ostr << os;

    // dump all fields
    for(ifld=m_fields.begin(); ifld!=m_fields.end(); ifld++) {
        (*ifld)->dump(ostr,indt+INDENT);
    }
}

//------------------------------------------------------------------------------

void HardwareBitString::read(const DOMNode* bit) {

    DOMNode* attr;
    std::string bitName, sizName, mskName, mdfName;
    unsigned int iatt;
    bool flag;
    const HardwareModule* mod = getCompiler()->getModule();
    const CompilerConfiguration* cfg = getCompiler()->getConfig();

    // read bitstring attributes
    attr = bit->getAttributes()->getNamedItem(TRX("name"));
    if(attr) bitName = TRX(attr->getNodeValue());
    attr = bit->getAttributes()->getNamedItem(TRX("size"));
    if(attr) sizName = TRX(attr->getNodeValue());
    attr = bit->getAttributes()->getNamedItem(TRX("mask"));
    if(attr) mskName = TRX(attr->getNodeValue());
    attr = bit->getAttributes()->getNamedItem(TRX("modf"));
    if(attr) mdfName = TRX(attr->getNodeValue());

    // print bitstring attributes
    if(cfg->print_level>=4) {
        std::ostringstream buf;
        CompilerStringList att; att.delimiter(','); att.space(1);
        if(!sizName.empty()) att.add("size=\""+sizName+"\"");
        if(!mskName.empty()) att.add("mask=\""+mskName+"\"");
        if(!mdfName.empty()) att.add("modf=\""+mdfName+"\"");
        COUT("found bitstring \"%s\"%s",bitName.c_str(),!(att.str().empty())?", ":"",att.str().c_str());
    }

    // check bitstring attribute NAME
    if(bitName.empty()) {
        CERR("bitstring \"%s\" has no name",bitName.c_str());
        throw CompilerException("NO NAME");
    }

    // check bitstring attribute SIZE
    if(sizName.empty()) {
       if(getElement()) {
           m_size = DATA_TYPE_SIZE[getElement()->type()];
       }
       else {
           m_size = DATA_TYPE_SIZE[D32];
       }
    }
    else {
        try {
            m_size = HardwareCommon::readNumber(sizName);
        }
        catch(const CompilerException& ce) {
            CERR("Exception \"%s\" while reading bitsring \"%s\", size \"%s\"",ce.what(),bitName.c_str(),sizName.c_str());
            throw;
        }
        if(m_size <= 0) {
            CERR("bitstring \"%s\" has illegal size %d",bitName.c_str(),m_size);
            throw CompilerException("INVALID ATTRIBUTE VALUE");
        }
        if(getElement()) {
            if(dynamic_cast<const HardwareMemory*>(getElement())) {
                m_size = m_size * DATA_TYPE_SIZE[getElement()->type()];
            }
            else {
                if(m_size > DATA_TYPE_SIZE[getElement()->type()]) {
                    CERR("bitstring \"%s\" has size %d greater than element type size %d",bitName.c_str(),m_size,DATA_TYPE_SIZE[getElement()->type()]);
                    throw CompilerException("INVALID ATTRIBUTE VALUE");
                }
            }
        }
    }

    // check bitstring attribute MASK
    if(mskName.empty()) {
        if(getElement()) {
            m_mask = getElement()->mask();
            if(cfg->print_level>=10) COUT("element size[mask] = %d",m_mask.size());
        }
        else {
            RCD::BitSet msk(m_size);
            msk.set();
            m_mask = msk;
            if(cfg->print_level>=10) COUT("NEW size[mask] = %d",m_mask.size());
        }

    }
    else {
        if(cfg->print_level>=10) COUT("READ mask = \"%s\"",mskName.c_str());
        if(m_mask.read(mskName)) {
            CERR("Error while reading bitstring \"%s\", mask \"%s\"",bitName.c_str(),mskName.c_str());
            throw CompilerException("INVALID ATTRIBUTE VALUE");
        }
        if(cfg->print_level>=10) COUT("READ size[mask] = %d",m_mask.size());
// R. Spiwoks, 06-FEB-2019, m_mask is of type BitSet and has size of multiples of 32
//        if(!getElement() or (getElement()->hardwareType() != "memory")) {
//            if(m_mask.size() != m_size) {
//                CERR("bitstring \"%s\" has mask with size %d different from bitstring size %d",bitName.c_str(),m_mask.size(),m_size);
//                throw CompilerException("INVALID ATTRIBUTE VALUE");
//            }
//        }
    }
    if(cfg->print_level>=10) COUT("bitstring \"%s\" has mask \"%s\"",bitName.c_str(),m_mask.string().c_str());
    if(m_mask.null()) {
        CERR("bitstring \"%s\" has empty mask \"%s\"",bitName.c_str(),m_mask.string().c_str());
        throw CompilerException("INVALID ATTRIBUTE VALUE");
    }

    // check bitstring attribute MODF
    if(mdfName.empty()) {
        m_modf = (getElement()) ? getElement()->modf() : RW;
    }
    else {
        for(flag=false, iatt=0; iatt<DATA_MODF_NUMBER; iatt++) {
            if(mdfName == DATA_MODF_NAME[iatt]) {
                m_modf = static_cast<DATA_MODF>(iatt);
                flag = true;
                break;
            }
        }
        if(flag) {
            if(getElement() && !modfIsCompatible(getElement()->modf())) {
                CERR("bitstring \"%s\" modf (\"%s\") incompatible with modf of element \"%s\" (\"%s\")",bitName.c_str(),DATA_MODF_NAME[m_modf].c_str(),getElement()->name().c_str(),DATA_MODF_NAME[getElement()->modf()].c_str());
                throw CompilerException("INVALID ATTRIBUTE VALUE");
            }
        }
        else {
            CERR("bitstring \"%s\" has unknown modf \"%s\"",bitName.c_str(),mdfName.c_str());
            throw CompilerException("UNKNOWN MODIFIER");
        }
    }

    // set bitstring attribute
    if(isBitString(bit) || !(getElement())) {
        m_name = bitName;
    }
    // create unique name from module and element
    else {
        m_name = mod->name() + "_" + getElement()->nameName() + CPP_TYPE_POSTFIX;
    }

    // read all fields
    readFields(bit);

    // promote implicit modf
    if(cfg->promote_modf) {
        if(m_modf == RW) {
            if(!hasReadableField()) m_modf = W;
            if(!hasWritableField()) m_modf = R;
        }
    }

    // sort fields if necessary
    if(cfg->field_sort_type == CompilerConfiguration::BYNAME) {
        m_fields.sort(compareHardwareFieldByName);
    }
    else if(cfg->field_sort_type == CompilerConfiguration::BYADDR) {
        m_fields.sort(compareHardwareFieldByAddr);
    }
}

//------------------------------------------------------------------------------

void HardwareBitString::readFields(const DOMNode* bit) {

    DOMNodeList* fldList;
    DOMNode* fld;
    std::string fldName;
    int ifld;
    const CompilerConfiguration* cfg = getCompiler()->getConfig();

    // get all fields
    fldList = bit->getChildNodes();
    if(cfg->print_level>=4) {
        std::string bitName = (bit->getNodeType() == DOMNode::ELEMENT_NODE) ? m_name : TRX(bit->getNodeName());
        COUT("bitstring \"%s\" has %d child%s",bitName.c_str(),fldList->getLength(),fldList->getLength()>1?"ren":"");
    }

    // traverse all fields
    for(ifld=0; ifld<(int)fldList->getLength(); ifld++) {
        fld = fldList->item(ifld);

        // entity reference node type
        if(fld->getNodeType() == DOMNode::ENTITY_REFERENCE_NODE) {
            if(cfg->print_level>=4) COUT("found field entity reference \"%s\"",TRX(fld->getNodeName()));
            readFields(fld);
            continue;
        }
        // element node type
        else if(fld->getNodeType() != DOMNode::ELEMENT_NODE) continue;

        // named field
        if(HardwareField::isField(fld)) {
             fldName = TRX(fld->getAttributes()->getNamedItem(TRX("name"))->getNodeValue());
             HardwareField* afld = new HardwareField(fldName);
             afld->setBitString(this);
             afld->read(fld);
             addField(afld);
        }

        // unknown type
        else {
            CERR("child of bitstring \"%s\" of unknown type \"%s\"",m_name.c_str(),TRX(fld->getNodeName()));
            throw CompilerException("UNKNOWN TYPE");
        }
    }
}

//------------------------------------------------------------------------------

void HardwareBitString::write(std::ostream& ostr) const {

    HardwareList<HardwareField>::const_iterator ifld;
    std::ostringstream indent, buf;

    // indentation
    if(m_decl == EXTERNAL) {
        indent << "  ";
        if(m_modf != RW) buf << " modf=\"" << DATA_MODF_NAME[m_modf] << "\"";
    }
    else {
        indent << ((m_element->getBlock()->name().empty())? "" : "  ") << "    ";
        if((m_modf != RW) && (m_modf != m_element->modf())) buf << " modf=\"" << DATA_MODF_NAME[m_modf] << "\"";
    }

    // write bitstring start tag
    ostr << indent.str() << "<" << XML_BITSTRING << " name=\"" << m_name << "\"" << buf.str() << ">" << std::endl;

    // write all fields
    for(ifld=m_fields.begin(); ifld!=m_fields.end(); ifld++) {
        (*ifld)->write(ostr);
    }

    // write bitstring end tag
    ostr << indent.str() << "</" << XML_BITSTRING << ">" << std::endl;
}

//------------------------------------------------------------------------------

void HardwareBitString::addField(HardwareField* fld) {

    m_fields.addItem(fld);
}

//------------------------------------------------------------------------------

bool HardwareBitString::findField(const std::string& name, HardwareField** rfld) const {

    return(m_fields.findItem(name,rfld));
}

//------------------------------------------------------------------------------

bool HardwareBitString::fitMaskMultiple(const RCD::BitSet& msk, const unsigned int mul, const unsigned int off) const {

    // check if mask empty
    if(msk.null()) return(true);

    // check against bitstring mask
    const CompilerConfiguration* cfg = getCompiler()->getConfig();
    if(cfg->print_level>=10) COUT("MASK (%3d) = \"%s\"\n>> BIT~ (%3d) = \"%s\"",msk.size(),msk.string().c_str(),(~m_mask).size(),(~m_mask).string().c_str());

    return((HardwareCommon::getMaskMultiple(msk,mul,off)&(~m_mask)).null());
}

//------------------------------------------------------------------------------

bool HardwareBitString::findMaskOverlap(const RCD::BitSet& msk, const unsigned int mul, const unsigned int off, HardwareField** rfld) const {

    HardwareList<HardwareField>::const_iterator ifld;
    HardwareField* fld;
    const CompilerConfiguration* cfg = getCompiler()->getConfig();

    for(ifld=m_fields.begin(); ifld!=m_fields.end(); ifld++) {
        fld = *ifld;
        if(cfg->print_level>=10) COUT("field \"%s\", mask = %s, msk = %s, and = %s",fld->name().c_str(),HardwareCommon::getMaskMultiple(fld->mask(),fld->multiple(),fld->offset()).string().c_str(),HardwareCommon::getMaskMultiple(msk,mul,off).string().c_str(),(HardwareCommon::getMaskMultiple(fld->mask(),fld->multiple(),fld->offset())&HardwareCommon::getMaskMultiple(msk,mul,off)).string().c_str());
        if(!(HardwareCommon::getMaskMultiple(fld->mask(),fld->multiple(),fld->offset()) & HardwareCommon::getMaskMultiple(msk,mul,off)).null()) {
            *rfld = *ifld;
            return(true);
        }
    }

    return(false);
}

//------------------------------------------------------------------------------

bool HardwareBitString::isEmpty() const {

    if(m_fields.empty()) return(true);

    if(m_fields.size() == 1) {
        if(m_fields.front()->name() == "") return(true);
    }

    return(false);
}

//------------------------------------------------------------------------------

unsigned int HardwareBitString::maxMultipleField() const {

    unsigned int mlt(0);
    HardwareList<HardwareField>::const_iterator ifld;

    for(ifld=m_fields.begin(); ifld!=m_fields.end(); ifld++) {
        if((*ifld)->multiple() > mlt) mlt = (*ifld)->multiple();
    }

    return(mlt);
}

//------------------------------------------------------------------------------

bool HardwareBitString::hasReadableField() const {

    HardwareList<HardwareField>::const_iterator ifld;

    for(ifld=m_fields.begin(); ifld!=m_fields.end(); ifld++) {
        if(((*ifld)->modf() == R) || ((*ifld)->modf() == RW)) return(true);
    }

    return(false);
}

//------------------------------------------------------------------------------

bool HardwareBitString::hasWritableField() const {

    HardwareList<HardwareField>::const_iterator ifld;

    for(ifld=m_fields.begin(); ifld!=m_fields.end(); ifld++) {
        if(((*ifld)->modf() == W) || ((*ifld)->modf() == RW)) return(true);
    }

    return(false);
}

//------------------------------------------------------------------------------

unsigned int HardwareBitString::getFullMask() const {

    RCD::BitSet mask(m_size), shf;

    for(auto ifld=m_fields.begin(); ifld!=m_fields.end(); ifld++) {
        shf = (*ifld)->mask();
        for(unsigned int idx=0; idx<(*ifld)->multiple(); idx++) {
            mask = mask | shf;
            shf = shf << (*ifld)->offset();
        }
    }

// XXXX, 06-JUN-2018, R. Spiwoks, restrict to D32 for backwards compatibility
    return(mask.numberConst()[0]);
}

//------------------------------------------------------------------------------

RCD::BitSet HardwareBitString::getFullMaskBitSet() const {

    RCD::BitSet mask(m_size), shf;

    for(auto ifld=m_fields.begin(); ifld!=m_fields.end(); ifld++) {
        shf = (*ifld)->mask();
        for(unsigned int idx=0; idx<(*ifld)->multiple(); idx++) {
            mask = mask | shf;
            shf = shf << (*ifld)->offset();
        }
    }

    return(mask);
}

//------------------------------------------------------------------------------

void HardwareBitString::setElement(const HardwareElement* elm) {

    m_element = elm;
    m_mask = elm->mask();
}

//------------------------------------------------------------------------------

bool HardwareBitString::isBitString(const XERCES_CPP_NAMESPACE::DOMNode* bit) {

    if(bit && (TRX(bit->getNodeName()) == XML_BITSTRING)) return(true);

    return(false);
}
