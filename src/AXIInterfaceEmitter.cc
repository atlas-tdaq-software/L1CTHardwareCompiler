//******************************************************************************
// file: AXIInterfaceEmitter.cc
// desc: library for L1CT HardwareCompiler
// auth: 27-NOV-2017 R. Spiwoks
//******************************************************************************

#include <vector>
#include <algorithm>

#include "RCDUtilities/RCDUtilities.h"
#include "L1CTHardwareCompiler/AXIInterfaceEmitter.h"
#include "L1CTHardwareCompiler/CompilerOutputFile.h"
#include "L1CTHardwareCompiler/CompilerException.h"
#include "L1CTHardwareCompiler/CompilerStringList.h"
#include "L1CTHardwareCompiler/HardwareCompiler.h"
#include "L1CTHardwareCompiler/HardwareBlock.h"
#include "L1CTHardwareCompiler/HardwareElement.h"
#include "L1CTHardwareCompiler/HardwareRegister.h"
#include "L1CTHardwareCompiler/HardwareMemory.h"
#include "L1CTHardwareCompiler/HardwareBitString.h"
#include "L1CTHardwareCompiler/HardwareWord.h"
#include "L1CTHardwareCompiler/HardwareField.h"
#include "L1CTHardwareCompiler/HardwareValue.h"
#include "L1CTHardwareCompiler/HardwareCommon.h"

using namespace L1CTHC;

//------------------------------------------------------------------------------

AXIInterfaceEmitter::AXIInterfaceEmitter() : CompilerCCEmitter() {

    // nothing to be done
}

//------------------------------------------------------------------------------

AXIInterfaceEmitter::~AXIInterfaceEmitter() {

    // nothing to be done
}

//-----------------------------------------------------------------------------

std::string AXIInterfaceEmitter::compileModuleHardwareIncludesDeclaration(const HardwareModule* mod) {

    const CompilerConfiguration* cfg = getCompiler()->getConfig();
    std::vector<std::string> hdrName;
    std::ostringstream buf;

    getCompiler()->getDataTypeHeaderFileNames(hdrName);

    for(auto i=0U; i<hdrName.size(); i++) {
        buf << "#include \"" << cfg->package_name << "/" << hdrName[i] << "\"" << std::endl;
    }
    buf << std::endl;

    buf << "#ifdef __ZYNQ__" << std::endl;
    buf << "#include \"zynq_util/Utility.h\"" << std::endl;
    buf << "#define XUTIL ZYNQ::Utility" << std::endl;
    buf << "#else" << std::endl;
    buf << "#include \"MuctpiCommon/MuctpiCommon.h\"" << std::endl;
    buf << "#define XUTIL LVL1::MuctpiCommon" << std::endl;
    buf << "#endif" << std::endl;
    buf << std::endl;
    buf << "#include \"MuctpiSupport/MemoryInterface.h\"" << std::endl;
    buf << std::endl;

    return(buf.str());
}

//-----------------------------------------------------------------------------

std::string AXIInterfaceEmitter::compileModuleHardwareIncludesDefinition(const HardwareModule* mod) {

    // get module output files
    CompilerOutputFile* modHH;
    CompilerOutputFile* modCC;
    try {
        m_compiler->getModuleFiles(&modHH,&modCC);
    }
    catch (const CompilerException& ce) {
        CERR("Exception \"%s\" while trying to get module files",ce.what());
        throw;
    }
    const CompilerConfiguration* cfg = getCompiler()->getConfig();

    std::ostringstream buf;

    buf << "#include <" << cfg->package_name << "/" << modHH->name() << ">" << std::endl;
    buf << std::endl;
    buf << "#ifdef __ZYNQ__" << std::endl;
    buf << "#include \"zynq_log/Logging.h\"" << std::endl;
    buf << "#else" << std::endl;
    buf << "#include \"RCDUtilities/RCDUtilities.h\"" << std::endl;
    buf << "#endif" << std::endl;
    buf << std::endl;

    return(buf.str());
}

//-----------------------------------------------------------------------------

std::string AXIInterfaceEmitter::compileModuleHardwareCtorDtorDeclaration(const HardwareModule* mod) {

    std::ostringstream buf;
    std::string modName = mod->name();

    buf << "    " << modName << "(MemoryInterface*);" << std::endl
        << "   ~" << modName << "();" << std::endl
        << std::endl
        << "    MemoryInterface* getMemoryInterface() const { return(m_mem); }" << std::endl
        << "    unsigned int operator()() const             { return(m_status); }" << std::endl
        << std::endl;

    return(buf.str());
}

//-----------------------------------------------------------------------------

std::string AXIInterfaceEmitter::compileModuleHardwareCtorDtorDefinition(const HardwareModule* mod) {

    std::ostringstream buf;
    std::string modName, addName, sizName, typName, amdName;

    modName = mod->name();
    addName = mod->addrName();
    sizName = mod->sizeName();
    typName = mod->amodName();
    amdName = mod->codeName();

    buf << "//------------------------------------------------------------------------------" << std::endl
        << std::endl
        << modName << "::" << modName << "(MemoryInterface* mem) : m_mem(mem) {" << std::endl
        << std::endl
        << "    // nothing to be done" << std::endl
        << "}" << std::endl
        << std::endl
        << "//------------------------------------------------------------------------------" << std::endl
        << std::endl
        << modName << "::~" << modName << "() {" << std::endl
        << std::endl
        << "    // nothing to be done" << std::endl
        << "}" << std::endl
        << std::endl;

    return(buf.str());
}

//-----------------------------------------------------------------------------

std::string AXIInterfaceEmitter::compileModuleHardwareMembers(const HardwareModule* /* mod */) {

    std::ostringstream buf;

    buf << "    MemoryInterface*            m_mem;" << std::endl
        << "    unsigned int                m_status;" << std::endl
        << std::endl;

    return(buf.str());
}

// -----------------------------------------------------------------------------

std::string AXIInterfaceEmitter::compileModuleHardwareReadSingle(const HardwareElement* elm) {

    const CompilerConfiguration* cfg = getCompiler()->getConfig();
    const HardwareBlock* blk = elm->getBlock();
    const HardwareMemory* mem = dynamic_cast<const HardwareMemory*>(elm);

    std::ostringstream buf;
    CompilerStringList add; add.delimiter('+');

    // compose address
    compileModuleHardwareBlockList(blk,add);
    add.add(elm->addrName());
    if(elm->multiple()>1) add.add("nelm*"+elm->offsetName());
    if(mem && (mem->access()==HardwareMemory::RAM)) add.add("indx");

    // emit read single
    if(cfg->debug_level>=1) buf << "    COUT(\"reading single addr = 0x%08x\"," << add.str() << ");" <<std::endl;
    buf << "    rtnv = m_mem->read(" << add.str() << ",rdat);" << std::endl;

    return(buf.str());
}

// -----------------------------------------------------------------------------

std::string AXIInterfaceEmitter::compileModuleHardwareWriteSingle(const HardwareElement* elm) {

    const CompilerConfiguration* cfg = getCompiler()->getConfig();
    const HardwareBlock* blk = elm->getBlock();
    const HardwareMemory* mem = dynamic_cast<const HardwareMemory*>(elm);

    std::ostringstream buf;
    CompilerStringList add; add.delimiter('+');

    // compose address
    compileModuleHardwareBlockList(blk,add);
    add.add(elm->addrName());
    if(elm->multiple()>1) add.add("nelm*"+elm->offsetName());
    if(mem && (mem->access()==HardwareMemory::RAM)) add.add("indx");

    // emit write single
    if(cfg->debug_level>=1) buf << "    COUT(\"writing single addr = 0x%08x, data = 0x%08x\"," << add.str() << ",wdat);" <<std::endl;
    buf << "    rtnv = m_mem->write(" << add.str() << ",wdat);" << std::endl;

    return(buf.str());
}

// -----------------------------------------------------------------------------

std::string AXIInterfaceEmitter::compileModuleHardwareReadVector(const HardwareElement* elm) {

    const HardwareMemory* mem = dynamic_cast<const HardwareMemory*>(elm);

    if(!mem) return("");

    const CompilerConfiguration* cfg = getCompiler()->getConfig();
    const HardwareBlock* blk = elm->getBlock();

    std::string typName, elmMask, incAddr;
    std::ostringstream buf;
    CompilerStringList add; add.delimiter('+');

    typName = HardwareObject::DATA_TYPE_TYPE[elm->type()];
    elmMask = (elm->mask() != HardwareObject::DATA_TYPE_MMAX[elm->type()]) ? ("&"+elm->maskName()) : "";

    // compose address
    compileModuleHardwareBlockList(blk,add);
    add.add(elm->addrName());
    if(elm->multiple()>1) add.add("nelm*"+elm->offsetName());
    if(mem->access()==HardwareMemory::RAM) add.add("axi_off");

    // emit read vector
    buf << "    unsigned int addr = " << add.str() << ";" << std::endl;
    buf << "    " << typName << " rdat;" << std::endl;
    if(cfg->debug_level>=1) buf << "    COUT(\"reading vector addr = 0x%08x, size = 0x%08x\",addr,data.size());" << std::endl;
    buf << "    for(unsigned int i=0; i<data.size(); i++) {" << std::endl;
    buf << "        if((rtnv = m_mem->read(addr,rdat)) != MemoryInterface::SUCCESS) return(rtnv);" << std::endl;
    buf << "        data[i] = rdat" << elmMask << ";" << std::endl;
    if(mem->access() == HardwareMemory::RAM) buf << "        addr++;" << std::endl;
    buf << "    }" << std::endl;
    buf << std::endl;

    return(buf.str());
}

// -----------------------------------------------------------------------------

std::string AXIInterfaceEmitter::compileModuleHardwareWriteVector(const HardwareElement* elm) {

    const CompilerConfiguration* cfg = getCompiler()->getConfig();
    const HardwareBlock* blk = elm->getBlock();
    const HardwareMemory* mem = dynamic_cast<const HardwareMemory*>(elm);

    std::string typName, elmMask;
    std::ostringstream buf;
    CompilerStringList add; add.delimiter('+');

    typName = HardwareObject::DATA_TYPE_TYPE[elm->type()];
    elmMask = (elm->mask() != HardwareObject::DATA_TYPE_MMAX[elm->type()]) ? ("&"+elm->maskName()) : "";

    // compose address
    compileModuleHardwareBlockList(blk,add);
    add.add(elm->addrName());
    if(elm->multiple()>1) add.add("nelm*"+elm->offsetName());
    if(mem->access()==HardwareMemory::RAM) add.add("axi_off");

    // emit write vector
    buf << "    unsigned int addr = " << add.str() << ";" << std::endl;
    buf << "    " << typName << " wdat;" << std::endl;
    if(cfg->debug_level>=1) buf << "    COUT(\"writing vector addr = 0x%08x, size = 0x%08x\",addr,data.size());" << std::endl;
    buf << "    for(unsigned int i=0; i<data.size(); i++) {" << std::endl;
    buf << "        wdat = data[i]" << elmMask << ";" << std::endl;
    buf << "        if((rtnv = m_mem->write(addr,wdat)) != MemoryInterface::SUCCESS) return(rtnv);" << std::endl;
    if(mem->access() == HardwareMemory::RAM) buf << "        addr++;" << std::endl;
    buf << "    }" << std::endl;
    buf << std::endl;

    return(buf.str());
}

// -----------------------------------------------------------------------------

std::string AXIInterfaceEmitter::compileModuleHardwareReadBlock(const HardwareElement* elm) {

    const CompilerConfiguration* cfg = getCompiler()->getConfig();
    const HardwareBlock* blk = elm->getBlock();
    const HardwareMemory* mem = dynamic_cast<const HardwareMemory*>(elm);

    std::string typName;
    std::ostringstream buf;
    CompilerStringList add; add.delimiter('+');

    typName = HardwareObject::DATA_TYPE_TYPE[elm->type()];

    // compose address
    compileModuleHardwareBlockList(blk,add);
    add.add(elm->addrName());
    if(elm->multiple()>1) add.add("nelm*"+elm->offsetName());
    if(mem->access()==HardwareMemory::RAM) add.add("axi_off");

    // emit read block
    if(cfg->debug_level>=1) buf << "    COUT(\"reading block addr = 0x%08x, size = 0x%08x\"," << add.str() << ",size);" << std::endl;
    buf << "    rtnv = m_mem->read(" << add.str() << ",size,&data[dat_off]);" << std::endl;
    buf << std::endl;

    return(buf.str());
}

// -----------------------------------------------------------------------------

std::string AXIInterfaceEmitter::compileModuleHardwareWriteBlock(const HardwareElement* elm) {

    const CompilerConfiguration* cfg = getCompiler()->getConfig();
    const HardwareBlock* blk = elm->getBlock();
    const HardwareMemory* mem = dynamic_cast<const HardwareMemory*>(elm);

    std::string typName;
    std::ostringstream buf;
    CompilerStringList add; add.delimiter('+');

    typName = HardwareObject::DATA_TYPE_TYPE[elm->type()];

    // compose address
    compileModuleHardwareBlockList(blk,add);
    add.add(elm->addrName());
    if(elm->multiple()>1) add.add("nelm*"+elm->offsetName());
    if(mem->access()==HardwareMemory::RAM) add.add("axi_off");

    // emit write block
    if(cfg->debug_level>=1) buf << "    COUT(\"writing block addr = 0x%08x, size = 0x%08x\"," << add.str() << ",size);" << std::endl;
    buf << "    rtnv = m_mem->write(" << add.str() << ",size,&data[dat_off]);" << std::endl
        << std::endl;

    return(buf.str());
}

// -----------------------------------------------------------------------------

std::string AXIInterfaceEmitter::compileModuleHardwareReadFile(const HardwareElement* elm) {

    const CompilerConfiguration* cfg = getCompiler()->getConfig();
    const HardwareBlock* blk = elm->getBlock();
    const HardwareMemory* mem = dynamic_cast<const HardwareMemory*>(elm);

    std::string typName;
    std::ostringstream buf;
    CompilerStringList add; add.delimiter('+');

    typName = HardwareObject::DATA_TYPE_TYPE[elm->type()];

    // compose address
    compileModuleHardwareBlockList(blk,add);
    add.add(elm->addrName());
    if(elm->multiple()>1) add.add("nelm*"+elm->offsetName());
    if(mem->access()==HardwareMemory::RAM) add.add("axi_off");

    // emit read file
    if(cfg->debug_level>=1) buf << "    COUT(\"reading data into file \\\"%s\\\"\",fn.c_str());" << std::endl;
    buf << "    if((rtnv = m_mem->read(" << add.str() << ",size,fn)) != MemoryInterface::SUCCESS) {" << std::endl;
    buf << "        CERR(\"reading data into file \\\"%s\\\"\",fn.c_str());" << std::endl;
    buf << "        return(rtnv);" << std::endl;
    buf << "    }" << std::endl;
    buf << std::endl;

    return(buf.str());
}

// -----------------------------------------------------------------------------

std::string AXIInterfaceEmitter::compileModuleHardwareWriteFile(const HardwareElement* elm) {

    const CompilerConfiguration* cfg = getCompiler()->getConfig();
    const HardwareBlock* blk = elm->getBlock();
    const HardwareMemory* mem = dynamic_cast<const HardwareMemory*>(elm);

    std::string typName;
    std::ostringstream buf;
    CompilerStringList add; add.delimiter('+');

    typName = HardwareObject::DATA_TYPE_TYPE[elm->type()];

    // compose address
    compileModuleHardwareBlockList(blk,add);
    add.add(elm->addrName());
    if(elm->multiple()>1) add.add("nelm*"+elm->offsetName());
    if(mem->access()==HardwareMemory::RAM) add.add("axi_off");

    // emit write file
    if(cfg->debug_level>=1) buf << "    COUT(\"writing data from file \\\"%s\\\"\",fn.c_str());" << std::endl;
    buf << "    if((rtnv = m_mem->write(" << add.str() << ",size,fn)) != MemoryInterface::SUCCESS) {" << std::endl;
    buf << "        CERR(\"writing data from file \\\"%s\\\"\",fn.c_str());" << std::endl;
    buf << "        return(rtnv);" << std::endl;
    buf << "    }" << std::endl;
    buf << std::endl;

    return(buf.str());
}

// -----------------------------------------------------------------------------

std::string AXIInterfaceEmitter::compileMenuHardwareGlobals(const HardwareModule* /* mod */) {

    std::ostringstream buf;

    return(buf.str());
}

// -----------------------------------------------------------------------------

std::string AXIInterfaceEmitter::compileMenuHardwareOpen(const HardwareModule* mod) {

    std::ostringstream buf;
    std::string modName, modDesc, addName, offName, sizName, amtName, amdName;
    unsigned int modMmax;

    modName = mod->name();
    modDesc = mod->desc();
    modMmax = HardwareObject::ADDR_TYPE_MMAX[mod->amod()];
    addName = modName + "::addr()";
    offName = "mod->getOffset()";
    sizName = modName + "::size()";
    amtName = modName + "::amod()";
    amdName = modName + "::code()";

    buf << "        // get module offset" << std::endl
        << "        std::printf(\"Module \\\"" << modName << "\\\" (\\\"" << modDesc << "\\\"): addr = 0x%08x\\n\"," << addName << ");" <<std::endl
        << "        unsigned int off = (unsigned int)enterHex(\"Module \\\"" << modName << "\\\" address offset (hex)\",0," << hex(8) << modMmax << dec << ");" <<std::endl
        << "        if(off < 0x100) off = off << 24;" << std::endl
        << std::endl
        << "        // create " << modName << " module" << std::endl
        << "        mod = new " << modName << "(vme,off);" << std::endl
        << "        if((*mod)()) {" << std::endl
        << "            CERR(\"opening module \\\"" << modName << "\\\" at vme = 0x%08x, size = 0x%08x, type = \\\"%s\\\"\"," << offName << "," << sizName << "," << amdName << ".c_str());" << std::endl
        << "            std::exit(-1);" << std::endl
        << "        }" <<std::endl
        << "        COUT(\"opened module \\\"" << modName << "\\\" at vme = 0x%08x, size = 0x%08x, type = \\\"%s\\\"\"," << offName << "," << sizName << "," << amdName << ".c_str());" << std::endl
        << std::endl;

    return(buf.str());
}

// -----------------------------------------------------------------------------

std::string AXIInterfaceEmitter::compileMenuHardwareClose(const HardwareModule* mod) {

    std::ostringstream buf;
    std::string modName;

    modName = mod->name();

    buf << "        // delete " << modName << " module" << std::endl
        << "        delete mod;" << std::endl
        << std::endl
        << "        COUT(\"closed module \\\"" << modName << "\\\"\",\"\");" << std::endl
        << std::endl;

    return(buf.str());
}

//-----------------------------------------------------------------------------

void AXIInterfaceEmitter::compileModuleHardwareBlockList(const HardwareBlock* blk, CompilerStringList& par) {

    const CompilerConfiguration* cfg = getCompiler()->getConfig();
    HardwareList<const HardwareBlock> blst;
    HardwareList<const HardwareBlock>::iterator ilst;
    const HardwareBlock* ablk;
    unsigned int iblk;
    std::ostringstream buf;

    blk->getBlockHierarchy(blst);
    if(cfg->print_level>=4) COUT("blk = \"%s\"",blk->name().c_str());

    for(iblk=0, ilst=blst.begin(); ilst!=blst.end(); ilst++, iblk++) {
        ablk = *ilst;

        if(cfg->print_level>=4) std::printf("blk[%d] = \"%s\"\n",iblk,ablk->name().c_str());
        if(ablk->addr()) par.add(ablk->addrName());

        if(ablk->multiple() > 1) {
            buf.str(""); buf << "nbk" << iblk << "*" << ablk->offsetName();
            par.add(buf.str());
        }
    }
}
