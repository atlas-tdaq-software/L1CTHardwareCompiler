//******************************************************************************
// file: IPBusEmitter.cc
// desc: library for L1CT HardwareCompiler
// auth: 21-JUN-2013 R. Spiwoks
//******************************************************************************

#include <vector>
#include <algorithm>

#include "RCDUtilities/RCDUtilities.h"
#include "L1CTHardwareCompiler/IPBusEmitter.h"
#include "L1CTHardwareCompiler/CompilerOutputFile.h"
#include "L1CTHardwareCompiler/CompilerException.h"
#include "L1CTHardwareCompiler/CompilerStringList.h"
#include "L1CTHardwareCompiler/HardwareCompiler.h"
#include "L1CTHardwareCompiler/HardwareBlock.h"
#include "L1CTHardwareCompiler/HardwareElement.h"
#include "L1CTHardwareCompiler/HardwareRegister.h"
#include "L1CTHardwareCompiler/HardwareMemory.h"
#include "L1CTHardwareCompiler/HardwareBitString.h"
#include "L1CTHardwareCompiler/HardwareWord.h"
#include "L1CTHardwareCompiler/HardwareField.h"
#include "L1CTHardwareCompiler/HardwareValue.h"
#include "L1CTHardwareCompiler/HardwareCommon.h"

using namespace L1CTHC;

//------------------------------------------------------------------------------

IPBusEmitter::IPBusEmitter() : CompilerCCEmitter() {

    // nothing to be done
}

//------------------------------------------------------------------------------

IPBusEmitter::~IPBusEmitter() {

    // nothing to be done
}

//-----------------------------------------------------------------------------

std::string IPBusEmitter::compileModuleHardwareIncludesDeclaration(const HardwareModule* /* mod */) {

    std::ostringstream buf;

    buf << "#include \"uhal/uhal.hpp\"" << std::endl
        << "#include <boost/filesystem.hpp>" << std::endl
        << "#include <vector>" << std::endl
        << "#include <algorithm>" << std::endl
        << "#include <typeinfo>" << std::endl
        << "#include <string>" << std::endl;

    return(buf.str());
}

//-----------------------------------------------------------------------------

std::string IPBusEmitter::compileModuleHardwareIncludesDefinition(const HardwareModule* /* mod */) {

    std::ostringstream buf;

    // nothing to be done

    return(buf.str());
}

//-----------------------------------------------------------------------------

std::string IPBusEmitter::compileModuleHardwareCtorDtorDeclaration(const HardwareModule* mod) {

    std::ostringstream buf;
    std::string modName = mod->name();

    buf << "    " << modName << "(uhal::HwInterface* m_hw) : hw(m_hw) {};" << std::endl
        << "   ~" << modName << "() { delete hw; };" << std::endl
        << std::endl;

    return(buf.str());
}

//-----------------------------------------------------------------------------

std::string IPBusEmitter::compileModuleHardwareCtorDtorDefinition(const HardwareModule* /* mod */) {

    std::ostringstream buf;

    // nothing to be done

    return(buf.str());
}

//-----------------------------------------------------------------------------

std::string IPBusEmitter::compileModuleHardwareMembers(const HardwareModule* /* mod */) {

    std::ostringstream buf;

    buf << "    // hw connection" << std::endl
        << "    uhal::HwInterface* hw;" << std::endl
        << std::endl;

    return(buf.str());
}

// -----------------------------------------------------------------------------

std::string IPBusEmitter::compileModuleHardwareReadSingle(const HardwareElement* elm) {

    const HardwareMemory* mem = dynamic_cast<const HardwareMemory*>(elm);
    const HardwareRegister* reg = dynamic_cast<const HardwareRegister*>(elm);
    std::ostringstream buf;
    std::string nodeAddr = "";

    buf << "    try {" << std::endl;

    getNodeString(elm, &buf, &nodeAddr);

    if(reg) {
        //register
        buf << "    uhal::ValWord< uint32_t > reg = (*hw)" << nodeAddr << ".read();" << std::endl
            << "    (*hw).dispatch();" << std::endl
            << "    rdat = reg.value();" << std::endl
            << "    rtnv = 0;" << std::endl;
    }

    if(mem){
        if(!(mem->access()==HardwareMemory::RAM)){
            //fifo
            buf << "    uhal::ValWord< uint32_t > reg = (*hw)" << nodeAddr << ".read();" << std::endl
                << "    (*hw).dispatch();" << std::endl
                << "    rdat = reg.value();" << std::endl
                << "    rtnv = 0;" << std::endl;
        }
        else {
            //mem
            buf << "    uhal::ValVector< uint32_t > reg = (*hw)" << nodeAddr << ".readBlockOffset(1, indx);" << std::endl
                << "    (*hw).dispatch();" << std::endl
                << "    rdat = reg[0];" << std::endl
                << "    rtnv = 0;" << std::endl;
        }
    }

    buf << "    } catch (...) {" << std::endl
        << "        std::cerr << \"uhal error occured\" << std::endl;" << std::endl
        << "        rtnv = -1;" << std::endl
        << "    }" << std::endl;

    return(buf.str());
}

// -----------------------------------------------------------------------------

std::string IPBusEmitter::compileModuleHardwareWriteSingle(const HardwareElement* elm) {

    const HardwareMemory* mem = dynamic_cast<const HardwareMemory*>(elm);
    const HardwareRegister* reg = dynamic_cast<const HardwareRegister*>(elm);

    std::ostringstream buf;
    std::string nodeAddr = "";

    buf << "    try {" << std::endl;

    getNodeString(elm, &buf, &nodeAddr);

    if(reg) {
        //register
        buf << "    (*hw)" << nodeAddr << ".write(wdat);" << std::endl
            << "    (*hw).dispatch();" << std::endl
            << "    rtnv = 0;" << std::endl;
    }

    if(mem) {
        if(!(mem->access()==HardwareMemory::RAM)) {
            //fifo
            buf << "    (*hw)" << nodeAddr << ".write(wdat);" << std::endl
                << "    (*hw).dispatch();" << std::endl
                << "    rtnv = 0;" << std::endl;
        }
        else {
            //mem
            buf << "    std::vector<uint32_t> wdat_v;" << std::endl
                << "    wdat_v.push_back(wdat);" << std::endl
                << "    (*hw)" << nodeAddr << ".writeBlockOffset(wdat_v, indx);" << std::endl
                << "    (*hw).dispatch();" << std::endl
                << "    rtnv = 0;" << std::endl;
        }
    }

    buf << "    } catch (...) {" << std::endl
        << "        std::cerr << \"uhal error occured\" << std::endl;" << std::endl
        << "        rtnv = -1;" << std::endl
        << "    }" << std::endl;

    return(buf.str());
}

// -----------------------------------------------------------------------------

std::string IPBusEmitter::compileModuleHardwareReadVector(const HardwareElement* elm) {

    const HardwareMemory* mem = dynamic_cast<const HardwareMemory*>(elm);

    std::ostringstream buf;
    std::string nodeAddr = "";
    std::string elmMask;

    buf << "    try {" << std::endl;

    getNodeString(elm, &buf, &nodeAddr);

    elmMask = (elm->mask() != HardwareObject::DATA_TYPE_MMAX[elm->type()]) ? ("&"+elm->maskName()) : "";

    if(mem) {
        if(!(mem->access()==HardwareMemory::RAM)){
            //fifo
            buf << "    for(unsigned int i=0; i<data.size(); i++) {" << std::endl
                << "        uhal::ValWord< uint32_t > reg = (*hw)" << nodeAddr << ".read();" << std::endl
                << "        (*hw).dispatch();" << std::endl
                << "        data[i] = reg.value();" << std::endl
                << "    }" << std::endl
                << "    rtnv = 0;" << std::endl;
        }
        else {
            //mem
            buf << "    uhal::ValVector< uint32_t > mem = (*hw)" << nodeAddr << ".readBlockOffset(data.size(), vme_off);" << std::endl
                << "    (*hw).dispatch();" << std::endl
                << "    for(unsigned int i=0; i<data.size(); i++) data[i] = mem[i]" << elmMask << ";" << std::endl
                << "    rtnv = 0;" << std::endl;
        }
    }

    buf << "    } catch (...) {" << std::endl
        << "        std::cerr << \"uhal error occured\" << std::endl;" << std::endl
        << "        rtnv = -1;" << std::endl
        << "    }" << std::endl;

    return(buf.str());
}

// -----------------------------------------------------------------------------

std::string IPBusEmitter::compileModuleHardwareWriteVector(const HardwareElement* elm) {

    const HardwareMemory* mem = dynamic_cast<const HardwareMemory*>(elm);

    std::ostringstream buf;
    std::string nodeAddr = "";
    std::string elmMask;

    buf << "    try {" << std::endl;

    getNodeString(elm, &buf, &nodeAddr);

    //elmMask = (elm->mask() != HardwareObject::DATA_TYPE_MMAX[elm->type()]) ? ("&"+elm->maskName()) : ""; //what is this for?

    if(mem) {
        if(!(mem->access()==HardwareMemory::RAM)){
            //fifo
            buf << "    for(unsigned int i=0; i<data.size(); i++) (*hw)" << nodeAddr << ".write(data[i]);" << std::endl
                << "    (*hw).dispatch();" << std::endl
                << "    rtnv = 0;" << std::endl;
        }
        else {
            //mem
            buf << "    (*hw)" << nodeAddr << ".writeBlockOffset(data, vme_off);" << std::endl
                << "    (*hw).dispatch();" << std::endl
                << "    rtnv = 0;" << std::endl;
        }
    }

    buf << "    } catch (...) {" << std::endl
        << "        std::cerr << \"uhal error occured\" << std::endl;" << std::endl
        << "        rtnv = -1;" << std::endl
        << "    }" << std::endl;

    return(buf.str());
}

// -----------------------------------------------------------------------------

std::string IPBusEmitter::compileModuleHardwareReadBlock(const HardwareElement* /* elm */) {

    std::ostringstream buf;

    // nothing to be done

    return(buf.str());
}

// -----------------------------------------------------------------------------

std::string IPBusEmitter::compileModuleHardwareWriteBlock(const HardwareElement* /* elm */) {

    std::ostringstream buf;

    // nothing to be done

    return(buf.str());
}

// -----------------------------------------------------------------------------

std::string IPBusEmitter::compileModuleHardwareReadFile(const HardwareElement* /* elm */) {

    return(std::string());
}

// -----------------------------------------------------------------------------

std::string IPBusEmitter::compileModuleHardwareWriteFile(const HardwareElement* /* elm */) {

    return(std::string());
}

// -----------------------------------------------------------------------------

std::string IPBusEmitter::compileMenuHardwareGlobals(const HardwareModule* /* mod */) {

    std::ostringstream buf;

    // nothing to be done

    return(buf.str());
}

// -----------------------------------------------------------------------------

std::string IPBusEmitter::compileMenuHardwareOpen(const HardwareModule* mod) {

    std::ostringstream buf;
    std::string modName;

    modName = mod->name();

    buf << "    try {" << std::endl;

    buf << "        // create HwInterface" << std::endl
        << "        uhal::HwInterface* m_hw = new uhal::HwInterface(uhal::ConnectionManager::getDevice (\"kc705\",\"ipbusudp-2.0://192.168.0.8:50001\",\"file://example_design.xml\"));" << std::endl
        << "        // create " << modName << " module" << std::endl
        << "        mod = new " << modName << "(m_hw);" << std::endl
        << "        std::cout << std::setfill(' ');" << std::endl
        << std::endl;

    buf << "    } catch (...) {" << std::endl
        << "        std::cerr << \"uhal error occured\" << std::endl;" << std::endl
        << "        return -1;" << std::endl
        << "    }" << std::endl;

    return(buf.str());
}

// -----------------------------------------------------------------------------

std::string IPBusEmitter::compileMenuHardwareClose(const HardwareModule* /* mod */) {

    std::ostringstream buf;

    buf << "    try {" << std::endl;

    buf << "        // delete IPBUS" << std::endl
        << "        delete mod;" << std::endl
        << std::endl;

    buf << "    }catch (...) {" << std::endl
        << "        std::cerr << \"uhal error occured\" << std::endl;" << std::endl
        << "        return -1;" << std::endl
        << "    }" << std::endl;

    return(buf.str());
}

//-----------------------------------------------------------------------------

void IPBusEmitter::getNodeString(const HardwareElement* elm, std::ostringstream* pBuf, std::string* pNodeAddr) {

    const HardwareBlock* blk = elm->getBlock();
    HardwareList<const HardwareBlock> blst;
    HardwareList<const HardwareBlock>::iterator ilst;
    const HardwareBlock* ablk;
    unsigned int iblk;

    blk->getBlockHierarchy(blst);

    (*pBuf) << "    std::vector<std::string> nodeList;" << std::endl
            << "    std::string endNode(\"" << elm->name() << "\");" << std::endl
            << "    std::string uScore(\"_\");" << std::endl;

    //add all blocks
    for(iblk=0, ilst=blst.begin(); ilst!=blst.end(); ilst++, iblk++) {
        ablk = *ilst;

        (*pBuf) << "    nodeList.push_back(\"" << ablk->name() << "\");" << std::endl;

        pNodeAddr->append(".getNode((nodeList[");
        pNodeAddr->append(std::to_string(iblk));
        pNodeAddr->append("]");

        if(ablk->multiple() > 1) {
            pNodeAddr->append(" + uScore + std::to_string(nbk");
            pNodeAddr->append(std::to_string(iblk));
            pNodeAddr->append(")");
        }
        pNodeAddr->append(").c_str())");
    }

    //add element
    pNodeAddr->append(".getNode((endNode");

    if(elm->multiple()>1){
        pNodeAddr->append(" + uScore + std::to_string(nelm)");
    }

    pNodeAddr->append(").c_str())");
}
