//******************************************************************************
// file: NullEmitter.cc
// desc: library for L1CT HardwareCompiler
// auth: 21-JUN-2013 R. Spiwoks
//******************************************************************************

#include <vector>
#include <algorithm>

#include "RCDUtilities/RCDUtilities.h"
#include "L1CTHardwareCompiler/NullEmitter.h"

using namespace L1CTHC;

//------------------------------------------------------------------------------

NullEmitter::NullEmitter() : CompilerCCEmitter() {

    // nothing to be done
}

//------------------------------------------------------------------------------

NullEmitter::~NullEmitter() {

    // nothing to be done
}

//-----------------------------------------------------------------------------

std::string NullEmitter::compileModuleHardwareIncludesDeclaration(const HardwareModule* /* mod */) {

    std::ostringstream buf;

    // nothing to be done

    return(buf.str());
}

//-----------------------------------------------------------------------------

std::string NullEmitter::compileModuleHardwareIncludesDefinition(const HardwareModule* /* mod */) {

    std::ostringstream buf;

    // nothing to be done

    return(buf.str());
}

//-----------------------------------------------------------------------------

std::string NullEmitter::compileModuleHardwareCtorDtorDeclaration(const HardwareModule* /* mod */) {

    std::ostringstream buf;

    // nothing to be done

    return(buf.str());
}

//-----------------------------------------------------------------------------

std::string NullEmitter::compileModuleHardwareCtorDtorDefinition(const HardwareModule* /* mod */) {

    std::ostringstream buf;

    // nothing to be done

    return(buf.str());
}

//-----------------------------------------------------------------------------

std::string NullEmitter::compileModuleHardwareMembers(const HardwareModule* /* mod */) {

    std::ostringstream buf;

    // nothing to be done

    return(buf.str());
}

// -----------------------------------------------------------------------------

std::string NullEmitter::compileModuleHardwareReadSingle(const HardwareElement* /* elm */) {

    std::ostringstream buf;

    // nothing to be done

    return(buf.str());
}

// -----------------------------------------------------------------------------

std::string NullEmitter::compileModuleHardwareWriteSingle(const HardwareElement* /* elm */) {

    std::ostringstream buf;

    // nothing to be done

    return(buf.str());
}

// -----------------------------------------------------------------------------

std::string NullEmitter::compileModuleHardwareReadVector(const HardwareElement* /* elm */) {

    std::ostringstream buf;

    // nothing to be done

    return(buf.str());
}

// -----------------------------------------------------------------------------

std::string NullEmitter::compileModuleHardwareWriteVector(const HardwareElement* /* elm */) {

    std::ostringstream buf;

    // nothing to be done

    return(buf.str());
}

// -----------------------------------------------------------------------------

std::string NullEmitter::compileModuleHardwareReadBlock(const HardwareElement* /* elm */) {

    std::ostringstream buf;

    // nothing to be done

    return(buf.str());
}

// -----------------------------------------------------------------------------

std::string NullEmitter::compileModuleHardwareWriteBlock(const HardwareElement* /* elm */) {

    std::ostringstream buf;

    // nothing to be done

    return(buf.str());
}

// -----------------------------------------------------------------------------

std::string NullEmitter::compileModuleHardwareReadFile(const HardwareElement* /* elm */) {

    return(std::string());
}

// -----------------------------------------------------------------------------

std::string NullEmitter::compileModuleHardwareWriteFile(const HardwareElement* /* elm */) {

    return(std::string());
}

// -----------------------------------------------------------------------------

std::string NullEmitter::compileMenuHardwareGlobals(const HardwareModule* /* mod */) {

    std::ostringstream buf;

    // nothing to be done

    return(buf.str());
}

// -----------------------------------------------------------------------------

std::string NullEmitter::compileMenuHardwareOpen(const HardwareModule* /* mod */) {

    std::ostringstream buf;

    // nothing to be done

    return(buf.str());
}

// -----------------------------------------------------------------------------

std::string NullEmitter::compileMenuHardwareClose(const HardwareModule* /* mod */) {

    std::ostringstream buf;

    // nothing to be done

    return(buf.str());
}
