//******************************************************************************
// file: HardwareCommon.cc
// desc: pure static class for L1CT HardwareCompiler common types, methods, and constants
// auth: 25-MAR-2013 R. Spiwoks, from TriggerMenuCompiler
//******************************************************************************

#include <sys/time.h>
#include <cstdlib>
#include <cmath>
#include <cerrno>
#include <sstream>
#include <algorithm>

#include "RCDUtilities/RCDUtilities.h"
#include "L1CTHardwareCompiler/HardwareCommon.h"
#include "L1CTHardwareCompiler/CompilerException.h"

using namespace L1CTHC;

//------------------------------------------------------------------------------

double HardwareCommon::getTime() {

    struct timeval tv;
    gettimeofday(&tv,0);
    return(static_cast<double>(tv.tv_sec) + static_cast<double>(tv.tv_usec)/1000000.0);
}

//------------------------------------------------------------------------------

std::string HardwareCommon::rightFillString(const std::string& str, const unsigned int len, const char chr) {

    if(str.length() >= len) return(str);

    unsigned int dif, i;
    std::ostringstream buf;

    buf << str;

    dif = len - str.length();
    for(i=0; i<dif; i++) buf << chr;

    return(buf.str());
}

//------------------------------------------------------------------------------

unsigned int HardwareCommon::readNumber(const std::string& str) {

    // copy string
    std::string s(str);

    // remove space(s)
    s.erase(remove(s.begin(),s.end(),' '),s.end());

    // check if string empty
    if(s.empty()) {
        throw CompilerException("EMPTY INPUT");
    }

    // check if string starts with minus sign
    if(s[0] == '-') {
        throw CompilerException("NEGATIVE VALUE");
    }

    unsigned int val;
    int base(10);
    char* end;

    // check if string is hexadecimal
    if((s.size() >= 2) && ((s.substr(0,2) == "0x") || s.substr(0,2) == "0X")) base = 16;

    // try to convert string to integer
    errno = 0;
    val = std::strtoul(s.c_str(),&end,base);
    if(end[0] != '\0') {
        throw CompilerException("ILLEGAL CHARACTER");
    }
    else if(errno == ERANGE) {
        throw CompilerException("OUT OF RANGE");
    }
    else if(errno == EINVAL) {
        throw CompilerException("INVALID CONVERSION");
    }

    return(val);
}
//------------------------------------------------------------------------------

unsigned int HardwareCommon::getMaskWidth(const unsigned int msk) {

    // check if mask empty
    if(msk == 0) return(0);

    return(getMaskIndexHI(msk)-getMaskIndexLO(msk)+1);
}

//------------------------------------------------------------------------------

unsigned int HardwareCommon::getMaskIndexLO(const unsigned int msk) {

    // check if mask empty
    if(msk == 0) return(0);

    unsigned int idx, lo(0);

    for(idx=0; idx<RCD::BitSet::BITS_PER_WORD; idx++) {
        if(msk & (0x00000001U << idx)) {
            lo = idx;
            break;
        }
    }

    return(lo);
}

//------------------------------------------------------------------------------

unsigned int HardwareCommon::getMaskIndexHI(const unsigned int msk) {

    // check if mask empty
    if(msk == 0) return(0);

    unsigned int idx, hi(0);

    for(idx=(RCD::BitSet::BITS_PER_WORD - 1); true; idx--) {
        if(msk & (0x00000001U << idx)) {
            hi = idx;
            break;
        }
        if(idx == 0) break;
    }

    return(hi);
}

//------------------------------------------------------------------------------

unsigned int HardwareCommon::getMaskValue(const RCD::BitSet& msk) {

    // check if mask empty
    if(msk.null()) return(0);

    unsigned int idy(0), val(0);

    for(unsigned int idx=0; idx<msk.size(); idx++) {
        if(msk[idx]) {
            val |= (0x00000001U << idy);
            idy++;
        }
        if(idy > RCD::BitSet::BITS_PER_WORD) {
            CERR("MASK GREATER (%d) THAN WORD (%d)",idy,RCD::BitSet::BITS_PER_WORD);
            throw CompilerException("MASK GREATER THAN WORD");
        }
    }

    return(val);
}

//------------------------------------------------------------------------------

unsigned int HardwareCommon::getMaskWidth(const RCD::BitSet& msk) {

    // check if mask empty
    if(msk.null()) return(0);

    return(getMaskIndexHI(msk)-getMaskIndexLO(msk)+1);
}

//------------------------------------------------------------------------------

unsigned int HardwareCommon::getMaskIndexLO(const RCD::BitSet& msk) {

    // check if mask empty
    if(msk.null()) return(0);

    unsigned int idx, jdx, lo(0);
    
    for(idx=0; idx<msk.numberConst().size(); idx++) {
        for(jdx=0; jdx<RCD::BitSet::BITS_PER_WORD; jdx++) {
            if(msk.numberConst()[idx] & (0x00000001U << jdx)) {
                lo = idx*RCD::BitSet::BITS_PER_WORD + jdx;
                return(lo);
            }
        }
    }

    return(lo);
}

//------------------------------------------------------------------------------

unsigned int HardwareCommon::getMaskIndexHI(const RCD::BitSet& msk) {

    // check if mask empty
    if(msk.null()) return(0);

    unsigned int idx, jdx, hi(0);

    for(idx=(msk.numberConst().size()-1); true; idx--) {
        for(jdx=(RCD::BitSet::BITS_PER_WORD-1); true; jdx--) {
            if(msk.numberConst()[idx] & (0x00000001U << jdx)) {
                hi = idx*RCD::BitSet::BITS_PER_WORD + jdx;
                return(hi);
            }
            if(jdx == 0) break;
        }
        if(idx == 0) break;
    }

    return(hi);
}

//------------------------------------------------------------------------------

RCD::BitSet HardwareCommon::getMaskMultiple(const RCD::BitSet& msk, const unsigned int mul, const unsigned int off) {

    // check if mask empty
    if(msk.null()) return(msk);

    // check if multiplicity null
    if(mul == 0) {
         RCD::BitSet val(msk);
         val.reset();
         return(val);
    }

    // check if mask identical
    if((mul == 1) || (off == 0)) return(msk);

    RCD::BitSet val(0), shf(msk);

    // shift one after the other in order to avoid unpredictable results
    for(unsigned int idx=0; idx<mul; idx++) {
        val = val | shf;
        shf = shf << off;
    }

    return(val);
}

//------------------------------------------------------------------------------

unsigned int HardwareCommon::getMaskFromIndices(const unsigned int lo, const unsigned int hi) {

    // check if indices wrong
    if((lo > hi) || (hi >= RCD::BitSet::BITS_PER_WORD)) return(0);

    unsigned int val(0), idx;

    for(idx=lo; idx<=hi; idx++) val |= (0x00000001U << idx);

    return(val);
}

//------------------------------------------------------------------------------

unsigned int HardwareCommon::getDecNumber(const unsigned int num) {

    // check if number empty
    if(num == 0) return(0);

    // log10-size of number
    return(static_cast<unsigned int>(std::log10(static_cast<double>(num))) + 1);
}

//------------------------------------------------------------------------------

unsigned int HardwareCommon::getHexNumber(const unsigned int num) {

    // check if number empty
    if(num == 0) return(0);

    // log16-size of number
    return(static_cast<unsigned int>(std::log10(static_cast<double>(num))/std::log10(16.0)) + 1);
}

//------------------------------------------------------------------------------

std::string HardwareCommon::getMaskFormat(const unsigned int msk) {

    // check if mask empty
    if(msk == 0) return("");

    std::ostringstream buf;

    // format based on log16-size of mask
    buf << "0x%0" << getHexNumber(msk) << "x";

    return(buf.str());
}

//------------------------------------------------------------------------------

std::string HardwareCommon::getNumberFormat(const unsigned int num) {

    // check if number empty
    if(num == 0) return("");

    std::ostringstream buf;

    // format based on log10-size of number
    buf << "%" << getDecNumber(num) << "d";

    return(buf.str());
}

//------------------------------------------------------------------------------

int HardwareCommon::splitString(const std::string& s, const std::string& del, std::vector<std::string>& rslt) {

    // return if input string emtpy
    if(s.empty()) return(0);

    std::string::size_type idx0(0), idx1(0);

    // reset result vector
    rslt.clear();

    // scan input string
    while(true) {
        idx1 = s.find(del,idx0);
        if(idx1 != std::string::npos) {
            rslt.push_back(s.substr(idx0,idx1-idx0));
        }
        else {
            rslt.push_back(s.substr(idx0));
            break;
        }
        idx0 = idx1+1;
    }

    return(rslt.size());
}
