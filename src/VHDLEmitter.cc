//******************************************************************************
// file: VHDLEmitter.cc
// desc: library for L1CT HardwareCompiler
// auth: 21-JUN-2013 R. Spiwoks
//******************************************************************************

#include <iomanip>
#include <algorithm>
#include <unistd.h>
#include <time.h>
#include <pwd.h>
#include <bitset>

#include "RCDUtilities/RCDUtilities.h"
#include "L1CTHardwareCompiler/VHDLEmitter.h"
#include "L1CTHardwareCompiler/CompilerOutputFile.h"
#include "L1CTHardwareCompiler/CompilerException.h"
#include "L1CTHardwareCompiler/CompilerStringList.h"
#include "L1CTHardwareCompiler/HardwareCompiler.h"
#include "L1CTHardwareCompiler/HardwareBlock.h"
#include "L1CTHardwareCompiler/HardwareElement.h"
#include "L1CTHardwareCompiler/HardwareRegister.h"
#include "L1CTHardwareCompiler/HardwareMemory.h"
#include "L1CTHardwareCompiler/HardwareBitString.h"
#include "L1CTHardwareCompiler/HardwareWord.h"
#include "L1CTHardwareCompiler/HardwareField.h"
#include "L1CTHardwareCompiler/HardwareValue.h"
#include "L1CTHardwareCompiler/HardwareCommon.h"

using namespace L1CTHC;

//------------------------------------------------------------------------------

const std::string VHDLEmitter::VHDL_COMPANY_NAME        = "CERN PH-ESE";
const std::string VHDLEmitter::VHDL_PLATFORM_NAME       = "Windows 7";
const std::string VHDLEmitter::VHDL_STANDARD_NAME       = "VHDL-2008";
const std::string VHDLEmitter::VHDL_COPYRIGHT_NAME      = "2018 CERN PH-ESE";
const std::string VHDLEmitter::VHDL_VERSION_NAME        = "1.0";
const std::string VHDLEmitter::VHDL_PACKAGE_POSTFIX     = "_pkg";

//------------------------------------------------------------------------------

VHDLEmitter::VHDLEmitter() : CompilerEmitter() {

    // nothing to be done
}

//------------------------------------------------------------------------------

VHDLEmitter::~VHDLEmitter() {

    // nothing to be done
}

//-----------------------------------------------------------------------------

void VHDLEmitter::compile() {

    const HardwareModule* mod = getCompiler()->getModule();

    // write preamble
    writePackagePreamble(mod);

    // compile package declaration
    compilePackageDeclaration(mod);

    // compile package body
    compilePackageBody(mod);

    // compile example
    compileExample(mod);

    // nothing to be done
}

//-----------------------------------------------------------------------------

void VHDLEmitter::writePackagePreamble(const HardwareModule* mod) {

    // get package VHDL file
    CompilerOutputFile* pkgVHD;
    try {
        m_compiler->getPackageVHDLFile(&pkgVHD);
    }
    catch (const CompilerException& ce) {
        CERR("Exception \"%s\" while trying to get package VHDL files",ce.what());
        throw;
    }

    // get package output stream
    std::ofstream& fpkgVHD(pkgVHD->file());

    // structure for time and user name
    time_t ntm;
    struct tm* now;
    std::ostringstream dat;
    struct passwd* pwd;

    // get date and user name
    ntm = time((time_t*)0);
    now = localtime(&ntm);
    dat << std::setfill('0') << std::setw(2) << now->tm_mday << "-" << std::setw(2) << (now->tm_mon+1) << "-" << std::setw(2) << (now->tm_year-100);
    pwd = getpwuid(getuid());

    fpkgVHD << "-------------------------------------------------------------------------------" << std::endl
            << "-- Title      : " << mod->name() << " package" << std::endl
            << "-- Project    : L1CT HardwareCompiler" << std::endl
            << "-------------------------------------------------------------------------------" << std::endl
            << "-- File       : " << pkgVHD->name() << std::endl
            << "-- Author     : " << pwd->pw_gecos << std::endl
            << "-- Company    : " << VHDL_COMPANY_NAME << std::endl
            << "-- Created    : " << dat.str() << std::endl
            << "-- Last update: " << dat.str() << std::endl
            << "-- Platform   : " << VHDL_PLATFORM_NAME << std::endl
            << "-- Standard   : " << VHDL_STANDARD_NAME << std::endl
            << "-------------------------------------------------------------------------------" << std::endl
            << "-- Description: " << mod->desc() << std::endl
            << "-------------------------------------------------------------------------------" << std::endl
            << "-- Copyright (c) " << VHDL_COPYRIGHT_NAME << std::endl
            << "-------------------------------------------------------------------------------" << std::endl
            << "-- Revisions  :" << std::endl
            << "-- Date      Version  Author  Description" << std::endl
            << "-- " << dat.str() << "  " << std::setw(9) << std::left << VHDL_VERSION_NAME << std::setw(8) << pwd->pw_name <<  "Created" << std::right << std::endl
            << "-------------------------------------------------------------------------------" << std::endl
            << "-- Compiler   : " << m_compiler->banner() << std::endl
            << "-- Call       : \"" << m_compiler->getConfig()->program_call << "\"" << std::endl
            << "-------------------------------------------------------------------------------" << std::endl;
}

//-----------------------------------------------------------------------------

void VHDLEmitter::compilePackageDeclaration(const HardwareModule* mod) {

    // get package VHDL file
    CompilerOutputFile* pkgVHD;
    try {
        m_compiler->getPackageVHDLFile(&pkgVHD);
    }
    catch (const CompilerException& ce) {
        CERR("Exception \"%s\" while trying to get package VHDL files",ce.what());
        throw;
    }

    // get package output stream
    std::ofstream& fpkgVHD(pkgVHD->file());

    std::string modName, pkgName, blkName, regName, regType, memName, memType, fulName;
    std::ostringstream smodVHD[HardwareBlock::VHDL_TYPE_NUMBER];

    HardwareList<HardwareBlock>::const_iterator iblk;

    modName = mod->name();
    pkgName = modName + VHDL_PACKAGE_POSTFIX;

    // header
    fpkgVHD << std::endl
            << "library ieee;" << std::endl
            << "use ieee.std_logic_1164.all;" << std::endl
            << "use ieee.numeric_std_unsigned.all;" << std::endl
            << std::endl
            << "package " << pkgName << " is" << std::endl;

    // module
    fpkgVHD << std::endl
            << "  -----------------------------------------------------------------------------" << std::endl
            << "  -- Module " << modName << " constants" << std::endl
            << "  -----------------------------------------------------------------------------" << std::endl
            << "  constant MODULE_BASE_ADDR  : integer := 16#" << nhex(8) << mod->addr() << dec << "#;" << std::endl
            << "  constant MODULE_SIZE       : integer := 16#" << nhex(8) << mod->size() << dec << "#;" << std::endl
            << "  constant MODULE_ADDR_WIDTH : integer := " << HardwareCommon::getMaskWidth(mod->size()-1) << ";" << std::endl
            << std::endl
            << "  subtype addr_t is integer range 0 to 2**MODULE_ADDR_WIDTH-1;" << std::endl
            << "  subtype addr_slv_t is std_logic_vector(MODULE_ADDR_WIDTH-1 downto 0);" << std::endl
            << std::endl
            << "  type addr_array_t is array (natural range <>) of addr_t;" << std::endl;

    // data types defined outside a HardwareElement
    compileDataTypeDeclaration(pkgVHD);

    // loop over all blocks
    for(iblk=mod->beginBlock(); iblk!=mod->endBlock(); iblk++) {
        compilePackageBlock(*iblk,smodVHD);
    }

    // module type
    fpkgVHD << std::endl;
    fpkgVHD << "  -----------------------------------------------------------------------------" << std::endl;
    fpkgVHD << "  -- Module " << modName << std::endl;
    fpkgVHD << "  -----------------------------------------------------------------------------" << std::endl;
    fpkgVHD << std::endl;
    for(unsigned int i=0; i<HardwareBlock::VHDL_TYPE_NUMBER; i++) {
        if(!smodVHD[i].str().empty()) {
            fpkgVHD << "  type " << modName << HardwareBlock::VHDL_TYPE_TAGS[i] << "_mod_t is record" << std::endl;
            fpkgVHD << smodVHD[i].str();
            fpkgVHD << std::endl;
            fpkgVHD << "  end record " << modName << HardwareBlock::VHDL_TYPE_TAGS[i] << "_mod_t;" << std::endl;
        }
    }

    // trailer
    fpkgVHD << std::endl;
    fpkgVHD << "end package " << pkgName << ";" << std::endl;
}

//-----------------------------------------------------------------------------

void VHDLEmitter::compileDataTypeDeclaration(CompilerOutputFile* pkgVHD) {

    HardwareCompiler::HardwareDataTypeMapConstIterator idat;
    HardwareList<const HardwareDataType>* datList;
    HardwareList<const HardwareDataType>::const_iterator jdat;
    const HardwareDataType* dat;

    // get all data types
    for(idat=getCompiler()->beginDataTypeMap(); idat!=getCompiler()->endDataTypeMap(); idat++) {
        datList = idat->second;
        dat = *(datList->begin());

        // loop over all data of same type
        for(jdat=datList->begin(); jdat!=datList->end(); jdat++) {
            dat = *jdat;
            if(!(dat->getElement())) compileDataTypeDeclaration(dat,pkgVHD);
        }
    }
}

//-----------------------------------------------------------------------------

void VHDLEmitter::compileDataTypeDeclaration(const HardwareDataType* dat, CompilerOutputFile* pkgVHD) {

    const HardwareWord* wrd;
    const HardwareBitString* bit;

    // attempt converting to HardwareWord
    if((wrd = dynamic_cast<const HardwareWord*>(dat))) {
        compileWordDeclaration(wrd,pkgVHD);
        return;
    }
    // attempt converting to BitString
    else if((bit = dynamic_cast<const HardwareBitString*>(dat))) {
        compileBitStringDeclaration(bit,pkgVHD);
    }
    else {
        CERR("Unknown HardwareDataType \"%s\"",dat->fileName().c_str());
        throw CompilerException("UNKNOWN DATA TYPE");
    }
}

//-----------------------------------------------------------------------------

void VHDLEmitter::compileWordDeclaration(const HardwareWord* wrd, CompilerOutputFile* pkgVHD) {

    std::string blkName, elmName, conName, slvType, elmType;

    const HardwareElement* elm = wrd->getElement();

    blkName = (elm) ? elm->getBlock()->nameName() : "";
    elmName = (elm) ? (blkName.empty() ? elm->name() : (blkName+"_"+elm->name())) : wrd->nameName();
    conName = elmName; transform(conName.begin(),conName.end(),conName.begin(),toupper);
    slvType = elmName + "_slv_t";
    elmType = elmName + "_reg_t";

    // get data type output streams
    std::ofstream& fpkgVHD(pkgVHD->file());

    // header
    fpkgVHD << std::endl
            << "  -----------------------------------------------------------------------------" << std::endl
            << "  -- Word \"" << elmName << "\"" << std::endl
            << "  -----------------------------------------------------------------------------" << std::endl;

    if(!elm) {
        fpkgVHD << "  constant " << conName << "_WIDTH  : integer := " << HardwareCommon::getMaskWidth(wrd->getFullMask()) << ";" << std::endl;
    }

    // element type (std logic vector)
    fpkgVHD << std::endl
            << "  subtype " << slvType << " is std_logic_vector(" << conName << "_WIDTH-1 downto 0);" << std::endl
            << "  subtype " << elmType << " is " << slvType << ";" << std::endl;

    // element type array
    if((elm) && (elm->multiple()>1)) {
        fpkgVHD << std::endl
                << "  type " <<  (elmName + "_reg_array_t") << " is array (0 to " << conName << "_NUMBER-1) of " << elmName << "_reg_t;" << std::endl;
    }
}

//-----------------------------------------------------------------------------

void VHDLEmitter::compileBitStringDeclaration(const HardwareBitString* bit, CompilerOutputFile* pkgVHD) {

    std::string blkName, elmName, conName, typName, typType, elmType, slvType, fldName, fulName, fldType, valName;
    /* unsigned int fldIxlo, fldIxhi, fldForm; */
    std::ostringstream fldDecl, spkgVHD, cmt;
    bool flag;

    const HardwareElement* elm = bit->getElement();
    HardwareList<HardwareField>::const_iterator ifld;
    HardwareField* fld;
    HardwareList<HardwareValue>::const_iterator ival;
    HardwareValue* val;

    blkName = (elm) ? elm->getBlock()->nameName() : "";
    elmName = (elm) ? (blkName.empty() ? elm->name() : (blkName+"_"+elm->name())) : bit->nameName();
    conName = elmName; transform(conName.begin(),conName.end(),conName.begin(),toupper);
    typName = bit->isEmpty() ? (elmName+"_reg_t") : (elmName+"_slv_t");
    typType = ((elm) && (elm->multiple()>1)) ? (elmName + "_reg_array_t") : (elmName + "_reg_t");
    elmType = elmName + "_reg_t";
    slvType = elmName + "_slv_t";

    // get data type output streams
    std::ofstream& fpkgVHD(pkgVHD->file());

    // if bit string is a reference
    if(bit->isReference()) {
        const HardwareElement* elmR = bit->getReference()->getElement();
        std::string blkNameR, elmNameR;
        blkNameR = (elmR) ? (elmR->getBlock()->name().empty() ? "" : elmR->getBlock()->name()) : "";
        elmNameR = (elmR) ? (blkNameR.empty() ? elmR->name() : (blkNameR+"_"+elmR->name())) : bit->getReference()->nameName();
        fpkgVHD << std::endl
                << "  subtype " << elmType << " is " << elmNameR << "_reg_t;" << std::endl
                << "  subtype " << slvType << " is " << elmNameR << "_slv_t;" << std::endl;
        if((elmR) && (elmR->multiple()>1)) {
            fpkgVHD << std::endl
                    << "  type " << elmName << "_reg_array_t is array (0 to " << conName << "_NUMBER-1) of " << elmType << ";" << std::endl;
        }
        return;
    }

    // header
    fpkgVHD << std::endl
            << "  -----------------------------------------------------------------------------" << std::endl
            << "  -- Bitstring \"" << elmName << "\"" << std::endl
            << "  -----------------------------------------------------------------------------" << std::endl;

    if(!elm) {
        fpkgVHD << std::endl;
        fpkgVHD << "  constant " << conName << "_WIDTH  : integer := " << (HardwareCommon::getMaskIndexHI(bit->getFullMaskBitSet()) + 1) << ";" << std::endl;
    }

    // element type (std logic vector)
    fpkgVHD << std::endl
            << "  subtype " << typName << " is std_logic_vector(" << conName << "_WIDTH-1 downto 0);" << std::endl;

    // for all fields
    for(ifld=bit->beginField(); ifld!= bit->endField(); ifld++) {
        fld = *ifld;
        fldName = (elm) ? (blkName.empty() ? (elm->name()+"_"+fld->name()) : (blkName+"_"+elm->name()+"_"+fld->name())) : (bit->name()+"_"+fld->name());
        fulName = fldName; transform(fulName.begin(),fulName.end(),fulName.begin(),toupper);
        fldType = fldName + "_t";
        //fldIxlo = HardwareCommon::getMaskIndexLO(HardwareCommon::getMaskMultiple(fld->mask(),fld->multiple(),fld->offset()));
        //fldIxhi = HardwareCommon::getMaskIndexHI(HardwareCommon::getMaskMultiple(fld->mask(),fld->multiple(),fld->offset()));
        //fldForm = (HardwareCommon::getMaskWidth(fld->mask() - 1) / HardwareCommon::HEX_SIZE) + 1;

        cmt.str("");
        cmt << (elm?(!elm->name().empty()?("Register \""+elm->name()+"\""):""):"");
        cmt << (cmt.str().empty()?"Bitfield":", bitfield") << (" \""+fld->name()+"\"");
        fpkgVHD << std::endl
                << "  -- " << cmt.str() << std::endl;

        // multiple field
        if(fld->multiple()>1) {
            fpkgVHD << "  constant " << fulName << "_NUMBER : integer := " << fld->multiple() << ";" << std::endl
                    << "  constant " << fulName << "_OFFSET : integer := " << fld->offset() << ";" << std::endl;
        }
        if(HardwareCommon::getMaskWidth(fld->mask())>1) {
            fpkgVHD << "  constant " << fulName << "_WIDTH  : integer := " << HardwareCommon::getMaskWidth(fld->mask()) << ";" << std::endl;
        }
        fpkgVHD << "  constant " << fulName << "_SHIFT  : integer := " << HardwareCommon::getMaskIndexLO(fld->mask()) << ";" << std::endl;
        if(fld->multiple()>1) {
            if(HardwareCommon::getMaskWidth(fld->mask())>1) {
                spkgVHD << "    " << fld->name() << " : std_logic_vector_array_t(0 to " << fulName << "_NUMBER-1)(" << fulName << "_WIDTH-1 downto 0);" << std::endl;
            }
            else {
               spkgVHD << "    " << fld->name() << " : std_logic_vector(0 to " << fulName << "_NUMBER-1);" << std::endl;
            }
        }
        else {
            if(HardwareCommon::getMaskWidth(fld->mask())>1) {
                if(!fld->isEmpty()) {
                    spkgVHD << "    " << fld->name() << " : " << fldType << ";" << std::endl;
                }
                else {
                    spkgVHD << "    " << fld->name() << " : std_logic_vector(" << fulName << "_WIDTH-1 downto 0);" << std::endl;
                }
            }
            else {
               spkgVHD << "    " << fld->name() << " : std_logic;" << std::endl;
            }
        }

        // for all values
        int mskWidth = HardwareCommon::getMaskWidth(fld->mask());
        int mskShift = HardwareCommon::getMaskIndexLO(fld->mask());
        for(flag=true, ival=fld->beginValue(); ival!= fld->endValue(); ival++) {
            val = *ival;
            valName = fldName + "_" + val->name();
            cmt.str("");
            cmt << (elm?(!elm->name().empty()?("Register \""+elm->name()+"\""):""):"");
            cmt << (cmt.str().empty()?"Bitfield":", bitfield") << (" \""+fld->name()+"\"");

            if(flag) {
                fpkgVHD << std::endl;
                fpkgVHD << "  -- " << cmt.str() << ", value constants" << std::endl;
                if(HardwareCommon::getMaskWidth(fld->mask())>1) {
                    fpkgVHD << "  subtype " << fldType << " is std_logic_vector(" << fulName << "_WIDTH-1 downto 0);" << std::endl;
                }

                flag = false;
            }

            //fpkgVHD << "  constant " << valName << " : " << fldType << " := x\"" << nhex(fldForm) << val->data() << dec << "\";" << std::endl;
            if(HardwareCommon::getMaskWidth(fld->mask())>1) {
                fpkgVHD << "  constant " << valName << " : " << fldType << " := \"" << compileStdLogic(val->data()>>mskShift,mskWidth) << "\";" << std::endl;
            }
            else {
                fpkgVHD << "  constant " << valName << " : " << "std_logic" << " := '" << compileStdLogic(val->data()>>mskShift,mskWidth) << "';" << std::endl;
            }
        }
    }

    // element type (bit string)
    fpkgVHD << std::endl
            << "  -- Record describing register \"" << elmName << "\" in terms of bitfields" << std::endl
            << "  type " << elmType << " is record" << std::endl
            << spkgVHD.str()
            << "  end record " << elmType << ";" << std::endl
            << std::endl
            << "  function reg2slv (reg : " << elmType << ") return " << slvType << ";" << std::endl
            << "  function slv2reg (slv : " << slvType << ") return " << elmType << ";" << std::endl;

    // element type array
    if((elm) && (elm->multiple()>1)) {
        fpkgVHD << std::endl
                << "  type " << typType << " is array (0 to " << conName << "_NUMBER-1) of " << elmName << "_reg_t;" << std::endl;
    }
}

//-----------------------------------------------------------------------------

std::string VHDLEmitter::compileStdLogic(const RCD::BitSet& data, const unsigned int width) {

    if(width < 1) return("");

    std::ostringstream buf;
    for(int i = width-1; i >= 0; i--) {
        buf << (data[i] & 1);
    }

    return(buf.str());
}

//-----------------------------------------------------------------------------

void VHDLEmitter::compilePackageBlock(HardwareBlock* blk, std::ostringstream smodVHD[HardwareBlock::VHDL_TYPE_NUMBER]) {

    // get package VHDL file
    CompilerOutputFile* pkgVHD;
    try {
        m_compiler->getPackageVHDLFile(&pkgVHD);
    }
    catch (const CompilerException& ce) {
        CERR("Exception \"%s\" while trying to get package VHDL files",ce.what());
        throw;
    }

    const CompilerConfiguration* cfg = getCompiler()->getConfig();

    // get package output stream
    std::ofstream& fpkgVHD(pkgVHD->file());

    const HardwareModule* mod = getCompiler()->getModule();
    std::string modName, pkgName, blkName, regName, regType, memName, memType, fulName;
    unsigned int modForm, memMask, mskSize;
    std::ostringstream sblkVHD[HardwareBlock::VHDL_TYPE_NUMBER], cmt;

    HardwareList<HardwareBlock>::const_iterator iblk;
    HardwareList<HardwareElement>::const_iterator ielm;
    const HardwareElement* elm;
    const HardwareRegister* reg;
    const HardwareMemory* mem;

    modName = mod->name();
    pkgName = modName + VHDL_PACKAGE_POSTFIX;
    modForm = mod->form();

    blkName = blk->nameName();

    // block type
    if(!blkName.empty() and (blk->multiple() > 1)) {
        fpkgVHD << std::endl;
        fpkgVHD << "  -----------------------------------------------------------------------------" << std::endl;
        fpkgVHD << "  -- Block \"" << blkName << "\" constants" << std::endl;
        fpkgVHD << "  -----------------------------------------------------------------------------" << std::endl;
        fpkgVHD << "  constant " << blkName << "_NUMBER : integer := " << blk->multiple() << ";" << std::endl;
        fpkgVHD << "  constant " << blkName << "_OFFSET : integer := " << blk->offset() << ";" << std::endl;
     }

    // loop over all elements
    for(ielm=blk->beginElement(); ielm!=blk->endElement(); ielm++) {
        elm = *ielm;
        reg = dynamic_cast<const HardwareRegister*>(elm);
        mem = dynamic_cast<const HardwareMemory*>(elm);

        // register
        if(reg) {
            HardwareList<const HardwareBlock> lst;
            HardwareList<const HardwareBlock>::reverse_iterator rlst;
            std::vector<const HardwareObject*> vec;
            std::ostringstream sdecVHD, saddVHD;
            RArg rarg;
            unsigned int blkNumber(0);

            reg->getBlock()->getBlockHierarchy(lst);
            for(auto ilst=lst.begin(); ilst!=lst.end(); ilst++) vec.push_back(*ilst);
            vec.push_back(reg);
            regName = blkName.empty() ? reg->name() : (blkName+"_"+reg->name());
            fulName = regName; transform(fulName.begin(),fulName.end(),fulName.begin(),toupper);
            regType = (reg->multiple()>1) ? (regName + "_reg_array_t") : (regName + "_reg_t");
            cmt.str("");
            cmt << (blkName.empty()?"":("Block \""+blkName+"\""));
            cmt << (cmt.str().empty()?"Register":", register");
            if(reg->multiple()>1) cmt << (" array");
            cmt << (" \""+reg->name()+"\"");

             // header
            fpkgVHD << std::endl
                    << "  -----------------------------------------------------------------------------" << std::endl
                    << "  -- " << cmt.str() << std::endl
                    << "  -----------------------------------------------------------------------------" << std::endl;

            // multiple register constants
            if(reg->multiple()>1) {
                fpkgVHD << "  constant " << fulName << "_NUMBER : integer := " << reg->multiple() << ";" << std::endl;
                fpkgVHD << "  constant " << fulName << "_OFFSET : integer := " << reg->offset() << ";" << std::endl;
            }

            // register constants
            if(reg->multiple()>1) {
                 sdecVHD << "addr_array_t (0 to " << fulName << "_NUMBER-1)";
            }
            else {
                 sdecVHD << "addr_t";
            }
            for(rlst=lst.rbegin(); rlst!=lst.rend(); rlst++) {
                const HardwareBlock *ablk(*rlst);
                if(ablk->multiple()>1) {
                    fpkgVHD << "  type " << fulName << "_blk" << blkNumber << "_addr_array_t is array (0 to " << ablk->nameName() << "_NUMBER-1) of " << sdecVHD.str() << ";" << std::endl;
                    sdecVHD.str(""); sdecVHD << fulName << "_blk" << blkNumber << "_addr_array_t";
                    blkNumber++;
                }
            }
            fpkgVHD << "  constant " << fulName << "_ADDR   : " << sdecVHD.str() << " := ";
            if(cfg->print_level>=2) std::printf("compilePackageBlock: reg = \"%s\":\n",fulName.c_str());
            compilePackageBlockAddress(vec,rarg,saddVHD);
            if(cfg->print_level>=2) std::printf("compilePackageBlock: str = \"\n%s\"\n",saddVHD.str().c_str());
            fpkgVHD << std::endl << saddVHD.str() << dec << ";" << std::endl;
            fpkgVHD << "  constant " << fulName << "_WIDTH  : integer := " << (HardwareCommon::getMaskIndexHI(reg->mask())+1) << ";" << std::endl;

            // register data type
            compileDataTypeDeclaration(reg->getDataType(),pkgVHD);

            // for block or module type
            if(reg->modf() == HardwareObject::R) {
                sblkVHD[HardwareBlock::VHDL_MISO] << "    " << reg->name() << " : " << regType << ";" << std::endl;
                blk->setVhdlType(HardwareBlock::VHDL_MISO,true);
            }
            else {
                sblkVHD[HardwareBlock::VHDL_MOSI] << "    " << reg->name() << " : " << regType << ";" << std::endl;
                blk->setVhdlType(HardwareBlock::VHDL_MOSI,true);
            }
        }

        // memory
        else if(mem) {
            memName = blkName.empty() ? mem->name() : (blkName+"_"+mem->name());
            fulName = memName; transform(fulName.begin(),fulName.end(),fulName.begin(),toupper);
            memType = (mem->multiple()>1) ? (memName + "_mem_array_t") : (memName + "_mem_t");
            cmt.str("");
            cmt << (blkName.empty()?"":("Block \""+blkName+"\""));
            std::string acc = mem->accessName();
            if(cmt.str().empty()) {
                toupper(acc[0]);
                cmt << (acc);
            }
            else {
                 cmt << (", "+acc);
            }
            if(mem->multiple()>1) cmt << " array";
            cmt << (" \""+mem->name()+"\"");

             // header
            fpkgVHD << std::endl
                    << "  -----------------------------------------------------------------------------" << std::endl
                    << "  -- " << cmt.str() << std::endl
                    << "  -----------------------------------------------------------------------------" << std::endl;

            // multiple memory constants
            if(mem->multiple()>1) {
                fpkgVHD << "  constant " << fulName << "_NUMBER : integer := " << mem->multiple() << ";" << std::endl;
            }

            // memory constants
            if(mem->multiple()>1) {
                fpkgVHD << "  constant " << fulName << "_ADDR   : addr_array_t (0 to " << fulName << "_NUMBER-1) := (";
                for(unsigned int i=0; i<mem->multiple(); i++) {
                    fpkgVHD << "16#" << nhex(modForm) << (blk->addr()+mem->addr()+mem->offset()*i) << dec << "#" << (i==mem->multiple()-1?"":", ");
                }
                fpkgVHD << ");" << std::endl;
            }
            else {
                fpkgVHD << "  constant " << fulName << "_ADDR   : addr_t  := " << "16#" << nhex(modForm) << (blk->addr()+mem->addr()) << dec << "#;" << std::endl;
            }
            memMask = HardwareCommon::getMaskFromIndices(HardwareCommon::getMaskIndexLO(blk->addr()+mem->addr()),
                                                         HardwareCommon::getMaskIndexHI(mod->size()-1));
            mskSize = HardwareCommon::getMaskWidth(mod->size()-1);
            fpkgVHD << "  constant " << fulName << "_SIZE   : addr_t  := " << "16#" << nhex(modForm) << mem->size() << dec << "#;" << std::endl
                    << std::endl
                    << "  constant " << fulName << "_ADDR_MASK : addr_slv_t := " << mskSize << "x\"" << nhex(modForm) << memMask << dec << "\";" << std::endl
                    << "  subtype " << fulName << "_ADDR_RANGE is addr_t range "
                    << "16#" << nhex(modForm) << (blk->addr()+mem->addr()) << "# to "
                    << "16#" << nhex(modForm) << (blk->addr()+mem->addr()+mem->size()-1) << dec << "#;" << std::endl
                    << std::endl
                    << "  constant " << fulName << "_WIDTH  : integer := " << HardwareCommon::getMaskWidth(mem->mask()) << ";" << std::endl
                    << "  subtype " << fulName << "_data_t is std_logic_vector(" << fulName << "_WIDTH-1 downto 0);" << std::endl;
            fpkgVHD << std::endl;
            fpkgVHD << "  type " << memType << " is array (0 to " << fulName << "_SIZE) of " << fulName << "_data_t;" << std::endl;

            // for block or module type
            sblkVHD[HardwareBlock::VHDL_NONE] << "    " << mem->name() << " : " << memType << ";" << std::endl;
            blk->setVhdlType(HardwareBlock::VHDL_NONE,true);
        }
    }

    // loop over all nested blocks
    for(iblk=blk->beginBlock(); iblk!=blk->endBlock(); iblk++) {
        HardwareBlock *ablk(*iblk);
        compilePackageBlock(ablk,smodVHD);
        for(unsigned int i=0; i<HardwareBlock::VHDL_TYPE_NUMBER; i++) {
            if(ablk->getVhdlType(static_cast<HardwareBlock::VHDL_TYPE>(i))) {
                if(ablk->multiple() > 1) {
                    sblkVHD[i] << "    " << ablk->name() << " : " << ablk->nameName() << HardwareBlock::VHDL_TYPE_TAGS[i] << "_blk_array_t" << ";" << std::endl;
                }
                else {
                    sblkVHD[i] << "    " << ablk->name() << " : " << ablk->nameName() << HardwareBlock::VHDL_TYPE_TAGS[i] << "_blk_t" << ";" << std::endl;
                }
            }
        }
    }

    // block type
    if(!blkName.empty()) {
        bool strg(false);
        for(unsigned int i=0; i<HardwareBlock::VHDL_TYPE_NUMBER; i++) {
            if(!sblkVHD[i].str().empty()) {
                strg = true;
                break;
            }
        }
        if(strg) {
            fpkgVHD << "  -----------------------------------------------------------------------------" << std::endl;
            fpkgVHD << "  -- Block \"" << blkName << "\" type(s)" << std::endl;
            fpkgVHD << "  -----------------------------------------------------------------------------" << std::endl;
            for(unsigned int i=0; i<HardwareBlock::VHDL_TYPE_NUMBER; i++) {
                if(!sblkVHD[i].str().empty()) {
                    fpkgVHD << "  type " << blkName << HardwareBlock::VHDL_TYPE_TAGS[i] << "_blk_t is record" << std::endl;
                    fpkgVHD << sblkVHD[i].str();
                    fpkgVHD << "  end record " << blkName << HardwareBlock::VHDL_TYPE_TAGS[i] << "_blk_t;" << std::endl;
                    fpkgVHD << std::endl;
                    if(blk->multiple() > 1) {
                        fpkgVHD << "  type " << blkName << HardwareBlock::VHDL_TYPE_TAGS[i] << "_blk_array_t is array (0 to " << blkName << "_NUMBER - 1) of " << blkName << HardwareBlock::VHDL_TYPE_TAGS[i] << "_blk_t;" << std::endl;
                        fpkgVHD << std::endl;
                    }
                    sblkVHD[i].str("");
                }
            }
        }
        if(!blk->getUpperBlock()) {
            for(unsigned int i=0; i<HardwareBlock::VHDL_TYPE_NUMBER; i++) {
                if(blk->getVhdlType(static_cast<HardwareBlock::VHDL_TYPE>(i))) {
                    if(blk->multiple() > 1) {
                        smodVHD[i] << "    " << blkName << " : " << blkName << HardwareBlock::VHDL_TYPE_TAGS[i] << "_blk_array_t;" << std::endl;
                    }
                    else {
                        smodVHD[i] << "    " << blkName << " : " << blkName << HardwareBlock::VHDL_TYPE_TAGS[i] << "_blk_t;" << std::endl;
                    }
                }
            }
        }
    }
    else {
        if(!blk->getUpperBlock()) {
            for(unsigned int i=0; i<HardwareBlock::VHDL_TYPE_NUMBER; i++) {
                smodVHD[i] << sblkVHD[i].str();
            }
        }
    }
}

//-----------------------------------------------------------------------------

void VHDLEmitter::compilePackageBlockAddress(const std::vector<const HardwareObject*>& vec, RArg& rarg, std::ostringstream& str) {

    const CompilerConfiguration* cfg = getCompiler()->getConfig();

    // initial indentation
    if(str.str().empty()) str << "    ";

    if(rarg.index < vec.size()) {
        const HardwareObject* obj(vec[rarg.index]);
        const unsigned int mlt(obj->multiple());
        RArg narg;

        // build a new recursive argument (stack variables)
        narg.index = rarg.index + 1;
        narg.addr = rarg.addr + obj->addr();

        // loop over all multiples at this index
        for(unsigned int cnt=0; cnt<mlt; cnt++) {

            // print debug if necessary
            if(cfg->print_level>=2) std::printf("compilePackageBlockAddress: idx = %d, cnt/mlt = %d/%d, addr = 0x%08x, obj = \"%s\"\n",rarg.index,cnt,mlt,narg.addr,obj->name().c_str());

            // print parentheses before
            if(cnt==0) {
                str << "(";
            }
            else {
                str << " ";
            }

            // descend one index
            compilePackageBlockAddress(vec,narg,str);

            // print parentheses after
            if(cnt < (mlt-1)) {
                str << ",";
                if(narg.index < (vec.size())) {
                    str << std::endl << "    ";
                    for(int i=0; i<int(narg.index-1); i++) str << " ";
                }
            }
            if(cnt == (mlt-1)) {
                str << ")";
            }
            narg.addr += obj->offset();
        }
    }
    else {

        // print debug if necessary
        if(cfg->print_level>=2) std::printf("compilePackageBlockAddress: idx = %d,  -- FINAL --   addr = 0x%08x\n",rarg.index, rarg.addr);

        // print address
        str << "16#" << nhex(getCompiler()->getModule()->form()) << rarg.addr << "#";
        return;
    }
}

//-----------------------------------------------------------------------------

void VHDLEmitter::compilePackageBody(const HardwareModule* mod) {

    // get package VHDL file
    CompilerOutputFile* pkgVHD;
    try {
        m_compiler->getPackageVHDLFile(&pkgVHD);
    }
    catch (const CompilerException& ce) {
        CERR("Exception \"%s\" while trying to get package VHDL files",ce.what());
        throw;
    }

    // get package output stream
    std::ofstream& fpkgVHD(pkgVHD->file());

    std::string modName, pkgName;

    HardwareList<HardwareBlock>::const_iterator iblk;
    const HardwareBlock* blk;
    HardwareList<HardwareElement>::const_iterator ielm;

    modName = mod->name();
    pkgName = modName + VHDL_PACKAGE_POSTFIX;

    // header
    fpkgVHD << std::endl
            << "-------------------------------------------------------------------------------" << std::endl
            << "-------------------------------------------------------------------------------" << std::endl
            << std::endl
            << "package body " << pkgName << " is" << std::endl;

    // data types defined outside a HardwareElement
    compileDataTypeBody(pkgVHD);

    // loop over all blocks
    for(iblk=mod->beginBlock(); iblk!=mod->endBlock(); iblk++) {
        blk = *iblk;
        compileDataTypeBody(blk,pkgVHD);
    }

    // trailer
    fpkgVHD << std::endl
            << "end package body " << pkgName << ";" << std::endl;
}

//-----------------------------------------------------------------------------

void VHDLEmitter::compileDataTypeBody(CompilerOutputFile* pkgVHD) {

    HardwareCompiler::HardwareDataTypeMapConstIterator idat;
    HardwareList<const HardwareDataType>* datList;
    HardwareList<const HardwareDataType>::const_iterator jdat;
    const HardwareDataType* dat;

    // get all data types
    for(idat=getCompiler()->beginDataTypeMap(); idat!=getCompiler()->endDataTypeMap(); idat++) {
        datList = idat->second;
        dat = *(datList->begin());

        // loop over all data of same type
        for(jdat=datList->begin(); jdat!=datList->end(); jdat++) {
            dat = *jdat;
            if(!(dat->getElement())) compileDataTypeBody(dat,pkgVHD);
        }
    }
}

//-----------------------------------------------------------------------------

void VHDLEmitter::compileDataTypeBody(const HardwareBlock* blk, CompilerOutputFile* pkgVHD) {

    HardwareList<HardwareElement>::const_iterator ielm;
    const HardwareElement* elm;
    const HardwareDataType* dat;
    HardwareList<HardwareBlock>::const_iterator iblk;

    // loop over all elements
    for(ielm=blk->beginElement(); ielm!=blk->endElement(); ielm++) {
        elm = *ielm;
        dat = elm->getDataType();
        compileDataTypeBody(dat,pkgVHD);
    }

    // loop over all nested blocks
    for(iblk=blk->beginBlock(); iblk!=blk->endBlock(); iblk++) {
        compileDataTypeBody(*iblk,pkgVHD);
    }
}

//-----------------------------------------------------------------------------

void VHDLEmitter::compileDataTypeBody(const HardwareDataType* dat, CompilerOutputFile* pkgVHD) {

    const HardwareWord* wrd;
    const HardwareBitString* bit;

    // attempt converting to HardwareWord
    if((wrd = dynamic_cast<const HardwareWord*>(dat))) {
        // nothing to be done
        return;
    }
    // attempt converting to BitString
    else if((bit = dynamic_cast<const HardwareBitString*>(dat))) {
        compileBitStringBody(bit,pkgVHD);
    }
    else {
        CERR("Unknown HardwareDataType \"%s\"",dat->fileName().c_str());
        throw CompilerException("UNKNOWN DATA TYPE");
    }
}

//-----------------------------------------------------------------------------

void VHDLEmitter::compileBitStringBody(const HardwareBitString* bit, CompilerOutputFile* pkgVHD) {

    // if bit string is a reference
    if(bit->isReference()) return;

    // return if element not register
    const HardwareElement* elm = bit->getElement();
    const HardwareRegister* reg = dynamic_cast<const HardwareRegister*>(elm);
    if((elm) && !(reg)) return;

    std::string blkName, elmName, elmType, slvType, fldName, fulName, fldType, valName;
    std::ostringstream sslvVHD, cmt;

    HardwareList<HardwareField>::const_iterator ifld;
    HardwareField* fld;

    blkName = (elm) ? (elm->getBlock()->nameName().empty() ? "" : elm->getBlock()->nameName()) : "";
    elmName = (elm) ? (blkName.empty() ? elm->name() : (blkName+"_"+elm->name())) : bit->nameName();
    elmType = elmName + "_reg_t";
    slvType = elmName + "_slv_t";

    // get data type output streams
    std::ofstream& fpkgVHD(pkgVHD->file());

    cmt << (blkName.empty()?"":("block \""+blkName+"\""));
    if(reg) {
        cmt << (cmt.str().empty()?"register":", register") << (" \""+reg->name()+"\"");
        if(reg->multiple()>1) cmt << " array";
    }
    else {
        cmt << (cmt.str().empty()?"":" ") << ("\""+elmName+"\"");
    }

    // header
    fpkgVHD << std::endl
            << "  -----------------------------------------------------------------------------" << std::endl
            << "  -- Conversion functions for " << cmt.str() << std::endl
            << "  -----------------------------------------------------------------------------" << std::endl;

    // conversion function REG -> SLV
    fpkgVHD << std::endl
            << "  function reg2slv (reg : " << elmType << ") return " << slvType << " is" << std::endl
            << "    variable slv : " << slvType << ";" << std::endl;
    if(bit->maxMultipleField()>1) {
        fpkgVHD << "    variable idx : integer;" << std::endl;
    }
    fpkgVHD << "  begin" << std::endl
            << "    slv := (others => '0');" <<std::endl;

    // for all fields
    for(ifld=bit->beginField(); ifld!= bit->endField(); ifld++) {
        fld = *ifld;
        fldName = fld->name();
        fulName = (elm) ? (blkName.empty() ? (elm->name()+"_"+fld->name()) : (blkName+"_"+elm->name()+"_"+fld->name())) : (bit->name()+"_"+fld->name());
        transform(fulName.begin(),fulName.end(),fulName.begin(),toupper);

        // multiple field
        if(fld->multiple()>1) {

            // REG -> SLV conversion
            fpkgVHD << "    idx := " << fulName << "_SHIFT;" << std::endl
                    << "    for i in 0 to " << fulName << "_NUMBER-1 loop" << std::endl;
            if(HardwareCommon::getMaskWidth(fld->mask())>1) {
                fpkgVHD << "      slv(idx+" << fulName << "_WIDTH-1 downto idx) := reg." << fldName << "(i);" << std::endl;
            }
            else {
                fpkgVHD << "      slv(idx) := reg." << fldName << "(i);" << std::endl;
            }
            fpkgVHD << "      idx := idx + " << fulName << "_OFFSET;" << std::endl
                    << "    end loop;  -- i" << std::endl;

            // SLV -> REG conversion
            sslvVHD << "    idx := " << fulName << "_SHIFT;" << std::endl
                    << "    for i in 0 to " << fulName << "_NUMBER-1 loop" << std::endl;
            if(HardwareCommon::getMaskWidth(fld->mask())>1) {
                sslvVHD << "      reg." << fld->name() << "(i) := slv(idx+" << fulName << "_WIDTH-1 downto idx);" << std::endl;
            }
            else {
                sslvVHD << "      reg." << fld->name() << "(i) := slv(idx);" << std::endl;
            }
            sslvVHD << "      idx := idx + " << fulName << "_OFFSET;" << std::endl
                    << "    end loop;  -- i" << std::endl;
        }

        // single field
        else {
            if(HardwareCommon::getMaskWidth(fld->mask())>1) {

                fpkgVHD << "    slv(" << fulName << "_WIDTH+" << fulName <<"_SHIFT-1 downto " << fulName << "_SHIFT) := reg." << fld->name() << ";" << std::endl;
                sslvVHD << "    reg." << fld->name() << " := slv(" << fulName << "_WIDTH+" << fulName << "_SHIFT-1 downto " << fulName << "_SHIFT);" << std::endl;
            }
            else {
                fpkgVHD << "    slv(" << fulName << "_SHIFT) := reg." << fld->name() << ";" << std::endl;
                sslvVHD << "    reg." << fld->name() << " := slv(" << fulName << "_SHIFT);" << std::endl;
            }
        }
    }

    fpkgVHD << "    return slv;" << std::endl
            << "  end reg2slv;" <<std::endl;

    // conversion function SLV -> REG
    fpkgVHD << std::endl
            << "  function slv2reg (slv : " << slvType << ") return " << elmType << " is" << std::endl
            << "    variable reg : " << elmType << ";" << std::endl;
    if(bit->maxMultipleField()>1) {
        fpkgVHD << "    variable idx : integer;" << std::endl;
    }
    fpkgVHD << "  begin" << std::endl
            << sslvVHD.str()
            << "    return reg;" << std::endl
            << "  end slv2reg;" <<std::endl;
}

//-----------------------------------------------------------------------------

void VHDLEmitter::compileExample(const HardwareModule* /*mod*/) {

    // to be done
}
