//******************************************************************************
// file: CompilerAddressMap.cc
// desc: Address Map for L1CT HardwareCompiler
// auth: 27-MAY-2013 R. Spiwoks
//******************************************************************************

#include <sstream>

#include "RCDUtilities/RCDUtilities.h"
#include "L1CTHardwareCompiler/CompilerAddressMap.h"
#include "L1CTHardwareCompiler/CompilerException.h"
#include "L1CTHardwareCompiler/HardwareCompiler.h"
#include "L1CTHardwareCompiler/HardwareBlock.h"
#include "L1CTHardwareCompiler/HardwareElement.h"
#include "L1CTHardwareCompiler/HardwareRegister.h"
#include "L1CTHardwareCompiler/HardwareMemory.h"
#include "L1CTHardwareCompiler/HardwareCommon.h"

using namespace L1CTHC;

//------------------------------------------------------------------------------

CompilerAddressMap::CompilerAddressItem::CompilerAddressItem(std::vector<const HardwareBlock*>& bvec, std::vector<unsigned int>& bnum, const HardwareElement* elm, const unsigned int eist) {

    std::ostringstream buf;

    // reset lower address
    m_lower_address = 0;

    // loop over blocks
    if(bvec.size() > 0) {
        const HardwareBlock* ablk;
        unsigned int i;
        bool flag;

        for(flag=false, i=0; i<bvec.size(); i++) {
            ablk = bvec[i];

            // add block name
            if(!ablk->name().empty()) {
                m_name += ablk->name();
                if(ablk->multiple()>1) {
                    buf.str(""); buf << "[" << bnum[i] << "]";
                    m_name += buf.str();
                }
                flag = true;
            }
            if(flag) m_name += "/";

            // add block address and offset to lower address
            m_lower_address += ablk->addr() + bnum[i]*ablk->offset();
        }
    }

    // add element name
    m_name += elm->name();
    if(elm->multiple()>1) {
        buf.str(""); buf << "[" << eist << "]";
        m_name += buf.str();
    }

    // add element address and offset to lower address
    m_lower_address += elm->addr() + eist*elm->offset();

    // calculate upper address
    m_upper_address = m_lower_address + elm->addrSize() - 1;
}

//------------------------------------------------------------------------------

CompilerAddressMap::CompilerAddressItem::~CompilerAddressItem() {

    // nothing to be done
}

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

CompilerAddressMap::CompilerAddressMap() : m_compiler(0) {

    // nothing to be done
}

//------------------------------------------------------------------------------

CompilerAddressMap::~CompilerAddressMap() {

    // nothing to be done
}

//------------------------------------------------------------------------------

void CompilerAddressMap::addElement(const HardwareElement* elm) {

    HardwareList<const HardwareBlock> blst;
    HardwareList<const HardwareBlock>::iterator ilst;

    std::vector<unsigned int> bnum;
    unsigned int indx = 0;

    elm->getBlock()->getBlockHierarchy(blst);
    m_current_blocks.clear();
    for(ilst=blst.begin(); ilst!=blst.end(); ilst++) m_current_blocks.push_back(*ilst);
    m_current_element = elm;

    addBlocksAndElement(bnum,indx);
}

//------------------------------------------------------------------------------

void CompilerAddressMap::addBlocksAndElement(std::vector<unsigned int>& bnum, const unsigned int indx) {

    // create CompilerAddressItems when at end of block hierarchy
    if(indx >= m_current_blocks.size()) {
        const CompilerAddressItem* ritm;
        std::ostringstream buf;
        unsigned int ielm;

        // loop over element if multiple
        for(ielm=0; ielm<m_current_element->multiple(); ielm++) {
            CompilerAddressItem* item = new CompilerAddressItem(m_current_blocks,bnum,m_current_element,ielm);

            // check if address used
            if(findOverlap(item,&ritm)) {

                if(getCompiler()->getConfig()->strict_check) {
                    CERR("address of hardware element \"%s\" ([0x%08x .. 0x%08x]) overlaps with\n>> address of hardware element \"%s\" ([0x%08x .. 0x%08x])",item->name().c_str(),item->getLowerAddress(),item->getUpperAddress(),ritm->name().c_str(),ritm->getLowerAddress(),ritm->getUpperAddress());
                    throw CompilerException("ADDRESS ALREADY USED");
                }
                else {
                    if(getCompiler()->getConfig()->print_level>=1) COUT("address of hardware element \"%s\" ([0x%08x .. 0x%08x]) overlaps with\n>> address of hardware element \"%s\" ([0x%08x .. 0x%08x]) => CONTINUE USING BOTH",item->name().c_str(),item->getLowerAddress(),item->getUpperAddress(),ritm->name().c_str(),ritm->getLowerAddress(),ritm->getUpperAddress());
                }
            }

            // insert address into map
            m_map.insert(std::make_pair(item->getLowerAddress(),item));
        }
        return;
    }

    // call method recursively if not at end of block hierarchy
    for(unsigned int i=0; i<m_current_blocks[indx]->multiple(); i++) {
        std::vector<unsigned int> cnum(m_current_blocks.size());

        for(unsigned int j=0; j<indx; j++) cnum[j] = bnum[j];
        cnum[indx] = i;
        addBlocksAndElement(cnum,indx+1);
    }
}

//------------------------------------------------------------------------------

bool CompilerAddressMap::findOverlap(const CompilerAddressItem* itma, const CompilerAddressItem** ritm) const {

    std::map<unsigned int, const CompilerAddressItem*>::const_iterator itm;

    // check addresses
    for(itm=m_map.begin(); itm!=m_map.end(); itm++) {
        if(findOverlap(itma,itm->second,false)) {
            *ritm = itm->second;
            return(true);
        }
    }

    return(false);
}

//------------------------------------------------------------------------------

bool CompilerAddressMap::findOverlap(const CompilerAddressItem* itma, const CompilerAddressItem* itmb, const bool bound) const {

    unsigned int loa, upa, lob, upb;

    // check address overlap, include boundary if necessary
    loa = itma->getLowerAddress();
    upa = itma->getUpperAddress(); if(bound) upa += 1;
    lob = itmb->getLowerAddress();
    upb = itmb->getUpperAddress(); if(bound) upb += 1;
    return(((loa >= lob) && (loa <= upb)) || ((upa >= lob) && (upa <= upb)));
}

//------------------------------------------------------------------------------

void CompilerAddressMap::dump() const {

    std::map<unsigned int, const CompilerAddressItem*>::const_iterator itm;
    const CompilerAddressItem* pitm(0);

    std::printf("================================================================================\n");
    std::printf("ADDRESS MAP:\n");
    std::printf("--------------------------------------------------------------------------------\n");
    std::printf("LOWER ADDR - UPPER ADDR => HardwareElement\n");
    std::printf("--------------------------------------------------------------------------------\n");
    for(itm=m_map.begin(); itm!=m_map.end(); itm++) {
        if((itm!=m_map.begin()) && (!findOverlap(pitm,itm->second,true))) {
            std::printf("--------------------------------------------------------------------------------\n");
        }
        std::printf("0x%08x - 0x%08x => \"%s\"\n",itm->first,itm->second->getUpperAddress(),itm->second->name().c_str());
        pitm = itm->second;
    }
}

//------------------------------------------------------------------------------

void CompilerAddressMap::setCompiler(HardwareCompiler* compiler) {

    if(m_compiler) {
        CERR("CompilerAddressMap already attached to HardwareCompiler","");
        throw CompilerException("COMPILER ALREADY SET");
    }
    m_compiler = compiler;
}
