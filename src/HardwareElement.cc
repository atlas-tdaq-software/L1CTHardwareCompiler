//******************************************************************************
// file: HardwareElement.cc
// desc: library for L1CT HardwareCompiler
// auth: 01-MAR-2013 R. Spiwoks, re-design from RCDModuleDesign
//******************************************************************************

#include <sstream>
#include <algorithm>

#include "L1CTHardwareCompiler/HardwareCommon.h"
#include "L1CTHardwareCompiler/HardwareElement.h"
#include "L1CTHardwareCompiler/HardwareDataType.h"

using namespace L1CTHC;

//------------------------------------------------------------------------------

const std::string HardwareElement::CPP_MASK_PREFIX = "MASK_";

//------------------------------------------------------------------------------

HardwareElement::HardwareElement(const std::string& name) : HardwareObject(name), m_block(0), m_mask(DATA_TYPE_MMAX[D32]), m_data(0), m_datb(0) {

    // nothing to be done
}

//------------------------------------------------------------------------------

HardwareElement::~HardwareElement() {

    // nothing to be done
}

//------------------------------------------------------------------------------

std::string HardwareElement::maskName() const {

    std::string n(CPP_MASK_PREFIX + nameName());
    transform(n.begin(),n.end(),n.begin(),toupper);
    return(n);
}
