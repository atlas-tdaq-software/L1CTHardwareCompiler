//******************************************************************************
// file: VMEbusEmitter.cc
// desc: library for L1CT HardwareCompiler
// auth: 25-APR-2013 R. Spiwoks
//******************************************************************************

#include <vector>
#include <algorithm>

#include "RCDUtilities/RCDUtilities.h"
#include "L1CTHardwareCompiler/VMEbusEmitter.h"
#include "L1CTHardwareCompiler/CompilerOutputFile.h"
#include "L1CTHardwareCompiler/CompilerException.h"
#include "L1CTHardwareCompiler/CompilerStringList.h"
#include "L1CTHardwareCompiler/HardwareCompiler.h"
#include "L1CTHardwareCompiler/HardwareBlock.h"
#include "L1CTHardwareCompiler/HardwareElement.h"
#include "L1CTHardwareCompiler/HardwareRegister.h"
#include "L1CTHardwareCompiler/HardwareMemory.h"
#include "L1CTHardwareCompiler/HardwareBitString.h"
#include "L1CTHardwareCompiler/HardwareWord.h"
#include "L1CTHardwareCompiler/HardwareField.h"
#include "L1CTHardwareCompiler/HardwareValue.h"
#include "L1CTHardwareCompiler/HardwareCommon.h"

using namespace L1CTHC;

//------------------------------------------------------------------------------

VMEbusEmitter::VMEbusEmitter() : CompilerCCEmitter() {

    // nothing to be done
}

//------------------------------------------------------------------------------

VMEbusEmitter::~VMEbusEmitter() {

    // nothing to be done
}

//-----------------------------------------------------------------------------

std::string VMEbusEmitter::compileModuleHardwareIncludesDeclaration(const HardwareModule* /* mod */) {

    std::ostringstream buf;

    // nothing to be done

    return(buf.str());
}

//-----------------------------------------------------------------------------

std::string VMEbusEmitter::compileModuleHardwareIncludesDefinition(const HardwareModule* /* mod */) {

    std::ostringstream buf;

    // nothing to be done

    return(buf.str());
}

//-----------------------------------------------------------------------------

std::string VMEbusEmitter::compileModuleHardwareCtorDtorDeclaration(const HardwareModule* mod) {

    std::ostringstream buf;
    std::string modName = mod->name();

    buf << "    " << modName << "(RCD::VME*, unsigned int);" << std::endl
        << "   ~" << modName << "();" << std::endl
        << std::endl
        << "    RCD::VME* getVme() const                    { return(m_vme); }" << std::endl
        << "    RCD::VMEMasterMap* getVmeMasterMap() const  { return(m_vmm); }" << std::endl
        << "    unsigned int getOffset() const              { return(m_off); }" << std::endl
        << "    unsigned int operator()() const             { return(m_status); }" << std::endl
        << std::endl;

    return(buf.str());
}

//-----------------------------------------------------------------------------

std::string VMEbusEmitter::compileModuleHardwareCtorDtorDefinition(const HardwareModule* mod) {

    std::ostringstream buf;
    std::string modName, addName, sizName, typName, amdName;

    modName = mod->name();
    addName = mod->addrName();
    sizName = mod->sizeName();
    typName = mod->amodName();
    amdName = mod->codeName();

    buf << "//------------------------------------------------------------------------------" << std::endl
        << std::endl
        << modName << "::" << modName << "(RCD::VME* vme, unsigned int off) : m_vme(vme), m_vmm(0) {" << std::endl
        << std::endl
        << "    // set offset" << std::endl
        << "    m_off = off + " << addName << ";" << std::endl
        << std::endl
        << "    // open VME master mapping" << std::endl
        << "    m_vmm = m_vme->MasterMap(m_off," << sizName << "," << typName << ",0);" << std::endl
        << "    if((m_status = ((*m_vmm)()))) {" << std::endl
        << "        CERR(\"" << modName << ": creating VME master mapping at addr = 0x%08x, size = 0x%08x, type = \\\"%s\\\"\",m_off," << sizName << "," << amdName << ".c_str());" << std::endl
        << "        return;" << std::endl
        << "    }" << std::endl
        << "}" << std::endl
        << std::endl
        << "//------------------------------------------------------------------------------" << std::endl
        << std::endl
        << modName << "::~" << modName << "() {" << std::endl
        << std::endl
        << "    // close VME master mapping" << std::endl
        << "    if(m_vmm) m_vme->MasterUnmap(m_vmm);" << std::endl
        << "}" << std::endl
        << std::endl;

    return(buf.str());
}

//-----------------------------------------------------------------------------

std::string VMEbusEmitter::compileModuleHardwareMembers(const HardwareModule* /* mod */) {

    std::ostringstream buf;

    buf << "    RCD::VME*             m_vme;" << std::endl
        << "    RCD::VMEMasterMap*    m_vmm;" << std::endl
        << "    unsigned int          m_off;" << std::endl
        << "    unsigned int          m_status;" << std::endl
        << std::endl;

    return(buf.str());
}

// -----------------------------------------------------------------------------

std::string VMEbusEmitter::compileModuleHardwareReadSingle(const HardwareElement* elm) {

    const CompilerConfiguration* cfg = getCompiler()->getConfig();
    const HardwareBlock* blk = elm->getBlock();
    const HardwareMemory* mem = dynamic_cast<const HardwareMemory*>(elm);

    std::ostringstream buf;
    CompilerStringList add; add.delimiter('+');

    // compose address
    compileModuleHardwareBlockList(blk,add);
    add.add(elm->addrName());
    if(elm->multiple()>1) add.add("nelm*"+elm->offsetName());
    if(mem && (mem->access()==HardwareMemory::RAM)) add.add("indx*"+HardwareObject::DATA_TYPE_BYTE(mem->type()));

    // emit read single
    if(cfg->debug_level>=1) buf << "    COUT(\"reading single addr = 0x%08x\"," << add.str() << ");" <<std::endl;
    buf << "    rtnv = m_vmm->ReadSafe(" << add.str() << ",&rdat);" << std::endl;

    return(buf.str());
}

// -----------------------------------------------------------------------------

std::string VMEbusEmitter::compileModuleHardwareWriteSingle(const HardwareElement* elm) {

    const CompilerConfiguration* cfg = getCompiler()->getConfig();
    const HardwareBlock* blk = elm->getBlock();
    const HardwareMemory* mem = dynamic_cast<const HardwareMemory*>(elm);

    std::ostringstream buf;
    CompilerStringList add; add.delimiter('+');

    // compose address
    compileModuleHardwareBlockList(blk,add);
    add.add(elm->addrName());
    if(elm->multiple()>1) add.add("nelm*"+elm->offsetName());
    if(mem && (mem->access()==HardwareMemory::RAM)) add.add("indx*"+HardwareObject::DATA_TYPE_BYTE(mem->type()));

    // emit write single
    if(cfg->debug_level>=1) buf << "    COUT(\"writing single addr = 0x%08x, data = 0x%08x\"," << add.str() << ",wdat);" <<std::endl;
    buf << "    rtnv = m_vmm->WriteSafe(" << add.str() << ",wdat);" << std::endl;

    return(buf.str());
}

// -----------------------------------------------------------------------------

std::string VMEbusEmitter::compileModuleHardwareReadVector(const HardwareElement* elm) {

    const HardwareMemory* mem = dynamic_cast<const HardwareMemory*>(elm);

    if(!mem) return("");

    const CompilerConfiguration* cfg = getCompiler()->getConfig();
    const HardwareBlock* blk = elm->getBlock();

    std::string typName, elmMask, incAddr;
    std::ostringstream buf;
    CompilerStringList add; add.delimiter('+');

    typName = HardwareObject::DATA_TYPE_TYPE[elm->type()];
    elmMask = (elm->mask() != HardwareObject::DATA_TYPE_MMAX[elm->type()]) ? ("&"+elm->maskName()) : "";

    // compose address
    compileModuleHardwareBlockList(blk,add);
    add.add(elm->addrName());
    if(elm->multiple()>1) add.add("nelm*"+elm->offsetName());
    if(mem->access()==HardwareMemory::RAM) add.add("vme_off*sizeof("+typName+")");

    // emit read vector
    buf << "    unsigned int addr = " << add.str() << ";" << std::endl;
    if(cfg->debug_level>=1) buf << "    COUT(\"reading vector addr = 0x%08x, size = 0x%08x\",addr,data.size());" << std::endl;
    buf << "    " << typName << " rdat;" << std::endl
        << "    for(unsigned int i=0; i<data.size(); i++) {" << std::endl
        << "        if((rtnv = m_vmm->ReadSafe(addr,&rdat)) != VME_SUCCESS) return(rtnv);" << std::endl
        << "        data[i] = rdat" << elmMask << ";" << std::endl;
    if(mem->access() == HardwareMemory::RAM) buf << "        addr += sizeof(" << typName << ");" << std::endl;
    buf << "    }" << std::endl
        << std::endl;

    return(buf.str());
}

// -----------------------------------------------------------------------------

std::string VMEbusEmitter::compileModuleHardwareWriteVector(const HardwareElement* elm) {

    const CompilerConfiguration* cfg = getCompiler()->getConfig();
    const HardwareBlock* blk = elm->getBlock();
    const HardwareMemory* mem = dynamic_cast<const HardwareMemory*>(elm);

    std::string typName, elmMask;
    std::ostringstream buf;
    CompilerStringList add; add.delimiter('+');

    typName = HardwareObject::DATA_TYPE_TYPE[elm->type()];
    elmMask = (elm->mask() != HardwareObject::DATA_TYPE_MMAX[elm->type()]) ? ("&"+elm->maskName()) : "";

    // compose address
    compileModuleHardwareBlockList(blk,add);
    add.add(elm->addrName());
    if(elm->multiple()>1) add.add("nelm*"+elm->offsetName());
    if(mem->access()==HardwareMemory::RAM) add.add("vme_off*sizeof("+typName+")");

    // emit write vector
    buf << "    unsigned int addr = " << add.str() << ";" << std::endl;
    if(cfg->debug_level>=1) buf << "    COUT(\"writing vector addr = 0x%08x, size = 0x%08x\",addr,data.size());" << std::endl;
    buf << "    " << typName << " wdat;" << std::endl
        << "    for(unsigned int i=0; i<data.size(); i++) {" << std::endl
        << "        wdat = data[i]" << elmMask << ";" << std::endl
        << "        if((rtnv = m_vmm->WriteSafe(addr,wdat)) != VME_SUCCESS) return(rtnv);" << std::endl;
    if(mem->access()==HardwareMemory::RAM) {
        buf << "         addr += sizeof(" << typName << ");" << std::endl;
    }
    buf << "    }" << std::endl
        << std::endl;

    return(buf.str());
}

// -----------------------------------------------------------------------------

std::string VMEbusEmitter::compileModuleHardwareReadBlock(const HardwareElement* elm) {

    const CompilerConfiguration* cfg = getCompiler()->getConfig();
    const HardwareBlock* blk = elm->getBlock();
    const HardwareMemory* mem = dynamic_cast<const HardwareMemory*>(elm);

    std::string typName;
    std::ostringstream buf;
    CompilerStringList add; add.delimiter('+');

    typName = HardwareObject::DATA_TYPE_TYPE[elm->type()];

    // compose address
    compileModuleHardwareBlockList(blk,add);
    add.add(elm->addrName());
    if(elm->multiple()>1) add.add("nelm*"+elm->offsetName());
    if(mem->access()==HardwareMemory::RAM) add.add("vme_off*sizeof("+typName+")");

    // emit read block
    if(cfg->debug_level>=1) buf << "    COUT(\"reading block addr = 0x%08x, size = 0x%08x\",m_off+" << add.str() << ",size);" << std::endl;
    buf << "    rtnv = m_vme->RunBlockTransfer(*seg,seg_off,m_off+" << add.str() << ",size,VME_DMA_D32R);" << std::endl
        << std::endl;

    return(buf.str());
}

// -----------------------------------------------------------------------------

std::string VMEbusEmitter::compileModuleHardwareWriteBlock(const HardwareElement* elm) {

    const CompilerConfiguration* cfg = getCompiler()->getConfig();
    const HardwareBlock* blk = elm->getBlock();
    const HardwareMemory* mem = dynamic_cast<const HardwareMemory*>(elm);

    std::string typName;
    std::ostringstream buf;
    CompilerStringList add; add.delimiter('+');

    typName = HardwareObject::DATA_TYPE_TYPE[elm->type()];

    // compose address
    compileModuleHardwareBlockList(blk,add);
    add.add(elm->addrName());
    if(elm->multiple()>1) add.add("nelm*"+elm->offsetName());
    if(mem->access()==HardwareMemory::RAM) add.add("vme_off*sizeof("+typName+")");

    // emit write block
    if(cfg->debug_level>=1) buf << "    COUT(\"writing block addr = 0x%08x, size = 0x%08x\",m_off+" << add.str() << ",size);" << std::endl;
    buf << "    rtnv = m_vme->RunBlockTransfer(*seg,seg_off,m_off+" << add.str() << ",size,VME_DMA_D32W);" << std::endl
        << std::endl;

    return(buf.str());
}

// -----------------------------------------------------------------------------

std::string VMEbusEmitter::compileModuleHardwareReadFile(const HardwareElement* elm) {

    return(std::string());
}

// -----------------------------------------------------------------------------

std::string VMEbusEmitter::compileModuleHardwareWriteFile(const HardwareElement* elm) {

    return(std::string());
}

// -----------------------------------------------------------------------------

std::string VMEbusEmitter::compileMenuHardwareGlobals(const HardwareModule* /* mod */) {

    std::ostringstream buf;

    buf << "// VME singleton" << std::endl
        << "RCD::VME* vme;" << std::endl
        << std::endl;

    return(buf.str());
}

// -----------------------------------------------------------------------------

std::string VMEbusEmitter::compileMenuHardwareOpen(const HardwareModule* mod) {

    std::ostringstream buf;
    std::string modName, modDesc, addName, offName, sizName, amtName, amdName;
    unsigned int modMmax;

    modName = mod->name();
    modDesc = mod->desc();
    modMmax = HardwareObject::ADDR_TYPE_MMAX[mod->amod()];
    addName = modName + "::addr()";
    offName = "mod->getOffset()";
    sizName = modName + "::size()";
    amtName = modName + "::amod()";
    amdName = modName + "::code()";

    buf << "        // open VME driver/library" << std::endl
        << "        vme = RCD::VME::Open();" << std::endl
        << std::endl
        << "        // get module offset" << std::endl
        << "        std::printf(\"Module \\\"" << modName << "\\\" (\\\"" << modDesc << "\\\"): addr = 0x%08x\\n\"," << addName << ");" <<std::endl
        << "        unsigned int off = (unsigned int)enterHex(\"Module \\\"" << modName << "\\\" address offset (hex)\",0," << hex(8) << modMmax << dec << ");" <<std::endl
        << "        if(off < 0x100) off = off << 24;" << std::endl
        << std::endl
        << "        // create " << modName << " module" << std::endl
        << "        mod = new " << modName << "(vme,off);" << std::endl
        << "        if((*mod)()) {" << std::endl
        << "            CERR(\"opening module \\\"" << modName << "\\\" at vme = 0x%08x, size = 0x%08x, type = \\\"%s\\\"\"," << offName << "," << sizName << "," << amdName << ".c_str());" << std::endl
        << "            std::exit(-1);" << std::endl
        << "        }" <<std::endl
        << "        COUT(\"opened module \\\"" << modName << "\\\" at vme = 0x%08x, size = 0x%08x, type = \\\"%s\\\"\"," << offName << "," << sizName << "," << amdName << ".c_str());" << std::endl
        << std::endl;

    return(buf.str());
}

// -----------------------------------------------------------------------------

std::string VMEbusEmitter::compileMenuHardwareClose(const HardwareModule* mod) {

    std::ostringstream buf;
    std::string modName;

    modName = mod->name();

    buf << "        // delete " << modName << " module" << std::endl
        << "        delete mod;" << std::endl
        << std::endl
        << "        // close VME driver/library" << std::endl
        << "        RCD::VME::Close();" << std::endl
        << "        COUT(\"closed module \\\"" << modName << "\\\"\",\"\");" << std::endl
        << std::endl;

    return(buf.str());
}

//-----------------------------------------------------------------------------

void VMEbusEmitter::compileModuleHardwareBlockList(const HardwareBlock* blk, CompilerStringList& par) {

    const CompilerConfiguration* cfg = getCompiler()->getConfig();
    HardwareList<const HardwareBlock> blst;
    HardwareList<const HardwareBlock>::iterator ilst;
    const HardwareBlock* ablk;
    unsigned int iblk;
    std::ostringstream buf;

    blk->getBlockHierarchy(blst);
    if(cfg->print_level>=4) COUT("blk = \"%s\"",blk->name().c_str());

    for(iblk=0, ilst=blst.begin(); ilst!=blst.end(); ilst++, iblk++) {
        ablk = *ilst;

        if(cfg->print_level>=4) std::printf("blk[%d] = \"%s\"\n",iblk,ablk->name().c_str());
        if(ablk->addr()) par.add(ablk->addrName());

        if(ablk->multiple() > 1) {
            buf.str(""); buf << "nbk" << iblk << "*" << ablk->offsetName();
            par.add(buf.str());
        }
    }
}
