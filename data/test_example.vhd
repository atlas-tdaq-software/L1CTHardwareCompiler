-------------------------------------------------------------------------------
-- Title      : Example of using package generated by HardwareCompiler
-- Project    : HardwareCompiler
-------------------------------------------------------------------------------
-- File       : test_example.vhd
-- Author     : Stefan Haas
-- Company    : CERN PH-ESE
-- Created    : 18-07-13
-- Last update: 19-07-13
-- Platform   : Windows 7
-- Standard   : VHDL-2008
-------------------------------------------------------------------------------
-- Description: 
-------------------------------------------------------------------------------
-- Copyright (c) 2013 CERN PH-ESE
-------------------------------------------------------------------------------
-- Revisions  :
-- Date      Version  Author  Description
-- 18-07-13  1.0      haass   Created
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std_unsigned.all;

use work.test_pkg.all;

entity test_example is
  
  port (clk     : in  std_logic;
        rst     : in  std_logic;
        -- VME user interface
        wr      : in  std_logic;
        addr    : in  std_logic_vector(MODULE_ADDR_WIDTH-1 downto 0);
        din     : in  slv_32b;
        dout    : out slv_32b;
        -- Register interface
        mod_out : out TEST_mod_t);

end entity test_example;

-------------------------------------------------------------------------------

architecture rtl of test_example is

begin  -- architecture rtl

  -----------------------------------------------------------------------------
  -- Write registers
  -----------------------------------------------------------------------------
  write_regs_p : process (clk, rst) is
    variable vme_addr : slv_32b;
    variable index    : integer;
  begin  -- process
    if rst = '1' then                   -- asynchronous reset (active high)
      mod_out.Reg0 <= (others         => '0');
      mod_out.Reg1 <= slv2reg((others => '0'));
--      mod_out.Reg2 <= (others => '0');
    elsif rising_edge(clk) then         -- rising clock edge
      if (wr) then
        vme_addr := resize(addr, 32) sll 2;
        case? vme_addr is
          when REG0_ADDR         => mod_out.Reg0         <= din(Reg0_reg_t'range);
          when REG1_ADDR         => mod_out.Reg1         <= slv2reg(din(Reg1_slv_t'range));
-- when REG2_ADDR =>
--            index               := to_integer(vme_addr srl REG2_OFFS_SHIFT);
--            mod_out.Reg2(index) <= slv2reg(din(Reg2_slv_t'range));
          when REG2_ADDR(0)      => mod_out.Reg2(0)      <= slv2reg(din(Reg2_slv_t'range));
          when REG2_ADDR(1)      => mod_out.Reg2(1)      <= slv2reg(din(Reg2_slv_t'range));
          when BBLK_REG3_ADDR(0) => mod_out.BBLK.Reg3(0) <= slv2reg(din(BBLK_Reg3_slv_t'range));
          when BBLK_REG3_ADDR(1) => mod_out.BBLK.Reg3(1) <= slv2reg(din(BBLK_Reg3_slv_t'range));
          when BBLK_REG4_ADDR(0) => mod_out.BBLK.Reg4(0) <= slv2reg(din(BBLK_Reg4_slv_t'range));
          when BBLK_REG4_ADDR(1) => mod_out.BBLK.Reg4(1) <= slv2reg(din(BBLK_Reg4_slv_t'range));
          when BBLK_REG4_ADDR(2) => mod_out.BBLK.Reg4(2) <= slv2reg(din(BBLK_Reg4_slv_t'range));
          when BBLK_REG4_ADDR(3) => mod_out.BBLK.Reg4(3) <= slv2reg(din(BBLK_Reg4_slv_t'range));
          when others            => null;
        end case?;
      end if;
    end if;
  end process;

  -----------------------------------------------------------------------------
  -- Read registers
  -----------------------------------------------------------------------------
  read_regs_p : process (clk) is
    variable vme_addr : slv_32b;
    variable index    : integer;
  begin  -- process
    if rising_edge(clk) then            -- rising clock edge
      dout     <= 32B"0";               -- default value
      vme_addr := resize(addr, 32) sll 2;
      case? vme_addr is
        when REG0_ADDR         => dout(Reg0_reg_t'range) <= mod_out.Reg0;
        when REG1_ADDR         => dout(Reg1_slv_t'range) <= reg2slv(mod_out.Reg1);
-- when REG2_ADDR =>
--          index                  := to_integer((vme_addr and REG2_OFFS_MASK) srl REG2_OFFS_SHIFT);
--          dout(Reg2_slv_t'range) <= reg2slv(mod_out.Reg2(index));
        when REG2_ADDR(0)      => dout(Reg2_slv_t'range) <= reg2slv(mod_out.Reg2(0));
        when REG2_ADDR(1)      => dout(Reg2_slv_t'range) <= reg2slv(mod_out.Reg2(1));
        when BBLK_REG3_ADDR(0) => dout(BBLK_Reg3_slv_t'range) <= reg2slv(mod_out.BBLK.Reg3(0));
        when BBLK_REG3_ADDR(1) => dout(BBLK_Reg3_slv_t'range) <= reg2slv(mod_out.BBLK.Reg3(1));
        when BBLK_REG4_ADDR(0) => dout(BBLK_Reg4_slv_t'range) <= reg2slv(mod_out.BBLK.Reg4(0));
        when BBLK_REG4_ADDR(1) => dout(BBLK_Reg4_slv_t'range) <= reg2slv(mod_out.BBLK.Reg4(1));
        when BBLK_REG4_ADDR(2) => dout(BBLK_Reg4_slv_t'range) <= reg2slv(mod_out.BBLK.Reg4(2));
        when BBLK_REG4_ADDR(3) => dout(BBLK_Reg4_slv_t'range) <= reg2slv(mod_out.BBLK.Reg4(3));
        when others            => null;
      end case?;
    end if;
  end process;

end architecture rtl;

