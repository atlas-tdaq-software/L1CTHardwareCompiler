-------------------------------------------------------------------------------
-- Title      : Test package
-- Project    : L1CT HardwareCompiler
-------------------------------------------------------------------------------
-- File       : test_pkg.vhd
-- Author     : Stefan Haas
-- Company    : CERN PH-ESE
-- Created    : 16-07-13
-- Last update: 19-07-13
-- Platform   : Windows 7
-- Standard   : VHDL-2008
-------------------------------------------------------------------------------
-- Description: Package with constant declarations from L1CT HardwareCompiler
-------------------------------------------------------------------------------
-- Copyright (c) 2013 CERN PH-ESE
-------------------------------------------------------------------------------
-- Revisions  :
-- Date      Version  Author  Description
-- 16-07-13  1.0      haass   Created
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

package test_pkg is

  -----------------------------------------------------------------------------
  -- Useful  type declarations, should be moved into a separate package
  -----------------------------------------------------------------------------
  subtype slv_32b is std_logic_vector(31 downto 0);
  type slv_32b_array is array (natural range <>) of slv_32b;

  type std_logic_vector_array_t is array (natural range <>) of std_logic_vector;

  -----------------------------------------------------------------------------
  -- Module TEST constants
  -----------------------------------------------------------------------------
  constant MODULE_BASE_ADDR  : slv_32b := x"00000000";
  constant MODULE_SIZE       : slv_32b := x"00100000";
  constant MODULE_MASK       : slv_32b := x"000fffff";
  constant MODULE_ADDR_WIDTH : integer := 24;

  -----------------------------------------------------------------------------
  -- Register Reg0
  -----------------------------------------------------------------------------
  constant REG0_ADDR  : slv_32b := x"0002c000";
  constant REG0_MASK  : slv_32b := x"ffffffff";
  constant REG0_WIDTH : integer := 32;
  subtype Reg0_reg_t is std_logic_vector(REG0_WIDTH-1 downto 0);

  -----------------------------------------------------------------------------
  -- Register Reg1
  -----------------------------------------------------------------------------
  constant REG1_ADDR  : slv_32b := x"0002c004";
  constant REG1_MASK  : slv_32b := x"00003fff";
  constant REG1_WIDTH : integer := 14;

  subtype reg1_slv_t is std_logic_vector(REG1_WIDTH-1 downto 0);

  subtype Reg1_Field1A_t is std_logic_vector(3 downto 0);

  -- Field1A value constants
  constant Reg1_Field1A_Value1A0 : Reg1_Field1A_t := X"0";
  constant Reg1_Field1A_Value1A1 : Reg1_Field1A_t := X"1";
  constant Reg1_Field1A_Value1A2 : Reg1_Field1A_t := X"2";
  constant Reg1_Field1A_Value1A3 : Reg1_Field1A_t := X"3";
  constant Reg1_Field1A_Value1A4 : Reg1_Field1A_t := X"4";
  -- etc.

  type Reg1_reg_t is record  -- struct describing the bit fields in register Reg1
    Field1A : Reg1_Field1A_t;
    Field1B : std_logic_vector(7 downto 4);
    Field1C : std_logic_vector(11 downto 8);
    Field1D : std_logic_vector(12 downto 12);
    Field1E : std_logic_vector(13 downto 13);
  end record Reg1_reg_t;

  function reg2slv (reg : Reg1_reg_t) return Reg1_slv_t;
  function slv2reg (slv : Reg1_slv_t) return Reg1_reg_t;

  -----------------------------------------------------------------------------
  -- Register Reg2 array
  -----------------------------------------------------------------------------
--  constant REG2_ADDR       : slv_32b := x"0002c1" & "00------";  --x"0002c100";
  constant REG2_NUMBER     : integer := 2;
  constant REG2_OFFS       : slv_32b := x"00000010";
  constant REG2_ADDR       : slv_32b_array(0 to REG2_NUMBER-1) := (x"0002c100", x"0002c110");
  constant REG2_WIDTH      : integer := 28;
  constant REG2_MASK       : slv_32b := x"0fffffff";
  constant REG2_OFFS_MASK  : slv_32b := x"00000010";
  constant REG2_OFFS_SHIFT : integer := 4;

  -- Register REG2 bitfield Field2A
  constant REG2_FIELD2A_NUMBER : integer := 2;
  constant REG2_FIELD2A_OFFSET : integer := 4;
  constant REG2_FIELD2A_WIDTH  : integer := 4;
  constant REG2_FIELD2A_SHIFT  : integer := 0;

  -- Register REG2 bitfield Field2B
  constant REG2_FIELD2B_NUMBER : integer := 4;
  constant REG2_FIELD2B_OFFSET : integer := 2;
  constant REG2_FIELD2B_WIDTH  : integer := 2;
  constant REG2_FIELD2B_SHIFT  : integer := 8;

  -- Register REG2 bitfield Field2C
  constant REG2_FIELD2C_NUMBER : integer := 2;
  constant REG2_FIELD2C_OFFSET : integer := 4;
  constant REG2_FIELD2C_WIDTH  : integer := 4;
  constant REG2_FIELD2C_SHIFT  : integer := 16;

  -- Register REG2 bitfield Field2D
  constant REG2_FIELD2D_NUMBER : integer := 4;
  constant REG2_FIELD2D_OFFSET : integer := 1;
  constant REG2_FIELD2D_WIDTH  : integer := 1;
  constant REG2_FIELD2D_SHIFT  : integer := 24;

  subtype Reg2_slv_t is std_logic_vector(REG2_WIDTH-1 downto 0);

  type Reg2_reg_t is record
    Field2A : std_logic_vector_array_t(0 to REG2_FIELD2A_NUMBER-1)(REG2_FIELD2A_WIDTH-1 downto 0);
    Field2B : std_logic_vector_array_t(0 to REG2_FIELD2B_NUMBER-1)(REG2_FIELD2B_WIDTH-1 downto 0);
    Field2C : std_logic_vector_array_t(0 to REG2_FIELD2C_NUMBER-1)(REG2_FIELD2C_WIDTH-1 downto 0);
    Field2D : std_logic_vector(3 downto 0);
  end record Reg2_reg_t;

  function reg2slv (reg : Reg2_reg_t) return Reg2_slv_t;
  function slv2reg (slv : Reg2_slv_t) return Reg2_reg_t;

  type Reg2_reg_array_t is array (0 to REG2_NUMBER-1) of Reg2_reg_t;

  -----------------------------------------------------------------------------
  -- Block BBLK register array Reg3
  -----------------------------------------------------------------------------
  constant BBLK_REG3_MASK   : std_logic_vector(15 downto 0) := x"ffff";
  constant BBLK_REG3_WIDTH  : integer                       := 16;
  constant BBLK_REG3_NUMBER : integer                       := 2;
  constant BBLK_REG3_OFFS   : slv_32b                       := x"00000004";
  constant BBLK_REG3_ADDR   : slv_32b_array(0 to BBLK_REG3_NUMBER-1) := (x"0002c200", x"0002c204");

  -- Block BBLK3 register Reg3 bitfield Field3A
  constant BBLK_REG3_FIELD3A_NUMBER : integer := 4;
  constant BBLK_REG3_FIELD3A_OFFSET : integer := 4;
  constant BBLK_REG3_FIELD3A_WIDTH  : integer := 4;
  constant BBLK_REG3_FIELD3A_SHIFT  : integer := 0;

  subtype BBLK_Reg3_slv_t is std_logic_vector(BBLK_REG3_WIDTH-1 downto 0);

  type BBLK_Reg3_reg_t is record
    Field3A : std_logic_vector_array_t(0 to BBLK_REG3_FIELD3A_NUMBER)(BBLK_REG3_FIELD3A_WIDTH-1 downto 0);
  end record BBLK_Reg3_reg_t;

  function reg2slv (reg : BBLK_Reg3_reg_t) return BBLK_Reg3_slv_t;
  function slv2reg (slv : BBLK_Reg3_slv_t) return BBLK_Reg3_reg_t;

  type BBLK_Reg3_reg_array_t is array (0 to BBLK_REG3_NUMBER-1) of BBLK_Reg3_reg_t;

  -----------------------------------------------------------------------------
  -- Block BBLK register array Reg4
  -----------------------------------------------------------------------------
  constant BBLK_REG4_WIDTH  : integer := 4;
  constant BBLK_REG4_MASK   : slv_32b := x"0000ffff";
  constant BBLK_REG4_NUMBER : integer := 4;
  constant BBLK_REG4_OFFS   : slv_32b := x"00000004";
  constant BBLK_REG4_ADDR   : slv_32b_array := (x"0002c300", x"0002c304", x"0002c308", x"0002c30c");

  subtype BBLK_Reg4_reg_t is BBLK_Reg3_reg_t;
  subtype BBLK_Reg4_slv_t is BBLK_Reg3_slv_t;

  type BBLK_Reg4_reg_array_t is array (0 to BBLK_REG4_NUMBER-1) of BBLK_Reg4_reg_t;

  -----------------------------------------------------------------------------
  -- Block BBLK
  -----------------------------------------------------------------------------

  type BBLK_blk_t is record
    Reg3 : BBLK_Reg3_reg_array_t;
    Reg4 : BBLK_Reg4_reg_array_t;
  end record BBLK_blk_t;

  -----------------------------------------------------------------------------
  -- Module TEST
  -----------------------------------------------------------------------------
  type TEST_mod_t is record
    Reg0 : Reg0_reg_t;
    Reg1 : Reg1_reg_t;
    Reg2 : Reg2_reg_array_t;
    BBLK : BBLK_blk_t;
  end record TEST_mod_t;

end package test_pkg;

-------------------------------------------------------------------------------

package body test_pkg is

  -----------------------------------------------------------------------------
  -- Conversion functions for Reg1
  -----------------------------------------------------------------------------
  function reg2slv (reg : Reg1_reg_t) return Reg1_slv_t is
    variable slv : Reg1_slv_t;
  begin
    slv                    := (others => '0');
    slv(reg.Field1A'range) := reg.Field1A;
    slv(reg.Field1B'range) := reg.Field1B;
    slv(reg.Field1C'range) := reg.Field1C;
    slv(reg.Field1D'range) := reg.Field1D;
    slv(reg.Field1E'range) := reg.Field1D;
    return slv;
  end reg2slv;

  function slv2reg (slv : Reg1_slv_t) return Reg1_reg_t is
    variable reg : Reg1_reg_t;
  begin
    reg.Field1A := slv(reg.Field1A'range);
    reg.Field1B := slv(reg.Field1B'range);
    reg.Field1C := slv(reg.Field1C'range);
    reg.Field1D := slv(reg.Field1D'range);
    reg.Field1E := slv(reg.Field1E'range);
    return reg;
  end slv2reg;

  -----------------------------------------------------------------------------
  -- Conversion functions for Reg2
  -----------------------------------------------------------------------------
  function reg2slv (reg : Reg2_reg_t) return Reg2_slv_t is
    variable slv : Reg2_slv_t;
    variable idx : integer;
  begin
    slv := (others => '0');
    idx := REG2_FIELD2A_SHIFT;
    for i in 0 to REG2_FIELD2A_NUMBER-1 loop
      slv(idx+REG2_FIELD2A_WIDTH-1 downto idx) := reg.Field2A(i);
      idx                                      := idx + REG2_FIELD2A_OFFSET;
    end loop;  -- i
    idx := REG2_FIELD2B_SHIFT;
    for i in 0 to REG2_FIELD2B_NUMBER-1 loop
      slv(idx+REG2_FIELD2B_WIDTH-1 downto idx) := reg.Field2B(i);
      idx                                      := idx + REG2_FIELD2B_OFFSET;
    end loop;  -- i
    idx := REG2_FIELD2C_SHIFT;
    for i in 0 to REG2_FIELD2C_NUMBER-1 loop
      slv(idx+REG2_FIELD2C_WIDTH-1 downto idx) := reg.Field2C(i);
      idx                                      := idx + REG2_FIELD2C_OFFSET;
    end loop;  -- i
    idx := REG2_FIELD2D_SHIFT;
    for i in 0 to REG2_FIELD2D_NUMBER-1 loop
      slv(idx) := reg.Field2D(i);
      idx      := idx + REG2_FIELD2D_OFFSET;
    end loop;  -- i
    return slv;
  end reg2slv;

  function slv2reg (slv : Reg2_slv_t) return Reg2_reg_t is
    variable reg : Reg2_reg_t;
    variable idx : integer;
  begin
    idx := REG2_FIELD2A_SHIFT;
    for i in 0 to REG2_FIELD2A_NUMBER-1 loop
      reg.Field2A(i) := slv(idx+REG2_FIELD2A_WIDTH-1 downto idx);
      idx            := idx + REG2_FIELD2A_OFFSET;
    end loop;  -- i
    idx := REG2_FIELD2B_SHIFT;
    for i in 0 to REG2_FIELD2B_NUMBER-1 loop
      reg.Field2B(i) := slv(idx+REG2_FIELD2B_WIDTH-1 downto idx);
      idx            := idx + REG2_FIELD2B_OFFSET;
    end loop;  -- i
    idx := REG2_FIELD2C_SHIFT;
    for i in 0 to REG2_FIELD2C_NUMBER-1 loop
      reg.Field2C(i) := slv(idx+REG2_FIELD2C_WIDTH-1 downto idx);
      idx            := idx + REG2_FIELD2C_OFFSET;
    end loop;  -- i
    idx := REG2_FIELD2D_SHIFT;
    for i in 0 to REG2_FIELD2D_NUMBER-1 loop
      reg.Field2D(i) := slv(idx+REG2_FIELD2D_WIDTH-1);
      idx            := idx + REG2_FIELD2D_OFFSET;
    end loop;  -- i
    return reg;
  end slv2reg;

  -----------------------------------------------------------------------------
  -- Conversion functions for block BBLK register Reg3
  -----------------------------------------------------------------------------
  function reg2slv (reg : BBLK_Reg3_reg_t) return BBLK_Reg3_slv_t is
    variable slv : BBLK_Reg3_slv_t;
    variable idx : integer;
  begin
    slv := (others => '0');
    idx := BBLK_REG3_FIELD3A_SHIFT;
    for i in 0 to BBLK_REG3_FIELD3A_NUMBER-1 loop
      slv(idx+BBLK_REG3_FIELD3A_WIDTH-1 downto idx) := reg.Field3A(i);
      idx                                           := idx + BBLK_REG3_FIELD3A_OFFSET;
    end loop;  -- i
    return slv;
  end reg2slv;

  function slv2reg (slv : BBLK_Reg3_slv_t) return BBLK_Reg3_reg_t is
    variable reg : BBLK_Reg3_reg_t;
    variable idx : integer;
  begin
    idx := BBLK_REG3_FIELD3A_SHIFT;
    for i in 0 to BBLK_REG3_FIELD3A_NUMBER-1 loop
      reg.Field3A(i) := slv(idx+BBLK_REG3_FIELD3A_WIDTH-1 downto idx);
      idx            := idx + BBLK_REG3_FIELD3A_OFFSET;
    end loop;  -- i
    return reg;
  end slv2reg;

end package body test_pkg;
