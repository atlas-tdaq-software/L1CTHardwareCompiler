#ifndef _L1CTHC_COMPILERSTRINGLIST_H_
#define _L1CTHC_COMPILERSTRINGLIST_H_

//******************************************************************************
// file: CompilerStringList.h
// desc: library for L1CT HardwareCompiler
// auth: 19-JUN-2013 R. Spiwoks
//******************************************************************************

#include <string>
#include <sstream>

namespace L1CTHC {

// CompilerStringList class
class CompilerStringList {

  public:

    // public constructor/destructor
    CompilerStringList() : m_del(',')           {};
   ~CompilerStringList()                        {};

    //public methods
    void delimiter(const char c)                { m_del = c; }
    void space(const unsigned int s)            { m_spc = ""; for(unsigned int i=0; i<s; i++) m_spc += " "; }
    void add(const std::string& s)              { if(!(s.empty())) { delim(); m_buf << s; } }
    void clear()                                { m_buf.str(""); }

    std::string str()                           { return(m_buf.str()); }
    std::string lstr()                          { return(edge() + m_buf.str()); }
    std::string rstr()                          { return(m_buf.str() + edge()); }
    std::string bstr()                          { return(edge() + m_buf.str() + edge()); }

  private:

    // private members
    std::ostringstream                          m_buf;
    char                                        m_del;
    std::string                                 m_spc;

    // private methods
    void delim()                                { if(!(m_buf.str().empty())) { m_buf << m_del << m_spc; } }
    std::string edge()                          { return((m_buf.str().empty()) ? "" : (m_del + m_spc)); }

};      // class CompilerStringList

}       // namespace L1CTHC

#endif  // _L1CTHC_COMPILERSTRINGLIST_H_
