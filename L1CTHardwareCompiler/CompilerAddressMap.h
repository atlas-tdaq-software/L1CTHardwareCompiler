#ifndef _L1CTHC_COMPILERADDRESSMAP_H_
#define _L1CTHC_COMPILERADDRESSMAP_H_

//******************************************************************************
// file: CompilerAddressMap.h
// desc: Address Map for L1CT HardwareCompiler
// auth: 27-MAY-2013 R. Spiwoks
//******************************************************************************

#include <string>
#include <vector>
#include <map>

namespace L1CTHC {

// forward declarations
class HardwareCompiler;
class HardwareElement;
class HardwareBlock;

// CompilerAddressMap class
class CompilerAddressMap {

  public:

    // public constructor/destructor
    CompilerAddressMap();
   ~CompilerAddressMap();

    // public methods
    void addElement(const HardwareElement*);
    void addBlocksAndElement(std::vector<unsigned int>&, const unsigned int);
    void dump() const;
    HardwareCompiler* getCompiler() const               { return(m_compiler); }
    void setCompiler(HardwareCompiler*);

 private:

    // private nested class CompilerAddressItem
    class CompilerAddressItem {

      public:

        // public constructor/destructor
        CompilerAddressItem(std::vector<const HardwareBlock*>&, std::vector<unsigned int>&, const HardwareElement*, const unsigned int);
       ~CompilerAddressItem();

        // public methods
        unsigned int getLowerAddress() const            { return(m_lower_address); }
        unsigned int getUpperAddress() const            { return(m_upper_address); }
        std::string name() const                        { return(m_name); }

      private:

        // private members
        std::string                                     m_name;
        unsigned int                                    m_lower_address;
        unsigned int                                    m_upper_address;

    };      // class CompilerAddressItem

    // private members
    std::multimap<unsigned int, const CompilerAddressItem*>     m_map;
    HardwareCompiler*                                           m_compiler;
    std::vector<const HardwareBlock*>                           m_current_blocks;
    const HardwareElement*                                      m_current_element;

    // private method
    bool findOverlap(const CompilerAddressItem*, const CompilerAddressItem**) const;
    bool findOverlap(const CompilerAddressItem*, const CompilerAddressItem*, const bool = false) const;

};      // class CompilerAddressMap

}       // namespace L1CTHC

#endif  // _L1CTHC_COMPILERADDRESSMAP_H_
