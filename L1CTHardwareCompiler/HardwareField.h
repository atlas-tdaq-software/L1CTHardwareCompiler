#ifndef _L1CTHC_HARDWAREFIELD_H_
#define _L1CTHC_HARDWAREFIELD_H_

//******************************************************************************
// file: HardwareField.h
// desc: library for L1CT HardwareCompiler
// auth: 19-MAR-2013 R. Spiwoks, re-design from RCDModuleDesign
//******************************************************************************

#include "L1CTHardwareCompiler/HardwareList.h"
#include "L1CTHardwareCompiler/HardwareObject.h"
#include "RCDBitString/BitSet.h"

namespace L1CTHC {

// forward declaration
class HardwareBitString;
class HardwareValue;
class CompilerOutputFile;

// HardwareField class
class HardwareField : public HardwareObject {

  public:

    // public constructor/destructor
    HardwareField(const std::string& = "");
    virtual ~HardwareField();

    // public methods
    virtual std::string hardwareType() const                            { return("field"); }
    virtual std::string nameName() const                                { std::string n(m_name); transform(n.begin(),n.end(),n.begin(),toupper); return(n); }
    virtual void dump(std::ostream& = std::cout, const unsigned int = 0) const;
    virtual void read(const XERCES_CPP_NAMESPACE::DOMNode*);
    virtual void write(std::ostream& = std::cout) const;

    RCD::BitSet mask() const                                            { return(m_mask); }
    std::string maskName() const;
    DATA_FORM form() const                                              { return(m_form); }

    void setBitString(const HardwareBitString* bit)                     { m_bitstring = bit; }
    const HardwareBitString* getBitString() const                       { return(m_bitstring); }

    void addValue(HardwareValue*);
    bool findValue(const std::string&, HardwareValue**) const;
    bool firstValue(const HardwareValue* val) const                     { return(val == (*m_values.begin())); }
    bool isEmpty() const;
    static bool isField(const XERCES_CPP_NAMESPACE::DOMNode*);
    HardwareList<HardwareValue>::const_iterator beginValue() const      { return(m_values.begin()); }
    HardwareList<HardwareValue>::const_iterator endValue() const        { return(m_values.end()); }

  private:

    // private constants
    static const std::string            XML_FIELD;
    static const std::string            XML_NO;
    static const std::string            XML_YES;
    static const std::string            CPP_MASK_PREFIX;

    // private member
    const HardwareBitString*            m_bitstring;
    RCD::BitSet                         m_mask;
    DATA_FORM                           m_form;
    HardwareList<HardwareValue>         m_values;

    // private method
    void readValues(const XERCES_CPP_NAMESPACE::DOMNode*);

};      // class HardwareField

// global functions for sorting
bool compareHardwareFieldByName(HardwareField*, HardwareField*);
bool compareHardwareFieldByAddr(HardwareField*, HardwareField*);

}       // namespace L1CTHC

#endif  // _L1CTHC_HARDWAREFIELD_H_
