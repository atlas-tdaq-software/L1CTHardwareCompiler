#ifndef _L1CTHC_HARDWARECOMMON_H_
#define _L1CTHC_HARDWARECOMMON_H_

//******************************************************************************
// file: HardwareCommon.h
// desc: pure static class for L1CT HardwareCompiler common types, methods, and constants
// auth: 27-FEB-2013 R. Spiwoks, re-design from RCDModuleDesign
//******************************************************************************

#include <iostream>
#include <iomanip>
#include <vector>
#include "RCDBitString/BitSet.h"

namespace L1CTHC {

// HardwareCommon class
class HardwareCommon {

  public:

    // public constant
    static const unsigned int           BIT_SIZE        = 8;
    static const unsigned int           HEX_SIZE        = 4;

    // public method
    static double getTime();
    static std::string rightFillString(const std::string&, const unsigned int, const char = ' ');
    static std::string spaceString(const unsigned int n)        { return(rightFillString("",n)); }
    static unsigned int readNumber(const std::string&);

    static unsigned int getMaskWidth(const unsigned int);
    static unsigned int getMaskIndexLO(const unsigned int);
    static unsigned int getMaskIndexHI(const unsigned int);

    static unsigned int getMaskValue(const RCD::BitSet&);
    static unsigned int getMaskWidth(const RCD::BitSet&);
    static unsigned int getMaskIndexLO(const RCD::BitSet&);
    static unsigned int getMaskIndexHI(const RCD::BitSet&);
    static RCD::BitSet getMaskMultiple(const RCD::BitSet&, const unsigned int, const unsigned int);

    static unsigned int getMaskFromIndices(const unsigned int, const unsigned int);
    static unsigned int getDecNumber(const unsigned int);
    static unsigned int getHexNumber(const unsigned int);
    static std::string getMaskFormat(const unsigned int);
    static std::string getNumberFormat(const unsigned int);
    static int splitString(const std::string&, const std::string&, std::vector<std::string>&);

  private:

    // private constructor and destructor
    HardwareCommon()                    {}
   ~HardwareCommon()                    {}

};      // class HardwareCommon

// global static constants
static const int                        SUCCESS         =    0;
static const int                        FAILURE         =   -1;
static const int                        STRING_LENGTH   = 1024;
static const unsigned int               INDENT          =    2;

// global output stream manipulators (for convenience)
class hex {
  public:
    explicit hex(unsigned int w) : m_w(w) {}
  private:
    unsigned int m_w;
    template <class charT, class traits>
        friend std::basic_ostream<charT,traits>& operator<< (std::basic_ostream<charT,traits>& s, const hex& w) {
            s << "0x" << std::hex << std::setfill('0') << std::setw(w.m_w);
            return s;
        }
};

// -----------------------------------------------------------------------------

class nhex {
  public:
    explicit nhex(unsigned int w) : m_w(w) {}
  private:
    unsigned int m_w;
    template <class charT, class traits>
        friend std::basic_ostream<charT,traits>& operator<< (std::basic_ostream<charT,traits>& s, const nhex& w) {
            s << std::hex << std::setfill('0') << std::setw(w.m_w);
            return s;
        }
};

// -----------------------------------------------------------------------------

template <class charT, class traits>
    inline std::basic_ostream<charT,traits>& dec(std::basic_ostream<charT,traits>& s) {
        s << std::dec << std::setfill(' ');
        return(s);
    }

// -----------------------------------------------------------------------------

class left {
  public:
    explicit left(unsigned int w) : m_w(w) {}
  private:
    unsigned int m_w;
    template <class charT, class traits>
        friend std::basic_ostream<charT,traits>& operator<< (std::basic_ostream<charT,traits>& s, const left& w) {
            s << std::setw(w.m_w) << std::left;
            return s;
        }
};

// -----------------------------------------------------------------------------

class right {
  public:
    explicit right(unsigned int w) : m_w(w) {}
  private:
    unsigned int m_w;
    template <class charT, class traits>
        friend std::basic_ostream<charT,traits>& operator<< (std::basic_ostream<charT,traits>& s, const right& w) {
            s << std::setw(w.m_w) << std::right;
            return s;
        }
};

}       // namespace L1CTHC

#endif  // _L1CTHC_HARDWARECOMMON_H_
