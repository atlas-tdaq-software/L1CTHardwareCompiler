#ifndef _L1CTHC_COMPILERERRORHANDLER_H_
#define _L1CTHC_COMPILERERRORHANDLER_H_

//******************************************************************************
// file: CompilerErrorHandler.h
// desc: DOM parse error handler: print-out of errors
// auth: 10-MAY-2013 R. Spiwoks
//******************************************************************************

#include <xercesc/sax/HandlerBase.hpp>

namespace L1CTHC {

// CompilerErrorHandler class
class CompilerErrorHandler : public XERCES_CPP_NAMESPACE::HandlerBase {

  public:
    void warning(const XERCES_CPP_NAMESPACE::SAXParseException&);
    void error(const XERCES_CPP_NAMESPACE::SAXParseException&);
    void fatalError(const XERCES_CPP_NAMESPACE::SAXParseException&);

};      // class CompilerErrorHandler

}       // namespace L1CTHC

#endif  // _L1CTHC_COMPILERERRORHANDLER_H_
