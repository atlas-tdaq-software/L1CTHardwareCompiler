#ifndef _L1CTHC_HARDWAREMODULE_H_
#define _L1CTHC_HARDWAREMODULE_H_

//******************************************************************************
// file: HardwareModule.h
// desc: library for L1CT HardwareCompiler
// auth: 01-MAR-2013 R. Spiwoks, re-design from RCDModuleDesign
//******************************************************************************

#include <list>
#include <xercesc/dom/DOM.hpp>

#include "L1CTHardwareCompiler/HardwareObject.h"
#include "L1CTHardwareCompiler/HardwareList.h"

namespace L1CTHC {

// forward declarations
class HardwareBlock;
class HardwareMemory;
class CompilerOutputFile;

// HardwareModule class
class HardwareModule : public HardwareObject {

  public:

    // public constructor/destructor
    HardwareModule(const std::string& = "");
    virtual ~HardwareModule();

    // public methods
    virtual std::string hardwareType() const    { return("module"); }
    virtual std::string nameName() const        { return(m_name); }
    virtual void dump(std::ostream& = std::cout, const unsigned int = 0) const;
    virtual void read(const XERCES_CPP_NAMESPACE::DOMNode*);
    virtual void write(std::ostream& = std::cout) const;

    unsigned int size() const                   { return(m_size); };
    unsigned int form() const                   { return(m_form); };
    ADDR_TYPE    amod() const                   { return(m_amod); };
    std::string  desc() const                   { return(m_desc); };
    std::string addrName() const                { return(CPP_ADDR_NAME); }
    std::string sizeName() const                { return(CPP_SIZE_NAME); }
    std::string amodName() const                { return(CPP_AMOD_NAME); }
    std::string codeName() const                { return(CPP_CODE_NAME); }

    void read(const std::string&);
    void addBlock(HardwareBlock*);
    bool findBlock(const std::string&, HardwareBlock**) const;
    static bool isModule(const XERCES_CPP_NAMESPACE::DOMNode*);
    HardwareList<HardwareBlock>::const_iterator beginBlock() const      { return(m_blocks.begin()); }
    HardwareList<HardwareBlock>::const_iterator endBlock() const        { return(m_blocks.end()); }
    const HardwareMemory* getMaximumSizeMemory() const;

  private:

    // private constant
    static const std::string            XML_MODULE;
    static const std::string            CPP_ADDR_NAME;
    static const std::string            CPP_SIZE_NAME;
    static const std::string            CPP_AMOD_NAME;
    static const std::string            CPP_CODE_NAME;

    // private members
    unsigned int                        m_size;
    unsigned int                        m_form;
    ADDR_TYPE                           m_amod;
    std::string                         m_desc;
    HardwareList<HardwareBlock>         m_blocks;

    // private method
    XERCES_CPP_NAMESPACE::DOMNode* findModule(const XERCES_CPP_NAMESPACE::DOMDocument*) const;
    void readBlocks(const XERCES_CPP_NAMESPACE::DOMNode*);
    void readDataTypes(const XERCES_CPP_NAMESPACE::DOMNode*);

};      // class HardwareModule

}       // namespace L1CTHC

#endif  // _L1CTHC_HARDWAREMODULE_H_
