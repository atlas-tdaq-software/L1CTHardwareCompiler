#ifndef _L1CTHC_VHDLEMITTER_H_
#define _L1CTHC_VHDLEMITTER_H_

//******************************************************************************
// file: VHDLEmitter.h
// desc: library for L1CT HardwareCompiler
// auth: 23-JUL-2013 R. Spiwoks
//******************************************************************************

#include "L1CTHardwareCompiler/CompilerEmitter.h"
#include "L1CTHardwareCompiler/HardwareBlock.h"
#include "RCDBitString/BitSet.h"

namespace L1CTHC {

// forward declaration
class CompilerStringList;
class HardwareObject;

// VHDLEmitter class
class VHDLEmitter : public CompilerEmitter {

  public:

    // public constructor/destructor
    VHDLEmitter();
    virtual ~VHDLEmitter();

    virtual void compile();

  private:

    // private type declaration
    struct RArg {
        unsigned int    index;
        unsigned int    addr;
        RArg()                  : index(0), addr(0) {}
        RArg(unsigned int a)    : index(0), addr(a) {}
    };

    // private methods
    void writePackagePreamble(const HardwareModule*);

    void compilePackageDeclaration(const HardwareModule*);
    void compileDataTypeDeclaration(CompilerOutputFile*);
    void compileDataTypeDeclaration(const HardwareDataType*, CompilerOutputFile*);
    void compileWordDeclaration(const HardwareWord*, CompilerOutputFile*);
    void compileBitStringDeclaration(const HardwareBitString*, CompilerOutputFile*);
    void compilePackageBlock(HardwareBlock*, std::ostringstream[HardwareBlock::VHDL_TYPE_NUMBER]);
    void compilePackageBlockAddress(const std::vector<const HardwareObject*>&, RArg&, std::ostringstream&);

    void compilePackageBody(const HardwareModule*);
    void compileDataTypeBody(CompilerOutputFile*);
    void compileDataTypeBody(const HardwareBlock*, CompilerOutputFile*);
    void compileDataTypeBody(const HardwareDataType*, CompilerOutputFile*);
    void compileBitStringBody(const HardwareBitString*, CompilerOutputFile*);

    void compileExample(const HardwareModule*);

    // private helper
    static std::string compileStdLogic(const RCD::BitSet&, const unsigned int);

    // private constants
    static const std::string    VHDL_COMPANY_NAME;
    static const std::string    VHDL_PLATFORM_NAME;
    static const std::string    VHDL_STANDARD_NAME;
    static const std::string    VHDL_COPYRIGHT_NAME;
    static const std::string    VHDL_VERSION_NAME;
    static const std::string    VHDL_PACKAGE_POSTFIX;

};      // class VHDLEmitter

}       // namespace L1CTHC

#endif  // _L1CTHC_VHDLEMITTER_H_
