#ifndef _L1CTHC_COMPILERCONFIGURATION_H_
#define _L1CTHC_COMPILERCONFIGURATION_H_

//******************************************************************************
// file: CompilerConfiguration.h
// desc: configuration for L1CT HardwareCompiler
// auth: 27-FEB-2013 R. Spiwoks, re-design from RCDModuleDesign
//******************************************************************************

#include <iostream>
#include <string>

namespace L1CTHC {

// CompilerConfiguration class
class CompilerConfiguration {

  public:

    // public type definition
    typedef enum               { NUL, MEM, VME, IPB, VHD, AXI } TYPE;
    static const unsigned int   TYPE_NUMBER = AXI + 1;
    static const std::string    TYPE_NAME[TYPE_NUMBER];

    typedef enum               { NONE, BYNAME, BYDATA, BYADDR } SORT;
    static const unsigned int   SORT_NUMBER = BYADDR + 1;
    static const std::string    SORT_NAME[SORT_NUMBER];

    typedef enum               { MBPA_NO, MBPA_YES, MBPA_OPT } MBPA;
    static const unsigned int   MBPA_NUMBER = MBPA_OPT + 1;
    static const std::string    MBPA_NAME[MBPA_NUMBER];

    // public constructor and destructor
    CompilerConfiguration();
   ~CompilerConfiguration();

    // public methods
    void config(int, char*[]);
    void usage(std::ostream& = std::cout) const;
    void print(std::ostream& = std::cout) const;
    int check() const;

  public:
    std::string         module_file;
    std::string         output_file;
    std::string         compile_file;
    std::string         namespace_name;
    std::string         package_name;
    TYPE                compile_type;
    bool                parse_validate;
    bool                strict_check;
    bool                address_public;
    bool                bitmask_public;
    bool                bitmask_statics;
    bool                alternative_methods;
    SORT                value_sort_type;
    SORT                field_sort_type;
    bool                promote_mask;
    bool                promote_modf;
    bool                compile_enum;
    MBPA                menu_cmem_bpa;
    unsigned int        debug_level;
    unsigned int        print_level;
    std::string         program_call;

  private:

    // private helper
    static const std::string programCall(int, char*[]);

};      // class CompilerConfiguration

}       // namespace L1CTHC

#endif  // _L1CTHC_COMPILERCONFIGURATION_H_
