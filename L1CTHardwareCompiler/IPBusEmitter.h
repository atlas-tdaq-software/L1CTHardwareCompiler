#ifndef _L1CTHC_IPBUSEMITTER_H_
#define _L1CTHC_IPBUSEMITTER_H_

//******************************************************************************
// file: IPBusEmitter.h
// desc: library for L1CT HardwareCompiler
// auth: 23-JUL-2013 R. Spiwoks
//******************************************************************************

#include "L1CTHardwareCompiler/CompilerCCEmitter.h"

namespace L1CTHC {

// IPBusEmitter class
class IPBusEmitter : public CompilerCCEmitter {

  public:

    // public constructor/destructor
    IPBusEmitter();
    virtual ~IPBusEmitter();

  private:

    // private methods
    virtual std::string compileModuleHardwareIncludesDeclaration(const HardwareModule*);
    virtual std::string compileModuleHardwareIncludesDefinition(const HardwareModule*);
    virtual std::string compileModuleHardwareCtorDtorDeclaration(const HardwareModule*);
    virtual std::string compileModuleHardwareCtorDtorDefinition(const HardwareModule*);
    virtual std::string compileModuleHardwareMembers(const HardwareModule*);
    virtual std::string compileModuleHardwareReadSingle(const HardwareElement*);
    virtual std::string compileModuleHardwareWriteSingle(const HardwareElement*);
    virtual std::string compileModuleHardwareReadVector(const HardwareElement*);
    virtual std::string compileModuleHardwareWriteVector(const HardwareElement*);
    virtual std::string compileModuleHardwareReadBlock(const HardwareElement*);
    virtual std::string compileModuleHardwareWriteBlock(const HardwareElement*);
    virtual std::string compileModuleHardwareReadFile(const HardwareElement*);
    virtual std::string compileModuleHardwareWriteFile(const HardwareElement*);

    virtual std::string compileMenuHardwareGlobals(const HardwareModule*);
    virtual std::string compileMenuHardwareOpen(const HardwareModule*);
    virtual std::string compileMenuHardwareClose(const HardwareModule*);
	
	void getNodeString(const HardwareElement*, std::ostringstream*, std::string*);


};      // class IPBusEmitter

}       // namespace L1CTHC

#endif  // _L1CTHC_IPBUSEMITTER_H_
