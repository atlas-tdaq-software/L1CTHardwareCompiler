#ifndef _L1CTHC_HARDWAREELEMENT_H_
#define _L1CTHC_HARDWAREELEMENT_H_

//******************************************************************************
// file: HardwareElement.h
// desc: library for L1CT HardwareCompiler
// auth: 01-MAR-2013 R. Spiwoks, re-design from RCDModuleDesign
//******************************************************************************

#include "L1CTHardwareCompiler/HardwareCommon.h"
#include "L1CTHardwareCompiler/HardwareObject.h"
#include "L1CTHardwareCompiler/HardwareDataType.h"

namespace L1CTHC {

// forwarde declarations
class HardwareBlock;

// HardwareElement class
class HardwareElement : public HardwareObject {

  public:

    // public constant
    static const unsigned int NUM_DATA_TYPES                    = 2;

    // public constructor/destructor
    HardwareElement(const std::string& = "");
    virtual ~HardwareElement();

    // public methods
    virtual std::string nameName() const = 0;
    virtual void dump(std::ostream& = std::cout, const unsigned int = 0) const = 0;
    virtual void read(const XERCES_CPP_NAMESPACE::DOMNode*) = 0;
    virtual void write(std::ostream& = std::cout) const = 0;
    virtual unsigned int addrSize() const = 0;

    std::string typeName() const                                { return(DATA_TYPE_NAME[m_type]); }
    unsigned int mask() const                                   { return(m_mask); }
    std::string maskName() const;
                                                                // resolve reference if necessary
    const HardwareDataType* getDataType() const                 { return((m_data)&&m_data->isReference()?m_data->getReference():m_data); }
    const HardwareDataType* getAlternativeDataType() const      { return((m_datb)&&m_datb->isReference()?m_datb->getReference():m_datb); }
    void setBlock(const HardwareBlock* blk)                     { m_block = blk; }
    const HardwareBlock* getBlock() const                       { return(m_block); }

  protected:

    // protected members
    const HardwareBlock*        m_block;
    unsigned int                m_mask;
    HardwareDataType*           m_data;
    HardwareDataType*           m_datb; // alternative data (if necessary)

  private:

    // private constant
    static const std::string    CPP_MASK_PREFIX;

};      // class HardwareElement

}       // namespace L1CTHC

#endif  // _L1CTHC_HARDWAREELEMENT_H_
