#ifndef _L1CTHC_HARDWAREDATATYPE_H_
#define _L1CTHC_HARDWAREDATATYPE_H_

//******************************************************************************
// file: HardwareDataType.h
// desc: library for L1CT HardwareCompiler
// auth: 01-MAR-2013 R. Spiwoks, re-design from RCDModuleDesign
//******************************************************************************

#include "L1CTHardwareCompiler/HardwareObject.h"

namespace L1CTHC {

// forward declaration
class HardwareElement;
class CompilerOutputFile;

// HardwareDataType class
class HardwareDataType : public HardwareObject {

  public:

    // public types and constants
    typedef enum                { INTERNAL, EXTERNAL } DECL_TYPE;
    static const unsigned int   DECL_TYPE_NUMBER = EXTERNAL + 1;
    static const std::string    DECL_TYPE_NAME[DECL_TYPE_NUMBER];

    // public constructor/destructor
    HardwareDataType(const std::string& = "", const DECL_TYPE = INTERNAL);
    HardwareDataType(const HardwareDataType*); // constructor for reference to HardwareDataType
    virtual ~HardwareDataType();

    // public methods
    virtual std::string nameName() const = 0;
    virtual void dump(std::ostream& = std::cout, const unsigned int = 0) const = 0;
    virtual void read(const XERCES_CPP_NAMESPACE::DOMNode*) = 0;
    virtual void write(std::ostream& = std::cout) const = 0;
    virtual std::string fileName() const = 0;
    virtual bool isEmpty() const = 0;
    virtual unsigned int getFullMask() const = 0;
    bool isReference() const                            { return((m_reference)); }
    const HardwareDataType* getReference() const        { return(m_reference); }

    DECL_TYPE decl() const                              { return(m_decl); }
    virtual void setElement(const HardwareElement* elm) { m_element = elm; }
    const HardwareElement* getElement() const           { return(m_element); }

  protected:

    // protected members
    DECL_TYPE                                   m_decl;
    const HardwareDataType*                     m_reference;
    const HardwareElement*                      m_element;

};      // class HardwareDataType

}       // namespace L1CTHC

#endif  // _L1CTHC_HARDWAREDATATYPE_H_
