#ifndef _L1CTHC_HARDWAREMEMORY_H_
#define _L1CTHC_HARDWAREMEMORY_H_

//******************************************************************************
// file: HardwareMemory.h
// desc: library for L1CT HardwareCompiler
// auth: 10-JUN-2013 R. Spiwoks
//******************************************************************************

#include "L1CTHardwareCompiler/HardwareElement.h"
#include "L1CTHardwareCompiler/HardwareBlock.h"

namespace L1CTHC {

// HardwareMemory class
class HardwareMemory : public HardwareElement {

  public:

    // public types and constants
    typedef enum                        { RAM, FIFO } ACCESS_TYPE;
    static const unsigned int           ACCESS_TYPE_NUMBER = FIFO + 1;
    static const std::string            ACCESS_TYPE_NAME[ACCESS_TYPE_NUMBER];

    typedef enum                        { VECTOR, BLOCK, BOTH } INTERFACE_TYPE;
    static const unsigned int           INTERFACE_TYPE_NUMBER = BOTH + 1;
    static const std::string            INTERFACE_TYPE_NAME[INTERFACE_TYPE_NUMBER];

    // public constructor/destructor
    HardwareMemory(const std::string& = "");
    virtual ~HardwareMemory();

    // public methods
    virtual std::string hardwareType() const    { return("memory"); }
    virtual std::string nameName() const        { return((m_block && (!m_block->nameName().empty())) ? (m_block->nameName()+"_"+m_name) : m_name); }
    virtual void dump(std::ostream& = std::cout, const unsigned int = 0) const;
    virtual void read(const XERCES_CPP_NAMESPACE::DOMNode*);
    virtual void write(std::ostream& = std::cout) const;
    virtual unsigned int addrSize() const       { return(m_size); }

    unsigned int size() const                   { return(m_size); }
    unsigned int indx() const                   { return(m_size / (HardwareObject::DATA_TYPE_SIZE[m_type]/HardwareCommon::BIT_SIZE)); }
    ACCESS_TYPE access() const                  { return(m_access); }
    std::string sizeName() const;
    std::string indxName() const;
    std::string accessType() const              { return(ACCESS_TYPE_NAME[m_access]); }
    std::string accessName() const              { return(XML_ACCESS_NAME[m_access]); }
    INTERFACE_TYPE interface() const            { return(m_interface); }
    static bool isMemory(const XERCES_CPP_NAMESPACE::DOMNode*);

  private:

    // private constants
    static const std::string            XML_ACCESS_NAME[ACCESS_TYPE_NUMBER];
    static const std::string            CPP_SIZE_PREFIX;
    static const std::string            CPP_INDX_POSTFIX;

    // private member
    unsigned int                        m_size;
    ACCESS_TYPE                         m_access;
    INTERFACE_TYPE                      m_interface;

    // private method
    void readDataType(const XERCES_CPP_NAMESPACE::DOMNode*);
    void readDataTypeLoop(const XERCES_CPP_NAMESPACE::DOMNode*);

};      // class HardwareMemory

}       // namespace L1CTHC

#endif  // _L1CTHC_HARDWAREMEMORY_H_
