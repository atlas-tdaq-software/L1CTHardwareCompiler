#ifndef _L1CTHC_HARDWAREVALUE_H_
#define _L1CTHC_HARDWAREVALUE_H_

//******************************************************************************
// file: HardwareValue.h
// desc: library for L1CT HardwareCompiler
// auth: 19-MAR-2013 R. Spiwoks, re-design from RCDModuleDesign
//******************************************************************************

#include "L1CTHardwareCompiler/HardwareList.h"
#include "L1CTHardwareCompiler/HardwareObject.h"
#include "RCDBitString/BitSet.h"

namespace L1CTHC {

// forwarddeclaration
class HardwareField;

// HardwareValue class
class HardwareValue : public HardwareObject {

  public:

    // public constructor/destructor
    HardwareValue(const std::string& = "");
    virtual ~HardwareValue();

    // public methods
    virtual std::string hardwareType() const    { return("value"); }
    virtual std::string nameName() const        { std::string n(m_name); transform(n.begin(),n.end(),n.begin(),toupper); return(n); }
    virtual void dump(std::ostream& = std::cout, const unsigned int = 0) const;
    virtual void read(const XERCES_CPP_NAMESPACE::DOMNode*);
    virtual void write(std::ostream& = std::cout) const;

    void setField(const HardwareField* fld)     { m_field = fld; }
    const HardwareField* getField() const       { return(m_field); }
    RCD::BitSet data() const                    { return(m_data); };
    std::string dataName() const;
    static bool isValue(const XERCES_CPP_NAMESPACE::DOMNode*);

  private:

    // private constant
    static const std::string    XML_VALUE;
    static const std::string    CPP_DATA_PREFIX;

    // private member
    const HardwareField*        m_field;
    RCD::BitSet                 m_data;

};      // class HardwareValue

// global functions for sorting
bool compareHardwareValueByName(HardwareValue*, HardwareValue*);
bool compareHardwareValueByData(HardwareValue*, HardwareValue*);

}       // namespace L1CTHC

#endif  // _L1CTHC_HARDWAREVALUE_H_
