#ifndef _L1CTHC_HARDWARECOMPILER_H_
#define _L1CTHC_HARDWARECOMPILER_H_

//******************************************************************************
// file: HardwareCompiler.h
// desc: library for L1CT HardwareCompiler
// auth: 27-FEB-2013 R. Spiwoks, re-design from RCDModuleDesign
//******************************************************************************

#include <vector>
#include <string>
#include <iostream>

#include "L1CTHardwareCompiler/HardwareModule.h"
#include "L1CTHardwareCompiler/CompilerConfiguration.h"
#include "L1CTHardwareCompiler/CompilerAddressMap.h"
#include "L1CTHardwareCompiler/CompilerEmitter.h"
#include "L1CTHardwareCompiler/HardwareModule.h"

namespace L1CTHC {

// forward declaration
class HardwareDataType;
class CompilerOutputFile;

// HardwareCompiler class
class HardwareCompiler {

  public:

    // public type definition
    typedef std::map<const std::string, HardwareList<const HardwareDataType>* >::const_iterator HardwareDataTypeMapConstIterator;

    // public constructor/destructor
    HardwareCompiler();
   ~HardwareCompiler();
    static const std::string banner();

    // public methods
    int compile(const CompilerConfiguration&);
    void write() const;
    void getModuleFiles(CompilerOutputFile**, CompilerOutputFile**);
    void getModuleHeaderFileName(std::string&) const;
    void addToDataTypeMap(const HardwareDataType*);
    bool getFromDataTypeMap(const std::string&, const std::string&, const HardwareDataType**);
    void getDataTypeFiles(const HardwareDataType*, CompilerOutputFile**, CompilerOutputFile**);
    void getDataTypeHeaderFileNames(std::vector<std::string>&) const;
    void getMenuFile(CompilerOutputFile**);
    void getPackageVHDLFile(CompilerOutputFile**);
    void getExampleVHDLFile(CompilerOutputFile**);
    void addToAddressMap(const HardwareElement*);

    const CompilerConfiguration* getConfig() const              { return(&m_cfg); }
    const CompilerAddressMap* getAddressMap()                   { return(m_map); }
    const CompilerEmitter* getEmitter() const                   { return(m_emt); }
    const HardwareModule* getModule() const                     { return(m_mod); }
    std::string name() const                                    { return(m_name); }
    HardwareDataTypeMapConstIterator beginDataTypeMap() const   { return(m_dat.begin()); }
    HardwareDataTypeMapConstIterator endDataTypeMap() const     { return(m_dat.end()); }

  private:

    // private constants
    static const int                                    MAJOR_REVISION_NUMBER;
    static const int                                    MINOR_REVISION_NUMBER;
    static const std::string                            AUTHOR;
    static const std::string                            DATE;
    static const std::string                            CPP_HEAD_EXTENSION;
    static const std::string                            CPP_CODE_EXTENSION;
    static const std::string                            CPP_INCLUDE_POSTFIX;
    static const std::string                            VHDL_PKG_EXTENSION;
    static const std::string                            VHDL_XMP_EXTENSION;

    // private type definition
    typedef std::map<const std::string, HardwareList<const HardwareDataType>* > HardwareDataTypeMap;

    // private members
    CompilerConfiguration                               m_cfg;
    CompilerAddressMap*                                 m_map;
    CompilerEmitter*                                    m_emt;
    HardwareModule*                                     m_mod;
    HardwareDataTypeMap                                 m_dat;
    CompilerOutputFile*                                 m_modHH;
    CompilerOutputFile*                                 m_modCC;
    HardwareList<CompilerOutputFile>                    m_datHH;
    HardwareList<CompilerOutputFile>                    m_datCC;
    CompilerOutputFile*                                 m_menCC;
    CompilerOutputFile*                                 m_pkgVHDL;
    CompilerOutputFile*                                 m_xmpVHDL;
    double                                              m_time_start;
    std::string                                         m_name;

    // private method
    static const std::string getVersion();
    static const std::string getAuthor()                { return(AUTHOR); }
    static const std::string getBuildTime();
    void writeDocType(std::ostream& = std::cout) const;
    void openFiles();
    void closeFiles();
    void dumpDataTypeMap();

};      // class HardwareCompiler

}       // namespace L1CTHC

#endif  // _L1CTHC_HARDWARECOMPILER_H_
