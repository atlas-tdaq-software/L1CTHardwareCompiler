#ifndef _L1CTHC_HARDWAREBLOCK_H_
#define _L1CTHC_HARDWAREBLOCK_H_

//******************************************************************************
// file: HardwareBlock.h
// desc: library for L1CT HardwareCompiler
// auth: 01-MAR-2013 R. Spiwoks, re-design from RCDModuleDesign
//******************************************************************************

#include <list>
#include <algorithm>

#include "L1CTHardwareCompiler/HardwareObject.h"
#include "L1CTHardwareCompiler/HardwareList.h"

namespace L1CTHC {

// forward declaration
class HardwareElement;

// HardwareBlock class
class HardwareBlock : public HardwareObject {

  public:

    // public types and constants
    typedef enum                                        { VHDL_NONE, VHDL_MISO, VHDL_MOSI } VHDL_TYPE;
    static const unsigned int                           VHDL_TYPE_NUMBER = VHDL_MOSI + 1;
    static const std::string                            VHDL_TYPE_NAME[VHDL_TYPE_NUMBER];
    static const std::string                            VHDL_TYPE_TAGS[VHDL_TYPE_NUMBER];

    // public constructor/destructor
    HardwareBlock(const std::string& = "");
    virtual ~HardwareBlock();

    // public methods
    virtual std::string hardwareType() const    { return("block"); }
    virtual std::string nameName() const;
    virtual void dump(std::ostream& = std::cout, const unsigned int = 0) const;
    virtual void read(const XERCES_CPP_NAMESPACE::DOMNode*);
    virtual void write(std::ostream& = std::cout) const;

    std::string menuName() const;
    void addElement(HardwareElement*);
    bool findElement(const std::string&, HardwareElement**) const;
    static bool isBlock(const XERCES_CPP_NAMESPACE::DOMNode*);
    HardwareList<HardwareBlock>::const_iterator beginBlock() const      { return(m_blocks.begin()); }
    HardwareList<HardwareBlock>::const_iterator endBlock() const        { return(m_blocks.end()); }
    void setUpperBlock(const HardwareBlock* blk)                        { m_upper_block = blk; }
    const HardwareBlock* getUpperBlock() const                          { return(m_upper_block); }
    void getBlockHierarchy(HardwareList<const HardwareBlock>&) const;
    HardwareList<HardwareElement>::const_iterator beginElement() const  { return(m_elements.begin()); }
    HardwareList<HardwareElement>::const_iterator endElement() const    { return(m_elements.end()); }
    bool getVhdlType(const VHDL_TYPE t) const                           { return(m_vhdl_type[t]); }
    void setVhdlType(const VHDL_TYPE t, bool v)                         { m_vhdl_type[t] = v; }

  private:

    // private constants
    static const std::string            XML_BLOCK;

    // private members
    HardwareList<HardwareBlock>         m_blocks;
    const HardwareBlock*                m_upper_block;
    HardwareList<HardwareElement>       m_elements;
    bool                                m_vhdl_type[VHDL_TYPE_NUMBER];

    // private method
    void readBlocksAndElements(const XERCES_CPP_NAMESPACE::DOMNode*);
    void addBlock(HardwareBlock*);
    bool findBlock(const std::string&, HardwareBlock**) const;
    bool hasReadableElement() const;
    bool hasWritableElement() const;

};      // class HardwareBlock

}       // namespace L1CTHC

#endif  // _L1CTHC_HARDWAREBLOCK_H_
