#ifndef _L1CTHC_HARDWAREOBJECT_H_
#define _L1CTHC_HARDWAREOBJECT_H_

//******************************************************************************
// file: HardwareObject.h
// desc: library for L1CT HardwareCompiler
// auth: 01-MAR-2013 R. Spiwoks, re-design from RCDModuleDesign
//******************************************************************************

#include <string>
#include <iostream>
#include <list>

#include <xercesc/dom/DOM.hpp>

#define TRX(string) XMLString::transcode(string)

namespace L1CTHC {

// forward declaration
class HardwareCompiler;

// HardwareObject class
class HardwareObject {

  public:

    // public types and constants                       // cf. vme_rrc/vme_rcc/vme_rcc.h
    typedef enum                                        { A16, A24, A32, ILLEGAL3, ILLEGAL4, CRCSR, USER1, USER2 } ADDR_TYPE;
    static const unsigned int                           ADDR_TYPE_NUMBER = USER2 + 1;
    static const std::string                            ADDR_TYPE_NAME[ADDR_TYPE_NUMBER];
    static const unsigned int                           ADDR_TYPE_MMAX[ADDR_TYPE_NUMBER];

    typedef enum                                        { D08, D16, D32 } DATA_TYPE;
    static const unsigned int                           DATA_TYPE_NUMBER = D32 + 1;
    static const std::string                            DATA_TYPE_NAME[DATA_TYPE_NUMBER];
    static const std::string                            DATA_TYPE_TYPE[DATA_TYPE_NUMBER];
    static const std::string                            DATA_TYPE_MASK[DATA_TYPE_NUMBER];
    static const unsigned int                           DATA_TYPE_MMAX[DATA_TYPE_NUMBER];
    static const unsigned int                           DATA_TYPE_SIZE[DATA_TYPE_NUMBER];
    static const std::string DATA_TYPE_BYTE(const DATA_TYPE);

    typedef enum                                        { R, W, RW } DATA_MODF;
    static const unsigned int                           DATA_MODF_NUMBER = RW + 1;
    static const std::string                            DATA_MODF_NAME[DATA_MODF_NUMBER];

    typedef enum                                        { STRING, NUMBER, BOOLEAN, ASCII } DATA_FORM;
    static const unsigned int                           DATA_FORM_NUMBER = ASCII + 1;
    static const std::string                            DATA_FORM_NAME[DATA_FORM_NUMBER];
    static const std::string                            DATA_FORM_TYPE[DATA_FORM_NUMBER];
    static const std::string                            DATA_FORM_RTNV[DATA_FORM_NUMBER];
    static const unsigned int                           ASCII_WIDTH = 8;

    // public constructor/destructor
    HardwareObject(const std::string& = "");
    virtual ~HardwareObject();

    // public methods
    std::string name() const                            { return(m_name); }
    unsigned int addr() const                           { return(m_addr); };
    DATA_TYPE type() const                              { return(m_type); };
    bool isTypeCompatible(const HardwareObject*, const HardwareObject**) const;
    bool findTypeList(const DATA_TYPE) const;
    void createTypeList(const std::string&);
    void copyTypeList(const HardwareObject*);
    void defaultTypeList();
    bool isDefaultTypeList() const;
    std::string printTypeList() const;
    bool hasEmptyTypeList() const                       { return(m_type_list.empty()); }
    DATA_MODF modf() const                              { return(m_modf); }
    bool isReadable() const                             { return((m_modf == R) || (m_modf == RW)); }
    bool isWritable() const                             { return((m_modf == W) || (m_modf == RW)); }
    bool modfIsCompatible(const DATA_MODF m) const      { return((m == RW) || (m == m_modf)); }
    unsigned int multiple() const                       { return(m_multiple); }
    unsigned int offset() const                         { return(m_offset); }
    HardwareCompiler* getCompiler() const               { return(m_compiler); }
    static void setCompiler(HardwareCompiler*);

    virtual std::string hardwareType() const = 0;
    virtual std::string nameName() const = 0;
    virtual std::string addrName() const;
    virtual std::string multipleName() const;
    virtual std::string offsetName() const;
    virtual void dump(std::ostream& = std::cout, const unsigned int = 0) const = 0;
    virtual void read(const XERCES_CPP_NAMESPACE::DOMNode*) = 0;
    virtual void write(std::ostream& = std::cout) const = 0;

  protected:

    // protected constant
    static const unsigned int   XML_TAB_LENGTH          = 40;

    // protected members
    std::string                 m_name;
    unsigned int                m_addr;
    DATA_TYPE                   m_type;
    std::list<DATA_TYPE>        m_type_list;
    DATA_MODF                   m_modf;
    unsigned int                m_multiple;
    unsigned int                m_offset;

  private:

    // private constant
    static const std::string    CPP_ADDR_PREFIX;
    static const std::string    CPP_NUMBER_POSTFIX;
    static const std::string    CPP_OFFSET_PREFIX;

    // private member
    static HardwareCompiler*    m_compiler;

    // prevent copying of object class
    HardwareObject(const HardwareObject&)               { };
    HardwareObject& operator=(const HardwareObject&)    { return(*this); };

};      // class HardwareObject

// global functions for sorting
bool compareDataType(HardwareObject::DATA_TYPE, HardwareObject::DATA_TYPE);

}       // namespace L1CTHC

#endif  // _L1CTHC_HARDWAREOBJECT_H_
