#ifndef _L1CTHC_VMEBUSEMITTER_H_
#define _L1CTHC_VMEBUSEMITTER_H_

//******************************************************************************
// file: VMEbusEmitter.h
// desc: library for L1CT HardwareCompiler
// auth: 25-APR-2013 R. Spiwoks
//******************************************************************************

#include "L1CTHardwareCompiler/CompilerCCEmitter.h"

namespace L1CTHC {

// VMEbusEmitter class
class VMEbusEmitter : public CompilerCCEmitter {

  public:

    // public constructor/destructor
    VMEbusEmitter();
    virtual ~VMEbusEmitter();

  private:

    // private methods
    virtual std::string compileModuleHardwareIncludesDeclaration(const HardwareModule*);
    virtual std::string compileModuleHardwareIncludesDefinition(const HardwareModule*);
    virtual std::string compileModuleHardwareCtorDtorDeclaration(const HardwareModule*);
    virtual std::string compileModuleHardwareCtorDtorDefinition(const HardwareModule*);
    virtual std::string compileModuleHardwareMembers(const HardwareModule*);
    virtual std::string compileModuleHardwareReadSingle(const HardwareElement*);
    virtual std::string compileModuleHardwareWriteSingle(const HardwareElement*);
    virtual std::string compileModuleHardwareReadVector(const HardwareElement*);
    virtual std::string compileModuleHardwareWriteVector(const HardwareElement*);
    virtual std::string compileModuleHardwareReadBlock(const HardwareElement*);
    virtual std::string compileModuleHardwareWriteBlock(const HardwareElement*);
    virtual std::string compileModuleHardwareReadFile(const HardwareElement*);
    virtual std::string compileModuleHardwareWriteFile(const HardwareElement*);

    virtual std::string compileMenuHardwareGlobals(const HardwareModule*);
    virtual std::string compileMenuHardwareOpen(const HardwareModule*);
    virtual std::string compileMenuHardwareClose(const HardwareModule*);

    void compileModuleHardwareBlockList(const HardwareBlock*, CompilerStringList&);

};      // class VMEbusEmitter

}       // namespace L1CTHC

#endif  // _L1CTHC_VMEBUSEMITTER_H_
