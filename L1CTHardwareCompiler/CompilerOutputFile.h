#ifndef _L1CTHC_COMPILEROUTPUTFILE_H_
#define _L1CTHC_COMPILERPUTPUTFILE_H_

//******************************************************************************
// file: CompilerOutputFile.h
// desc: output file for L1CT HardwareCompiler
// auth: 20-MAR-2013 R. Spiwoks
//******************************************************************************

#include <string>
#include <fstream>
#include <sstream>

namespace L1CTHC {

// forward declarations
class HardwareCompiler;

// CompilerOutputFile class
class CompilerOutputFile {

  public:

    // public constructor and destructor
    CompilerOutputFile(const std::string&);
   ~CompilerOutputFile();

    // public methods
    std::string& name()                 { return(m_name); }
    std::ofstream& file()               { return(m_file); }

    void writePreamble(const HardwareCompiler*, const std::string&, const std::string& = "");
    void writePostscript();

  private:

    // private constant
    static const char                  MON[12][4];

    // private members
    std::string                         m_name;
    std::string                         m_incl;
    std::ofstream                       m_file;

    // prevent copying of output file class
    CompilerOutputFile(const CompilerOutputFile&)               { };
    CompilerOutputFile& operator=(const CompilerOutputFile&)    { return(*this); };

};      // class CompilerOutputFile

}       // namespace L1CTHC

#endif  // _L1CTHC_COMPILEROUTPUTFILE_H_
