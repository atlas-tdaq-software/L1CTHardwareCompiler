#ifndef _L1CTHC_HARDWAREWORD_H_
#define _L1CTHC_HARDWAREWROD_H_

//******************************************************************************
// file: HardwareWord.h
// desc: library for L1CT HardwareCompiler
// auth: 21-MAR-2013 R. Spiwoks
//******************************************************************************

#include "L1CTHardwareCompiler/HardwareDataType.h"

namespace L1CTHC {

// forward declaration
class CompilerOutputFile;

// HardwareWord class
class HardwareWord : public HardwareDataType {

  public:

    // public constructor/destructor
    HardwareWord(const std::string& = "", const DECL_TYPE = INTERNAL);
    HardwareWord(const HardwareWord* w) : HardwareDataType(w) {}; // constructor for reference to HardwareWord
    virtual ~HardwareWord();

    // public methods
    virtual std::string hardwareType() const    { return("word"); }
    virtual std::string nameName() const;
    virtual void dump(std::ostream& = std::cout, const unsigned int = 0) const;
    virtual void read(const XERCES_CPP_NAMESPACE::DOMNode*);
    virtual void write(std::ostream& = std::cout) const;

    virtual std::string fileName() const        { return(""); }
    virtual bool isEmpty() const                { return(true); }
    virtual unsigned int getFullMask() const    { return(DATA_TYPE_MMAX[m_type]); }
    static const std::string FILENAME()         { return(""); }

    void type(const DATA_TYPE& type)            { m_type = type; m_name = DATA_TYPE_NAME[m_type]; }
    std::string typeName() const                { return(DATA_TYPE_NAME[m_type]); }

  private:

    // private member

};      // class HardwareWord

}       // namespace L1CTHC

#endif  // _L1CTHC_HARDWAREWORD_H_
