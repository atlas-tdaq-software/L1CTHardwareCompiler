#ifndef _L1CTHC_AXIINTERFACEEMITTER_H_
#define _L1CTHC_AXIINTERFACEEMITTER_H_

//******************************************************************************
// file: AXIInterfaceEmitter.h
// desc: library for L1CT HardwareCompiler
// auth: 27-NOV-2017 R. Spiwoks
//******************************************************************************

#include "L1CTHardwareCompiler/CompilerCCEmitter.h"

namespace L1CTHC {

// AXIInterfaceEmitter class
class AXIInterfaceEmitter : public CompilerCCEmitter {

  public:

    // public constructor/destructor
    AXIInterfaceEmitter();
    virtual ~AXIInterfaceEmitter();

  private:

    // private methods
    virtual std::string compileModuleHardwareIncludesDeclaration(const HardwareModule*);
    virtual std::string compileModuleHardwareIncludesDefinition(const HardwareModule*);
    virtual std::string compileModuleHardwareCtorDtorDeclaration(const HardwareModule*);
    virtual std::string compileModuleHardwareCtorDtorDefinition(const HardwareModule*);
    virtual std::string compileModuleHardwareMembers(const HardwareModule*);
    virtual std::string compileModuleHardwareReadSingle(const HardwareElement*);
    virtual std::string compileModuleHardwareWriteSingle(const HardwareElement*);
    virtual std::string compileModuleHardwareReadVector(const HardwareElement*);
    virtual std::string compileModuleHardwareWriteVector(const HardwareElement*);
    virtual std::string compileModuleHardwareReadBlock(const HardwareElement*);
    virtual std::string compileModuleHardwareWriteBlock(const HardwareElement*);
    virtual std::string compileModuleHardwareReadFile(const HardwareElement*);
    virtual std::string compileModuleHardwareWriteFile(const HardwareElement*);

    virtual std::string compileMenuHardwareGlobals(const HardwareModule*);
    virtual std::string compileMenuHardwareOpen(const HardwareModule*);
    virtual std::string compileMenuHardwareClose(const HardwareModule*);

    void compileModuleHardwareBlockList(const HardwareBlock*, CompilerStringList&);

};      // class AXIInterfaceEmitter

}       // namespace L1CTHC

#endif  // _L1CTHC_AXIINTERFACEEMITTER_H_
