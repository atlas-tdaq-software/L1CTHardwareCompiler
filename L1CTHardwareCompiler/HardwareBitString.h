#ifndef _L1CTHC_HARDWAREBITSTRING_H_
#define _L1CTHC_HARDWAREBITSTRING_H_

//******************************************************************************
// file: HardwareBitString.h
// desc: library for L1CT HardwareCompiler
// auth: 19-MAR-2013 R. Spiwoks, re-design from RCDModuleDesign
//******************************************************************************

#include "L1CTHardwareCompiler/HardwareList.h"
#include "L1CTHardwareCompiler/HardwareDataType.h"
#include "L1CTHardwareCompiler/HardwareField.h"
#include "RCDBitString/BitSet.h"

namespace L1CTHC {

// forward declarations
class CompilerOutputFile;

// HardwareBitString class
class HardwareBitString : public HardwareDataType {

  public:

    // public constructor/destructor
    HardwareBitString(const std::string& = "", const DECL_TYPE = INTERNAL);
    HardwareBitString(const HardwareBitString* b) : HardwareDataType(b) {}; // constructor for reference to HardwareBitString
    virtual ~HardwareBitString();

    // public methods
    virtual std::string hardwareType() const                    { return("bitstring"); }
    virtual std::string nameName() const;
    virtual void dump(std::ostream& = std::cout,const unsigned int = 0) const;
    virtual void read(const XERCES_CPP_NAMESPACE::DOMNode*);
    virtual void write(std::ostream& = std::cout) const;
    virtual std::string fileName() const                        { return(CPP_FILE_NAME); }
    virtual bool isEmpty() const;
    virtual unsigned int getFullMask() const;
    RCD::BitSet getFullMaskBitSet() const;
    virtual void setElement(const HardwareElement*);
    static const std::string FILENAME()                         { return(CPP_FILE_NAME); }
    RCD::BitSet mask() const                                    { return(m_mask); }

    void addField(HardwareField*);
    bool findField(const std::string&, HardwareField**) const;
    bool fitMaskMultiple(const RCD::BitSet&, const unsigned int, const unsigned int) const;
    bool findMaskOverlap(const RCD::BitSet&, const unsigned int, const unsigned int, HardwareField **) const;
    unsigned int maxMultipleField() const;
    static bool isBitString(const XERCES_CPP_NAMESPACE::DOMNode*);
    HardwareList<HardwareField>::const_iterator beginField() const      { return(m_fields.begin()); }
    HardwareList<HardwareField>::const_iterator endField() const        { return(m_fields.end()); }

  private:

    // private constant
    static const std::string                    XML_BITSTRING;
    static const std::string                    CPP_FILE_NAME;
    static const std::string                    CPP_TYPE_POSTFIX;

    // private member
    unsigned int                                m_size;
    RCD::BitSet                                 m_mask;
    HardwareList<HardwareField>                 m_fields;

    // private method
    void readFields(const XERCES_CPP_NAMESPACE::DOMNode*);
    bool hasReadableField() const;
    bool hasWritableField() const;

};      // class HardwareBitString

}       // namespace L1CTHC

#endif  // _L1CTHC_HARDWAREBITSTRING_H_
