#ifndef _L1CTHC_HARDWARELIST_H_
#define _L1CTHC_HARDWARELIST_H_

//******************************************************************************
// file: HardwareList.h
// desc: template class for L1CT Hardware Compiler common list class
// auth: 19-MAR-2013 R. Spiwoks, from TriggerMenuCompiler
//******************************************************************************

#include <list>
#include <string>
#include <iostream>

namespace L1CTHC {

// HardwareList class
template <class Item> class HardwareList : public std::list<Item*> {

  public:
    HardwareList() : std::list<Item*>()                        {}
   ~HardwareList()                                             {}

    typename HardwareList<Item>::iterator begin()                    { return(std::list<Item*>::begin()); }
    typename HardwareList<Item>::const_iterator begin() const        { return(std::list<Item*>::begin()); }
    typename HardwareList<Item>::reverse_iterator rbegin()           { return(std::list<Item*>::rbegin()); }
    typename HardwareList<Item>::iterator end()                      { return(std::list<Item*>::end()); }
    typename HardwareList<Item>::const_iterator end() const          { return(std::list<Item*>::end()); }
    typename HardwareList<Item>::reverse_iterator rend()             { return(std::list<Item*>::rend()); }

    void addItem(Item*);
    bool findItem(const std::string&, Item** = 0) const;        // by name (string)
    void dump(std::ostream& = std::cout) const;

};      // class HardwareList

//------------------------------------------------------------------------------

template <class Item> void HardwareList<Item>::addItem(Item* item) {

    this->push_back(item);
}

//------------------------------------------------------------------------------

template <class Item> bool HardwareList<Item>::findItem(const std::string& name, Item** ritm) const {

    typename HardwareList<Item>::const_iterator iitm;
    Item* aitm;

    for(iitm=begin(); iitm!=end(); iitm++) {
        aitm = *iitm;

        // compare object names
        if(name == aitm->name()) {
            if(ritm) *ritm = aitm;
            return(true);
        }
    }

    return(false);
}

//------------------------------------------------------------------------------

template <class Item> void HardwareList<Item>::dump(std::ostream& ostr) const {

    typename HardwareList<Item>::const_iterator iitm;
    Item* aitm;

    for(iitm=begin(); iitm!=end(); iitm++) {
        aitm = *iitm;
        aitm->dump(ostr);
    }
}

}       // namespace L1CTHC

#endif  // _L1CTHC_HARDWARELIST_H_
