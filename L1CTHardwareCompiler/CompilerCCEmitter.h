#ifndef _L1CTHC_COMPILERCCEMITTER_H_
#define _L1CTHC_COMPILERCCEMITTER_H_

//******************************************************************************
// file: CompilerCCEmitter.h
// desc: library for L1CT HardwareCompiler
// auth: 23-JUL-2013 R. Spiwoks
//******************************************************************************

#include "L1CTHardwareCompiler/CompilerEmitter.h"

#include <string>
#include <sstream>

namespace L1CTHC {

// forward declarations
class HardwareCompiler;
class CompilerOutputFile;
class CompilerStringList;
class HardwareModule;
class HardwareBlock;
class HardwareElement;
class HardwareMemory;
class HardwareDataType;
class HardwareWord;
class HardwareBitString;
class HardwareField;
class HardwareValue;

// CompilerCCEmitter class
class CompilerCCEmitter : public CompilerEmitter {

  public:

    // public constructor/destructor
    CompilerCCEmitter();
    virtual ~CompilerCCEmitter();

    // public methods
    virtual void compile();

  protected:

    // protected constant
    static const unsigned int                           CPP_TAB_LENGTH = 12;

    // private methods
    void compileModule(const HardwareModule*);
    void compileModuleBlock(const HardwareBlock*, std::ostringstream&, std::ostringstream&);
    void compileModuleBlockList(const HardwareBlock*, CompilerStringList&, const std::string&, const bool = false);
    std::string compileModuleBlockMultipleRange(const HardwareBlock*);
    std::string compileModuleElementMultipleRange(const HardwareElement*);
    std::string compileModuleMemoryIndexRange(const HardwareMemory*);
    std::string compileModuleMemorySizeRange(const HardwareMemory*, const bool);
    std::string compileModuleElementSingle(const HardwareElement*);
    std::string compileModuleElementVector(const HardwareElement*);
    std::string compileModuleElementBlock(const HardwareElement*);
    std::string compileModuleElementFile(const HardwareElement*);

    void compileDataType();
    void compileDataType(const HardwareDataType*, CompilerOutputFile*, CompilerOutputFile*);
    void compileDataTypeWriteHead(const HardwareDataType*, CompilerOutputFile*, CompilerOutputFile*);
    void compileDataTypeWriteTail(const HardwareDataType*, CompilerOutputFile*, CompilerOutputFile*);

    void compileBitString(const HardwareBitString*, CompilerOutputFile*, CompilerOutputFile*);
    std::string compileBitStringEnumHead(const HardwareBitString*); 
    std::string compileBitStringEnumBody(const HardwareBitString*); 
    std::string compileFieldRead(const HardwareField*);
    std::string compileFieldWrite(const HardwareField*);
    std::string compileBitStringPrint(const HardwareBitString*);

    void compileMenu(const HardwareModule*);
    void compileMenuBlock(const HardwareBlock*, std::ostringstream&);
    std::string compileMenuBlockListForm(const HardwareBlock*);
    std::string compileMenuBlockMultipleEnter(const HardwareBlock*);
    std::string compileMenuElementMultipleEnter(const HardwareElement*);
    std::string compileMenuMemoryIndexEnter(const HardwareMemory*);
    std::string compileMenuElementWriteForm(const HardwareElement*);
    std::string compileMenuDataTypeWriteItem(const HardwareDataType*);
    std::string compileMenuElementSingle(const HardwareElement*);
    std::string compileMenuElementVector(const HardwareElement*);
    std::string compileMenuElementBlock(const HardwareElement*);
    std::string compileMenuElementFile(const HardwareElement*);

    virtual std::string compileModuleHardwareIncludesDeclaration(const HardwareModule*) = 0;
    virtual std::string compileModuleHardwareIncludesDefinition(const HardwareModule*) = 0;
    virtual std::string compileModuleHardwareCtorDtorDeclaration(const HardwareModule*) = 0;
    virtual std::string compileModuleHardwareCtorDtorDefinition(const HardwareModule*) = 0;
    virtual std::string compileModuleHardwareMembers(const HardwareModule*) = 0;
    virtual std::string compileModuleHardwareReadSingle(const HardwareElement*) = 0;
    virtual std::string compileModuleHardwareWriteSingle(const HardwareElement*) = 0;
    virtual std::string compileModuleHardwareReadVector(const HardwareElement*) = 0;
    virtual std::string compileModuleHardwareWriteVector(const HardwareElement*) = 0;
    virtual std::string compileModuleHardwareReadBlock(const HardwareElement*) = 0;
    virtual std::string compileModuleHardwareWriteBlock(const HardwareElement*) = 0;
    virtual std::string compileModuleHardwareReadFile(const HardwareElement*) = 0;
    virtual std::string compileModuleHardwareWriteFile(const HardwareElement*) = 0;

    virtual std::string compileMenuHardwareGlobals(const HardwareModule*) = 0;
    virtual std::string compileMenuHardwareOpen(const HardwareModule*) = 0;
    virtual std::string compileMenuHardwareClose(const HardwareModule*) = 0;

};      // class CompilerCCEmitter

}       // namespace L1CTHC

#endif  // _L1CTHC_COMPILERCCEMITTER_H_
