#ifndef _L1CTHC_HARDWAREREGISTER_H_
#define _L1CTHC_HARDWAREREGISTER_H_

//******************************************************************************
// file: HardwareRegister.h
// desc: library for L1CT HardwareCompiler
// auth: 01-MAR-2013 R. Spiwoks, re-design from RCDModuleDesign
//******************************************************************************

#include "L1CTHardwareCompiler/HardwareElement.h"
#include "L1CTHardwareCompiler/HardwareBlock.h"

namespace L1CTHC {

// HardwareRegister class
class HardwareRegister : public HardwareElement {

  public:

    // public constructor/destructor
    HardwareRegister(const std::string& = "");
    virtual ~HardwareRegister();

    // public methods
    virtual std::string hardwareType() const    { return("register"); }
    virtual std::string nameName() const        { return((m_block && (!m_block->nameName().empty())) ? (m_block->nameName()+"_"+m_name) : m_name); }
    virtual void dump(std::ostream& = std::cout, const unsigned int = 0) const;
    virtual void read(const XERCES_CPP_NAMESPACE::DOMNode*);
    virtual void write(std::ostream& = std::cout) const;
    virtual unsigned int addrSize() const       { return(DATA_TYPE_SIZE[m_type]/HardwareCommon::BIT_SIZE); }

    static bool isRegister(const XERCES_CPP_NAMESPACE::DOMNode*);

  private:

    // private constants
    static const std::string            XML_REGISTER;

    // private method
    void readDataType(const XERCES_CPP_NAMESPACE::DOMNode*);
    void readDataTypeLoop(const XERCES_CPP_NAMESPACE::DOMNode*);

};      // class HardwareRegister

}       // namespace L1CTHC

#endif  // _L1CTHC_HARDWAREREGISTER_H_
