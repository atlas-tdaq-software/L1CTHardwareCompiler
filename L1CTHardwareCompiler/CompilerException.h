#ifndef _L1CTHC_COMPILEREXCEPTION_H_
#define _L1CTHC_COMPILEREXCEPTION_H_

//******************************************************************************
// file: CompilerException.h
// desc: exception of L1CT HardwareCompiler
// auth: 01-MAR-2013 R. Spiwoks, re-design from RCDModuleDesign
//******************************************************************************

#include <string>
#include <exception>

namespace L1CTHC {

// CompilerException class
class CompilerException : public std::exception {

  public:

    // public type and constants
    typedef enum { DEBUG, LOG, INFO, WARNING, ERROR, FATAL } SEVERITY;
    static const int            SEVERITY_NUMBER = FATAL + 1;
    static const std::string    SEVERITY_NAME[SEVERITY_NUMBER];

    // public constructor and destructor
    CompilerException(const std::string = "", const SEVERITY = ERROR);
    virtual ~CompilerException() throw();

    // public methods
    virtual const char* what() const throw()    { return(m_message.c_str()); }

  private:

    // private member
    SEVERITY                    m_severity;
    std::string                 m_message;

};      // class CompilerException

}       // namespace L1CTHC

#endif  // _L1CTHC_COMPILEREXCEPTION_H_
