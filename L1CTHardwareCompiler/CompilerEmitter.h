#ifndef _L1CTHC_COMPILEREMITTER_H_
#define _L1CTHC_COMPILEREMITTER_H_

//******************************************************************************
// file: CompilerEmitter.h
// desc: library for L1CT HardwareCompiler
// auth: 25-APR-2013 R. Spiwoks
//******************************************************************************

#include <string>
#include <sstream>

namespace L1CTHC {

// forward declarations
class HardwareCompiler;
class CompilerOutputFile;
class HardwareModule;
class HardwareBlock;
class HardwareElement;
class HardwareMemory;
class HardwareDataType;
class HardwareWord;
class HardwareBitString;
class HardwareField;
class HardwareValue;

// CompilerEmitter class
class CompilerEmitter {

  public:

    // public constructor/destructor
    CompilerEmitter();
    virtual ~CompilerEmitter();

    // public methods
    virtual void compile() = 0;

    HardwareCompiler* getCompiler() const               { return(m_compiler); }
    void setCompiler(HardwareCompiler*);

  protected:

    // protected member
    HardwareCompiler*            m_compiler;

};      // class CompilerEmitter

}       // namespace L1CTHC

#endif  // _L1CTHC_COMPILEREMITTER_H_
